package com.ruoyi.project.system.prizelog.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.prizelog.mapper.PrizeLogMapper;
import com.ruoyi.project.system.prizelog.domain.PrizeLog;
import com.ruoyi.project.system.prizelog.service.IPrizeLogService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 中奖记录Service业务层处理
 * 
 * @author LCL
 * @date 2020-11-09
 */
@Service
public class PrizeLogServiceImpl implements IPrizeLogService 
{
    @Resource
    private IdGererateFactory idGererateFactory;

    @Autowired
    private PrizeLogMapper prizeLogMapper;

    /**
     * 查询中奖记录
     * 
     * @param id 中奖记录ID
     * @return 中奖记录
     */
    @Override
    public PrizeLog selectPrizeLogById(String id)
    {
        return prizeLogMapper.selectPrizeLogById(id);
    }

    /**
     * 查询中奖记录列表
     * 
     * @param prizeLog 中奖记录
     * @return 中奖记录
     */
    @Override
    public List<PrizeLog> selectPrizeLogList(PrizeLog prizeLog)
    {
        return prizeLogMapper.selectPrizeLogList(prizeLog);
    }

    /**
     * 新增中奖记录
     * 
     * @param prizeLog 中奖记录
     * @return 结果
     */
    @Override
    public int insertPrizeLog(PrizeLog prizeLog)
    {
        prizeLog.setId(this.idGererateFactory.nextStringId());
        return prizeLogMapper.insertPrizeLog(prizeLog);
    }

    /**
     * 修改中奖记录
     * 
     * @param prizeLog 中奖记录
     * @return 结果
     */
    @Override
    public int updatePrizeLog(PrizeLog prizeLog)
    {
        return prizeLogMapper.updatePrizeLog(prizeLog);
    }

    /**
     * 删除中奖记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePrizeLogByIds(String ids)
    {
        return prizeLogMapper.deletePrizeLogByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除中奖记录信息
     * 
     * @param id 中奖记录ID
     * @return 结果
     */
    @Override
    public int deletePrizeLogById(String id)
    {
        return prizeLogMapper.deletePrizeLogById(id);
    }
}
