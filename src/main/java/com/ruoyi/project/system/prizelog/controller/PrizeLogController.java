package com.ruoyi.project.system.prizelog.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.prizelog.domain.PrizeLog;
import com.ruoyi.project.system.prizelog.service.IPrizeLogService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 中奖记录Controller
 * 
 * @author LCL
 * @date 2020-11-09
 */
@Controller
@RequestMapping("/system/prizelog")
public class PrizeLogController extends BaseController
{
    private String prefix = "system/prizelog";

    @Autowired
    private IPrizeLogService prizeLogService;

    @RequiresPermissions("system:prizelog:view")
    @GetMapping()
    public String prizelog()
    {
        return prefix + "/prizelog";
    }

    /**
     * 查询中奖记录列表
     */
    @RequiresPermissions("system:prizelog:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PrizeLog prizeLog)
    {
        startPage();
        List<PrizeLog> list = prizeLogService.selectPrizeLogList(prizeLog);
        return getDataTable(list);
    }

    /**
     * 导出中奖记录列表
     */
    @RequiresPermissions("system:prizelog:export")
    @Log(title = "中奖记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PrizeLog prizeLog)
    {
        List<PrizeLog> list = prizeLogService.selectPrizeLogList(prizeLog);
        ExcelUtil<PrizeLog> util = new ExcelUtil<PrizeLog>(PrizeLog.class);
        return util.exportExcel(list, "prizelog");
    }

    /**
     * 新增中奖记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存中奖记录
     */
    @RequiresPermissions("system:prizelog:add")
    @Log(title = "中奖记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PrizeLog prizeLog)
    {
        return toAjax(prizeLogService.insertPrizeLog(prizeLog));
    }

    /**
     * 修改中奖记录
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        PrizeLog prizeLog = prizeLogService.selectPrizeLogById(id);
        mmap.put("prizeLog", prizeLog);
        return prefix + "/edit";
    }

    /**
     * 修改保存中奖记录
     */
    @RequiresPermissions("system:prizelog:edit")
    @Log(title = "中奖记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PrizeLog prizeLog)
    {
        return toAjax(prizeLogService.updatePrizeLog(prizeLog));
    }

    /**
     * 删除中奖记录
     */
    @RequiresPermissions("system:prizelog:remove")
    @Log(title = "中奖记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(prizeLogService.deletePrizeLogByIds(ids));
    }
}
