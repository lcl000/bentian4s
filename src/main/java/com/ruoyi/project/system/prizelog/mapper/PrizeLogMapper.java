package com.ruoyi.project.system.prizelog.mapper;

import java.util.List;
import com.ruoyi.project.system.prizelog.domain.PrizeLog;

/**
 * 中奖记录Mapper接口
 * 
 * @author LCL
 * @date 2020-11-09
 */
public interface PrizeLogMapper 
{
    /**
     * 查询中奖记录
     * 
     * @param id 中奖记录ID
     * @return 中奖记录
     */
    public PrizeLog selectPrizeLogById(String id);

    /**
     * 查询中奖记录列表
     * 
     * @param prizeLog 中奖记录
     * @return 中奖记录集合
     */
    public List<PrizeLog> selectPrizeLogList(PrizeLog prizeLog);

    /**
     * 新增中奖记录
     * 
     * @param prizeLog 中奖记录
     * @return 结果
     */
    public int insertPrizeLog(PrizeLog prizeLog);

    /**
     * 修改中奖记录
     * 
     * @param prizeLog 中奖记录
     * @return 结果
     */
    public int updatePrizeLog(PrizeLog prizeLog);

    /**
     * 删除中奖记录
     * 
     * @param id 中奖记录ID
     * @return 结果
     */
    public int deletePrizeLogById(String id);

    /**
     * 批量删除中奖记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePrizeLogByIds(String[] ids);
}
