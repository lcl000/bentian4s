package com.ruoyi.project.system.prize.domain;

import java.math.BigDecimal;
import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 奖品对象 prize
 * 
 * @author LCL
 * @date 2020-11-09
 */
public class Prize extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 奖品名称 */
    @Excel(name = "奖品名称")
    private String name;

    /** 0 优惠券 1 抵扣券 2 积分 */
    @Excel(name = "0 优惠券 1 抵扣券 2 积分")
    private Integer type;

    /** 奖品总数 */
    @Excel(name = "奖品总数")
    private Long sumNum;

    /** 剩余数量 */
    @Excel(name = "剩余数量")
    private Long num;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date created;

    /** 0 上架 1 下架 */
    @Excel(name = "0 上架 1 下架")
    private Integer status;

    /** 概率 */
    @Excel(name = "概率")
    private BigDecimal probability;

    /** 智通币数量 */
    @Excel(name = "智通币数量")
    private BigDecimal coinNum;

    /** 奖品图标 */
    @Excel(name = "奖品图标")
    private String icon;

    /** 相关资源ID */
    @Excel(name = "相关资源ID")
    private String resId;

    // 奖品
    private String couponName;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setSumNum(Long sumNum) 
    {
        this.sumNum = sumNum;
    }

    public Long getSumNum() 
    {
        return sumNum;
    }
    public void setNum(Long num) 
    {
        this.num = num;
    }

    public Long getNum() 
    {
        return num;
    }
    public void setCreated(Date created) 
    {
        this.created = created;
    }

    public Date getCreated() 
    {
        return created;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setProbability(BigDecimal probability)
    {
        this.probability = probability;
    }

    public BigDecimal getProbability()
    {
        return probability;
    }
    public void setCoinNum(BigDecimal coinNum)
    {
        this.coinNum = coinNum;
    }

    public BigDecimal getCoinNum()
    {
        return coinNum;
    }
    public void setIcon(String icon) 
    {
        this.icon = icon;
    }

    public String getIcon() 
    {
        return icon;
    }
    public void setResId(String resId) 
    {
        this.resId = resId;
    }

    public String getResId() 
    {
        return resId;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("type", getType())
            .append("sumNum", getSumNum())
            .append("num", getNum())
            .append("created", getCreated())
            .append("status", getStatus())
            .append("probability", getProbability())
            .append("coinNum", getCoinNum())
            .append("icon", getIcon())
            .append("resId", getResId())
            .toString();
    }
}
