package com.ruoyi.project.system.prize.mapper;

import java.util.List;
import com.ruoyi.project.system.prize.domain.Prize;

/**
 * 奖品Mapper接口
 * 
 * @author LCL
 * @date 2020-11-09
 */
public interface PrizeMapper 
{
    /**
     * 查询奖品
     * 
     * @param id 奖品ID
     * @return 奖品
     */
    public Prize selectPrizeById(String id);

    /**
     * 查询奖品列表
     * 
     * @param prize 奖品
     * @return 奖品集合
     */
    public List<Prize> selectPrizeList(Prize prize);

    /**
     * 新增奖品
     * 
     * @param prize 奖品
     * @return 结果
     */
    public int insertPrize(Prize prize);

    /**
     * 修改奖品
     * 
     * @param prize 奖品
     * @return 结果
     */
    public int updatePrize(Prize prize);

    /**
     * 删除奖品
     * 
     * @param id 奖品ID
     * @return 结果
     */
    public int deletePrizeById(String id);

    /**
     * 批量删除奖品
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePrizeByIds(String[] ids);
}
