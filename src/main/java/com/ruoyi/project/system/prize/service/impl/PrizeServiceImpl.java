package com.ruoyi.project.system.prize.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.prize.mapper.PrizeMapper;
import com.ruoyi.project.system.prize.domain.Prize;
import com.ruoyi.project.system.prize.service.IPrizeService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 奖品Service业务层处理
 * 
 * @author LCL
 * @date 2020-11-09
 */
@Service
public class PrizeServiceImpl implements IPrizeService 
{
    @Resource
    private IdGererateFactory idGererateFactory;

    @Autowired
    private PrizeMapper prizeMapper;

    /**
     * 查询奖品
     * 
     * @param id 奖品ID
     * @return 奖品
     */
    @Override
    public Prize selectPrizeById(String id)
    {
        return prizeMapper.selectPrizeById(id);
    }

    /**
     * 查询奖品列表
     * 
     * @param prize 奖品
     * @return 奖品
     */
    @Override
    public List<Prize> selectPrizeList(Prize prize)
    {
        return prizeMapper.selectPrizeList(prize);
    }

    /**
     * 新增奖品
     * 
     * @param prize 奖品
     * @return 结果
     */
    @Override
    public int insertPrize(Prize prize)
    {
        prize.setId(this.idGererateFactory.nextStringId());
        return prizeMapper.insertPrize(prize);
    }

    /**
     * 修改奖品
     * 
     * @param prize 奖品
     * @return 结果
     */
    @Override
    public int updatePrize(Prize prize)
    {
        return prizeMapper.updatePrize(prize);
    }

    /**
     * 删除奖品对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePrizeByIds(String ids)
    {
        return prizeMapper.deletePrizeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除奖品信息
     * 
     * @param id 奖品ID
     * @return 结果
     */
    @Override
    public int deletePrizeById(String id)
    {
        return prizeMapper.deletePrizeById(id);
    }
}
