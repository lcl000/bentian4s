package com.ruoyi.project.system.prize.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.coupon.coupon.domain.Coupon;
import com.ruoyi.project.system.coupon.coupon.service.ICouponService;
import com.ruoyi.project.system.prize.domain.Prize;
import com.ruoyi.project.system.prize.service.IPrizeService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 奖品Controller
 * 
 * @author LCL
 * @date 2020-11-09
 */
@Controller
@RequestMapping("/system/prize")
public class PrizeController extends BaseController
{
    private String prefix = "system/prize";

    @Autowired
    private IPrizeService prizeService;

    @Autowired
    private ICouponService couponService;

    @RequiresPermissions("system:prize:view")
    @GetMapping()
    public String prize()
    {
        return prefix + "/prize";
    }

    /**
     * 查询奖品列表
     */
    @RequiresPermissions("system:prize:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Prize prize)
    {
        startPage();
        List<Prize> list = prizeService.selectPrizeList(prize);
        return getDataTable(list);
    }

    /**
     * 导出奖品列表
     */
    @RequiresPermissions("system:prize:export")
    @Log(title = "奖品", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Prize prize)
    {
        List<Prize> list = prizeService.selectPrizeList(prize);
        ExcelUtil<Prize> util = new ExcelUtil<Prize>(Prize.class);
        return util.exportExcel(list, "prize");
    }

    /**
     * 新增奖品
     */
    @GetMapping("/add")
    public String add(ModelMap map)
    {
        //优惠券列表
        Coupon coupon = new Coupon();
        coupon.setStatus(0);
        coupon.setHasSeckill(0);
        coupon.setOverdue(0);
        coupon.setType(0);
        map.put("yhList",this.couponService.selectCouponList(coupon));
        //抵扣券列表
        coupon.setType(1);
        map.put("dkList",this.couponService.selectCouponList(coupon));
        return prefix + "/add";
    }

    /**
     * 新增保存奖品
     */
    @RequiresPermissions("system:prize:add")
    @Log(title = "奖品", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Prize prize)
    {
        prize.setCreated(DateUtils.getNowDate());
        prize.setNum(prize.getSumNum());
        if(prize.getType()==0){
            prize.setResId(prize.getResId().split(",")[0]);
        }else if(prize.getType()==1){
            prize.setResId(prize.getResId().split(",")[1]);
        }else{
            prize.setResId(null);
        }
        return toAjax(prizeService.insertPrize(prize));
    }

    /**
     * 修改奖品
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        Prize prize = prizeService.selectPrizeById(id);
        mmap.put("prize", prize);
        //优惠券列表
        Coupon coupon = new Coupon();
        coupon.setStatus(0);
        coupon.setHasSeckill(0);
        coupon.setOverdue(0);
        coupon.setType(0);
        mmap.put("yhList",this.couponService.selectCouponList(coupon));
        //抵扣券列表
        coupon.setType(1);
        mmap.put("dkList",this.couponService.selectCouponList(coupon));
        return prefix + "/edit";
    }

    /**
     * 修改保存奖品
     */
    @RequiresPermissions("system:prize:edit")
    @Log(title = "奖品", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Prize prize)
    {
        return toAjax(prizeService.updatePrize(prize));
    }

    /**
     * 删除奖品
     */
    @RequiresPermissions("system:prize:remove")
    @Log(title = "奖品", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(prizeService.deletePrizeByIds(ids));
    }
}
