package com.ruoyi.project.system.tspec.dto;

import com.ruoyi.project.system.tsvalue.domain.TypeSpecValue;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/10 17:26
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public class TypeSpecValueDto extends TypeSpecValue {
	//规格值
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
