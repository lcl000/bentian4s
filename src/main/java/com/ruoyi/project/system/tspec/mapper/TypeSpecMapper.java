package com.ruoyi.project.system.tspec.mapper;

import java.util.List;
import com.ruoyi.project.system.tspec.domain.TypeSpec;

/**
 * 分类规格Mapper接口
 * 
 * @author LCL
 * @date 2020-06-10
 */
public interface TypeSpecMapper 
{
    /**
     * 查询分类规格
     * 
     * @param id 分类规格ID
     * @return 分类规格
     */
    public TypeSpec selectTypeSpecById(String id);

    /**
     * 查询分类规格列表
     * 
     * @param typeSpec 分类规格
     * @return 分类规格集合
     */
    public List<TypeSpec> selectTypeSpecList(TypeSpec typeSpec);

    /**
     * 新增分类规格
     * 
     * @param typeSpec 分类规格
     * @return 结果
     */
    public int insertTypeSpec(TypeSpec typeSpec);

    /**
     * 修改分类规格
     * 
     * @param typeSpec 分类规格
     * @return 结果
     */
    public int updateTypeSpec(TypeSpec typeSpec);

    /**
     * 删除分类规格
     * 
     * @param id 分类规格ID
     * @return 结果
     */
    public int deleteTypeSpecById(String id);

    /**
     * 批量删除分类规格
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTypeSpecByIds(String[] ids);

	List<TypeSpec> findListBySpecIds(String[] ids);

	List<TypeSpec> findListByTypeId(String id);
}
