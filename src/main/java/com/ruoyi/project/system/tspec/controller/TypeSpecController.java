package com.ruoyi.project.system.tspec.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.tspec.domain.TypeSpec;
import com.ruoyi.project.system.tspec.service.ITypeSpecService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 分类规格Controller
 * 
 * @author LCL
 * @date 2020-06-10
 */
@Controller
@RequestMapping("/system/type/spec")
public class TypeSpecController extends BaseController
{
    private String prefix = "system/spec";

    @Autowired
    private ITypeSpecService typeSpecService;

    @RequiresPermissions("system:spec:view")
    @GetMapping()
    public String spec()
    {
        return prefix + "/spec";
    }

    /**
     * 查询分类规格列表
     */
    @RequiresPermissions("system:spec:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TypeSpec typeSpec)
    {
        startPage();
        List<TypeSpec> list = typeSpecService.selectTypeSpecList(typeSpec);
        return getDataTable(list);
    }

    /**
     * 导出分类规格列表
     */
    @RequiresPermissions("system:spec:export")
    @Log(title = "分类规格", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TypeSpec typeSpec)
    {
        List<TypeSpec> list = typeSpecService.selectTypeSpecList(typeSpec);
        ExcelUtil<TypeSpec> util = new ExcelUtil<TypeSpec>(TypeSpec.class);
        return util.exportExcel(list, "spec");
    }

    /**
     * 新增分类规格
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存分类规格
     */
    @RequiresPermissions("system:spec:add")
    @Log(title = "分类规格", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TypeSpec typeSpec)
    {
        return toAjax(typeSpecService.insertTypeSpec(typeSpec));
    }

    /**
     * 修改分类规格
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        TypeSpec typeSpec = typeSpecService.selectTypeSpecById(id);
        mmap.put("typeSpec", typeSpec);
        return prefix + "/edit";
    }

    /**
     * 修改保存分类规格
     */
    @RequiresPermissions("system:spec:edit")
    @Log(title = "分类规格", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TypeSpec typeSpec)
    {
        return toAjax(typeSpecService.updateTypeSpec(typeSpec));
    }

    /**
     * 删除分类规格
     */
    @RequiresPermissions("system:spec:remove")
    @Log(title = "分类规格", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(typeSpecService.deleteTypeSpecByIds(ids));
    }
}
