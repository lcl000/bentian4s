package com.ruoyi.project.system.tspec.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.tspec.mapper.TypeSpecMapper;
import com.ruoyi.project.system.tspec.domain.TypeSpec;
import com.ruoyi.project.system.tspec.service.ITypeSpecService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 分类规格Service业务层处理
 * 
 * @author LCL
 * @date 2020-06-10
 */
@Service
public class TypeSpecServiceImpl implements ITypeSpecService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private TypeSpecMapper typeSpecMapper;

    /**
     * 查询分类规格
     * 
     * @param id 分类规格ID
     * @return 分类规格
     */
    @Override
    public TypeSpec selectTypeSpecById(String id)
    {
        return typeSpecMapper.selectTypeSpecById(id);
    }

    /**
     * 查询分类规格列表
     * 
     * @param typeSpec 分类规格
     * @return 分类规格
     */
    @Override
    public List<TypeSpec> selectTypeSpecList(TypeSpec typeSpec)
    {
        return typeSpecMapper.selectTypeSpecList(typeSpec);
    }

    /**
     * 新增分类规格
     * 
     * @param typeSpec 分类规格
     * @return 结果
     */
    @Override
    public int insertTypeSpec(TypeSpec typeSpec)
    {
        typeSpec.setId(this.idGererateFactory.nextStringId());
        return typeSpecMapper.insertTypeSpec(typeSpec);
    }

    /**
     * 修改分类规格
     * 
     * @param typeSpec 分类规格
     * @return 结果
     */
    @Override
    public int updateTypeSpec(TypeSpec typeSpec)
    {
        return typeSpecMapper.updateTypeSpec(typeSpec);
    }

    /**
     * 删除分类规格对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteTypeSpecByIds(String ids)
    {
        return typeSpecMapper.deleteTypeSpecByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除分类规格信息
     * 
     * @param id 分类规格ID
     * @return 结果
     */
    @Override
    public int deleteTypeSpecById(String id)
    {
        return typeSpecMapper.deleteTypeSpecById(id);
    }

    @Override
    public List<TypeSpec> findListBySpecIds(String ids) {
        return this.typeSpecMapper.findListBySpecIds(Convert.toStrArray(ids));
    }

    @Override
    public List<TypeSpec> findListByTypeId(String id) {
        return this.typeSpecMapper.findListByTypeId(id);
    }
}
