package com.ruoyi.project.system.tspec.domain;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import com.ruoyi.project.system.spec.domain.GoodsSpec;
import com.ruoyi.project.system.tspec.dto.TypeSpecValueDto;

/**
 * 分类规格对象 type_spec
 * 
 * @author LCL
 * @date 2020-06-10
 */
public class TypeSpec extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 类别主键ID */
    @Excel(name = "类别主键ID")
    private String typeId;

    /** 规格主键ID */
    @Excel(name = "规格主键ID")
    private String specId;
    //关联的 规格信息
    private GoodsSpec goodsSpec;

    //关联的 规格值信息
    private List<TypeSpecValueDto> typeSpecValueDtoList;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setTypeId(String typeId) 
    {
        this.typeId = typeId;
    }

    public String getTypeId() 
    {
        return typeId;
    }
    public void setSpecId(String specId) 
    {
        this.specId = specId;
    }

    public String getSpecId() 
    {
        return specId;
    }

    public GoodsSpec getGoodsSpec() {
        return goodsSpec;
    }

    public void setGoodsSpec(GoodsSpec goodsSpec) {
        this.goodsSpec = goodsSpec;
    }

    public List<TypeSpecValueDto> getTypeSpecValueDtoList() {
        return typeSpecValueDtoList;
    }

    public void setTypeSpecValueDtoList(List<TypeSpecValueDto> typeSpecValueDtoList) {
        this.typeSpecValueDtoList = typeSpecValueDtoList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("typeId", getTypeId())
            .append("specId", getSpecId())
            .toString();
    }
}
