package com.ruoyi.project.system.banner.shop.service;

import java.util.List;
import com.ruoyi.project.system.banner.shop.domain.BannerShop;

/**
 * 积分商城轮播图Service接口
 * 
 * @author LCL
 * @date 2020-08-06
 */
public interface IBannerShopService 
{
    /**
     * 查询积分商城轮播图
     * 
     * @param id 积分商城轮播图ID
     * @return 积分商城轮播图
     */
    public BannerShop selectBannerShopById(String id);

    /**
     * 查询积分商城轮播图列表
     * 
     * @param bannerShop 积分商城轮播图
     * @return 积分商城轮播图集合
     */
    public List<BannerShop> selectBannerShopList(BannerShop bannerShop);

    /**
     * 新增积分商城轮播图
     * 
     * @param bannerShop 积分商城轮播图
     * @return 结果
     */
    public int insertBannerShop(BannerShop bannerShop);

    /**
     * 修改积分商城轮播图
     * 
     * @param bannerShop 积分商城轮播图
     * @return 结果
     */
    public int updateBannerShop(BannerShop bannerShop);

    /**
     * 批量删除积分商城轮播图
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBannerShopByIds(String ids);

    /**
     * 删除积分商城轮播图信息
     * 
     * @param id 积分商城轮播图ID
     * @return 结果
     */
    public int deleteBannerShopById(String id);

	List<BannerShop> findAll();
}
