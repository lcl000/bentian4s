package com.ruoyi.project.system.banner.platform.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.banner.platform.mapper.BannerPlatformMapper;
import com.ruoyi.project.system.banner.platform.domain.BannerPlatform;
import com.ruoyi.project.system.banner.platform.service.IBannerPlatformService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 平台轮播图Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-29
 */
@Service
public class BannerPlatformServiceImpl implements IBannerPlatformService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private BannerPlatformMapper bannerPlatformMapper;

    /**
     * 查询平台轮播图
     * 
     * @param id 平台轮播图ID
     * @return 平台轮播图
     */
    @Override
    public BannerPlatform selectBannerPlatformById(String id)
    {
        return bannerPlatformMapper.selectBannerPlatformById(id);
    }

    /**
     * 查询平台轮播图列表
     * 
     * @param bannerPlatform 平台轮播图
     * @return 平台轮播图
     */
    @Override
    public List<BannerPlatform> selectBannerPlatformList(BannerPlatform bannerPlatform)
    {
        return bannerPlatformMapper.selectBannerPlatformList(bannerPlatform);
    }

    /**
     * 新增平台轮播图
     * 
     * @param bannerPlatform 平台轮播图
     * @return 结果
     */
    @Override
    public int insertBannerPlatform(BannerPlatform bannerPlatform)
    {
        bannerPlatform.setId(this.idGererateFactory.nextStringId());
        return bannerPlatformMapper.insertBannerPlatform(bannerPlatform);
    }

    /**
     * 修改平台轮播图
     * 
     * @param bannerPlatform 平台轮播图
     * @return 结果
     */
    @Override
    public int updateBannerPlatform(BannerPlatform bannerPlatform)
    {
        return bannerPlatformMapper.updateBannerPlatform(bannerPlatform);
    }

    /**
     * 删除平台轮播图对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBannerPlatformByIds(String ids)
    {
        return bannerPlatformMapper.deleteBannerPlatformByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除平台轮播图信息
     * 
     * @param id 平台轮播图ID
     * @return 结果
     */
    @Override
    public int deleteBannerPlatformById(String id)
    {
        return bannerPlatformMapper.deleteBannerPlatformById(id);
    }

    @Override
    public List<BannerPlatform> findBannerList() {
        return this.bannerPlatformMapper.findBannerList();
    }
}
