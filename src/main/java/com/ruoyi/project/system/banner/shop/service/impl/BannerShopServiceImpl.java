package com.ruoyi.project.system.banner.shop.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.banner.shop.mapper.BannerShopMapper;
import com.ruoyi.project.system.banner.shop.domain.BannerShop;
import com.ruoyi.project.system.banner.shop.service.IBannerShopService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 积分商城轮播图Service业务层处理
 * 
 * @author LCL
 * @date 2020-08-06
 */
@Service
public class BannerShopServiceImpl implements IBannerShopService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private BannerShopMapper bannerShopMapper;

    /**
     * 查询积分商城轮播图
     * 
     * @param id 积分商城轮播图ID
     * @return 积分商城轮播图
     */
    @Override
    public BannerShop selectBannerShopById(String id)
    {
        return bannerShopMapper.selectBannerShopById(id);
    }

    /**
     * 查询积分商城轮播图列表
     * 
     * @param bannerShop 积分商城轮播图
     * @return 积分商城轮播图
     */
    @Override
    public List<BannerShop> selectBannerShopList(BannerShop bannerShop)
    {
        return bannerShopMapper.selectBannerShopList(bannerShop);
    }

    /**
     * 新增积分商城轮播图
     * 
     * @param bannerShop 积分商城轮播图
     * @return 结果
     */
    @Override
    public int insertBannerShop(BannerShop bannerShop)
    {
        bannerShop.setId(this.idGererateFactory.nextStringId());
        return bannerShopMapper.insertBannerShop(bannerShop);
    }

    /**
     * 修改积分商城轮播图
     * 
     * @param bannerShop 积分商城轮播图
     * @return 结果
     */
    @Override
    public int updateBannerShop(BannerShop bannerShop)
    {
        return bannerShopMapper.updateBannerShop(bannerShop);
    }

    /**
     * 删除积分商城轮播图对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBannerShopByIds(String ids)
    {
        return bannerShopMapper.deleteBannerShopByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除积分商城轮播图信息
     * 
     * @param id 积分商城轮播图ID
     * @return 结果
     */
    @Override
    public int deleteBannerShopById(String id)
    {
        return bannerShopMapper.deleteBannerShopById(id);
    }

    @Override
    public List<BannerShop> findAll() {
        return this.bannerShopMapper.findAll();
    }
}
