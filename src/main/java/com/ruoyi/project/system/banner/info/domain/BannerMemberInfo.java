package com.ruoyi.project.system.banner.info.domain;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 用户中心轮播图对象 banner_member_info
 * 
 * @author LCL
 * @date 2020-07-29
 */
public class BannerMemberInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 图片名称 */
    @Excel(name = "图片名称")
    private String name;

    /** 描述 */
    @Excel(name = "描述")
    private String notes;

    /** 创建人ID */
    @Excel(name = "创建人ID")
    private Long creator;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date created;

    /** 排序(正序) */
    @Excel(name = "排序(正序)")
    private Long sort;

    /** 前图图片访问路径 */
    @Excel(name = "前图图片访问路径")
    private String headUrl;

    /** 轮播状态:0 禁用 1 启用 */
    @Excel(name = "轮播状态:0 禁用 1 启用")
    private Integer status;

    /** 轮播类型: 0 跳转商品 1 不跳转 2 跳转分类 */
    @Excel(name = "轮播类型: 0 跳转商品 1 不跳转 2 跳转分类")
    private Integer type;

    /** 背景图图片访问路径 */
    @Excel(name = "背景图图片访问路径")
    private String backgroundUrl;

    /** 关联资源ID,根绝type字段来区别是什么资源 */
    @Excel(name = "关联资源ID,根绝type字段来区别是什么资源")
    private String resId;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setNotes(String notes) 
    {
        this.notes = notes;
    }

    public String getNotes() 
    {
        return notes;
    }
    public void setCreator(Long creator) 
    {
        this.creator = creator;
    }

    public Long getCreator() 
    {
        return creator;
    }
    public void setCreated(Date created) 
    {
        this.created = created;
    }

    public Date getCreated() 
    {
        return created;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setHeadUrl(String headUrl) 
    {
        this.headUrl = headUrl;
    }

    public String getHeadUrl() 
    {
        return headUrl;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setBackgroundUrl(String backgroundUrl) 
    {
        this.backgroundUrl = backgroundUrl;
    }

    public String getBackgroundUrl() 
    {
        return backgroundUrl;
    }
    public void setResId(String resId) 
    {
        this.resId = resId;
    }

    public String getResId() 
    {
        return resId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("notes", getNotes())
            .append("creator", getCreator())
            .append("created", getCreated())
            .append("sort", getSort())
            .append("headUrl", getHeadUrl())
            .append("status", getStatus())
            .append("type", getType())
            .append("backgroundUrl", getBackgroundUrl())
            .append("resId", getResId())
            .toString();
    }
}
