package com.ruoyi.project.system.banner.platform.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.activity.service.IActivityService;
import com.ruoyi.project.system.banner.platform.domain.BannerPlatform;
import com.ruoyi.project.system.banner.platform.service.IBannerPlatformService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.car.car.domain.Car;
import com.ruoyi.project.system.car.car.service.ICarService;
import com.ruoyi.project.system.goods.goods.domain.GoodsGoods;
import com.ruoyi.project.system.goods.goods.service.IGoodsGoodsService;
import com.ruoyi.project.system.used.used.domain.CarUsed;
import com.ruoyi.project.system.used.used.service.ICarUsedService;

/**
 * 平台轮播图Controller
 * 
 * @author LCL
 * @date 2020-07-29
 */
@Controller
@RequestMapping("/system/banner/platform")
public class BannerPlatformController extends BaseController
{
    private String prefix = "system/banner/platform";

    @Autowired
    private IBannerPlatformService bannerPlatformService;
    @Autowired
    private ICarService carService;
    @Autowired
    private IActivityService activityService;
    @Autowired
    private ICarUsedService carUsedService;


    @Autowired
    private IGoodsGoodsService goodsGoodsService;


    @RequiresPermissions("system:platform:view")
    @GetMapping()
    public String platform()
    {
        return prefix + "/platform";
    }

    /**
     * 查询平台轮播图列表
     */
    @RequiresPermissions("system:platform:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BannerPlatform bannerPlatform)
    {
        startPage();
        List<BannerPlatform> list = bannerPlatformService.selectBannerPlatformList(bannerPlatform);
        return getDataTable(list);
    }

    /**
     * 导出平台轮播图列表
     */
    @RequiresPermissions("system:platform:export")
    @Log(title = "平台轮播图", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BannerPlatform bannerPlatform)
    {
        List<BannerPlatform> list = bannerPlatformService.selectBannerPlatformList(bannerPlatform);
        ExcelUtil<BannerPlatform> util = new ExcelUtil<BannerPlatform>(BannerPlatform.class);
        return util.exportExcel(list, "platform");
    }

    /**
     * 新增平台轮播图
     */
    @GetMapping("/add")
    public String add(ModelMap map)
    {
        Car car = new Car();
        car.setStatus(0);
        map.put("carList",this.carService.selectCarList(car));
        map.put("activityList",this.activityService.findList());
        GoodsGoods goodsGoods = new GoodsGoods();
        goodsGoods.setStatus(0);
        map.put("goodsList",this.goodsGoodsService.selectGoodsGoodsList(goodsGoods));
        CarUsed carUsed = new CarUsed();
        carUsed.setStatus(0);
        map.put("usedList",this.carUsedService.selectCarUsedList(carUsed));
        return prefix + "/add";
    }

    /**
     * 新增保存平台轮播图
     */
    @RequiresPermissions("system:platform:add")
    @Log(title = "平台轮播图", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BannerPlatform bannerPlatform)
    {
        if(bannerPlatform.getType()!=1){
            String[] res = bannerPlatform.getResId().split(",");
            if(bannerPlatform.getType()==0){
                bannerPlatform.setResId(res[0]);
            }else if(bannerPlatform.getType()==2){
                bannerPlatform.setResId(res[1]);
            }else if(bannerPlatform.getType()==3){
                bannerPlatform.setResId(res[2]);
            }else if(bannerPlatform.getType()==4){
                bannerPlatform.setResId(res[3]);
            }
        }else{
            bannerPlatform.setResId(null);
        }
        bannerPlatform.setCreated(DateUtils.getNowDate());
        return toAjax(bannerPlatformService.insertBannerPlatform(bannerPlatform));
    }

    /**
     * 修改平台轮播图
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        BannerPlatform bannerPlatform = bannerPlatformService.selectBannerPlatformById(id);
        mmap.put("banner", bannerPlatform);
        Car car = new Car();
        car.setStatus(0);
        mmap.put("carList",this.carService.selectCarList(car));
        mmap.put("activityList",this.activityService.findList());
        GoodsGoods goodsGoods = new GoodsGoods();
        goodsGoods.setStatus(0);
        mmap.put("goodsList",this.goodsGoodsService.selectGoodsGoodsList(goodsGoods));
        CarUsed carUsed = new CarUsed();
        carUsed.setStatus(0);
        mmap.put("usedList",this.carUsedService.selectCarUsedList(carUsed));
        return prefix + "/edit";
    }

    /**
     * 修改保存平台轮播图
     */
    @RequiresPermissions("system:platform:edit")
    @Log(title = "平台轮播图", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BannerPlatform bannerPlatform)
    {
        //查询数据库获取旧数据
        BannerPlatform oldBanner = bannerPlatformService.selectBannerPlatformById(bannerPlatform.getId());
        //判断数据是否存在
        if(oldBanner==null){
            return AjaxResult.error("数据不存在");
        }
        if(bannerPlatform.getType()!=1){
            String[] res = bannerPlatform.getResId().split(",");
            if(bannerPlatform.getType()==0){
                bannerPlatform.setResId(res[0]);
            }else if(bannerPlatform.getType()==2){
                bannerPlatform.setResId(res[1]);
            }else if(bannerPlatform.getType()==3){
                bannerPlatform.setResId(res[2]);
            }else if(bannerPlatform.getType()==4){
                bannerPlatform.setResId(res[3]);
            }
        }else{
            bannerPlatform.setResId(null);
        }
        //更新数据
        bannerPlatformService.updateBannerPlatform(bannerPlatform);


        return AjaxResult.success();
    }

    /**
     * 删除平台轮播图
     */
    @RequiresPermissions("system:platform:remove")
    @Log(title = "平台轮播图", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(bannerPlatformService.deleteBannerPlatformByIds(ids));
    }
}
