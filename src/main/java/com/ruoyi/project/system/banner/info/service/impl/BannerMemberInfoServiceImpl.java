package com.ruoyi.project.system.banner.info.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.banner.info.mapper.BannerMemberInfoMapper;
import com.ruoyi.project.system.banner.info.domain.BannerMemberInfo;
import com.ruoyi.project.system.banner.info.service.IBannerMemberInfoService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 用户中心轮播图Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-29
 */
@Service
public class BannerMemberInfoServiceImpl implements IBannerMemberInfoService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private BannerMemberInfoMapper bannerMemberInfoMapper;

    /**
     * 查询用户中心轮播图
     * 
     * @param id 用户中心轮播图ID
     * @return 用户中心轮播图
     */
    @Override
    public BannerMemberInfo selectBannerMemberInfoById(String id)
    {
        return bannerMemberInfoMapper.selectBannerMemberInfoById(id);
    }

    /**
     * 查询用户中心轮播图列表
     * 
     * @param bannerMemberInfo 用户中心轮播图
     * @return 用户中心轮播图
     */
    @Override
    public List<BannerMemberInfo> selectBannerMemberInfoList(BannerMemberInfo bannerMemberInfo)
    {
        return bannerMemberInfoMapper.selectBannerMemberInfoList(bannerMemberInfo);
    }

    /**
     * 新增用户中心轮播图
     * 
     * @param bannerMemberInfo 用户中心轮播图
     * @return 结果
     */
    @Override
    public int insertBannerMemberInfo(BannerMemberInfo bannerMemberInfo)
    {
        bannerMemberInfo.setId(this.idGererateFactory.nextStringId());
        return bannerMemberInfoMapper.insertBannerMemberInfo(bannerMemberInfo);
    }

    /**
     * 修改用户中心轮播图
     * 
     * @param bannerMemberInfo 用户中心轮播图
     * @return 结果
     */
    @Override
    public int updateBannerMemberInfo(BannerMemberInfo bannerMemberInfo)
    {
        return bannerMemberInfoMapper.updateBannerMemberInfo(bannerMemberInfo);
    }

    /**
     * 删除用户中心轮播图对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBannerMemberInfoByIds(String ids)
    {
        return bannerMemberInfoMapper.deleteBannerMemberInfoByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户中心轮播图信息
     * 
     * @param id 用户中心轮播图ID
     * @return 结果
     */
    @Override
    public int deleteBannerMemberInfoById(String id)
    {
        return bannerMemberInfoMapper.deleteBannerMemberInfoById(id);
    }

    @Override
    public List<BannerMemberInfo> findBannerList() {
        return this.bannerMemberInfoMapper.findBannerList();
    }
}
