package com.ruoyi.project.system.banner.used.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.activity.service.IActivityService;
import com.ruoyi.project.system.banner.banner.dto.BannerDto;
import com.ruoyi.project.system.banner.used.domain.BannerUsed;
import com.ruoyi.project.system.banner.used.service.IBannerUsedService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.car.car.domain.Car;
import com.ruoyi.project.system.car.car.service.ICarService;
import com.ruoyi.project.system.goods.goods.domain.GoodsGoods;
import com.ruoyi.project.system.goods.goods.service.IGoodsGoodsService;
import com.ruoyi.project.system.used.used.domain.CarUsed;
import com.ruoyi.project.system.used.used.service.ICarUsedService;

/**
 * 二手车轮播图Controller
 * 
 * @author LCL
 * @date 2020-07-29
 */
@Controller
@RequestMapping("/system/banner/used")
public class BannerUsedController extends BaseController
{
    private String prefix = "system/banner/used";

    @Autowired
    private IBannerUsedService bannerUsedService;
    @Autowired
    private ICarService carService;
    @Autowired
    private IActivityService activityService;
    @Autowired
    private ICarUsedService carUsedService;


    @Autowired
    private IGoodsGoodsService goodsGoodsService;


    @RequiresPermissions("system:used:view")
    @GetMapping()
    public String used()
    {
        return prefix + "/used";
    }

    /**
     * 查询二手车轮播图列表
     */
    @RequiresPermissions("system:used:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BannerUsed bannerUsed)
    {
        startPage();
        List<BannerUsed> list = bannerUsedService.selectBannerUsedList(bannerUsed);
        return getDataTable(list);
    }

    /**
     * 导出二手车轮播图列表
     */
    @RequiresPermissions("system:used:export")
    @Log(title = "二手车轮播图", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BannerUsed bannerUsed)
    {
        List<BannerUsed> list = bannerUsedService.selectBannerUsedList(bannerUsed);
        ExcelUtil<BannerUsed> util = new ExcelUtil<BannerUsed>(BannerUsed.class);
        return util.exportExcel(list, "used");
    }

    /**
     * 新增二手车轮播图
     */
    @GetMapping("/add")
    public String add(ModelMap map)
    {
        Car car = new Car();
        car.setStatus(0);
        map.put("carList",this.carService.selectCarList(car));
        map.put("activityList",this.activityService.findList());
        GoodsGoods goodsGoods = new GoodsGoods();
        goodsGoods.setStatus(0);
        map.put("goodsList",this.goodsGoodsService.selectGoodsGoodsList(goodsGoods));
        CarUsed carUsed = new CarUsed();
        carUsed.setStatus(0);
        map.put("usedList",this.carUsedService.selectCarUsedList(carUsed));
        return prefix + "/add";
    }

    /**
     * 新增保存二手车轮播图
     */
    @RequiresPermissions("system:used:add")
    @Log(title = "二手车轮播图", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BannerUsed bannerUsed)
    {
        if(bannerUsed.getType()!=1){
            String[] res = bannerUsed.getResId().split(",");
            if(bannerUsed.getType()==0){
                bannerUsed.setResId(res[0]);
            }else if(bannerUsed.getType()==2){
                bannerUsed.setResId(res[1]);
            }else if(bannerUsed.getType()==3){
                bannerUsed.setResId(res[2]);
            }else if(bannerUsed.getType()==4){
                bannerUsed.setResId(res[3]);
            }
        }else{
            bannerUsed.setResId(null);
        }
        bannerUsed.setCreated(DateUtils.getNowDate());
        bannerUsedService.insertBannerUsed(bannerUsed);
        return AjaxResult.success();
    }

    /**
     * 修改二手车轮播图
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        BannerUsed banner = bannerUsedService.selectBannerUsedById(id);
        mmap.put("banner", banner);
        Car car = new Car();
        car.setStatus(0);
        mmap.put("carList",this.carService.selectCarList(car));
        mmap.put("activityList",this.activityService.findList());
        GoodsGoods goodsGoods = new GoodsGoods();
        goodsGoods.setStatus(0);
        mmap.put("goodsList",this.goodsGoodsService.selectGoodsGoodsList(goodsGoods));
        CarUsed carUsed = new CarUsed();
        carUsed.setStatus(0);
        mmap.put("usedList",this.carUsedService.selectCarUsedList(carUsed));
        return prefix + "/edit";
    }

    /**
     * 修改保存二手车轮播图
     */
    @RequiresPermissions("system:used:edit")
    @Log(title = "二手车轮播图", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BannerUsed bannerUsed)
    {
        //查询数据库获取旧数据
        BannerUsed oldBanner = bannerUsedService.selectBannerUsedById(bannerUsed.getId());
        //判断数据是否存在
        if(oldBanner==null){
            return AjaxResult.error("数据不存在");
        }
        if(bannerUsed.getType()!=1){
            String[] res = bannerUsed.getResId().split(",");
            if(bannerUsed.getType()==0){
                bannerUsed.setResId(res[0]);
            }else if(bannerUsed.getType()==2){
                bannerUsed.setResId(res[1]);
            }else if(bannerUsed.getType()==3){
                bannerUsed.setResId(res[2]);
            }else if(bannerUsed.getType()==4){
                bannerUsed.setResId(res[3]);
            }
        }else{
            bannerUsed.setResId(null);
        }
        //更新数据
        bannerUsedService.updateBannerUsed(bannerUsed);

        return AjaxResult.success();
    }

    /**
     * 删除二手车轮播图
     */
    @RequiresPermissions("system:used:remove")
    @Log(title = "二手车轮播图", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(bannerUsedService.deleteBannerUsedByIds(ids));
    }
}
