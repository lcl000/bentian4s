package com.ruoyi.project.system.banner.banner.dto;

import com.ruoyi.project.system.banner.banner.domain.Banner;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/7/4 14:34
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public class BannerDto extends Banner {
	//汽车名称
	private String carName;

	//活动名称
	private String activityName;

	public String getCarName() {
		return carName;
	}

	public void setCarName(String carName) {
		this.carName = carName;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
}
