package com.ruoyi.project.system.banner.banner.service;

import java.util.List;
import com.ruoyi.project.system.banner.banner.domain.Banner;
import com.ruoyi.project.system.banner.banner.dto.BannerDto;

/**
 * 轮播图Service接口
 * 
 * @author LCL
 * @date 2020-06-05
 */
public interface IBannerService 
{
    /**
     * 查询轮播图
     * 
     * @param id 轮播图ID
     * @return 轮播图
     */
    public BannerDto selectBannerById(String id);

    /**
     * 查询轮播图列表
     * 
     * @param banner 轮播图
     * @return 轮播图集合
     */
    public List<Banner> selectBannerList(Banner banner);

    /**
     * 新增轮播图
     * 
     * @param banner 轮播图
     * @return 结果
     */
    public int insertBanner(Banner banner);

    /**
     * 修改轮播图
     * 
     * @param banner 轮播图
     * @return 结果
     */
    public int updateBanner(Banner banner);

    /**
     * 批量删除轮播图
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBannerByIds(String ids);

    /**
     * 删除轮播图信息
     * 
     * @param id 轮播图ID
     * @return 结果
     */
    public int deleteBannerById(Long id);

	List<Banner> selectBannerByIds(String ids);

    List<Banner> findBannerList();
}
