package com.ruoyi.project.system.banner.banner.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.banner.banner.dto.BannerDto;
import com.ruoyi.project.system.banner.banner.mapper.BannerMapper;
import com.ruoyi.project.system.banner.banner.domain.Banner;
import com.ruoyi.project.system.banner.banner.service.IBannerService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 轮播图Service业务层处理
 * 
 * @author LCL
 * @date 2020-06-05
 */
@Service
public class BannerServiceImpl implements IBannerService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private BannerMapper bannerMapper;

    /**
     * 查询轮播图
     * 
     * @param id 轮播图ID
     * @return 轮播图
     */
    @Override
    public BannerDto selectBannerById(String id)
    {
        return bannerMapper.selectBannerById(id);
    }

    /**
     * 查询轮播图列表
     * 
     * @param banner 轮播图
     * @return 轮播图
     */
    @Override
    public List<Banner> selectBannerList(Banner banner)
    {
        return bannerMapper.selectBannerList(banner);
    }

    /**
     * 新增轮播图
     * 
     * @param banner 轮播图
     * @return 结果
     */
    @Override
    public int insertBanner(Banner banner)
    {
        banner.setId(idGererateFactory.nextStringId());
        return bannerMapper.insertBanner(banner);
    }

    /**
     * 修改轮播图
     * 
     * @param banner 轮播图
     * @return 结果
     */
    @Override
    public int updateBanner(Banner banner)
    {
        return bannerMapper.updateBanner(banner);
    }

    /**
     * 删除轮播图对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBannerByIds(String ids)
    {
        return bannerMapper.deleteBannerByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除轮播图信息
     * 
     * @param id 轮播图ID
     * @return 结果
     */
    @Override
    public int deleteBannerById(Long id)
    {
        return bannerMapper.deleteBannerById(id);
    }

    @Override
    public List<Banner> selectBannerByIds(String ids) {
        return bannerMapper.selectBannerByIds(Convert.toStrArray(ids));
    }

    @Override
    public List<Banner> findBannerList() {
        return this.bannerMapper.findBannerList();
    }
}
