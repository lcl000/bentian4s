package com.ruoyi.project.system.banner.platform.mapper;

import java.util.List;
import com.ruoyi.project.system.banner.platform.domain.BannerPlatform;

/**
 * 平台轮播图Mapper接口
 * 
 * @author LCL
 * @date 2020-07-29
 */
public interface BannerPlatformMapper 
{
    /**
     * 查询平台轮播图
     * 
     * @param id 平台轮播图ID
     * @return 平台轮播图
     */
    public BannerPlatform selectBannerPlatformById(String id);

    /**
     * 查询平台轮播图列表
     * 
     * @param bannerPlatform 平台轮播图
     * @return 平台轮播图集合
     */
    public List<BannerPlatform> selectBannerPlatformList(BannerPlatform bannerPlatform);

    /**
     * 新增平台轮播图
     * 
     * @param bannerPlatform 平台轮播图
     * @return 结果
     */
    public int insertBannerPlatform(BannerPlatform bannerPlatform);

    /**
     * 修改平台轮播图
     * 
     * @param bannerPlatform 平台轮播图
     * @return 结果
     */
    public int updateBannerPlatform(BannerPlatform bannerPlatform);

    /**
     * 删除平台轮播图
     * 
     * @param id 平台轮播图ID
     * @return 结果
     */
    public int deleteBannerPlatformById(String id);

    /**
     * 批量删除平台轮播图
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBannerPlatformByIds(String[] ids);

	List<BannerPlatform> findBannerList();
}
