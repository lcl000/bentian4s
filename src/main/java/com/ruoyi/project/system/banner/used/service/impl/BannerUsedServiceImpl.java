package com.ruoyi.project.system.banner.used.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.banner.used.mapper.BannerUsedMapper;
import com.ruoyi.project.system.banner.used.domain.BannerUsed;
import com.ruoyi.project.system.banner.used.service.IBannerUsedService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 二手车轮播图Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-29
 */
@Service
public class BannerUsedServiceImpl implements IBannerUsedService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private BannerUsedMapper bannerUsedMapper;

    /**
     * 查询二手车轮播图
     * 
     * @param id 二手车轮播图ID
     * @return 二手车轮播图
     */
    @Override
    public BannerUsed selectBannerUsedById(String id)
    {
        return bannerUsedMapper.selectBannerUsedById(id);
    }

    /**
     * 查询二手车轮播图列表
     * 
     * @param bannerUsed 二手车轮播图
     * @return 二手车轮播图
     */
    @Override
    public List<BannerUsed> selectBannerUsedList(BannerUsed bannerUsed)
    {
        return bannerUsedMapper.selectBannerUsedList(bannerUsed);
    }

    /**
     * 新增二手车轮播图
     * 
     * @param bannerUsed 二手车轮播图
     * @return 结果
     */
    @Override
    public int insertBannerUsed(BannerUsed bannerUsed)
    {
        bannerUsed.setId(this.idGererateFactory.nextStringId());
        return bannerUsedMapper.insertBannerUsed(bannerUsed);
    }

    /**
     * 修改二手车轮播图
     * 
     * @param bannerUsed 二手车轮播图
     * @return 结果
     */
    @Override
    public int updateBannerUsed(BannerUsed bannerUsed)
    {
        return bannerUsedMapper.updateBannerUsed(bannerUsed);
    }

    /**
     * 删除二手车轮播图对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBannerUsedByIds(String ids)
    {
        return bannerUsedMapper.deleteBannerUsedByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除二手车轮播图信息
     * 
     * @param id 二手车轮播图ID
     * @return 结果
     */
    @Override
    public int deleteBannerUsedById(String id)
    {
        return bannerUsedMapper.deleteBannerUsedById(id);
    }

	@Override
	public List<BannerUsed> findBannerList() {
		return this.bannerUsedMapper.findBannerList();
	}
}
