package com.ruoyi.project.system.banner.info.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.activity.service.IActivityService;
import com.ruoyi.project.system.banner.info.domain.BannerMemberInfo;
import com.ruoyi.project.system.banner.info.service.IBannerMemberInfoService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.car.car.domain.Car;
import com.ruoyi.project.system.car.car.service.ICarService;
import com.ruoyi.project.system.goods.goods.domain.GoodsGoods;
import com.ruoyi.project.system.goods.goods.service.IGoodsGoodsService;
import com.ruoyi.project.system.used.used.domain.CarUsed;
import com.ruoyi.project.system.used.used.service.ICarUsedService;

/**
 * 用户中心轮播图Controller
 * 
 * @author LCL
 * @date 2020-07-29
 */
@Controller
@RequestMapping("/system/banner/info")
public class BannerMemberInfoController extends BaseController
{
    private String prefix = "system/banner/info";

    @Autowired
    private IBannerMemberInfoService bannerMemberInfoService;
    @Autowired
    private ICarService carService;
    @Autowired
    private IActivityService activityService;
    @Autowired
    private ICarUsedService carUsedService;


    @Autowired
    private IGoodsGoodsService goodsGoodsService;


    @RequiresPermissions("system:info:view")
    @GetMapping()
    public String info()
    {
        return prefix + "/info";
    }

    /**
     * 查询用户中心轮播图列表
     */
    @RequiresPermissions("system:info:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BannerMemberInfo bannerMemberInfo)
    {
        startPage();
        List<BannerMemberInfo> list = bannerMemberInfoService.selectBannerMemberInfoList(bannerMemberInfo);
        return getDataTable(list);
    }

    /**
     * 导出用户中心轮播图列表
     */
    @RequiresPermissions("system:info:export")
    @Log(title = "用户中心轮播图", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BannerMemberInfo bannerMemberInfo)
    {
        List<BannerMemberInfo> list = bannerMemberInfoService.selectBannerMemberInfoList(bannerMemberInfo);
        ExcelUtil<BannerMemberInfo> util = new ExcelUtil<BannerMemberInfo>(BannerMemberInfo.class);
        return util.exportExcel(list, "info");
    }

    /**
     * 新增用户中心轮播图
     */
    @GetMapping("/add")
    public String add(ModelMap map)
    {
        Car car = new Car();
        car.setStatus(0);
        map.put("carList",this.carService.selectCarList(car));
        map.put("activityList",this.activityService.findList());
        GoodsGoods goodsGoods = new GoodsGoods();
        goodsGoods.setStatus(0);
        map.put("goodsList",this.goodsGoodsService.selectGoodsGoodsList(goodsGoods));
        CarUsed carUsed = new CarUsed();
        carUsed.setStatus(0);
        map.put("usedList",this.carUsedService.selectCarUsedList(carUsed));
        return prefix + "/add";
    }

    /**
     * 新增保存用户中心轮播图
     */
    @RequiresPermissions("system:info:add")
    @Log(title = "用户中心轮播图", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BannerMemberInfo bannerMemberInfo)
    {
        if(bannerMemberInfo.getType()!=1){
            String[] res = bannerMemberInfo.getResId().split(",");
            if(bannerMemberInfo.getType()==0){
                bannerMemberInfo.setResId(res[0]);
            }else if(bannerMemberInfo.getType()==2){
                bannerMemberInfo.setResId(res[1]);
            }else if(bannerMemberInfo.getType()==3){
                bannerMemberInfo.setResId(res[2]);
            }else if(bannerMemberInfo.getType()==4){
                bannerMemberInfo.setResId(res[3]);
            }
        }else{
            bannerMemberInfo.setResId(null);
        }
        bannerMemberInfo.setCreated(DateUtils.getNowDate());
        bannerMemberInfoService.insertBannerMemberInfo(bannerMemberInfo);
        return AjaxResult.success();
    }

    /**
     * 修改用户中心轮播图
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        BannerMemberInfo bannerMemberInfo = bannerMemberInfoService.selectBannerMemberInfoById(id);
        mmap.put("banner", bannerMemberInfo);
        Car car = new Car();
        car.setStatus(0);
        mmap.put("carList",this.carService.selectCarList(car));
        mmap.put("activityList",this.activityService.findList());
        GoodsGoods goodsGoods = new GoodsGoods();
        goodsGoods.setStatus(0);
        mmap.put("goodsList",this.goodsGoodsService.selectGoodsGoodsList(goodsGoods));
        CarUsed carUsed = new CarUsed();
        carUsed.setStatus(0);
        mmap.put("usedList",this.carUsedService.selectCarUsedList(carUsed));
        return prefix + "/edit";
    }

    /**
     * 修改保存用户中心轮播图
     */
    @RequiresPermissions("system:info:edit")
    @Log(title = "用户中心轮播图", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BannerMemberInfo bannerMemberInfo)
    {
        //查询数据库获取旧数据
        BannerMemberInfo oldBanner = bannerMemberInfoService.selectBannerMemberInfoById(bannerMemberInfo.getId());
        //判断数据是否存在
        if(oldBanner==null){
            return AjaxResult.error("数据不存在");
        }
        if(bannerMemberInfo.getType()!=1){
            String[] res = bannerMemberInfo.getResId().split(",");
            if(bannerMemberInfo.getType()==0){
                bannerMemberInfo.setResId(res[0]);
            }else if(bannerMemberInfo.getType()==2){
                bannerMemberInfo.setResId(res[1]);
            }else if(bannerMemberInfo.getType()==3){
                bannerMemberInfo.setResId(res[2]);
            }else if(bannerMemberInfo.getType()==4){
                bannerMemberInfo.setResId(res[3]);
            }
        }else{
            bannerMemberInfo.setResId(null);
        }
        //更新数据
        bannerMemberInfoService.updateBannerMemberInfo(bannerMemberInfo);


        return AjaxResult.success();
    }

    /**
     * 删除用户中心轮播图
     */
    @RequiresPermissions("system:info:remove")
    @Log(title = "用户中心轮播图", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(bannerMemberInfoService.deleteBannerMemberInfoByIds(ids));
    }
}
