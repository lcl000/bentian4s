package com.ruoyi.project.system.banner.shop.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.activity.service.IActivityService;
import com.ruoyi.project.system.banner.shop.domain.BannerShop;
import com.ruoyi.project.system.banner.shop.service.IBannerShopService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.car.car.domain.Car;
import com.ruoyi.project.system.car.car.service.ICarService;
import com.ruoyi.project.system.goods.goods.domain.GoodsGoods;
import com.ruoyi.project.system.goods.goods.service.IGoodsGoodsService;
import com.ruoyi.project.system.used.used.domain.CarUsed;
import com.ruoyi.project.system.used.used.service.ICarUsedService;

/**
 * 积分商城轮播图Controller
 * 
 * @author LCL
 * @date 2020-08-06
 */
@Controller
@RequestMapping("/system/banner/shop")
public class BannerShopController extends BaseController
{
    private String prefix = "system/banner/shop";

    @Autowired
    private IBannerShopService bannerShopService;

    @Autowired
    private IGoodsGoodsService goodsGoodsService;

    @Autowired
    private IActivityService activityService;
    @Autowired
    private ICarUsedService carUsedService;

    @Autowired
    private ICarService carService;



    @RequiresPermissions("system:shop:view")
    @GetMapping()
    public String shop()
    {
        return prefix + "/shop";
    }

    /**
     * 查询积分商城轮播图列表
     */
    @RequiresPermissions("system:shop:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BannerShop bannerShop)
    {
        startPage();
        List<BannerShop> list = bannerShopService.selectBannerShopList(bannerShop);
        return getDataTable(list);
    }

    /**
     * 导出积分商城轮播图列表
     */
    @RequiresPermissions("system:shop:export")
    @Log(title = "积分商城轮播图", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BannerShop bannerShop)
    {
        List<BannerShop> list = bannerShopService.selectBannerShopList(bannerShop);
        ExcelUtil<BannerShop> util = new ExcelUtil<BannerShop>(BannerShop.class);
        return util.exportExcel(list, "shop");
    }

    /**
     * 新增积分商城轮播图
     */
    @GetMapping("/add")
    public String add(ModelMap map)
    {
        Car car = new Car();
        car.setStatus(0);
        map.put("carList",this.carService.selectCarList(car));
        map.put("activityList",this.activityService.findList());
        GoodsGoods goodsGoods = new GoodsGoods();
        goodsGoods.setStatus(0);
        map.put("goodsList",this.goodsGoodsService.selectGoodsGoodsList(goodsGoods));
        CarUsed carUsed = new CarUsed();
        carUsed.setStatus(0);
        map.put("usedList",this.carUsedService.selectCarUsedList(carUsed));
        return prefix + "/add";
    }

    /**
     * 新增保存积分商城轮播图
     */
    @RequiresPermissions("system:shop:add")
    @Log(title = "积分商城轮播图", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BannerShop bannerShop)
    {
        if(bannerShop.getType()!=1){
            String[] res = bannerShop.getResId().split(",");
            if(bannerShop.getType()==0){
                bannerShop.setResId(res[0]);
            }else if(bannerShop.getType()==2){
                bannerShop.setResId(res[1]);
            }else if(bannerShop.getType()==3){
                bannerShop.setResId(res[2]);
            }else if(bannerShop.getType()==4){
                bannerShop.setResId(res[3]);
            }
        }else{
            bannerShop.setResId(null);
        }
        return toAjax(bannerShopService.insertBannerShop(bannerShop));
    }

    /**
     * 修改积分商城轮播图
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        BannerShop bannerShop = bannerShopService.selectBannerShopById(id);
        mmap.put("bannerShop", bannerShop);
        Car car = new Car();
        car.setStatus(0);
        mmap.put("carList",this.carService.selectCarList(car));
        mmap.put("activityList",this.activityService.findList());
        GoodsGoods goodsGoods = new GoodsGoods();
        goodsGoods.setStatus(0);
        mmap.put("goodsList",this.goodsGoodsService.selectGoodsGoodsList(goodsGoods));
        CarUsed carUsed = new CarUsed();
        carUsed.setStatus(0);
        mmap.put("usedList",this.carUsedService.selectCarUsedList(carUsed));
        return prefix + "/edit";
    }

    /**
     * 修改保存积分商城轮播图
     */
    @RequiresPermissions("system:shop:edit")
    @Log(title = "积分商城轮播图", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BannerShop bannerShop)
    {
        if(bannerShop.getType()!=1){
            String[] res = bannerShop.getResId().split(",");
            if(bannerShop.getType()==0){
                bannerShop.setResId(res[0]);
            }else if(bannerShop.getType()==2){
                bannerShop.setResId(res[1]);
            }else if(bannerShop.getType()==3){
                bannerShop.setResId(res[2]);
            }else if(bannerShop.getType()==4){
                bannerShop.setResId(res[3]);
            }
        }else{
            bannerShop.setResId(null);
        }
        return toAjax(bannerShopService.updateBannerShop(bannerShop));
    }

    /**
     * 删除积分商城轮播图
     */
    @RequiresPermissions("system:shop:remove")
    @Log(title = "积分商城轮播图", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(bannerShopService.deleteBannerShopByIds(ids));
    }
}
