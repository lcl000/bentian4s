package com.ruoyi.project.system.banner.info.service;

import java.util.List;
import com.ruoyi.project.system.banner.info.domain.BannerMemberInfo;

/**
 * 用户中心轮播图Service接口
 * 
 * @author LCL
 * @date 2020-07-29
 */
public interface IBannerMemberInfoService 
{
    /**
     * 查询用户中心轮播图
     * 
     * @param id 用户中心轮播图ID
     * @return 用户中心轮播图
     */
    public BannerMemberInfo selectBannerMemberInfoById(String id);

    /**
     * 查询用户中心轮播图列表
     * 
     * @param bannerMemberInfo 用户中心轮播图
     * @return 用户中心轮播图集合
     */
    public List<BannerMemberInfo> selectBannerMemberInfoList(BannerMemberInfo bannerMemberInfo);

    /**
     * 新增用户中心轮播图
     * 
     * @param bannerMemberInfo 用户中心轮播图
     * @return 结果
     */
    public int insertBannerMemberInfo(BannerMemberInfo bannerMemberInfo);

    /**
     * 修改用户中心轮播图
     * 
     * @param bannerMemberInfo 用户中心轮播图
     * @return 结果
     */
    public int updateBannerMemberInfo(BannerMemberInfo bannerMemberInfo);

    /**
     * 批量删除用户中心轮播图
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBannerMemberInfoByIds(String ids);

    /**
     * 删除用户中心轮播图信息
     * 
     * @param id 用户中心轮播图ID
     * @return 结果
     */
    public int deleteBannerMemberInfoById(String id);

	List<BannerMemberInfo> findBannerList();
}
