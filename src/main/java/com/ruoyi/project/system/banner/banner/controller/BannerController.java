package com.ruoyi.project.system.banner.banner.controller;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.activity.service.IActivityService;
import com.ruoyi.project.system.banner.banner.domain.Banner;
import com.ruoyi.project.system.banner.banner.dto.BannerDto;
import com.ruoyi.project.system.banner.banner.service.IBannerService;
import com.ruoyi.project.system.car.car.domain.Car;
import com.ruoyi.project.system.car.car.service.ICarService;
import com.ruoyi.project.system.goods.goods.domain.GoodsGoods;
import com.ruoyi.project.system.goods.goods.enums.GoodsStatus;
import com.ruoyi.project.system.goods.goods.service.IGoodsGoodsService;
import com.ruoyi.project.system.used.used.domain.CarUsed;
import com.ruoyi.project.system.used.used.service.ICarUsedService;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 轮播图Controller
 * 
 * @author LCL
 * @date 2020-06-05
 */
@Controller
@RequestMapping("/system/banner")
public class BannerController extends BaseController
{
    private String prefix = "system/banner/banner";

    @Autowired
    private IBannerService bannerService;
    @Autowired
    private ICarService carService;
    @Autowired
    private IActivityService activityService;
    @Autowired
    private ICarUsedService carUsedService;


    @Autowired
    private IGoodsGoodsService goodsGoodsService;

    @RequiresPermissions("system:banner:view")
    @GetMapping()
    public String banner()
    {
        return prefix + "/banner";
    }

    /**
     * 查询轮播图列表
     */
    @RequiresPermissions("system:banner:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Banner banner)
    {
        startPage();
        List<Banner> list = bannerService.selectBannerList(banner);
        return getDataTable(list);
    }

    /**
     * 导出轮播图列表
     */
    @RequiresPermissions("system:banner:export")
    @Log(title = "轮播图", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Banner banner)
    {
        List<Banner> list = bannerService.selectBannerList(banner);
        ExcelUtil<Banner> util = new ExcelUtil<Banner>(Banner.class);
        return util.exportExcel(list, "banner");
    }

    /**
     * 新增轮播图
     */
    @GetMapping("/add")
    public String add(ModelMap map)
    {
        Car car = new Car();
        car.setStatus(0);
        map.put("carList",this.carService.selectCarList(car));
        map.put("activityList",this.activityService.findList());
        GoodsGoods goodsGoods = new GoodsGoods();
        goodsGoods.setStatus(0);
        map.put("goodsList",this.goodsGoodsService.selectGoodsGoodsList(goodsGoods));
        CarUsed carUsed = new CarUsed();
        carUsed.setStatus(0);
        map.put("usedList",this.carUsedService.selectCarUsedList(carUsed));
        return prefix + "/add";
    }

    /**
     * 新增保存轮播图
     */
    @RequiresPermissions("system:banner:add")
    @Log(title = "轮播图", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    @Transactional
    public AjaxResult addSave(Banner banner)
    {
        if(banner.getType()!=1){
            String[] res = banner.getResId().split(",");
            if(banner.getType()==0){
                banner.setResId(res[0]);
            }else if(banner.getType()==2){
                banner.setResId(res[1]);
            }else if(banner.getType()==3){
                banner.setResId(res[2]);
            }else if(banner.getType()==4){
                banner.setResId(res[3]);
            }
        }else{
            banner.setResId(null);
        }
        banner.setCreated(DateUtils.getNowDate());
        bannerService.insertBanner(banner);
        return AjaxResult.success();
    }

    /**
     * 修改轮播图
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        BannerDto banner = bannerService.selectBannerById(id);
        mmap.put("banner", banner);
        Car car = new Car();
        car.setStatus(0);
        mmap.put("carList",this.carService.selectCarList(car));
        mmap.put("activityList",this.activityService.findList());
        GoodsGoods goodsGoods = new GoodsGoods();
        goodsGoods.setStatus(0);
        mmap.put("goodsList",this.goodsGoodsService.selectGoodsGoodsList(goodsGoods));
        CarUsed carUsed = new CarUsed();
        carUsed.setStatus(0);
        mmap.put("usedList",this.carUsedService.selectCarUsedList(carUsed));
        return prefix + "/edit";
    }

    /**
     * 修改保存轮播图
     */
    @RequiresPermissions("system:banner:edit")
    @Log(title = "轮播图", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    @Transactional
    public AjaxResult editSave(Banner banner)
    {
        //查询数据库获取旧数据
        BannerDto oldBanner = bannerService.selectBannerById(banner.getId());
        //判断数据是否存在
        if(oldBanner==null){
            return AjaxResult.error("数据不存在");
        }
        if(banner.getType()!=1){
            String[] res = banner.getResId().split(",");
            if(banner.getType()==0){
                banner.setResId(res[0]);
            }else if(banner.getType()==2){
                banner.setResId(res[1]);
            }else if(banner.getType()==3){
                banner.setResId(res[2]);
            }else if(banner.getType()==4){
                banner.setResId(res[3]);
            }
        }else{
            banner.setResId(null);
        }
        //更新数据
        bannerService.updateBanner(banner);


        return AjaxResult.success();
    }

    /**
     * 删除轮播图
     */
    @RequiresPermissions("system:banner:remove")
    @Log(title = "轮播图", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    @Transactional
    public AjaxResult remove(String ids)
    {
        //删除数据
        bannerService.deleteBannerByIds(ids);
        return AjaxResult.success();
    }
}
