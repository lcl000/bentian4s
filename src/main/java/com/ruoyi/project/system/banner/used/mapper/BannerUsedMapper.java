package com.ruoyi.project.system.banner.used.mapper;

import java.util.List;
import com.ruoyi.project.system.banner.used.domain.BannerUsed;

/**
 * 二手车轮播图Mapper接口
 * 
 * @author LCL
 * @date 2020-07-29
 */
public interface BannerUsedMapper 
{
    /**
     * 查询二手车轮播图
     * 
     * @param id 二手车轮播图ID
     * @return 二手车轮播图
     */
    public BannerUsed selectBannerUsedById(String id);

    /**
     * 查询二手车轮播图列表
     * 
     * @param bannerUsed 二手车轮播图
     * @return 二手车轮播图集合
     */
    public List<BannerUsed> selectBannerUsedList(BannerUsed bannerUsed);

    /**
     * 新增二手车轮播图
     * 
     * @param bannerUsed 二手车轮播图
     * @return 结果
     */
    public int insertBannerUsed(BannerUsed bannerUsed);

    /**
     * 修改二手车轮播图
     * 
     * @param bannerUsed 二手车轮播图
     * @return 结果
     */
    public int updateBannerUsed(BannerUsed bannerUsed);

    /**
     * 删除二手车轮播图
     * 
     * @param id 二手车轮播图ID
     * @return 结果
     */
    public int deleteBannerUsedById(String id);

    /**
     * 批量删除二手车轮播图
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBannerUsedByIds(String[] ids);

	List<BannerUsed> findBannerList();
}
