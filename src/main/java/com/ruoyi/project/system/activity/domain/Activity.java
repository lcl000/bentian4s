package com.ruoyi.project.system.activity.domain;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 活动对象 activity
 * 
 * @author LCL
 * @date 2020-07-29
 */
public class Activity extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 活动标题 */
    @Excel(name = "活动标题")
    private String name;

    /** 开始时间 */
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 结束时间 */
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 封面 */
    @Excel(name = "封面")
    private String mainUrl;

    /** 活动对象 */
    @Excel(name = "活动对象")
    private String object;

    /** 详情 */
    private String details;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setMainUrl(String mainUrl) 
    {
        this.mainUrl = mainUrl;
    }

    public String getMainUrl() 
    {
        return mainUrl;
    }
    public void setObject(String object) 
    {
        this.object = object;
    }

    public String getObject() 
    {
        return object;
    }
    public void setDetails(String details) 
    {
        this.details = details;
    }

    public String getDetails() 
    {
        return details;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("mainUrl", getMainUrl())
            .append("object", getObject())
            .append("details", getDetails())
            .toString();
    }
}
