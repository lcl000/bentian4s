package com.ruoyi.project.system.activity.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.activity.mapper.ActivityMapper;
import com.ruoyi.project.system.activity.domain.Activity;
import com.ruoyi.project.system.activity.service.IActivityService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 活动Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-29
 */
@Service
public class ActivityServiceImpl implements IActivityService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private ActivityMapper activityMapper;

    /**
     * 查询活动
     * 
     * @param id 活动ID
     * @return 活动
     */
    @Override
    public Activity selectActivityById(String id)
    {
        return activityMapper.selectActivityById(id);
    }

    /**
     * 查询活动列表
     * 
     * @param activity 活动
     * @return 活动
     */
    @Override
    public List<Activity> selectActivityList(Activity activity)
    {
        return activityMapper.selectActivityList(activity);
    }

    /**
     * 新增活动
     * 
     * @param activity 活动
     * @return 结果
     */
    @Override
    public int insertActivity(Activity activity)
    {
        activity.setId(this.idGererateFactory.nextStringId());
        return activityMapper.insertActivity(activity);
    }

    /**
     * 修改活动
     * 
     * @param activity 活动
     * @return 结果
     */
    @Override
    public int updateActivity(Activity activity)
    {
        return activityMapper.updateActivity(activity);
    }

    /**
     * 删除活动对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteActivityByIds(String ids)
    {
        return activityMapper.deleteActivityByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除活动信息
     * 
     * @param id 活动ID
     * @return 结果
     */
    @Override
    public int deleteActivityById(String id)
    {
        return activityMapper.deleteActivityById(id);
    }

    @Override
    public List<Activity> findList() {
        return this.activityMapper.findList();
    }
}
