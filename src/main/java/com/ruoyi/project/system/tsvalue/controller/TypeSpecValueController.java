package com.ruoyi.project.system.tsvalue.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.tsvalue.domain.TypeSpecValue;
import com.ruoyi.project.system.tsvalue.service.ITypeSpecValueService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 类别规格值Controller
 * 
 * @author LCL
 * @date 2020-06-10
 */
@Controller
@RequestMapping("/system/type/value")
public class TypeSpecValueController extends BaseController
{
    private String prefix = "system/value";

    @Autowired
    private ITypeSpecValueService typeSpecValueService;

    @RequiresPermissions("system:value:view")
    @GetMapping()
    public String value()
    {
        return prefix + "/value";
    }

    /**
     * 查询类别规格值列表
     */
    @RequiresPermissions("system:value:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TypeSpecValue typeSpecValue)
    {
        startPage();
        List<TypeSpecValue> list = typeSpecValueService.selectTypeSpecValueList(typeSpecValue);
        return getDataTable(list);
    }

    /**
     * 导出类别规格值列表
     */
    @RequiresPermissions("system:value:export")
    @Log(title = "类别规格值", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TypeSpecValue typeSpecValue)
    {
        List<TypeSpecValue> list = typeSpecValueService.selectTypeSpecValueList(typeSpecValue);
        ExcelUtil<TypeSpecValue> util = new ExcelUtil<TypeSpecValue>(TypeSpecValue.class);
        return util.exportExcel(list, "value");
    }

    /**
     * 新增类别规格值
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存类别规格值
     */
    @RequiresPermissions("system:value:add")
    @Log(title = "类别规格值", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TypeSpecValue typeSpecValue)
    {
        return toAjax(typeSpecValueService.insertTypeSpecValue(typeSpecValue));
    }

    /**
     * 修改类别规格值
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        TypeSpecValue typeSpecValue = typeSpecValueService.selectTypeSpecValueById(id);
        mmap.put("typeSpecValue", typeSpecValue);
        return prefix + "/edit";
    }

    /**
     * 修改保存类别规格值
     */
    @RequiresPermissions("system:value:edit")
    @Log(title = "类别规格值", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TypeSpecValue typeSpecValue)
    {
        return toAjax(typeSpecValueService.updateTypeSpecValue(typeSpecValue));
    }

    /**
     * 删除类别规格值
     */
    @RequiresPermissions("system:value:remove")
    @Log(title = "类别规格值", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(typeSpecValueService.deleteTypeSpecValueByIds(ids));
    }
}
