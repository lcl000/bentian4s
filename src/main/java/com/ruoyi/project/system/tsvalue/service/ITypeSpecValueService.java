package com.ruoyi.project.system.tsvalue.service;

import java.util.List;

import com.ruoyi.project.system.tspec.dto.TypeSpecValueDto;
import com.ruoyi.project.system.tsvalue.domain.TypeSpecValue;

/**
 * 类别规格值Service接口
 * 
 * @author LCL
 * @date 2020-06-10
 */
public interface ITypeSpecValueService 
{
    /**
     * 查询类别规格值
     * 
     * @param id 类别规格值ID
     * @return 类别规格值
     */
    public TypeSpecValue selectTypeSpecValueById(String id);

    /**
     * 查询类别规格值列表
     * 
     * @param typeSpecValue 类别规格值
     * @return 类别规格值集合
     */
    public List<TypeSpecValue> selectTypeSpecValueList(TypeSpecValue typeSpecValue);

    /**
     * 新增类别规格值
     * 
     * @param typeSpecValue 类别规格值
     * @return 结果
     */
    public int insertTypeSpecValue(TypeSpecValue typeSpecValue);

    /**
     * 修改类别规格值
     * 
     * @param typeSpecValue 类别规格值
     * @return 结果
     */
    public int updateTypeSpecValue(TypeSpecValue typeSpecValue);

    /**
     * 批量删除类别规格值
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTypeSpecValueByIds(String ids);

    /**
     * 删除类别规格值信息
     * 
     * @param id 类别规格值ID
     * @return 结果
     */
    public int deleteTypeSpecValueById(String id);

	List<TypeSpecValueDto> findListByTypeSpecId(String typeSpecId);

	int deleteByTypeSpecId(String typeSpecId);
}
