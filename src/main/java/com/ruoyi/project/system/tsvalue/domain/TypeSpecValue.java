package com.ruoyi.project.system.tsvalue.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 类别规格值对象 type_spec_value
 * 
 * @author LCL
 * @date 2020-06-10
 */
public class TypeSpecValue extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  主键ID */
    private String id;

    /** 类别规格ID */
    @Excel(name = "类别规格ID")
    private String typeSpecId;

    /** 规格值主键ID */
    @Excel(name = "规格值主键ID")
    private String specValueId;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setTypeSpecId(String typeSpecId) 
    {
        this.typeSpecId = typeSpecId;
    }

    public String getTypeSpecId() 
    {
        return typeSpecId;
    }
    public void setSpecValueId(String specValueId) 
    {
        this.specValueId = specValueId;
    }

    public String getSpecValueId() 
    {
        return specValueId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("typeSpecId", getTypeSpecId())
            .append("specValueId", getSpecValueId())
            .toString();
    }
}
