package com.ruoyi.project.system.tsvalue.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.tspec.dto.TypeSpecValueDto;
import com.ruoyi.project.system.tsvalue.mapper.TypeSpecValueMapper;
import com.ruoyi.project.system.tsvalue.domain.TypeSpecValue;
import com.ruoyi.project.system.tsvalue.service.ITypeSpecValueService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 类别规格值Service业务层处理
 * 
 * @author LCL
 * @date 2020-06-10
 */
@Service
public class TypeSpecValueServiceImpl implements ITypeSpecValueService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private TypeSpecValueMapper typeSpecValueMapper;

    /**
     * 查询类别规格值
     * 
     * @param id 类别规格值ID
     * @return 类别规格值
     */
    @Override
    public TypeSpecValue selectTypeSpecValueById(String id)
    {
        return typeSpecValueMapper.selectTypeSpecValueById(id);
    }

    /**
     * 查询类别规格值列表
     * 
     * @param typeSpecValue 类别规格值
     * @return 类别规格值
     */
    @Override
    public List<TypeSpecValue> selectTypeSpecValueList(TypeSpecValue typeSpecValue)
    {
        return typeSpecValueMapper.selectTypeSpecValueList(typeSpecValue);
    }

    /**
     * 新增类别规格值
     * 
     * @param typeSpecValue 类别规格值
     * @return 结果
     */
    @Override
    public int insertTypeSpecValue(TypeSpecValue typeSpecValue)
    {
        typeSpecValue.setId(this.idGererateFactory.nextStringId());
        return typeSpecValueMapper.insertTypeSpecValue(typeSpecValue);
    }

    /**
     * 修改类别规格值
     * 
     * @param typeSpecValue 类别规格值
     * @return 结果
     */
    @Override
    public int updateTypeSpecValue(TypeSpecValue typeSpecValue)
    {
        return typeSpecValueMapper.updateTypeSpecValue(typeSpecValue);
    }

    /**
     * 删除类别规格值对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteTypeSpecValueByIds(String ids)
    {
        return typeSpecValueMapper.deleteTypeSpecValueByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除类别规格值信息
     * 
     * @param id 类别规格值ID
     * @return 结果
     */
    @Override
    public int deleteTypeSpecValueById(String id)
    {
        return typeSpecValueMapper.deleteTypeSpecValueById(id);
    }

    @Override
    public List<TypeSpecValueDto> findListByTypeSpecId(String typeSpecId) {
        return this.typeSpecValueMapper.findListByTypeSpecId(typeSpecId);
    }

    @Override
    public int deleteByTypeSpecId(String typeSpecId) {
        return this.typeSpecValueMapper.deleteByTypeSpecId(typeSpecId);
    }
}
