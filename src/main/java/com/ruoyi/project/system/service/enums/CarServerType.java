package com.ruoyi.project.system.service.enums;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/9/29 17:58
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
public enum CarServerType {

	TYPE_0(0,"保养预约"),
	TYPE_1(1,"配件预约"),
	TYPE_2(2,"维修预约"),
	TYPE_3(3,"代步车预约"),
	TYPE_4(4,"上门取送车预约"),
	TYPE_5(5,"新车上牌"),
	TYPE_6(6,"二手车过户"),
	TYPE_7(7,"提档"),
	TYPE_8(8,"转入"),
	TYPE_9(9,"补牌/证"),
	TYPE_10(10,"抵/解押"),
	TYPE_11(11,"六年免检"),
	TYPE_12(12,"短租车"),
	TYPE_13(13,"贴膜"),
	TYPE_14(14,"美容");

	private int value;
	private String name;

	CarServerType(int value, String name) {
		this.value = value;
		this.name = name;
	}

	/**
	 * 数据范围校验
	 *
	 * @param value
	 * @return
	 */
	public static boolean contains(int value) {
		return null == getByValue(value)?Boolean.FALSE.booleanValue():Boolean.TRUE.booleanValue();
	}
	public int getValue() {
		return this.value;
	}

	public String getName(){
		return this.name;
	}

	public static CarServerType getByValue(int value){
		CarServerType allVaues[] = CarServerType.values();
		CarServerType status;
		for (int i=0,len = allVaues.length;i<len;i++){
			status = allVaues[i];
			if(status.getValue()==value){
				return status;
			}
		}
		return null;
	}


}
