package com.ruoyi.project.system.service.domain;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import com.ruoyi.project.system.membercar.domain.MemberCar;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 汽车服务对象 car_service
 * 
 * @author LCL
 * @date 2020-08-06
 */
public class CarService extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    private MemberCar memberCar;

    public MemberCar getMemberCar() {
        return memberCar;
    }

    public void setMemberCar(MemberCar memberCar) {
        this.memberCar = memberCar;
    }

    /** 主键ID */
    private String id;

    /** 服务类型: 0 保养预约 1 配件预约 2 维修预约 3 代步车预约 4 上门取送车预约 5 新车上牌 6 二手车过户 7 提档 8 转入 9 补牌/证 10 抵/解押 11 六年免检 12 短租车 13 贴膜 14 美容 */
    @Excel(name = "服务类型: 0 保养预约 1 配件预约 2 维修预约 3 代步车预约 4 上门取送车预约 5 新车上牌 6 二手车过户 7 提档 8 转入 9 补牌/证 10 抵/解押 11 六年免检 12 短租车 13 贴膜 14 美容")
    private Integer type;

    /** 门店主键ID */
    @Excel(name = "门店主键ID")
    private String dealerId;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 电话 */
    @Excel(name = "电话")
    private String mobile;

    /** 购车时间 */
    @Excel(name = "购车时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date payTime;

    /** 预约时间 */
    @Excel(name = "预约时间")
    private String applyTime;

    /** 状态: 0 未处理 1 已处理 2 作废 */
    @Excel(name = "状态: 0 未处理 1 已处理 2 作废")
    private Integer status;

    /** 用户主键ID **/
    private String memberId;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setDealerId(String dealerId) 
    {
        this.dealerId = dealerId;
    }

    public String getDealerId() 
    {
        return dealerId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setMobile(String mobile) 
    {
        this.mobile = mobile;
    }

    public String getMobile() 
    {
        return mobile;
    }
    public void setPayTime(Date payTime) 
    {
        this.payTime = payTime;
    }

    public Date getPayTime() 
    {
        return payTime;
    }
    public void setApplyTime(String applyTime) 
    {
        this.applyTime = applyTime;
    }

    public String getApplyTime() 
    {
        return applyTime;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("type", getType())
            .append("dealerId", getDealerId())
            .append("name", getName())
            .append("mobile", getMobile())
            .append("payTime", getPayTime())
            .append("applyTime", getApplyTime())
            .append("status", getStatus())
            .toString();
    }
}
