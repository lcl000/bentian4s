package com.ruoyi.project.system.service.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.service.dto.CarServiceDto;
import com.ruoyi.project.system.service.mapper.CarServiceMapper;
import com.ruoyi.project.system.service.domain.CarService;
import com.ruoyi.project.system.service.service.ICarServiceService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 汽车服务Service业务层处理
 * 
 * @author LCL
 * @date 2020-08-06
 */
@Service
public class CarServiceServiceImpl implements ICarServiceService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private CarServiceMapper carServiceMapper;

    /**
     * 查询汽车服务
     * 
     * @param id 汽车服务ID
     * @return 汽车服务
     */
    @Override
    public CarService selectCarServiceById(String id)
    {
        return carServiceMapper.selectCarServiceById(id);
    }

    /**
     * 查询汽车服务列表
     * 
     * @param carService 汽车服务
     * @return 汽车服务
     */
    @Override
    public List<CarServiceDto> selectCarServiceList(CarService carService)
    {
        return carServiceMapper.selectCarServiceList(carService);
    }

    /**
     * 新增汽车服务
     * 
     * @param carService 汽车服务
     * @return 结果
     */
    @Override
    public int insertCarService(CarService carService)
    {
        carService.setId(this.idGererateFactory.nextStringId());
        return carServiceMapper.insertCarService(carService);
    }

    /**
     * 修改汽车服务
     * 
     * @param carService 汽车服务
     * @return 结果
     */
    @Override
    public int updateCarService(CarService carService)
    {
        return carServiceMapper.updateCarService(carService);
    }

    /**
     * 删除汽车服务对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCarServiceByIds(String ids)
    {
        return carServiceMapper.deleteCarServiceByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除汽车服务信息
     * 
     * @param id 汽车服务ID
     * @return 结果
     */
    @Override
    public int deleteCarServiceById(String id)
    {
        return carServiceMapper.deleteCarServiceById(id);
    }

    @Override
    public List<CarService> selectNum(CarService select) {
        return this.carServiceMapper.selectNum(select);
    }
}
