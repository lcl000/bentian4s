package com.ruoyi.project.system.service.mapper;

import java.util.List;
import com.ruoyi.project.system.service.domain.CarService;
import com.ruoyi.project.system.service.dto.CarServiceDto;

/**
 * 汽车服务Mapper接口
 * 
 * @author LCL
 * @date 2020-08-06
 */
public interface CarServiceMapper 
{
    /**
     * 查询汽车服务
     * 
     * @param id 汽车服务ID
     * @return 汽车服务
     */
    public CarService selectCarServiceById(String id);

    /**
     * 查询汽车服务列表
     * 
     * @param carService 汽车服务
     * @return 汽车服务集合
     */
    public List<CarServiceDto> selectCarServiceList(CarService carService);

    /**
     * 新增汽车服务
     * 
     * @param carService 汽车服务
     * @return 结果
     */
    public int insertCarService(CarService carService);

    /**
     * 修改汽车服务
     * 
     * @param carService 汽车服务
     * @return 结果
     */
    public int updateCarService(CarService carService);

    /**
     * 删除汽车服务
     * 
     * @param id 汽车服务ID
     * @return 结果
     */
    public int deleteCarServiceById(String id);

    /**
     * 批量删除汽车服务
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCarServiceByIds(String[] ids);

	List<CarService> selectNum(CarService select);
}
