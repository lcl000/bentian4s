package com.ruoyi.project.system.service.controller;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.dealer.service.IDealerService;
import com.ruoyi.project.system.membercar.service.IMemberCarService;
import com.ruoyi.project.system.service.domain.CarService;
import com.ruoyi.project.system.service.dto.CarServiceDto;
import com.ruoyi.project.system.service.service.ICarServiceService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 汽车服务Controller
 * 
 * @author LCL
 * @date 2020-08-06
 */
@Controller
@RequestMapping("/system/service")
public class CarServiceController extends BaseController
{
    private String prefix = "system/service";

    @Autowired
    private IMemberCarService memberCarService;
    @Autowired
    private ICarServiceService carServiceService;
    @Autowired
    private IDealerService dealerService;

    @RequiresPermissions("system:service:view")
    @GetMapping()
    public String service(ModelMap map)
    {
        map.put("dealerList",this.dealerService.findAll(null));
        return prefix + "/service";
    }

    /**
     * 查询汽车服务列表
     */
    @RequiresPermissions("system:service:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CarService carService)
    {
        startPage();
        List<CarServiceDto> list = carServiceService.selectCarServiceList(carService);
        for(CarServiceDto mem :list){
            mem.setMemberCar(this.memberCarService.findByUid(mem.getMemberId()));
        }
        return getDataTable(list);
    }

    /**
     * 导出汽车服务列表
     */
    @RequiresPermissions("system:service:export")
    @Log(title = "汽车服务", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CarService carService)
    {
        List<CarServiceDto> list = carServiceService.selectCarServiceList(carService);
        ExcelUtil<CarServiceDto> util = new ExcelUtil<CarServiceDto>(CarServiceDto.class);
        return util.exportExcel(list, "service");
    }

    /**
     * 新增汽车服务
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存汽车服务
     */
    @RequiresPermissions("system:service:add")
    @Log(title = "汽车服务", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CarService carService)
    {
        return toAjax(carServiceService.insertCarService(carService));
    }

    /**
     * 修改汽车服务
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        CarService carService = carServiceService.selectCarServiceById(id);
        mmap.put("carService", carService);
        return prefix + "/edit";
    }

    /**
     * 修改保存汽车服务
     */
    @RequiresPermissions("system:service:edit")
    @Log(title = "汽车服务", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CarService carService)
    {
        return toAjax(carServiceService.updateCarService(carService));
    }

    /**
     * 删除汽车服务
     */
    @RequiresPermissions("system:service:remove")
    @Log(title = "汽车服务", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(carServiceService.deleteCarServiceByIds(ids));
    }


    @PostMapping("/agree/{id}")
    @ResponseBody
    public AjaxResult agree(@PathVariable String id){
        CarService carService = this.carServiceService.selectCarServiceById(id);
        if(carService==null){
            return AjaxResult.error();
        }
        CarService update = new CarService();
        update.setId(id);
        update.setStatus(1);
        this.carServiceService.updateCarService(update);

        return AjaxResult.success();
    }

    @PostMapping("/refuse/{id}")
    @ResponseBody
    public AjaxResult refuse(@PathVariable String id){
        CarService carService = this.carServiceService.selectCarServiceById(id);
        if(carService==null){
            return AjaxResult.error();
        }
        CarService update = new CarService();
        update.setId(id);
        update.setStatus(2);
        this.carServiceService.updateCarService(update);

        return AjaxResult.success();
    }


}
