package com.ruoyi.project.system.service.dto;

import com.ruoyi.project.system.service.domain.CarService;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/8/6 10:35
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
public class CarServiceDto extends CarService {
	private String dealerName;

	//车架号
	private String frameNo;
	//车型
	private String carTypeName;

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public String getFrameNo() {
		return frameNo;
	}

	public void setFrameNo(String frameNo) {
		this.frameNo = frameNo;
	}

	public String getCarTypeName() {
		return carTypeName;
	}

	public void setCarTypeName(String carTypeName) {
		this.carTypeName = carTypeName;
	}
}
