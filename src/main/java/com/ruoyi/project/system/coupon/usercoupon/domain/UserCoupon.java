package com.ruoyi.project.system.coupon.usercoupon.domain;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import com.ruoyi.project.system.membercar.domain.MemberCar;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 用户优惠券对象 user_coupon
 * 
 * @author LCL
 * @date 2020-08-11
 */
public class UserCoupon extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    private MemberCar memberCar;

    public MemberCar getMemberCar() {
        return memberCar;
    }

    public void setMemberCar(MemberCar memberCar) {
        this.memberCar = memberCar;
    }

    /** 主键ID */
    private String id;
    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 用户主键ID */
    private String memberId;

    /** 0 已使用 1 未使用 */
    @Excel(name = "0 未使用 1 已使用",sort = 7)
    private Integer status;

    /** 0 优惠券 1 抵用券 */
    @Excel(name = "0 优惠券 1 抵用券",sort = 8)
    private Integer type;

    /** 使用条件 */
    @Excel(name = "使用条件",sort = 9)
    private String useCondition;

    /** 免减金额 */
    @Excel(name = "免减金额",sort = 10)
    private BigDecimal price;

    /** 开始时间 */
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd",sort = 11)
    private Date startTime;

    /** 结束时间 */
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd",sort = 12)
    private Date endTime;

    /** 补充说明 */
    @Excel(name = "补充说明",sort = 13)
    private String notes;

    /** 排序 */
    private Integer sort;

    /** 优惠券主键ID */
    private String couponId;

    // 是否查询过期优惠券 0 查询未过期 1 查询过期 null 不适用该条件
    private Integer overdue;

    /** 使用时间 **/
    @Excel(name = "使用时间", width = 30, dateFormat = "yyyy-MM-dd",sort = 14)
    private Date usedTime;

    /** 核销人ID **/
    private String usedMemberId;

    /** 发券部门 **/
    @Excel(name = "发放部门",sort = 5)
    private String sendPost;

    //核销人姓名
    @Excel(name = "核销人姓名",sort = 15)
    private String usedMemberName;

    /** 创建时间 **/
    @Excel(name = "发放时间",sort = 6)
    private Date created;
    //查询用 开始时间
    private String btime;
    //查询用 结束时间
    private String etime;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setUseCondition(String useCondition)
    {
        this.useCondition = useCondition;
    }

    public String getUseCondition()
    {
        return useCondition;
    }
    public void setPrice(BigDecimal price)
    {
        this.price = price;
    }

    public BigDecimal getPrice()
    {
        return price;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setNotes(String notes) 
    {
        this.notes = notes;
    }

    public String getNotes() 
    {
        return notes;
    }
    public void setSort(Integer sort)
    {
        this.sort = sort;
    }

    public Integer getSort()
    {
        return sort;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public Integer getOverdue() {
        return overdue;
    }

    public void setOverdue(Integer overdue) {
        this.overdue = overdue;
    }

    public Date getUsedTime() {
        return usedTime;
    }

    public void setUsedTime(Date usedTime) {
        this.usedTime = usedTime;
    }

    public String getUsedMemberId() {
        return usedMemberId;
    }

    public void setUsedMemberId(String usedMemberId) {
        this.usedMemberId = usedMemberId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSendPost() {
        return sendPost;
    }

    public void setSendPost(String sendPost) {
        this.sendPost = sendPost;
    }

    public String getUsedMemberName() {
        return usedMemberName;
    }

    public void setUsedMemberName(String usedMemberName) {
        this.usedMemberName = usedMemberName;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getBtime() {
        return btime;
    }

    public void setBtime(String btime) {
        this.btime = btime;
    }

    public String getEtime() {
        return etime;
    }

    public void setEtime(String etime) {
        this.etime = etime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("memberId", getMemberId())
            .append("status", getStatus())
            .append("type", getType())
            .append("useCondition", getUseCondition())
            .append("price", getPrice())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("notes", getNotes())
            .append("sort", getSort())
            .toString();
    }
}
