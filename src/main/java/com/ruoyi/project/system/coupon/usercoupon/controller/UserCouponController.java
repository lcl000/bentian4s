package com.ruoyi.project.system.coupon.usercoupon.controller;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.coupon.usercoupon.domain.UserCoupon;
import com.ruoyi.project.system.coupon.usercoupon.dto.UserCouponDto;
import com.ruoyi.project.system.coupon.usercoupon.service.IUserCouponService;
import com.ruoyi.project.system.member.service.IMemberService;
import com.ruoyi.project.system.membercar.service.IMemberCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户优惠券Controller
 * 
 * @author LCL
 * @date 2020-08-11
 */
@Controller
@RequestMapping("/system/coupon/usercoupon")
public class UserCouponController extends BaseController
{
    private String prefix = "system/coupon/usercoupon";
    @Autowired
    private IMemberCarService memberCarService;
    @Autowired
    private IUserCouponService userCouponService;

    @Autowired
    private IMemberService memberService;

    @GetMapping()
    public String usercoupon(ModelMap map)
    {
//        map.put("memberList",this.memberService.findAll());
        return prefix + "/usercoupon";
    }

    /**
     * 查询用户优惠券列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(UserCoupon userCoupon)
    {
        startPage();
        List<UserCouponDto> list = userCouponService.selectUserCouponList(userCoupon);
        for(UserCouponDto mem :list){
            mem.setMemberCar(this.memberCarService.findByUid(mem.getMemberId()));
        }
        return getDataTable(list);
    }

    /**
     * 导出用户优惠券列表
     */
    @Log(title = "用户优惠券", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(UserCoupon userCoupon)
    {
        List<UserCouponDto> list = userCouponService.selectUserCouponList(userCoupon);
        ExcelUtil<UserCouponDto> util = new ExcelUtil<UserCouponDto>(UserCouponDto.class);
        return util.exportExcel(list, "usercoupon");
    }

    /**
     * 新增用户优惠券
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存用户优惠券
     */
    @Log(title = "用户优惠券", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(UserCoupon userCoupon)
    {
        userCoupon.setCreated(DateUtils.getNowDate());
        return toAjax(userCouponService.insertUserCoupon(userCoupon));
    }

    /**
     * 修改用户优惠券
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        UserCoupon userCoupon = userCouponService.selectUserCouponById(id);
        mmap.put("userCoupon", userCoupon);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户优惠券
     */
    @Log(title = "用户优惠券", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(UserCoupon userCoupon)
    {
        return toAjax(userCouponService.updateUserCoupon(userCoupon));
    }

    /**
     * 删除用户优惠券
     */
    @Log(title = "用户优惠券", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(userCouponService.deleteUserCouponByIds(ids));
    }
}
