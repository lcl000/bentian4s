package com.ruoyi.project.system.coupon.coupon.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.coupon.coupon.mapper.CouponMapper;
import com.ruoyi.project.system.coupon.coupon.domain.Coupon;
import com.ruoyi.project.system.coupon.coupon.service.ICouponService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 优惠券.抵用券Service业务层处理
 * 
 * @author LCL
 * @date 2020-08-11
 */
@Service
public class CouponServiceImpl implements ICouponService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private CouponMapper couponMapper;

    /**
     * 查询优惠券.抵用券
     * 
     * @param id 优惠券.抵用券ID
     * @return 优惠券.抵用券
     */
    @Override
    public Coupon selectCouponById(String id)
    {
        return couponMapper.selectCouponById(id);
    }

    /**
     * 查询优惠券.抵用券列表
     * 
     * @param coupon 优惠券.抵用券
     * @return 优惠券.抵用券
     */
    @Override
    public List<Coupon> selectCouponList(Coupon coupon)
    {
        return couponMapper.selectCouponList(coupon);
    }

    /**
     * 新增优惠券.抵用券
     * 
     * @param coupon 优惠券.抵用券
     * @return 结果
     */
    @Override
    public int insertCoupon(Coupon coupon)
    {
        coupon.setId(this.idGererateFactory.nextStringId());
        return couponMapper.insertCoupon(coupon);
    }

    /**
     * 修改优惠券.抵用券
     * 
     * @param coupon 优惠券.抵用券
     * @return 结果
     */
    @Override
    public int updateCoupon(Coupon coupon)
    {
        return couponMapper.updateCoupon(coupon);
    }

    /**
     * 删除优惠券.抵用券对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCouponByIds(String ids)
    {
        return couponMapper.deleteCouponByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除优惠券.抵用券信息
     * 
     * @param id 优惠券.抵用券ID
     * @return 结果
     */
    @Override
    public int deleteCouponById(String id)
    {
        return couponMapper.deleteCouponById(id);
    }
}
