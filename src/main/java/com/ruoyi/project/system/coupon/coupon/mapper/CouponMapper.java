package com.ruoyi.project.system.coupon.coupon.mapper;

import java.util.List;
import com.ruoyi.project.system.coupon.coupon.domain.Coupon;

/**
 * 优惠券.抵用券Mapper接口
 * 
 * @author LCL
 * @date 2020-08-11
 */
public interface CouponMapper 
{
    /**
     * 查询优惠券.抵用券
     * 
     * @param id 优惠券.抵用券ID
     * @return 优惠券.抵用券
     */
    public Coupon selectCouponById(String id);

    /**
     * 查询优惠券.抵用券列表
     * 
     * @param coupon 优惠券.抵用券
     * @return 优惠券.抵用券集合
     */
    public List<Coupon> selectCouponList(Coupon coupon);

    /**
     * 新增优惠券.抵用券
     * 
     * @param coupon 优惠券.抵用券
     * @return 结果
     */
    public int insertCoupon(Coupon coupon);

    /**
     * 修改优惠券.抵用券
     * 
     * @param coupon 优惠券.抵用券
     * @return 结果
     */
    public int updateCoupon(Coupon coupon);

    /**
     * 删除优惠券.抵用券
     * 
     * @param id 优惠券.抵用券ID
     * @return 结果
     */
    public int deleteCouponById(String id);

    /**
     * 批量删除优惠券.抵用券
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCouponByIds(String[] ids);
}
