package com.ruoyi.project.system.coupon.usercoupon.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.coupon.usercoupon.dto.UserCouponDto;
import com.ruoyi.project.system.coupon.usercoupon.mapper.UserCouponMapper;
import com.ruoyi.project.system.coupon.usercoupon.domain.UserCoupon;
import com.ruoyi.project.system.coupon.usercoupon.service.IUserCouponService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 用户优惠券Service业务层处理
 * 
 * @author LCL
 * @date 2020-08-11
 */
@Service
public class UserCouponServiceImpl implements IUserCouponService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private UserCouponMapper userCouponMapper;

    /**
     * 查询用户优惠券
     * 
     * @param id 用户优惠券ID
     * @return 用户优惠券
     */
    @Override
    public UserCoupon selectUserCouponById(String id)
    {
        return userCouponMapper.selectUserCouponById(id);
    }

    /**
     * 查询用户优惠券列表
     * 
     * @param userCoupon 用户优惠券
     * @return 用户优惠券
     */
    @Override
    public List<UserCouponDto> selectUserCouponList(UserCoupon userCoupon)
    {
        return userCouponMapper.selectUserCouponList(userCoupon);
    }

    /**
     * 新增用户优惠券
     * 
     * @param userCoupon 用户优惠券
     * @return 结果
     */
    @Override
    public int insertUserCoupon(UserCoupon userCoupon)
    {
        userCoupon.setId(this.idGererateFactory.nextStringId());
        return userCouponMapper.insertUserCoupon(userCoupon);
    }

    /**
     * 修改用户优惠券
     * 
     * @param userCoupon 用户优惠券
     * @return 结果
     */
    @Override
    public int updateUserCoupon(UserCoupon userCoupon)
    {
        return userCouponMapper.updateUserCoupon(userCoupon);
    }

    /**
     * 删除用户优惠券对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteUserCouponByIds(String ids)
    {
        return userCouponMapper.deleteUserCouponByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户优惠券信息
     * 
     * @param id 用户优惠券ID
     * @return 结果
     */
    @Override
    public int deleteUserCouponById(String id)
    {
        return userCouponMapper.deleteUserCouponById(id);
    }

    @Override
    public List<UserCouponDto> findListByUsedMemberIdAndType(String uid, Integer type) {
        return this.userCouponMapper.findListByUsedMemberIdAndType(uid,type);
    }
}
