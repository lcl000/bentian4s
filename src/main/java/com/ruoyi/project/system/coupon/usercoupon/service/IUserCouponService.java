package com.ruoyi.project.system.coupon.usercoupon.service;

import java.util.List;
import com.ruoyi.project.system.coupon.usercoupon.domain.UserCoupon;
import com.ruoyi.project.system.coupon.usercoupon.dto.UserCouponDto;

/**
 * 用户优惠券Service接口
 * 
 * @author LCL
 * @date 2020-08-11
 */
public interface IUserCouponService 
{
    /**
     * 查询用户优惠券
     * 
     * @param id 用户优惠券ID
     * @return 用户优惠券
     */
    public UserCoupon selectUserCouponById(String id);

    /**
     * 查询用户优惠券列表
     * 
     * @param userCoupon 用户优惠券
     * @return 用户优惠券集合
     */
    public List<UserCouponDto> selectUserCouponList(UserCoupon userCoupon);

    /**
     * 新增用户优惠券
     * 
     * @param userCoupon 用户优惠券
     * @return 结果
     */
    public int insertUserCoupon(UserCoupon userCoupon);

    /**
     * 修改用户优惠券
     * 
     * @param userCoupon 用户优惠券
     * @return 结果
     */
    public int updateUserCoupon(UserCoupon userCoupon);

    /**
     * 批量删除用户优惠券
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserCouponByIds(String ids);

    /**
     * 删除用户优惠券信息
     * 
     * @param id 用户优惠券ID
     * @return 结果
     */
    public int deleteUserCouponById(String id);

	List<UserCouponDto> findListByUsedMemberIdAndType(String uid, Integer type);
}
