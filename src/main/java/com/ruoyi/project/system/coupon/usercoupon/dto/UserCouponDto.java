package com.ruoyi.project.system.coupon.usercoupon.dto;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.project.system.coupon.usercoupon.domain.UserCoupon;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/8/13 15:02
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
public class UserCouponDto extends UserCoupon {

	// 用户昵称
	private String userName;
	// 用户手机号
	private String mobile;
	// 核销门店
	private String dealerName;
	// 绑车时用户姓名
	@Excel(name = "用户姓名",sort = 1)
	private String carName;
	// 绑车时用户手机号
	@Excel(name = "用户手机号",sort = 2)
	private String carMobile;
	// 车牌
	@Excel(name = "车牌",sort = 3)
	private String carLicensePlate;
	// 车架号
	@Excel(name = "车架号",sort = 4)
	private String carFrameNo;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public String getCarName() {
		return carName;
	}

	public void setCarName(String carName) {
		this.carName = carName;
	}

	public String getCarMobile() {
		return carMobile;
	}

	public void setCarMobile(String carMobile) {
		this.carMobile = carMobile;
	}

	public String getCarLicensePlate() {
		return carLicensePlate;
	}

	public void setCarLicensePlate(String carLicensePlate) {
		this.carLicensePlate = carLicensePlate;
	}

	public String getCarFrameNo() {
		return carFrameNo;
	}

	public void setCarFrameNo(String carFrameNo) {
		this.carFrameNo = carFrameNo;
	}
}
