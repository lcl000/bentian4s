package com.ruoyi.project.system.coupon.coupon.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.R;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.coupon.coupon.domain.Coupon;
import com.ruoyi.project.system.coupon.coupon.service.ICouponService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.coupon.usercoupon.domain.UserCoupon;
import com.ruoyi.project.system.coupon.usercoupon.service.IUserCouponService;

/**
 * 优惠券.抵用券Controller
 * 
 * @author LCL
 * @date 2020-08-11
 */
@Controller
@RequestMapping("/system/coupon/coupon")
public class CouponController extends BaseController
{
    private String prefix = "system/coupon/coupon";

    @Autowired
    private ICouponService couponService;

    @Autowired
    private IUserCouponService userCouponService;

    @RequiresPermissions("system/coupon:coupon:view")
    @GetMapping()
    public String coupon()
    {
        return prefix + "/coupon";
    }

    /**
     * 查询优惠券.抵用券列表
     */
    @RequiresPermissions("system/coupon:coupon:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Coupon coupon)
    {
        startPage();
        List<Coupon> list = couponService.selectCouponList(coupon);
        return getDataTable(list);
    }

    /**
     * 导出优惠券.抵用券列表
     */
    @RequiresPermissions("system/coupon:coupon:export")
    @Log(title = "优惠券.抵用券", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Coupon coupon)
    {
        List<Coupon> list = couponService.selectCouponList(coupon);
        ExcelUtil<Coupon> util = new ExcelUtil<Coupon>(Coupon.class);
        return util.exportExcel(list, "coupon");
    }

    /**
     * 新增优惠券.抵用券
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存优惠券.抵用券
     */
    @RequiresPermissions("system/coupon:coupon:add")
    @Log(title = "优惠券.抵用券", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Coupon coupon)
    {
        return toAjax(couponService.insertCoupon(coupon));
    }

    /**
     * 修改优惠券.抵用券
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        Coupon coupon = couponService.selectCouponById(id);
        mmap.put("coupon", coupon);
        return prefix + "/edit";
    }

    /**
     * 修改保存优惠券.抵用券
     */
    @RequiresPermissions("system/coupon:coupon:edit")
    @Log(title = "优惠券.抵用券", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Coupon coupon)
    {
        return toAjax(couponService.updateCoupon(coupon));
    }

    /**
     * 删除优惠券.抵用券
     */
    @RequiresPermissions("system/coupon:coupon:remove")
    @Log(title = "优惠券.抵用券", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(couponService.deleteCouponByIds(ids));
    }


    /**
     * 发送优惠券
     * @param id
     * @param uid
     * @return
     */
    @PostMapping("/send/{id}")
    @ResponseBody
    public AjaxResult send(@PathVariable String id, @RequestParam String uid,@RequestParam String sendPost){
        Coupon coupon = this.couponService.selectCouponById(id);
        if(coupon==null){
            return AjaxResult.error("优惠券不存在");
        }
        UserCoupon userCoupon = new UserCoupon();
        userCoupon.setCouponId(id);
        userCoupon.setEndTime(coupon.getEndTime());
        userCoupon.setMemberId(uid);
        userCoupon.setNotes(coupon.getNotes());
        userCoupon.setPrice(coupon.getPrice());
        userCoupon.setStartTime(coupon.getStartTime());
        userCoupon.setStatus(0);
        userCoupon.setType(coupon.getType());
        userCoupon.setUseCondition(coupon.getUseCondition());
        userCoupon.setName(coupon.getName());
        userCoupon.setSendPost(sendPost);
        userCoupon.setCreated(DateUtils.getNowDate());
        this.userCouponService.insertUserCoupon(userCoupon);
        return AjaxResult.success("添加成功");
    }
}
