package com.ruoyi.project.system.coupon.coupon.domain;

import java.math.BigDecimal;
import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 优惠券.抵用券对象 coupon
 * 
 * @author LCL
 * @date 2020-08-11
 */
public class Coupon extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    /** 类型 */
    @Excel(name = "类型")
    private Integer type;

    /** 使用条件 */
    private String useCondition;

    /** 免减金额 */
    @Excel(name = "免减金额")
    private BigDecimal price;

    /** 开始时间 */
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 结束时间 */
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 补充说明 */
    @Excel(name = "补充说明")
    private String notes;

    /** 是否是秒杀优惠券  */
    @Excel(name = "是否是秒杀优惠券 ")
    private Integer hasSeckill;

    /** 排序 */
    @Excel(name = "排序")
    private Integer sort;

    /** 数量 */
    @Excel(name = "数量")
    private Integer total;

    // 是否查询过期优惠券 0 查询未过期 1 查询过期 null 不适用该条件
    private Integer overdue;

    // 该用户是否领取过 0 未领取 1 已领取
    private Integer receiveStatus;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setUseCondition(String useCondition)
    {
        this.useCondition = useCondition;
    }

    public String getUseCondition()
    {
        return useCondition;
    }
    public void setPrice(BigDecimal price)
    {
        this.price = price;
    }

    public BigDecimal getPrice()
    {
        return price;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setNotes(String notes) 
    {
        this.notes = notes;
    }

    public String getNotes() 
    {
        return notes;
    }
    public void setHasSeckill(Integer hasSeckill) 
    {
        this.hasSeckill = hasSeckill;
    }

    public Integer getHasSeckill() 
    {
        return hasSeckill;
    }
    public void setSort(Integer sort) 
    {
        this.sort = sort;
    }

    public Integer getSort() 
    {
        return sort;
    }
    public void setTotal(Integer total) 
    {
        this.total = total;
    }

    public Integer getTotal() 
    {
        return total;
    }

    public Integer getOverdue() {
        return overdue;
    }

    public void setOverdue(Integer overdue) {
        this.overdue = overdue;
    }

    public Integer getReceiveStatus() {
        return receiveStatus;
    }

    public void setReceiveStatus(Integer receiveStatus) {
        this.receiveStatus = receiveStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("status", getStatus())
            .append("type", getType())
            .append("useCondition", getUseCondition())
            .append("price", getPrice())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("notes", getNotes())
            .append("hasSeckill", getHasSeckill())
            .append("sort", getSort())
            .append("total", getTotal())
            .toString();
    }
}
