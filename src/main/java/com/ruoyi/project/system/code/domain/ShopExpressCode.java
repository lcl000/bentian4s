package com.ruoyi.project.system.code.domain;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 快递编码对象 shop_express_code
 * 
 * @author LCL
 * @date 2020-07-01
 */
public class ShopExpressCode extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** 快递编码 */
    @Excel(name = "快递编码")
    private String code;

    /** 快递名称 */
    @Excel(name = "快递名称")
    private String name;

    /** 是否是常用快递 1-是,0-否 */
//    @Excel(name = "是否是常用快递 1-是,0-否")
//    private Integer default;

    /** 是否禁用 */
    @Excel(name = "是否禁用")
    private Integer disabled;

    /** 操作时间 */
    @Excel(name = "操作时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createAt;

    /** 删除标记 */
    private Integer delFlag;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
//    public void setDefault(Integer default)
//    {
//        this.default = default;
//    }
//
//    public Integer getDefault()
//    {
//        return default;
//    }
    public void setDisabled(Integer disabled) 
    {
        this.disabled = disabled;
    }

    public Integer getDisabled() 
    {
        return disabled;
    }
    public void setCreateAt(Date createAt) 
    {
        this.createAt = createAt;
    }

    public Date getCreateAt() 
    {
        return createAt;
    }
    public void setDelFlag(Integer delFlag) 
    {
        this.delFlag = delFlag;
    }

    public Integer getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("name", getName())
//            .append("default", getDefault())
            .append("disabled", getDisabled())
            .append("remark", getRemark())
            .append("createAt", getCreateAt())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
