package com.ruoyi.project.system.code.service;

import java.util.List;

import com.ruoyi.project.system.code.domain.ShopExpressCode;

/**
 * 快递编码Service接口
 * 
 * @author LCL
 * @date 2020-07-01
 */
public interface IShopExpressCodeService 
{
    /**
     * 查询快递编码
     * 
     * @param id 快递编码ID
     * @return 快递编码
     */
    public ShopExpressCode selectShopExpressCodeById(String id);

    /**
     * 查询快递编码列表
     * 
     * @param shopExpressCode 快递编码
     * @return 快递编码集合
     */
    public List<ShopExpressCode> selectShopExpressCodeList(ShopExpressCode shopExpressCode);

    /**
     * 新增快递编码
     * 
     * @param shopExpressCode 快递编码
     * @return 结果
     */
    public int insertShopExpressCode(ShopExpressCode shopExpressCode);

    /**
     * 修改快递编码
     * 
     * @param shopExpressCode 快递编码
     * @return 结果
     */
    public int updateShopExpressCode(ShopExpressCode shopExpressCode);

    /**
     * 批量删除快递编码
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteShopExpressCodeByIds(String ids);

    /**
     * 删除快递编码信息
     * 
     * @param id 快递编码ID
     * @return 结果
     */
    public int deleteShopExpressCodeById(String id);

	List<ShopExpressCode> findAll();

    ShopExpressCode findByName(String shippingName);
}
