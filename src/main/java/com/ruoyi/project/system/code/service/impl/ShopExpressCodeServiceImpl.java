package com.ruoyi.project.system.code.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.text.Convert;
import com.ruoyi.project.system.code.domain.ShopExpressCode;
import com.ruoyi.project.system.code.mapper.ShopExpressCodeMapper;
import com.ruoyi.project.system.code.service.IShopExpressCodeService;

/**
 * 快递编码Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-01
 */
@Service
public class ShopExpressCodeServiceImpl implements IShopExpressCodeService 
{
    @Autowired
    private ShopExpressCodeMapper shopExpressCodeMapper;

    /**
     * 查询快递编码
     * 
     * @param id 快递编码ID
     * @return 快递编码
     */
    @Override
    public ShopExpressCode selectShopExpressCodeById(String id)
    {
        return shopExpressCodeMapper.selectShopExpressCodeById(id);
    }

    /**
     * 查询快递编码列表
     * 
     * @param shopExpressCode 快递编码
     * @return 快递编码
     */
    @Override
    public List<ShopExpressCode> selectShopExpressCodeList(ShopExpressCode shopExpressCode)
    {
        return shopExpressCodeMapper.selectShopExpressCodeList(shopExpressCode);
    }

    /**
     * 新增快递编码
     * 
     * @param shopExpressCode 快递编码
     * @return 结果
     */
    @Override
    public int insertShopExpressCode(ShopExpressCode shopExpressCode)
    {
        return shopExpressCodeMapper.insertShopExpressCode(shopExpressCode);
    }

    /**
     * 修改快递编码
     * 
     * @param shopExpressCode 快递编码
     * @return 结果
     */
    @Override
    public int updateShopExpressCode(ShopExpressCode shopExpressCode)
    {
        return shopExpressCodeMapper.updateShopExpressCode(shopExpressCode);
    }

    /**
     * 删除快递编码对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteShopExpressCodeByIds(String ids)
    {
        return shopExpressCodeMapper.deleteShopExpressCodeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除快递编码信息
     * 
     * @param id 快递编码ID
     * @return 结果
     */
    @Override
    public int deleteShopExpressCodeById(String id)
    {
        return shopExpressCodeMapper.deleteShopExpressCodeById(id);
    }

    @Override
    public List<ShopExpressCode> findAll() {
        return this.shopExpressCodeMapper.findAll();
    }

    @Override
    public ShopExpressCode findByName(String shippingName) {
        return this.shopExpressCodeMapper.findByName(shippingName);
    }
}
