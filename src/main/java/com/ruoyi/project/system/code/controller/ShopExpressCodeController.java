package com.ruoyi.project.system.code.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import org.apache.shiro.authz.annotation.RequiresPermissions;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.code.domain.ShopExpressCode;
import com.ruoyi.project.system.code.service.IShopExpressCodeService;

/**
 * 快递编码Controller
 * 
 * @author LCL
 * @date 2020-07-01
 */
@Controller
@RequestMapping("/system/code")
public class ShopExpressCodeController extends BaseController
{
    private String prefix = "system/code";

    @Autowired
    private IShopExpressCodeService shopExpressCodeService;

    @RequiresPermissions("system:code:view")
    @GetMapping()
    public String code()
    {
        return prefix + "/code";
    }

    /**
     * 查询快递编码列表
     */
    @RequiresPermissions("system:code:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ShopExpressCode shopExpressCode)
    {
        startPage();
        List<ShopExpressCode> list = shopExpressCodeService.selectShopExpressCodeList(shopExpressCode);
        return getDataTable(list);
    }

    /**
     * 导出快递编码列表
     */
    @RequiresPermissions("system:code:export")
    @Log(title = "快递编码", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ShopExpressCode shopExpressCode)
    {
        List<ShopExpressCode> list = shopExpressCodeService.selectShopExpressCodeList(shopExpressCode);
        ExcelUtil<ShopExpressCode> util = new ExcelUtil<ShopExpressCode>(ShopExpressCode.class);
        return util.exportExcel(list, "code");
    }

    /**
     * 新增快递编码
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存快递编码
     */
    @RequiresPermissions("system:code:add")
    @Log(title = "快递编码", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ShopExpressCode shopExpressCode)
    {
        return toAjax(shopExpressCodeService.insertShopExpressCode(shopExpressCode));
    }

    /**
     * 修改快递编码
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        ShopExpressCode shopExpressCode = shopExpressCodeService.selectShopExpressCodeById(id);
        mmap.put("shopExpressCode", shopExpressCode);
        return prefix + "/edit";
    }

    /**
     * 修改保存快递编码
     */
    @RequiresPermissions("system:code:edit")
    @Log(title = "快递编码", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ShopExpressCode shopExpressCode)
    {
        return toAjax(shopExpressCodeService.updateShopExpressCode(shopExpressCode));
    }

    /**
     * 删除快递编码
     */
    @RequiresPermissions("system:code:remove")
    @Log(title = "快递编码", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(shopExpressCodeService.deleteShopExpressCodeByIds(ids));
    }
}
