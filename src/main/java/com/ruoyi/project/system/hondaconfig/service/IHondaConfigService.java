package com.ruoyi.project.system.hondaconfig.service;

import java.util.List;

import com.ruoyi.common.utils.CacheUtils;
import com.ruoyi.project.system.hondaconfig.domain.HondaConfig;

/**
 * 配置信息Service接口
 * 
 * @author LCL
 * @date 2020-07-15
 */
public interface IHondaConfigService
{
    /**
     * 查询配置信息
     * 
     * @param id 配置信息ID
     * @return 配置信息
     */
    public HondaConfig selectHondaConfigById(String id);

    /**
     * 查询配置信息列表
     * 
     * @param hondaConfig 配置信息
     * @return 配置信息集合
     */
    public List<HondaConfig> selectHondaConfigList(HondaConfig hondaConfig);

    /**
     * 新增配置信息
     * 
     * @param hondaConfig 配置信息
     * @return 结果
     */
    public int insertHondaConfig(HondaConfig hondaConfig);

    /**
     * 修改配置信息
     * 
     * @param hondaConfig 配置信息
     * @return 结果
     */
    public int updateHondaConfig(HondaConfig hondaConfig);

    /**
     * 批量删除配置信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteHondaConfigByIds(String ids);

    /**
     * 删除配置信息信息
     * 
     * @param id 配置信息ID
     * @return 结果
     */
    public int deleteHondaConfigById(String id);

	List<HondaConfig> findAll();
}
