package com.ruoyi.project.system.hondaconfig.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.hondaconfig.mapper.HondaConfigMapper;
import com.ruoyi.project.system.hondaconfig.domain.HondaConfig;
import com.ruoyi.project.system.hondaconfig.service.IHondaConfigService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 配置信息Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-15
 */
@Service
public class HondaConfigServiceImpl implements IHondaConfigService
{
    @Autowired
    private HondaConfigMapper hondaConfigMapper;

    /**
     * 查询配置信息
     * 
     * @param id 配置信息ID
     * @return 配置信息
     */
    @Override
    public HondaConfig selectHondaConfigById(String id)
    {
        return hondaConfigMapper.selectHondaConfigById(id);
    }

    /**
     * 查询配置信息列表
     * 
     * @param hondaConfig 配置信息
     * @return 配置信息
     */
    @Override
    public List<HondaConfig> selectHondaConfigList(HondaConfig hondaConfig)
    {
        return hondaConfigMapper.selectHondaConfigList(hondaConfig);
    }

    /**
     * 新增配置信息
     * 
     * @param hondaConfig 配置信息
     * @return 结果
     */
    @Override
    public int insertHondaConfig(HondaConfig hondaConfig)
    {
        return hondaConfigMapper.insertHondaConfig(hondaConfig);
    }

    /**
     * 修改配置信息
     * 
     * @param hondaConfig 配置信息
     * @return 结果
     */
    @Override
    public int updateHondaConfig(HondaConfig hondaConfig)
    {
        return hondaConfigMapper.updateHondaConfig(hondaConfig);
    }

    /**
     * 删除配置信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteHondaConfigByIds(String ids)
    {
        return hondaConfigMapper.deleteHondaConfigByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除配置信息信息
     * 
     * @param id 配置信息ID
     * @return 结果
     */
    @Override
    public int deleteHondaConfigById(String id)
    {
        return hondaConfigMapper.deleteHondaConfigById(id);
    }

    @Override
    public List<HondaConfig> findAll() {
        return this.hondaConfigMapper.findAll();
    }
}
