package com.ruoyi.project.system.hondaconfig.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.hondaconfig.domain.HondaConfig;
import com.ruoyi.project.system.hondaconfig.service.IHondaConfigService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 配置信息Controller
 * 
 * @author LCL
 * @date 2020-07-15
 */
@Controller
@RequestMapping("/system/hondaconfig")
public class HondaConfigController extends BaseController
{
    private String prefix = "system/hondaconfig";

    @Autowired
    private IHondaConfigService hondaConfigService;

    @RequiresPermissions("system:hondaconfig:view")
    @GetMapping()
    public String hondaconfig()
    {
        return prefix + "/hondaconfig";
    }

    /**
     * 查询配置信息列表
     */
    @RequiresPermissions("system:hondaconfig:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(HondaConfig hondaConfig)
    {
        startPage();
        List<HondaConfig> list = hondaConfigService.selectHondaConfigList(hondaConfig);
        return getDataTable(list);
    }

    /**
     * 导出配置信息列表
     */
    @RequiresPermissions("system:hondaconfig:export")
    @Log(title = "配置信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(HondaConfig hondaConfig)
    {
        List<HondaConfig> list = hondaConfigService.selectHondaConfigList(hondaConfig);
        ExcelUtil<HondaConfig> util = new ExcelUtil<HondaConfig>(HondaConfig.class);
        return util.exportExcel(list, "hondaconfig");
    }

    /**
     * 新增配置信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增配置信息
     */
    @GetMapping("/hondaconfig")
    public String hondaConfig(ModelMap map)
    {
        map.put("hondaConfig",this.hondaConfigService.findAll().get(0));
        return prefix + "/hondaconfig";
    }

    /**
     * 新增保存配置信息
     */
    @RequiresPermissions("system:hondaconfig:add")
    @Log(title = "配置信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(HondaConfig hondaConfig)
    {
        return toAjax(hondaConfigService.insertHondaConfig(hondaConfig));
    }

    /**
     * 修改配置信息
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        HondaConfig hondaConfig = hondaConfigService.selectHondaConfigById(id);
        mmap.put("hondaConfig", hondaConfig);
        return prefix + "/edit";
    }

    /**
     * 修改保存配置信息
     */
    @RequiresPermissions("system:hondaconfig:edit")
    @Log(title = "配置信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public String editSave(HondaConfig hondaConfig)
    {
        toAjax(hondaConfigService.updateHondaConfig(hondaConfig));
        return "redirect:/"+prefix + "/hondaconfig";
    }

    /**
     * 删除配置信息
     */
    @RequiresPermissions("system:hondaconfig:remove")
    @Log(title = "配置信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(hondaConfigService.deleteHondaConfigByIds(ids));
    }
}
