package com.ruoyi.project.system.hondaconfig.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 商城配置对象 honda_config
 *
 * @author LCL
 * @date 2020-08-11
 */
public class HondaConfig extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 分享海报 */
    @Excel(name = "分享海报")
    private String shareImg;

    /** 平台须知 */
    @Excel(name = "平台须知")
    private String platformProtocol;

    /** 积分规则 */
    @Excel(name = "积分规则")
    private String coinRule;

    /** 关于我们 */
    @Excel(name = "关于我们")
    private String about;

    /** 客服电话 */
    @Excel(name = "客服电话")
    private String customerMobile;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setShareImg(String shareImg)
    {
        this.shareImg = shareImg;
    }

    public String getShareImg()
    {
        return shareImg;
    }
    public void setPlatformProtocol(String platformProtocol)
    {
        this.platformProtocol = platformProtocol;
    }

    public String getPlatformProtocol()
    {
        return platformProtocol;
    }
    public void setCoinRule(String coinRule)
    {
        this.coinRule = coinRule;
    }

    public String getCoinRule()
    {
        return coinRule;
    }
    public void setAbout(String about)
    {
        this.about = about;
    }

    public String getAbout()
    {
        return about;
    }

    public String getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("shareImg", getShareImg())
                .append("platformProtocol", getPlatformProtocol())
                .append("coinRule", getCoinRule())
                .append("about", getAbout())
                .toString();
    }
}
