package com.ruoyi.project.system.spec.controller;

import com.ruoyi.common.utils.R;
import java.util.HashMap;
import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.text.Convert;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.spec.domain.GoodsSpec;
import com.ruoyi.project.system.spec.enums.GoodsSpecStatus;
import com.ruoyi.project.system.spec.service.IGoodsSpecService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.tspec.domain.TypeSpec;
import com.ruoyi.project.system.tspec.service.ITypeSpecService;
import com.ruoyi.project.system.tsvalue.service.ITypeSpecValueService;
import com.ruoyi.project.system.value.domain.GoodsSpecValue;
import com.ruoyi.project.system.value.enums.GoodsSpecValueStatus;
import com.ruoyi.project.system.value.service.IGoodsSpecValueService;

/**
 * 商品规格名Controller
 * 
 * @author LCL
 * @date 2020-06-09
 */
@Controller
@RequestMapping("/system/spec")
public class GoodsSpecController extends BaseController
{
    private String prefix = "system/spec";

    @Autowired
    private IGoodsSpecService goodsSpecService;

    @Autowired
    private IGoodsSpecValueService goodsSpecValueService;

    @Autowired
    private ITypeSpecService typeSpecService;

    @Autowired
    private ITypeSpecValueService typeSpecValueService;

    @RequiresPermissions("system:spec:view")
    @GetMapping()
    public String spec()
    {
        return prefix + "/spec";
    }


    @GetMapping("/spec")
    public String spec(ModelMap mmap)
    {
        mmap.put("goodsSpec",this.goodsSpecService.findAll());
        return prefix + "/../type/spec";
    }


    @ResponseBody
    @PostMapping("/specVal")
    public R specVal(@RequestParam("id")String id)
    {
        List<GoodsSpecValue> specValues = this.goodsSpecValueService.findAllBySpecId(id);
        return R.ok().put("data",specValues);
    }




    /**
     * 查询商品规格名列表
     */
    @RequiresPermissions("system:spec:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(GoodsSpec goodsSpec)
    {
        startPage();
        goodsSpec.setStatus(GoodsSpecStatus.NORMAL.getValue());
        List<GoodsSpec> list = goodsSpecService.selectGoodsSpecList(goodsSpec);
        return getDataTable(list);
    }

    /**
     * 导出商品规格名列表
     */
    @RequiresPermissions("system:spec:export")
    @Log(title = "商品规格名", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(GoodsSpec goodsSpec)
    {
        List<GoodsSpec> list = goodsSpecService.selectGoodsSpecList(goodsSpec);
        ExcelUtil<GoodsSpec> util = new ExcelUtil<GoodsSpec>(GoodsSpec.class);
        return util.exportExcel(list, "spec");
    }

    /**
     * 新增商品规格名
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存商品规格名
     */
    @RequiresPermissions("system:spec:add")
    @Log(title = "商品规格名", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    @Transactional
    public AjaxResult addSave(GoodsSpec goodsSpec,String[] spec_value)
    {
        goodsSpec.setCreated(DateUtils.getNowDate());
        goodsSpec.setStatus(GoodsSpecStatus.NORMAL.getValue());
        if(spec_value==null||spec_value.length==0){
            return AjaxResult.error("至少存在一个规格值");
        }
        goodsSpecService.insertGoodsSpec(goodsSpec);
        for(int i=0;i<spec_value.length;i++){
            GoodsSpecValue specValue = new GoodsSpecValue();
            specValue.setCreated(DateUtils.getNowDate());
            specValue.setSort(Integer.toUnsignedLong(i));
            specValue.setSpecId(goodsSpec.getId());
            specValue.setValue(spec_value[i]);
            specValue.setStatus(GoodsSpecValueStatus.NORMAL.getValue());
            this.goodsSpecValueService.insertGoodsSpecValue(specValue);
        }
        return AjaxResult.success();
    }

    /**
     * 修改商品规格名
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        GoodsSpec goodsSpec = goodsSpecService.selectGoodsSpecById(id);
        mmap.put("goodsSpec", goodsSpec);
        GoodsSpecValue goodsSpecValue = new GoodsSpecValue();
        goodsSpecValue.setSpecId(id);
        goodsSpecValue.setStatus(GoodsSpecValueStatus.NORMAL.getValue());
        List<GoodsSpecValue> goodsSpecValues = goodsSpecValueService.selectGoodsSpecValueList(goodsSpecValue);
        mmap.put("specValues",goodsSpecValues);
        return prefix + "/edit";
    }

    /**
     * 修改保存商品规格名
     */
    @RequiresPermissions("system:spec:edit")
    @Log(title = "商品规格名", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(GoodsSpec goodsSpec,String[] spec_value,String[] spec_value_id)
    {
        goodsSpec.setUpdateTime(DateUtils.getNowDate());
        goodsSpecService.updateGoodsSpec(goodsSpec);
        //删除规格下的值
        HashMap<String, Object> map = new HashMap<>();
        map.put("status",GoodsSpecValueStatus.DELETE.getValue());
        map.put("specId",goodsSpec.getId());
        this.goodsSpecValueService.updateBySpecId(map);
        //判断是修改还是新增
        for (int i = 0; i < spec_value.length; i++) {
            GoodsSpecValue specValue = new GoodsSpecValue();
            specValue.setSort(Integer.toUnsignedLong(i));
            specValue.setValue(spec_value[i]);
            specValue.setStatus(GoodsSpecValueStatus.NORMAL.getValue());
            if(StringUtils.isBlank(spec_value_id[i])){
                specValue.setCreated(DateUtils.getNowDate());
                specValue.setSpecId(goodsSpec.getId());
                this.goodsSpecValueService.insertGoodsSpecValue(specValue);
            }else{
                //修改信息
                specValue.setId(spec_value_id[i]);
                this.goodsSpecValueService.updateGoodsSpecValue(specValue);
            }
        }
        return AjaxResult.success();
    }

    /**
     * 删除商品规格名
     */
    @RequiresPermissions("system:spec:remove")
    @Log(title = "商品规格名", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    @Transactional
    public AjaxResult remove(String ids)
    {
//        GoodsSpec goodsSpec = new GoodsSpec();
//        goodsSpec.setUpdateTime(DateUtils.getNowDate());
//        goodsSpec.setStatus(GoodsSpecStatus.DELETE.getValue());
//        for(String id:Convert.toStrArray(ids)){
//            goodsSpec.setId(id);
//            this.goodsSpecService.updateGoodsSpec(goodsSpec);
//        }
        //删除下属规格值
        this.goodsSpecValueService.deleteBySpecIds(ids);
        //删除规格
        this.goodsSpecService.deleteGoodsSpecByIds(ids);

        //todo:删除分类规格中使用的相关资源
        List<TypeSpec> typeSpecList = this.typeSpecService.findListBySpecIds(ids);
        for(TypeSpec typeSpec:typeSpecList){
            this.typeSpecValueService.deleteByTypeSpecId(typeSpec.getId());
            this.typeSpecService.deleteTypeSpecById(typeSpec.getId());
        }
        return AjaxResult.success();
    }
}
