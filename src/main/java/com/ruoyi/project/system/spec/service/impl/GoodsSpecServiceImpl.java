package com.ruoyi.project.system.spec.service.impl;

import javax.annotation.Resource;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.spec.mapper.GoodsSpecMapper;
import com.ruoyi.project.system.spec.domain.GoodsSpec;
import com.ruoyi.project.system.spec.service.IGoodsSpecService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 商品规格名Service业务层处理
 * 
 * @author LCL
 * @date 2020-06-09
 */
@Service
public class GoodsSpecServiceImpl implements IGoodsSpecService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private GoodsSpecMapper goodsSpecMapper;

    /**
     * 查询商品规格名
     * 
     * @param id 商品规格名ID
     * @return 商品规格名
     */
    @Override
    public GoodsSpec selectGoodsSpecById(String id)
    {
        return goodsSpecMapper.selectGoodsSpecById(id);
    }

    /**
     * 查询商品规格名列表
     * 
     * @param goodsSpec 商品规格名
     * @return 商品规格名
     */
    @Override
    public List<GoodsSpec> selectGoodsSpecList(GoodsSpec goodsSpec)
    {
        return goodsSpecMapper.selectGoodsSpecList(goodsSpec);
    }

    /**
     * 新增商品规格名
     * 
     * @param goodsSpec 商品规格名
     * @return 结果
     */
    @Override
    public int insertGoodsSpec(GoodsSpec goodsSpec)
    {
        goodsSpec.setId(this.idGererateFactory.nextStringId());
        return goodsSpecMapper.insertGoodsSpec(goodsSpec);
    }

    /**
     * 修改商品规格名
     * 
     * @param goodsSpec 商品规格名
     * @return 结果
     */
    @Override
    public int updateGoodsSpec(GoodsSpec goodsSpec)
    {
        goodsSpec.setUpdateTime(DateUtils.getNowDate());
        return goodsSpecMapper.updateGoodsSpec(goodsSpec);
    }

    /**
     * 删除商品规格名对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteGoodsSpecByIds(String ids)
    {
        return goodsSpecMapper.deleteGoodsSpecByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商品规格名信息
     * 
     * @param id 商品规格名ID
     * @return 结果
     */
    @Override
    public int deleteGoodsSpecById(String id)
    {
        return goodsSpecMapper.deleteGoodsSpecById(id);
    }

    @Override
    public List<GoodsSpec> findAll() {
        return this.goodsSpecMapper.findAll();
    }
}
