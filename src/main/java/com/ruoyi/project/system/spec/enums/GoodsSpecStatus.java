package com.ruoyi.project.system.spec.enums;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/9 17:56
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public enum GoodsSpecStatus {
	NORMAL(0,"正常"),
	DELETE(1,"删除");

	private int value;
	private String name;

	GoodsSpecStatus(int value, String name) {
		this.value = value;
		this.name = name;
	}

	/**
	 * 数据范围校验
	 *
	 * @param value
	 * @return
	 */
	public static boolean contains(int value) {
		return null == getByValue(value)?Boolean.FALSE.booleanValue():Boolean.TRUE.booleanValue();
	}
	public int getValue() {
		return this.value;
	}

	public String getName(){
		return this.name;
	}

	public static GoodsSpecStatus getByValue(int value){
		GoodsSpecStatus allVaues[] = GoodsSpecStatus.values();
		GoodsSpecStatus status;
		for (int i=0,len = allVaues.length;i<len;i++){
			status = allVaues[i];
			if(status.getValue()==value){
				return status;
			}
		}
		return null;
	}

}
