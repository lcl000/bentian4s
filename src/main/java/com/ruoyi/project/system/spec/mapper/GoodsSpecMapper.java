package com.ruoyi.project.system.spec.mapper;

import java.util.List;
import com.ruoyi.project.system.spec.domain.GoodsSpec;

/**
 * 商品规格名Mapper接口
 * 
 * @author LCL
 * @date 2020-06-09
 */
public interface GoodsSpecMapper 
{
    /**
     * 查询商品规格名
     * 
     * @param id 商品规格名ID
     * @return 商品规格名
     */
    public GoodsSpec selectGoodsSpecById(String id);

    /**
     * 查询商品规格名列表
     * 
     * @param goodsSpec 商品规格名
     * @return 商品规格名集合
     */
    public List<GoodsSpec> selectGoodsSpecList(GoodsSpec goodsSpec);

    /**
     * 新增商品规格名
     * 
     * @param goodsSpec 商品规格名
     * @return 结果
     */
    public int insertGoodsSpec(GoodsSpec goodsSpec);

    /**
     * 修改商品规格名
     * 
     * @param goodsSpec 商品规格名
     * @return 结果
     */
    public int updateGoodsSpec(GoodsSpec goodsSpec);

    /**
     * 删除商品规格名
     * 
     * @param id 商品规格名ID
     * @return 结果
     */
    public int deleteGoodsSpecById(String id);

    /**
     * 批量删除商品规格名
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGoodsSpecByIds(String[] ids);

	List<GoodsSpec> findAll();
}
