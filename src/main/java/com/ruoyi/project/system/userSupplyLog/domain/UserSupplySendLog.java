package com.ruoyi.project.system.userSupplyLog.domain;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 用户供应卡配送记录对象 user_supply_send_log
 * 
 * @author LCL
 * @date 2020-07-20
 */
public class UserSupplySendLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 用户供应卡ID */
    @Excel(name = "用户供应卡ID")
    private String userSupplyId;

    /** 当前供应次数 */
    @Excel(name = "当前供应次数")
    private Integer num;

    /** 数量 */
    @Excel(name = "数量")
    private Integer total;

    /** 配送人名称 */
    @Excel(name = "配送人名称")
    private String username;

    /** 配送人电话 */
    @Excel(name = "配送人电话")
    private String mobile;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date created;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setUserSupplyId(String userSupplyId) 
    {
        this.userSupplyId = userSupplyId;
    }

    public String getUserSupplyId() 
    {
        return userSupplyId;
    }
    public void setNum(Integer num)
    {
        this.num = num;
    }

    public Integer getNum()
    {
        return num;
    }
    public void setTotal(Integer total)
    {
        this.total = total;
    }

    public Integer getTotal()
    {
        return total;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setMobile(String mobile) 
    {
        this.mobile = mobile;
    }

    public String getMobile() 
    {
        return mobile;
    }
    public void setCreated(Date created) 
    {
        this.created = created;
    }

    public Date getCreated() 
    {
        return created;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userSupplyId", getUserSupplyId())
            .append("num", getNum())
            .append("total", getTotal())
            .append("username", getUsername())
            .append("mobile", getMobile())
            .append("created", getCreated())
            .toString();
    }
}
