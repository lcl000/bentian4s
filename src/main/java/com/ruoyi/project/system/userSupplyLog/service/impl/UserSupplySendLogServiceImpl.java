package com.ruoyi.project.system.userSupplyLog.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.userSupplyLog.mapper.UserSupplySendLogMapper;
import com.ruoyi.project.system.userSupplyLog.domain.UserSupplySendLog;
import com.ruoyi.project.system.userSupplyLog.service.IUserSupplySendLogService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 用户供应卡配送记录Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-20
 */
@Service
public class UserSupplySendLogServiceImpl implements IUserSupplySendLogService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private UserSupplySendLogMapper userSupplySendLogMapper;

    /**
     * 查询用户供应卡配送记录
     * 
     * @param id 用户供应卡配送记录ID
     * @return 用户供应卡配送记录
     */
    @Override
    public UserSupplySendLog selectUserSupplySendLogById(String id)
    {
        return userSupplySendLogMapper.selectUserSupplySendLogById(id);
    }

    /**
     * 查询用户供应卡配送记录列表
     * 
     * @param userSupplySendLog 用户供应卡配送记录
     * @return 用户供应卡配送记录
     */
    @Override
    public List<UserSupplySendLog> selectUserSupplySendLogList(UserSupplySendLog userSupplySendLog)
    {
        return userSupplySendLogMapper.selectUserSupplySendLogList(userSupplySendLog);
    }

    /**
     * 新增用户供应卡配送记录
     * 
     * @param userSupplySendLog 用户供应卡配送记录
     * @return 结果
     */
    @Override
    public int insertUserSupplySendLog(UserSupplySendLog userSupplySendLog)
    {
        userSupplySendLog.setId(this.idGererateFactory.nextStringId());
        return userSupplySendLogMapper.insertUserSupplySendLog(userSupplySendLog);
    }

    /**
     * 修改用户供应卡配送记录
     * 
     * @param userSupplySendLog 用户供应卡配送记录
     * @return 结果
     */
    @Override
    public int updateUserSupplySendLog(UserSupplySendLog userSupplySendLog)
    {
        return userSupplySendLogMapper.updateUserSupplySendLog(userSupplySendLog);
    }

    /**
     * 删除用户供应卡配送记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteUserSupplySendLogByIds(String ids)
    {
        return userSupplySendLogMapper.deleteUserSupplySendLogByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户供应卡配送记录信息
     * 
     * @param id 用户供应卡配送记录ID
     * @return 结果
     */
    @Override
    public int deleteUserSupplySendLogById(String id)
    {
        return userSupplySendLogMapper.deleteUserSupplySendLogById(id);
    }
}
