package com.ruoyi.project.system.userSupplyLog.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.userSupplyLog.domain.UserSupplySendLog;
import com.ruoyi.project.system.userSupplyLog.service.IUserSupplySendLogService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 用户供应卡配送记录Controller
 * 
 * @author LCL
 * @date 2020-07-20
 */
@Controller
@RequestMapping("/system/userSupplyLog")
public class UserSupplySendLogController extends BaseController
{
    private String prefix = "system/userSupplyLog";

    @Autowired
    private IUserSupplySendLogService userSupplySendLogService;

    @RequiresPermissions("system:userSupplyLog:view")
    @GetMapping()
    public String userSupplyLog()
    {
        return prefix + "/userSupplyLog";
    }

    /**
     * 查询用户供应卡配送记录列表
     */
    @RequiresPermissions("system:userSupplyLog:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(UserSupplySendLog userSupplySendLog)
    {
        startPage();
        List<UserSupplySendLog> list = userSupplySendLogService.selectUserSupplySendLogList(userSupplySendLog);
        return getDataTable(list);
    }

    /**
     * 导出用户供应卡配送记录列表
     */
    @RequiresPermissions("system:userSupplyLog:export")
    @Log(title = "用户供应卡配送记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(UserSupplySendLog userSupplySendLog)
    {
        List<UserSupplySendLog> list = userSupplySendLogService.selectUserSupplySendLogList(userSupplySendLog);
        ExcelUtil<UserSupplySendLog> util = new ExcelUtil<UserSupplySendLog>(UserSupplySendLog.class);
        return util.exportExcel(list, "userSupplyLog");
    }

    /**
     * 新增用户供应卡配送记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存用户供应卡配送记录
     */
    @RequiresPermissions("system:userSupplyLog:add")
    @Log(title = "用户供应卡配送记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(UserSupplySendLog userSupplySendLog)
    {
        return toAjax(userSupplySendLogService.insertUserSupplySendLog(userSupplySendLog));
    }

    /**
     * 修改用户供应卡配送记录
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        UserSupplySendLog userSupplySendLog = userSupplySendLogService.selectUserSupplySendLogById(id);
        mmap.put("userSupplySendLog", userSupplySendLog);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户供应卡配送记录
     */
    @RequiresPermissions("system:userSupplyLog:edit")
    @Log(title = "用户供应卡配送记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(UserSupplySendLog userSupplySendLog)
    {
        return toAjax(userSupplySendLogService.updateUserSupplySendLog(userSupplySendLog));
    }

    /**
     * 删除用户供应卡配送记录
     */
    @RequiresPermissions("system:userSupplyLog:remove")
    @Log(title = "用户供应卡配送记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(userSupplySendLogService.deleteUserSupplySendLogByIds(ids));
    }
}
