package com.ruoyi.project.system.userSupplyLog.service;

import java.util.List;
import com.ruoyi.project.system.userSupplyLog.domain.UserSupplySendLog;

/**
 * 用户供应卡配送记录Service接口
 * 
 * @author LCL
 * @date 2020-07-20
 */
public interface IUserSupplySendLogService 
{
    /**
     * 查询用户供应卡配送记录
     * 
     * @param id 用户供应卡配送记录ID
     * @return 用户供应卡配送记录
     */
    public UserSupplySendLog selectUserSupplySendLogById(String id);

    /**
     * 查询用户供应卡配送记录列表
     * 
     * @param userSupplySendLog 用户供应卡配送记录
     * @return 用户供应卡配送记录集合
     */
    public List<UserSupplySendLog> selectUserSupplySendLogList(UserSupplySendLog userSupplySendLog);

    /**
     * 新增用户供应卡配送记录
     * 
     * @param userSupplySendLog 用户供应卡配送记录
     * @return 结果
     */
    public int insertUserSupplySendLog(UserSupplySendLog userSupplySendLog);

    /**
     * 修改用户供应卡配送记录
     * 
     * @param userSupplySendLog 用户供应卡配送记录
     * @return 结果
     */
    public int updateUserSupplySendLog(UserSupplySendLog userSupplySendLog);

    /**
     * 批量删除用户供应卡配送记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserSupplySendLogByIds(String ids);

    /**
     * 删除用户供应卡配送记录信息
     * 
     * @param id 用户供应卡配送记录ID
     * @return 结果
     */
    public int deleteUserSupplySendLogById(String id);
}
