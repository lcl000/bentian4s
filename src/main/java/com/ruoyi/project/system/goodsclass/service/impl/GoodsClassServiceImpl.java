package com.ruoyi.project.system.goodsclass.service.impl;

import javax.annotation.Resource;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.framework.web.domain.Ztree;
import com.ruoyi.framework.web.domain.ZtreeStr;
import com.ruoyi.project.system.goodsclass.dto.GoodsClassDto;
import com.ruoyi.project.system.goodsclass.mapper.GoodsClassMapper;
import com.ruoyi.project.system.goodsclass.domain.GoodsClass;
import com.ruoyi.project.system.goodsclass.service.IGoodsClassService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 商品类型Service业务层处理
 * 
 * @author LCL
 * @date 2020-06-06
 */
@Service
public class GoodsClassServiceImpl implements IGoodsClassService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private GoodsClassMapper goodsClassMapper;

    /**
     * 查询商品类型
     * 
     * @param id 商品类型ID
     * @return 商品类型
     */
    @Override
    public GoodsClass selectGoodsClassById(String id)
    {
        return goodsClassMapper.selectGoodsClassById(id);
    }

    /**
     * 查询商品类型列表
     * 
     * @param goodsClass 商品类型
     * @return 商品类型
     */
    @Override
    public List<GoodsClass> selectGoodsClassList(GoodsClass goodsClass)
    {
        return goodsClassMapper.selectGoodsClassList(goodsClass);
    }

    /**
     * 新增商品类型
     * 
     * @param goodsClass 商品类型
     * @return 结果
     */
    @Override
    public int insertGoodsClass(GoodsClass goodsClass)
    {
        goodsClass.setId(this.idGererateFactory.nextStringId());
        return goodsClassMapper.insertGoodsClass(goodsClass);
    }

    /**
     * 修改商品类型
     * 
     * @param goodsClass 商品类型
     * @return 结果
     */
    @Override
    public int updateGoodsClass(GoodsClass goodsClass)
    {
        return goodsClassMapper.updateGoodsClass(goodsClass);
    }

    /**
     * 删除商品类型对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteGoodsClassByIds(String ids)
    {
        return goodsClassMapper.deleteGoodsClassByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商品类型信息
     * 
     * @param id 商品类型ID
     * @return 结果
     */
    @Override
    public int deleteGoodsClassById(String id)
    {
        return goodsClassMapper.deleteGoodsClassById(id);
    }


    @Override
    public List<GoodsClass> findAll() {
        return this.goodsClassMapper.findAll();
    }

    @Override
    public List<GoodsClass> selectGoodsClassByIds(String ids) {
        return this.goodsClassMapper.selectGoodsClassByIds(Convert.toStrArray(ids));
    }

    @Override
    public GoodsClassDto selectGoodsDtoClassById(String id) {
        return this.goodsClassMapper.selectGoodsDtoClassById(id);
    }

    @Override
    public List<GoodsClass> findGoodsClassByParentIds(String ids) {
        return this.goodsClassMapper.findGoodsClassByParentIds(Convert.toStrArray(ids));
    }

    @Override
    public List<GoodsClass> findApiList() {
        return this.goodsClassMapper.findApiList();
    }

}
