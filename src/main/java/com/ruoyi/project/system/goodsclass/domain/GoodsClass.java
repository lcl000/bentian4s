package com.ruoyi.project.system.goodsclass.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 商品类型对象 goods_class
 * 
 * @author LCL
 * @date 2020-06-06
 */
public class GoodsClass extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 分类名称 */
    @Excel(name = "分类名称")
    private String name;

    /** 状态: 0:启用 1:不启用 */
    @Excel(name = "状态: 0:启用 1:不启用")
    private Integer status;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    public Long getSort() 
    {
        return sort;
    }

    public void setSort(Long sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("status", getStatus())
            .append("sort", getSort())
            .toString();
    }
}
