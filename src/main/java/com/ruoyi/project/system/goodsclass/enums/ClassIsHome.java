package com.ruoyi.project.system.goodsclass.enums;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/11 15:17
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public enum ClassIsHome {
	NORMAL(0,"展示"),
	NO_NORMAL(1,"不展示");

	private int value;
	private String name;

	ClassIsHome(int value, String name) {
		this.value = value;
		this.name = name;
	}

	/**
	 * 数据范围校验
	 *
	 * @param value
	 * @return
	 */
	public static boolean contains(int value) {
		return null == getByValue(value)?Boolean.FALSE.booleanValue():Boolean.TRUE.booleanValue();
	}
	public int getValue() {
		return this.value;
	}

	public String getName(){
		return this.name;
	}

	public static ClassIsHome getByValue(int value){
		ClassIsHome allVaues[] = ClassIsHome.values();
		ClassIsHome status;
		for (int i=0,len = allVaues.length;i<len;i++){
			status = allVaues[i];
			if(status.getValue()==value){
				return status;
			}
		}
		return null;
	}

}
