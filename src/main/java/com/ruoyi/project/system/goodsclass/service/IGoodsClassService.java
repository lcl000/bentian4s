package com.ruoyi.project.system.goodsclass.service;

import java.util.List;

import com.ruoyi.framework.web.domain.Ztree;
import com.ruoyi.framework.web.domain.ZtreeStr;
import com.ruoyi.project.system.goodsclass.domain.GoodsClass;
import com.ruoyi.project.system.goodsclass.dto.GoodsClassDto;

/**
 * 商品类型Service接口
 * 
 * @author LCL
 * @date 2020-06-06
 */
public interface IGoodsClassService 
{
    /**
     * 查询商品类型
     * 
     * @param id 商品类型ID
     * @return 商品类型
     */
    public GoodsClass selectGoodsClassById(String id);

    /**
     * 查询商品类型列表
     * 
     * @param goodsClass 商品类型
     * @return 商品类型集合
     */
    public List<GoodsClass> selectGoodsClassList(GoodsClass goodsClass);

    /**
     * 新增商品类型
     * 
     * @param goodsClass 商品类型
     * @return 结果
     */
    public int insertGoodsClass(GoodsClass goodsClass);

    /**
     * 修改商品类型
     * 
     * @param goodsClass 商品类型
     * @return 结果
     */
    public int updateGoodsClass(GoodsClass goodsClass);

    /**
     * 批量删除商品类型
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGoodsClassByIds(String ids);

    /**
     * 删除商品类型信息
     * 
     * @param id 商品类型ID
     * @return 结果
     */
    public int deleteGoodsClassById(String id);

    List<GoodsClass> findAll();

	List<GoodsClass> selectGoodsClassByIds(String ids);

	GoodsClassDto selectGoodsDtoClassById(String id);

    List<GoodsClass> findGoodsClassByParentIds(String ids);

	List<GoodsClass> findApiList();
}
