package com.ruoyi.project.system.sign.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.sign.domain.MemberSign;
import com.ruoyi.project.system.sign.service.IMemberSignService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 用户打卡Controller
 * 
 * @author LCL
 * @date 2020-08-15
 */
@Controller
@RequestMapping("/system/sign")
public class MemberSignController extends BaseController
{
    private String prefix = "system/sign";

    @Autowired
    private IMemberSignService memberSignService;

    @RequiresPermissions("system:sign:view")
    @GetMapping()
    public String sign()
    {
        return prefix + "/sign";
    }

    /**
     * 查询用户打卡列表
     */
    @RequiresPermissions("system:sign:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MemberSign memberSign)
    {
        startPage();
        List<MemberSign> list = memberSignService.selectMemberSignList(memberSign);
        return getDataTable(list);
    }

    /**
     * 导出用户打卡列表
     */
    @RequiresPermissions("system:sign:export")
    @Log(title = "用户打卡", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MemberSign memberSign)
    {
        List<MemberSign> list = memberSignService.selectMemberSignList(memberSign);
        ExcelUtil<MemberSign> util = new ExcelUtil<MemberSign>(MemberSign.class);
        return util.exportExcel(list, "sign");
    }

    /**
     * 新增用户打卡
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存用户打卡
     */
    @RequiresPermissions("system:sign:add")
    @Log(title = "用户打卡", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MemberSign memberSign)
    {
        return toAjax(memberSignService.insertMemberSign(memberSign));
    }

    /**
     * 修改用户打卡
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        MemberSign memberSign = memberSignService.selectMemberSignById(id);
        mmap.put("memberSign", memberSign);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户打卡
     */
    @RequiresPermissions("system:sign:edit")
    @Log(title = "用户打卡", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MemberSign memberSign)
    {
        return toAjax(memberSignService.updateMemberSign(memberSign));
    }

    /**
     * 删除用户打卡
     */
    @RequiresPermissions("system:sign:remove")
    @Log(title = "用户打卡", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(memberSignService.deleteMemberSignByIds(ids));
    }
}
