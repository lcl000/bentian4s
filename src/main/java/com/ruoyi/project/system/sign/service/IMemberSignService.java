package com.ruoyi.project.system.sign.service;

import java.util.List;
import com.ruoyi.project.system.sign.domain.MemberSign;

/**
 * 用户打卡Service接口
 * 
 * @author LCL
 * @date 2020-08-15
 */
public interface IMemberSignService 
{
    /**
     * 查询用户打卡
     * 
     * @param id 用户打卡ID
     * @return 用户打卡
     */
    public MemberSign selectMemberSignById(String id);

    /**
     * 查询用户打卡列表
     * 
     * @param memberSign 用户打卡
     * @return 用户打卡集合
     */
    public List<MemberSign> selectMemberSignList(MemberSign memberSign);

    /**
     * 新增用户打卡
     * 
     * @param memberSign 用户打卡
     * @return 结果
     */
    public int insertMemberSign(MemberSign memberSign);

    /**
     * 修改用户打卡
     * 
     * @param memberSign 用户打卡
     * @return 结果
     */
    public int updateMemberSign(MemberSign memberSign);

    /**
     * 批量删除用户打卡
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMemberSignByIds(String ids);

    /**
     * 删除用户打卡信息
     * 
     * @param id 用户打卡ID
     * @return 结果
     */
    public int deleteMemberSignById(String id);
}
