package com.ruoyi.project.system.sign.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.sign.mapper.MemberSignMapper;
import com.ruoyi.project.system.sign.domain.MemberSign;
import com.ruoyi.project.system.sign.service.IMemberSignService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 用户打卡Service业务层处理
 * 
 * @author LCL
 * @date 2020-08-15
 */
@Service
public class MemberSignServiceImpl implements IMemberSignService 
{
    @Autowired
    private MemberSignMapper memberSignMapper;

    /**
     * 查询用户打卡
     * 
     * @param id 用户打卡ID
     * @return 用户打卡
     */
    @Override
    public MemberSign selectMemberSignById(String id)
    {
        return memberSignMapper.selectMemberSignById(id);
    }

    /**
     * 查询用户打卡列表
     * 
     * @param memberSign 用户打卡
     * @return 用户打卡
     */
    @Override
    public List<MemberSign> selectMemberSignList(MemberSign memberSign)
    {
        return memberSignMapper.selectMemberSignList(memberSign);
    }

    /**
     * 新增用户打卡
     * 
     * @param memberSign 用户打卡
     * @return 结果
     */
    @Override
    public int insertMemberSign(MemberSign memberSign)
    {
        return memberSignMapper.insertMemberSign(memberSign);
    }

    /**
     * 修改用户打卡
     * 
     * @param memberSign 用户打卡
     * @return 结果
     */
    @Override
    public int updateMemberSign(MemberSign memberSign)
    {
        memberSign.setUpdateTime(DateUtils.getNowDate());
        return memberSignMapper.updateMemberSign(memberSign);
    }

    /**
     * 删除用户打卡对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMemberSignByIds(String ids)
    {
        return memberSignMapper.deleteMemberSignByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户打卡信息
     * 
     * @param id 用户打卡ID
     * @return 结果
     */
    @Override
    public int deleteMemberSignById(String id)
    {
        return memberSignMapper.deleteMemberSignById(id);
    }
}
