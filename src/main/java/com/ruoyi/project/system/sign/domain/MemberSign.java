package com.ruoyi.project.system.sign.domain;

import java.math.BigDecimal;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 用户打卡对象 member_sign
 * 
 * @author LCL
 * @date 2020-08-15
 */
public class MemberSign extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户主键ID */
    private String id;

    /** 累计打卡总数 */
    @Excel(name = "累计打卡总数")
    private Integer total;

    /** 连续打卡天数 */
    @Excel(name = "连续打卡天数")
    private Integer continuity;

    /** 累计获取积分 */
    @Excel(name = "累计获取积分")
    private BigDecimal sumCoin;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setTotal(Integer total)
    {
        this.total = total;
    }

    public Integer getTotal()
    {
        return total;
    }
    public void setContinuity(Integer continuity)
    {
        this.continuity = continuity;
    }

    public Integer getContinuity()
    {
        return continuity;
    }
    public void setSumCoin(BigDecimal sumCoin)
    {
        this.sumCoin = sumCoin;
    }

    public BigDecimal getSumCoin()
    {
        return sumCoin;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("total", getTotal())
            .append("continuity", getContinuity())
            .append("updateTime", getUpdateTime())
            .append("sumCoin", getSumCoin())
            .toString();
    }
}
