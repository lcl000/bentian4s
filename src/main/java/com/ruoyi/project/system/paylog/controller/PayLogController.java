package com.ruoyi.project.system.paylog.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.paylog.domain.PayLog;
import com.ruoyi.project.system.paylog.service.IPayLogService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 支付记录Controller
 * 
 * @author LCL
 * @date 2020-07-01
 */
@Controller
@RequestMapping("/system/log")
public class PayLogController extends BaseController
{
    private String prefix = "system/log";

    @Autowired
    private IPayLogService payLogService;

    @RequiresPermissions("system:log:view")
    @GetMapping()
    public String log()
    {
        return prefix + "/log";
    }

    /**
     * 查询支付记录列表
     */
    @RequiresPermissions("system:log:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PayLog payLog)
    {
        startPage();
        List<PayLog> list = payLogService.selectPayLogList(payLog);
        return getDataTable(list);
    }

    /**
     * 导出支付记录列表
     */
    @RequiresPermissions("system:log:export")
    @Log(title = "支付记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PayLog payLog)
    {
        List<PayLog> list = payLogService.selectPayLogList(payLog);
        ExcelUtil<PayLog> util = new ExcelUtil<PayLog>(PayLog.class);
        return util.exportExcel(list, "log");
    }

    /**
     * 新增支付记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存支付记录
     */
    @RequiresPermissions("system:log:add")
    @Log(title = "支付记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PayLog payLog)
    {
        return toAjax(payLogService.insertPayLog(payLog));
    }

    /**
     * 修改支付记录
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        PayLog payLog = payLogService.selectPayLogById(id);
        mmap.put("payLog", payLog);
        return prefix + "/edit";
    }

    /**
     * 修改保存支付记录
     */
    @RequiresPermissions("system:log:edit")
    @Log(title = "支付记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PayLog payLog)
    {
        return toAjax(payLogService.updatePayLog(payLog));
    }

    /**
     * 删除支付记录
     */
    @RequiresPermissions("system:log:remove")
    @Log(title = "支付记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(payLogService.deletePayLogByIds(ids));
    }
}
