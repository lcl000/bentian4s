package com.ruoyi.project.system.paylog.domain;

import java.math.BigDecimal;
import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 支付记录对象 pay_log
 *
 * @author LCL
 * @date 2020-07-01
 */
public class PayLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 微信官方支付单号 */
    @Excel(name = "微信官方支付单号")
    private String outTradeNo;

    /** 状态: 0 未支付 1 已支付 */
    @Excel(name = "状态: 0 未支付 1 已支付")
    private Integer status;

    /** 相关资源ID */
    @Excel(name = "相关资源ID")
    private String resId;

    /** 支付类型: 0 订单支付 1 会员支付 */
    @Excel(name = "支付类型: 0 订单支付 1 会员支付")
    private Integer type;

    /** 会员id */
    @Excel(name = "会员id")
    private String memberId;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date created;
    /** 金额 */
    @Excel(name = "金额")
    private BigDecimal price;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setOutTradeNo(String outTradeNo)
    {
        this.outTradeNo = outTradeNo;
    }

    public String getOutTradeNo()
    {
        return outTradeNo;
    }
    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Integer getStatus()
    {
        return status;
    }
    public void setResId(String resId)
    {
        this.resId = resId;
    }

    public String getResId()
    {
        return resId;
    }
    public void setType(Integer type)
    {
        this.type = type;
    }

    public Integer getType()
    {
        return type;
    }
    public void setMemberId(String memberId)
    {
        this.memberId = memberId;
    }

    public String getMemberId()
    {
        return memberId;
    }
    public void setCreated(Date created)
    {
        this.created = created;
    }

    public Date getCreated()
    {
        return created;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("outTradeNo", getOutTradeNo())
                .append("status", getStatus())
                .append("resId", getResId())
                .append("type", getType())
                .append("memberId", getMemberId())
                .append("created", getCreated())
                .append("price",getPrice())
                .toString();
    }
}
