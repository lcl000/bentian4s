package com.ruoyi.project.system.paylog.mapper;

import java.util.List;
import com.ruoyi.project.system.paylog.domain.PayLog;

/**
 * 支付记录Mapper接口
 * 
 * @author LCL
 * @date 2020-07-01
 */
public interface PayLogMapper 
{
    /**
     * 查询支付记录
     * 
     * @param id 支付记录ID
     * @return 支付记录
     */
    public PayLog selectPayLogById(String id);

    /**
     * 查询支付记录列表
     * 
     * @param payLog 支付记录
     * @return 支付记录集合
     */
    public List<PayLog> selectPayLogList(PayLog payLog);

    /**
     * 新增支付记录
     * 
     * @param payLog 支付记录
     * @return 结果
     */
    public int insertPayLog(PayLog payLog);

    /**
     * 修改支付记录
     * 
     * @param payLog 支付记录
     * @return 结果
     */
    public int updatePayLog(PayLog payLog);

    /**
     * 删除支付记录
     * 
     * @param id 支付记录ID
     * @return 结果
     */
    public int deletePayLogById(String id);

    /**
     * 批量删除支付记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePayLogByIds(String[] ids);

    PayLog selectPayLogByNo(String outTradeNo);
}
