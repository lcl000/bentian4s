package com.ruoyi.project.system.paylog.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.paylog.mapper.PayLogMapper;
import com.ruoyi.project.system.paylog.domain.PayLog;
import com.ruoyi.project.system.paylog.service.IPayLogService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 支付记录Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-01
 */
@Service
public class PayLogServiceImpl implements IPayLogService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private PayLogMapper payLogMapper;

    /**
     * 查询支付记录
     * 
     * @param id 支付记录ID
     * @return 支付记录
     */
    @Override
    public PayLog selectPayLogById(String id)
    {
        return payLogMapper.selectPayLogById(id);
    }

    /**
     * 查询支付记录列表
     * 
     * @param payLog 支付记录
     * @return 支付记录
     */
    @Override
    public List<PayLog> selectPayLogList(PayLog payLog)
    {
        return payLogMapper.selectPayLogList(payLog);
    }

    /**
     * 新增支付记录
     * 
     * @param payLog 支付记录
     * @return 结果
     */
    @Override
    public int insertPayLog(PayLog payLog)
    {
        payLog.setId(this.idGererateFactory.nextStringId());
        return payLogMapper.insertPayLog(payLog);
    }

    /**
     * 修改支付记录
     * 
     * @param payLog 支付记录
     * @return 结果
     */
    @Override
    public int updatePayLog(PayLog payLog)
    {
        return payLogMapper.updatePayLog(payLog);
    }

    /**
     * 删除支付记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePayLogByIds(String ids)
    {
        return payLogMapper.deletePayLogByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除支付记录信息
     * 
     * @param id 支付记录ID
     * @return 结果
     */
    @Override
    public int deletePayLogById(String id)
    {
        return payLogMapper.deletePayLogById(id);
    }

    @Override
    public PayLog selectPayLogByNo(String outTradeNo) {
        return this.payLogMapper.selectPayLogByNo(outTradeNo);
    }
}
