package com.ruoyi.project.system.used.appointment.domain;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 二手车预约对象 car_used_appointment
 * 
 * @author LCL
 * @date 2020-07-31
 */
public class CarUsedAppointment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 二手车主键ID */
    @Excel(name = "二手车主键ID")
    private String usedId;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 电话 */
    @Excel(name = "电话")
    private String mobile;

    /** 预约时间 */
    @Excel(name = "预约时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date appointmentTime;

    /** 车型 */
    @Excel(name = "车型")
    private String carType;

    /** 0 未体验 1 已体验 2 废弃 */
    @Excel(name = "0 未体验 1 已体验 2 废弃")
    private Integer status;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setUsedId(String usedId) 
    {
        this.usedId = usedId;
    }

    public String getUsedId() 
    {
        return usedId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setMobile(String mobile) 
    {
        this.mobile = mobile;
    }

    public String getMobile() 
    {
        return mobile;
    }
    public void setAppointmentTime(Date appointmentTime) 
    {
        this.appointmentTime = appointmentTime;
    }

    public Date getAppointmentTime() 
    {
        return appointmentTime;
    }
    public void setCarType(String carType) 
    {
        this.carType = carType;
    }

    public String getCarType() 
    {
        return carType;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("usedId", getUsedId())
            .append("name", getName())
            .append("mobile", getMobile())
            .append("appointmentTime", getAppointmentTime())
            .append("carType", getCarType())
            .append("status", getStatus())
            .toString();
    }
}
