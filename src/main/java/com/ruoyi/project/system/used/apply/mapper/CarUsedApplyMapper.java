package com.ruoyi.project.system.used.apply.mapper;

import java.util.List;
import com.ruoyi.project.system.used.apply.domain.CarUsedApply;
import com.ruoyi.project.system.used.apply.dto.CarUsedApplyDto;

/**
 * 二手车申请Mapper接口
 * 
 * @author LCL
 * @date 2020-07-31
 */
public interface CarUsedApplyMapper 
{
    /**
     * 查询二手车申请
     * 
     * @param id 二手车申请ID
     * @return 二手车申请
     */
    public CarUsedApply selectCarUsedApplyById(String id);

    /**
     * 查询二手车申请列表
     * 
     * @param carUsedApply 二手车申请
     * @return 二手车申请集合
     */
    public List<CarUsedApplyDto> selectCarUsedApplyList(CarUsedApply carUsedApply);

    /**
     * 新增二手车申请
     * 
     * @param carUsedApply 二手车申请
     * @return 结果
     */
    public int insertCarUsedApply(CarUsedApply carUsedApply);

    /**
     * 修改二手车申请
     * 
     * @param carUsedApply 二手车申请
     * @return 结果
     */
    public int updateCarUsedApply(CarUsedApply carUsedApply);

    /**
     * 删除二手车申请
     * 
     * @param id 二手车申请ID
     * @return 结果
     */
    public int deleteCarUsedApplyById(String id);

    /**
     * 批量删除二手车申请
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCarUsedApplyByIds(String[] ids);
}
