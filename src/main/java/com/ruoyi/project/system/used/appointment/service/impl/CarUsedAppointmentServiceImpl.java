package com.ruoyi.project.system.used.appointment.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.used.appointment.dto.CarUsedAppointmentDto;
import com.ruoyi.project.system.used.appointment.mapper.CarUsedAppointmentMapper;
import com.ruoyi.project.system.used.appointment.domain.CarUsedAppointment;
import com.ruoyi.project.system.used.appointment.service.ICarUsedAppointmentService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 二手车预约Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-31
 */
@Service
public class CarUsedAppointmentServiceImpl implements ICarUsedAppointmentService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private CarUsedAppointmentMapper carUsedAppointmentMapper;

    /**
     * 查询二手车预约
     * 
     * @param id 二手车预约ID
     * @return 二手车预约
     */
    @Override
    public CarUsedAppointment selectCarUsedAppointmentById(String id)
    {
        return carUsedAppointmentMapper.selectCarUsedAppointmentById(id);
    }

    /**
     * 查询二手车预约列表
     * 
     * @param carUsedAppointment 二手车预约
     * @return 二手车预约
     */
    @Override
    public List<CarUsedAppointmentDto> selectCarUsedAppointmentList(CarUsedAppointment carUsedAppointment)
    {
        return carUsedAppointmentMapper.selectCarUsedAppointmentList(carUsedAppointment);
    }

    /**
     * 新增二手车预约
     * 
     * @param carUsedAppointment 二手车预约
     * @return 结果
     */
    @Override
    public int insertCarUsedAppointment(CarUsedAppointment carUsedAppointment)
    {
        carUsedAppointment.setId(this.idGererateFactory.nextStringId());
        return carUsedAppointmentMapper.insertCarUsedAppointment(carUsedAppointment);
    }

    /**
     * 修改二手车预约
     * 
     * @param carUsedAppointment 二手车预约
     * @return 结果
     */
    @Override
    public int updateCarUsedAppointment(CarUsedAppointment carUsedAppointment)
    {
        return carUsedAppointmentMapper.updateCarUsedAppointment(carUsedAppointment);
    }

    /**
     * 删除二手车预约对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCarUsedAppointmentByIds(String ids)
    {
        return carUsedAppointmentMapper.deleteCarUsedAppointmentByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除二手车预约信息
     * 
     * @param id 二手车预约ID
     * @return 结果
     */
    @Override
    public int deleteCarUsedAppointmentById(String id)
    {
        return carUsedAppointmentMapper.deleteCarUsedAppointmentById(id);
    }
}
