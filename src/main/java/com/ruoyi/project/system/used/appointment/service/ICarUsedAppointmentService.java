package com.ruoyi.project.system.used.appointment.service;

import java.util.List;
import com.ruoyi.project.system.used.appointment.domain.CarUsedAppointment;
import com.ruoyi.project.system.used.appointment.dto.CarUsedAppointmentDto;

/**
 * 二手车预约Service接口
 * 
 * @author LCL
 * @date 2020-07-31
 */
public interface ICarUsedAppointmentService 
{
    /**
     * 查询二手车预约
     * 
     * @param id 二手车预约ID
     * @return 二手车预约
     */
    public CarUsedAppointment selectCarUsedAppointmentById(String id);

    /**
     * 查询二手车预约列表
     * 
     * @param carUsedAppointment 二手车预约
     * @return 二手车预约集合
     */
    public List<CarUsedAppointmentDto> selectCarUsedAppointmentList(CarUsedAppointment carUsedAppointment);

    /**
     * 新增二手车预约
     * 
     * @param carUsedAppointment 二手车预约
     * @return 结果
     */
    public int insertCarUsedAppointment(CarUsedAppointment carUsedAppointment);

    /**
     * 修改二手车预约
     * 
     * @param carUsedAppointment 二手车预约
     * @return 结果
     */
    public int updateCarUsedAppointment(CarUsedAppointment carUsedAppointment);

    /**
     * 批量删除二手车预约
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCarUsedAppointmentByIds(String ids);

    /**
     * 删除二手车预约信息
     * 
     * @param id 二手车预约ID
     * @return 结果
     */
    public int deleteCarUsedAppointmentById(String id);
}
