package com.ruoyi.project.system.used.appointment.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.car.type.service.ICarTypeService;
import com.ruoyi.project.system.used.appointment.domain.CarUsedAppointment;
import com.ruoyi.project.system.used.appointment.dto.CarUsedAppointmentDto;
import com.ruoyi.project.system.used.appointment.service.ICarUsedAppointmentService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 二手车预约Controller
 * 
 * @author LCL
 * @date 2020-07-31
 */
@Controller
@RequestMapping("/system/used/appointment")
public class CarUsedAppointmentController extends BaseController
{
    private String prefix = "system/used/appointment";

    @Autowired
    private ICarUsedAppointmentService carUsedAppointmentService;

    @Autowired
    private ICarTypeService carTypeService;

    @RequiresPermissions("system/userd:appointment:view")
    @GetMapping()
    public String appointment(ModelMap map)
    {
        map.put("carTypeList",this.carTypeService.findAll());
        return prefix + "/appointment";
    }

    /**
     * 查询二手车预约列表
     */
    @RequiresPermissions("system/userd:appointment:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CarUsedAppointment carUsedAppointment)
    {
        startPage();
        List<CarUsedAppointmentDto> list = carUsedAppointmentService.selectCarUsedAppointmentList(carUsedAppointment);
        return getDataTable(list);
    }

    /**
     * 导出二手车预约列表
     */
    @RequiresPermissions("system/userd:appointment:export")
    @Log(title = "二手车预约", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CarUsedAppointment carUsedAppointment)
    {
        List<CarUsedAppointmentDto> list = carUsedAppointmentService.selectCarUsedAppointmentList(carUsedAppointment);
        ExcelUtil<CarUsedAppointmentDto> util = new ExcelUtil<CarUsedAppointmentDto>(CarUsedAppointmentDto.class);
        return util.exportExcel(list, "appointment");
    }

    /**
     * 新增二手车预约
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存二手车预约
     */
    @RequiresPermissions("system/userd:appointment:add")
    @Log(title = "二手车预约", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CarUsedAppointment carUsedAppointment)
    {
        return toAjax(carUsedAppointmentService.insertCarUsedAppointment(carUsedAppointment));
    }

    /**
     * 修改二手车预约
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        CarUsedAppointment carUsedAppointment = carUsedAppointmentService.selectCarUsedAppointmentById(id);
        mmap.put("carUsedAppointment", carUsedAppointment);
        return prefix + "/edit";
    }

    /**
     * 修改保存二手车预约
     */
    @RequiresPermissions("system/userd:appointment:edit")
    @Log(title = "二手车预约", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CarUsedAppointment carUsedAppointment)
    {
        return toAjax(carUsedAppointmentService.updateCarUsedAppointment(carUsedAppointment));
    }

    /**
     * 删除二手车预约
     */
    @RequiresPermissions("system/userd:appointment:remove")
    @Log(title = "二手车预约", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(carUsedAppointmentService.deleteCarUsedAppointmentByIds(ids));
    }



    @PostMapping("/agree/{id}")
    @ResponseBody
    public AjaxResult agree(@PathVariable String id){
        CarUsedAppointment carUsedAppointment = this.carUsedAppointmentService.selectCarUsedAppointmentById(id);
        if(carUsedAppointment==null){
            return AjaxResult.error();
        }
        CarUsedAppointment update = new CarUsedAppointment();
        update.setId(id);
        update.setStatus(1);
        this.carUsedAppointmentService.updateCarUsedAppointment(update);

        return AjaxResult.success();
    }

    @PostMapping("/refuse/{id}")
    @ResponseBody
    public AjaxResult refuse(@PathVariable String id){
        CarUsedAppointment carUsedAppointment = this.carUsedAppointmentService.selectCarUsedAppointmentById(id);
        if(carUsedAppointment==null){
            return AjaxResult.error();
        }
        CarUsedAppointment update = new CarUsedAppointment();
        update.setId(id);
        update.setStatus(2);
        this.carUsedAppointmentService.updateCarUsedAppointment(update);

        return AjaxResult.success();
    }


}
