package com.ruoyi.project.system.used.used.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.brand.service.IBrandService;
import com.ruoyi.project.system.car.img.domain.CarImg;
import com.ruoyi.project.system.car.img.service.ICarImgService;
import com.ruoyi.project.system.car.type.domain.CarType;
import com.ruoyi.project.system.car.type.service.ICarTypeService;
import com.ruoyi.project.system.dealer.service.IDealerService;
import com.ruoyi.project.system.used.used.domain.CarUsed;
import com.ruoyi.project.system.used.used.dto.UsedDto;
import com.ruoyi.project.system.used.used.service.ICarUsedService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 二手车Controller
 * 
 * @author LCL
 * @date 2020-07-31
 */
@Controller
@RequestMapping("/system/used")
public class CarUsedController extends BaseController
{
    private String prefix = "system/used/used";

    @Autowired
    private ICarUsedService carUsedService;

    @Autowired
    private IBrandService brandService;
    @Autowired
    private ICarTypeService carTypeService;
    @Autowired
    private IDealerService dealerService;
    @Autowired
    private ICarImgService carImgService;

    @RequiresPermissions("system:used:view")
    @GetMapping()
    public String used(ModelMap map)
    {
        map.put("brand",this.brandService.findAll());
        map.put("type",this.carTypeService.findAll());
        map.put("dealerList",this.dealerService.findAll(null));
        return prefix + "/used";
    }

    /**
     * 查询二手车列表
     */
    @RequiresPermissions("system:used:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CarUsed carUsed)
    {
        startPage();
        List<UsedDto> list = carUsedService.selectCarUsedList(carUsed);
        return getDataTable(list);
    }

    /**
     * 导出二手车列表
     */
    @RequiresPermissions("system:used:export")
    @Log(title = "二手车", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CarUsed carUsed)
    {
        List<UsedDto> list = carUsedService.selectCarUsedList(carUsed);
        ExcelUtil<UsedDto> util = new ExcelUtil<UsedDto>(UsedDto.class);
        return util.exportExcel(list, "used");
    }

    /**
     * 新增二手车
     */
    @GetMapping("/add")
    public String add(ModelMap map)
    {
        map.put("brand",this.brandService.findAll());
        map.put("dealerList",this.dealerService.findAll(null));
        return prefix + "/add";
    }

    /**
     * 新增保存二手车
     */
    @RequiresPermissions("system:used:add")
    @Log(title = "二手车", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CarUsed carUsed,String[] carImg0)
    {
        //添加二手车信息
        carUsedService.insertCarUsed(carUsed);

        //添加轮播图
        for (int j = 0; j <carImg0.length ; j++) {
            CarImg carImg = new CarImg();
            carImg.setSort(j);
            carImg.setCarId(carUsed.getId());
            carImg.setUrl(carImg0[j]);
            carImg.setType(0);
            this.carImgService.insertCarImg(carImg);
        }
        return AjaxResult.success();
    }

    /**
     * 修改二手车
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        UsedDto carUsed = carUsedService.selectCarUsedById(id);
        mmap.put("carUsed", carUsed);
        mmap.put("brand",this.brandService.findAll());
        mmap.put("dealerList",this.dealerService.findAll(null));
        CarImg carImg = new CarImg();
        carImg.setCarId(id);
        carImg.setType(0);
        mmap.put("carImg0", this.carImgService.selectCarImgList(carImg));
        return prefix + "/edit";
    }

    /**
     * 修改保存二手车
     */
    @RequiresPermissions("system:used:edit")
    @Log(title = "二手车", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CarUsed carUsed,String[] carImg0)
    {
        //判断数据是否存在
        CarUsed oldCar = this.carUsedService.selectCarUsedById(carUsed.getId());
        if(oldCar==null){
            return AjaxResult.error("数据不存在");
        }
        //清除原本存在的图片数据
        this.carImgService.delByCid(carUsed.getId());
        //更新汽车信息
        this.carUsedService.updateCarUsed(carUsed);
        //添加轮播图
        for (int j = 0; j <carImg0.length ; j++) {
            CarImg carImg = new CarImg();
            carImg.setSort(j);
            carImg.setCarId(carUsed.getId());
            carImg.setUrl(carImg0[j]);
            carImg.setType(0);
            this.carImgService.insertCarImg(carImg);
        }
        return AjaxResult.success();
    }

    /**
     * 删除二手车
     */
    @RequiresPermissions("system:used:remove")
    @Log(title = "二手车", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(carUsedService.deleteCarUsedByIds(ids));
    }



    @GetMapping("/type/{brandId}/all")
    @ResponseBody
    public AjaxResult findAllByBrandId(@PathVariable String brandId){
        CarType carType = new CarType();
        carType.setBrandId(brandId);
        return AjaxResult.success(this.carTypeService.selectCarTypeList(carType));
    }
}
