package com.ruoyi.project.system.used.apply.domain;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 二手车申请对象 car_used_apply
 * 
 * @author LCL
 * @date 2020-07-31
 */
public class CarUsedApply extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 车型ID */
    @Excel(name = "车型ID")
    private String carType;

    /** 行驶里程(万公里) */
    @Excel(name = "行驶里程(万公里)")
    private Double mileage;

    /** 上牌日期 */
    @Excel(name = "上牌日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date registrationTime;

    /** 车辆所在地 */
    @Excel(name = "车辆所在地")
    private String carAddress;

    /** 上牌城市 */
    @Excel(name = "上牌城市")
    private String registrationCity;

    /** 车辆概括 */
    @Excel(name = "车辆概括")
    private String describe;

    /** 状态:0 未处理 1 已处理 */
    @Excel(name = "状态:0 未处理 1 已处理")
    private Integer status;

    //联系人姓名
    private String name;
    //联系人电话
    private String mobile;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setCarType(String carType) 
    {
        this.carType = carType;
    }

    public String getCarType() 
    {
        return carType;
    }
    public void setMileage(Double mileage) 
    {
        this.mileage = mileage;
    }

    public Double getMileage() 
    {
        return mileage;
    }
    public void setRegistrationTime(Date registrationTime) 
    {
        this.registrationTime = registrationTime;
    }

    public Date getRegistrationTime() 
    {
        return registrationTime;
    }
    public void setCarAddress(String carAddress) 
    {
        this.carAddress = carAddress;
    }

    public String getCarAddress() 
    {
        return carAddress;
    }
    public void setRegistrationCity(String registrationCity) 
    {
        this.registrationCity = registrationCity;
    }

    public String getRegistrationCity() 
    {
        return registrationCity;
    }
    public void setDescribe(String describe) 
    {
        this.describe = describe;
    }

    public String getDescribe() 
    {
        return describe;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("carType", getCarType())
            .append("mileage", getMileage())
            .append("registrationTime", getRegistrationTime())
            .append("carAddress", getCarAddress())
            .append("registrationCity", getRegistrationCity())
            .append("describe", getDescribe())
            .append("status", getStatus())
            .toString();
    }
}
