package com.ruoyi.project.system.used.apply.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.car.type.service.ICarTypeService;
import com.ruoyi.project.system.used.apply.domain.CarUsedApply;
import com.ruoyi.project.system.used.apply.dto.CarUsedApplyDto;
import com.ruoyi.project.system.used.apply.service.ICarUsedApplyService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 二手车申请Controller
 * 
 * @author LCL
 * @date 2020-07-31
 */
@Controller
@RequestMapping("/system/used/apply")
public class CarUsedApplyController extends BaseController
{
    private String prefix = "system/used/apply";

    @Autowired
    private ICarUsedApplyService carUsedApplyService;

    @Autowired
    private ICarTypeService carTypeService;

    @RequiresPermissions("system/userd:apply:view")
    @GetMapping()
    public String apply(ModelMap map)
    {
        map.put("carTypeList",this.carTypeService.findAll());
        return prefix + "/apply";
    }

    /**
     * 查询二手车申请列表
     */
    @RequiresPermissions("system/userd:apply:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CarUsedApply carUsedApply)
    {
        startPage();
        List<CarUsedApplyDto> list = carUsedApplyService.selectCarUsedApplyList(carUsedApply);
        return getDataTable(list);
    }

    /**
     * 导出二手车申请列表
     */
    @RequiresPermissions("system/userd:apply:export")
    @Log(title = "二手车申请", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CarUsedApply carUsedApply)
    {
        List<CarUsedApplyDto> list = carUsedApplyService.selectCarUsedApplyList(carUsedApply);
        ExcelUtil<CarUsedApplyDto> util = new ExcelUtil<CarUsedApplyDto>(CarUsedApplyDto.class);
        return util.exportExcel(list, "apply");
    }

    /**
     * 新增二手车申请
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存二手车申请
     */
    @RequiresPermissions("system/userd:apply:add")
    @Log(title = "二手车申请", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CarUsedApply carUsedApply)
    {
        return toAjax(carUsedApplyService.insertCarUsedApply(carUsedApply));
    }

    /**
     * 修改二手车申请
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        CarUsedApply carUsedApply = carUsedApplyService.selectCarUsedApplyById(id);
        mmap.put("carUsedApply", carUsedApply);
        return prefix + "/edit";
    }

    /**
     * 修改保存二手车申请
     */
    @RequiresPermissions("system/userd:apply:edit")
    @Log(title = "二手车申请", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CarUsedApply carUsedApply)
    {
        return toAjax(carUsedApplyService.updateCarUsedApply(carUsedApply));
    }

    /**
     * 删除二手车申请
     */
    @RequiresPermissions("system/userd:apply:remove")
    @Log(title = "二手车申请", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(carUsedApplyService.deleteCarUsedApplyByIds(ids));
    }



    @PostMapping("/agree/{id}")
    @ResponseBody
    public AjaxResult agree(@PathVariable String id){
        CarUsedApply carUsedApply = this.carUsedApplyService.selectCarUsedApplyById(id);
        if(carUsedApply==null){
            return AjaxResult.error();
        }
        CarUsedApply update = new CarUsedApply();
        update.setId(id);
        update.setStatus(1);
        this.carUsedApplyService.updateCarUsedApply(update);

        return AjaxResult.success();
    }
}
