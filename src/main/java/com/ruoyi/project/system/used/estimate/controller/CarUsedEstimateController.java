package com.ruoyi.project.system.used.estimate.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.brand.service.IBrandService;
import com.ruoyi.project.system.car.type.service.ICarTypeService;
import com.ruoyi.project.system.used.estimate.domain.CarUsedEstimate;
import com.ruoyi.project.system.used.estimate.dto.CarUsedEstimateDto;
import com.ruoyi.project.system.used.estimate.service.ICarUsedEstimateService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 二手车评估Controller
 * 
 * @author LCL
 * @date 2020-07-31
 */
@Controller
@RequestMapping("/system/used/estimate")
public class CarUsedEstimateController extends BaseController
{
    private String prefix = "system/used/estimate";

    @Autowired
    private ICarUsedEstimateService carUsedEstimateService;

    @Autowired
    private ICarTypeService carTypeService;

    @Autowired
    private IBrandService brandService;

    @RequiresPermissions("system/userd:estimate:view")
    @GetMapping()
    public String estimate(ModelMap map)
    {
        map.put("brandList",this.brandService.findAll());
        map.put("carTypeList",this.carTypeService.findAll());
        return prefix + "/estimate";
    }

    /**
     * 查询二手车评估列表
     */
    @RequiresPermissions("system/userd:estimate:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CarUsedEstimate carUsedEstimate)
    {
        startPage();
        List<CarUsedEstimateDto> list = carUsedEstimateService.selectCarUsedEstimateList(carUsedEstimate);
        return getDataTable(list);
    }

    /**
     * 导出二手车评估列表
     */
    @RequiresPermissions("system/userd:estimate:export")
    @Log(title = "二手车评估", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CarUsedEstimate carUsedEstimate)
    {
        List<CarUsedEstimateDto> list = carUsedEstimateService.selectCarUsedEstimateList(carUsedEstimate);
        ExcelUtil<CarUsedEstimateDto> util = new ExcelUtil<CarUsedEstimateDto>(CarUsedEstimateDto.class);
        return util.exportExcel(list, "estimate");
    }

    /**
     * 新增二手车评估
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存二手车评估
     */
    @RequiresPermissions("system/userd:estimate:add")
    @Log(title = "二手车评估", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CarUsedEstimate carUsedEstimate)
    {
        return toAjax(carUsedEstimateService.insertCarUsedEstimate(carUsedEstimate));
    }

    /**
     * 修改二手车评估
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        CarUsedEstimate carUsedEstimate = carUsedEstimateService.selectCarUsedEstimateById(id);
        mmap.put("carUsedEstimate", carUsedEstimate);
        return prefix + "/edit";
    }

    /**
     * 修改保存二手车评估
     */
    @RequiresPermissions("system/userd:estimate:edit")
    @Log(title = "二手车评估", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CarUsedEstimate carUsedEstimate)
    {
        return toAjax(carUsedEstimateService.updateCarUsedEstimate(carUsedEstimate));
    }

    /**
     * 删除二手车评估
     */
    @RequiresPermissions("system/userd:estimate:remove")
    @Log(title = "二手车评估", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(carUsedEstimateService.deleteCarUsedEstimateByIds(ids));
    }




    @PostMapping("/agree/{id}")
    @ResponseBody
    public AjaxResult agree(@PathVariable String id){
        CarUsedEstimate carUsedEstimate = this.carUsedEstimateService.selectCarUsedEstimateById(id);
        if(carUsedEstimate==null){
            return AjaxResult.error();
        }
        CarUsedEstimate update = new CarUsedEstimate();
        update.setId(id);
        update.setStatus(1);
        this.carUsedEstimateService.updateCarUsedEstimate(update);

        return AjaxResult.success();
    }
}
