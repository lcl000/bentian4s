package com.ruoyi.project.system.used.used.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 二手车对象 car_used
 * 
 * @author LCL
 * @date 2020-07-31
 */
public class CarUsed extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 二手车名称 */
    @Excel(name = "二手车名称")
    private String name;

    /** 价格 */
    @Excel(name = "价格")
    private Double price;

    /** 品牌ID */
    @Excel(name = "品牌ID")
    private String brandId;

    /** 车型ID */
    @Excel(name = "车型ID")
    private String carType;

    /** 店铺主键ID */
    @Excel(name = "店铺主键ID")
    private String dealerId;

    /** 主图 */
    @Excel(name = "主图")
    private String mainUrl;

    /** 状态: 0 正常 1 下架 */
    @Excel(name = "状态: 0 正常 1 下架")
    private Integer status;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 详情图 **/
    private String infoImg;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPrice(Double price) 
    {
        this.price = price;
    }

    public Double getPrice() 
    {
        return price;
    }
    public void setBrandId(String brandId) 
    {
        this.brandId = brandId;
    }

    public String getBrandId() 
    {
        return brandId;
    }
    public void setCarType(String carType) 
    {
        this.carType = carType;
    }

    public String getCarType() 
    {
        return carType;
    }
    public void setDealerId(String dealerId) 
    {
        this.dealerId = dealerId;
    }

    public String getDealerId() 
    {
        return dealerId;
    }
    public void setMainUrl(String mainUrl) 
    {
        this.mainUrl = mainUrl;
    }

    public String getMainUrl() 
    {
        return mainUrl;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }


    public String getInfoImg() {
        return infoImg;
    }

    public void setInfoImg(String infoImg) {
        this.infoImg = infoImg;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("price", getPrice())
            .append("brandId", getBrandId())
            .append("carType", getCarType())
            .append("dealerId", getDealerId())
            .append("mainUrl", getMainUrl())
            .append("status", getStatus())
            .append("sort", getSort())
            .toString();
    }
}
