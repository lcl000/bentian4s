package com.ruoyi.project.system.used.apply.dto;

import com.ruoyi.project.system.used.apply.domain.CarUsedApply;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/8/5 16:05
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
public class CarUsedApplyDto extends CarUsedApply {

	private String carTypeName;

	public String getCarTypeName() {
		return carTypeName;
	}

	public void setCarTypeName(String carTypeName) {
		this.carTypeName = carTypeName;
	}
}
