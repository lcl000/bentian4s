package com.ruoyi.project.system.used.estimate.domain;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 二手车评估对象 car_used_estimate
 * 
 * @author LCL
 * @date 2020-07-31
 */
public class CarUsedEstimate extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 品牌ID */
    @Excel(name = "品牌ID")
    private String brandId;

    /** 车型ID */
    @Excel(name = "车型ID")
    private String carType;

    /** 年限 */
    @Excel(name = "年限", width = 30, dateFormat = "yyyy-MM-dd")
    private Date years;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String mobile;

    /** 状态: 0 未处理 1 已处理 */
    @Excel(name = "状态: 0 未处理 1 已处理")
    private Integer status;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setBrandId(String brandId) 
    {
        this.brandId = brandId;
    }

    public String getBrandId() 
    {
        return brandId;
    }
    public void setCarType(String carType) 
    {
        this.carType = carType;
    }

    public String getCarType() 
    {
        return carType;
    }
    public void setYears(Date years) 
    {
        this.years = years;
    }

    public Date getYears() 
    {
        return years;
    }
    public void setMobile(String mobile) 
    {
        this.mobile = mobile;
    }

    public String getMobile() 
    {
        return mobile;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("brandId", getBrandId())
            .append("carType", getCarType())
            .append("years", getYears())
            .append("mobile", getMobile())
            .append("status", getStatus())
            .toString();
    }
}
