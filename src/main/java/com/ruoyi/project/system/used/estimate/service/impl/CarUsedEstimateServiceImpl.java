package com.ruoyi.project.system.used.estimate.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.used.estimate.dto.CarUsedEstimateDto;
import com.ruoyi.project.system.used.estimate.mapper.CarUsedEstimateMapper;
import com.ruoyi.project.system.used.estimate.domain.CarUsedEstimate;
import com.ruoyi.project.system.used.estimate.service.ICarUsedEstimateService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 二手车评估Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-31
 */
@Service
public class CarUsedEstimateServiceImpl implements ICarUsedEstimateService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private CarUsedEstimateMapper carUsedEstimateMapper;

    /**
     * 查询二手车评估
     * 
     * @param id 二手车评估ID
     * @return 二手车评估
     */
    @Override
    public CarUsedEstimate selectCarUsedEstimateById(String id)
    {
        return carUsedEstimateMapper.selectCarUsedEstimateById(id);
    }

    /**
     * 查询二手车评估列表
     * 
     * @param carUsedEstimate 二手车评估
     * @return 二手车评估
     */
    @Override
    public List<CarUsedEstimateDto> selectCarUsedEstimateList(CarUsedEstimate carUsedEstimate)
    {
        return carUsedEstimateMapper.selectCarUsedEstimateList(carUsedEstimate);
    }

    /**
     * 新增二手车评估
     * 
     * @param carUsedEstimate 二手车评估
     * @return 结果
     */
    @Override
    public int insertCarUsedEstimate(CarUsedEstimate carUsedEstimate)
    {
        carUsedEstimate.setId(this.idGererateFactory.nextStringId());
        return carUsedEstimateMapper.insertCarUsedEstimate(carUsedEstimate);
    }

    /**
     * 修改二手车评估
     * 
     * @param carUsedEstimate 二手车评估
     * @return 结果
     */
    @Override
    public int updateCarUsedEstimate(CarUsedEstimate carUsedEstimate)
    {
        return carUsedEstimateMapper.updateCarUsedEstimate(carUsedEstimate);
    }

    /**
     * 删除二手车评估对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCarUsedEstimateByIds(String ids)
    {
        return carUsedEstimateMapper.deleteCarUsedEstimateByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除二手车评估信息
     * 
     * @param id 二手车评估ID
     * @return 结果
     */
    @Override
    public int deleteCarUsedEstimateById(String id)
    {
        return carUsedEstimateMapper.deleteCarUsedEstimateById(id);
    }
}
