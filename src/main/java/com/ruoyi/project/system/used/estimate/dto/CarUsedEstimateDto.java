package com.ruoyi.project.system.used.estimate.dto;

import com.ruoyi.project.system.used.estimate.domain.CarUsedEstimate;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/8/5 17:08
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
public class CarUsedEstimateDto extends CarUsedEstimate {
	private String brandName;

	private String carTypeName;

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getCarTypeName() {
		return carTypeName;
	}

	public void setCarTypeName(String carTypeName) {
		this.carTypeName = carTypeName;
	}
}
