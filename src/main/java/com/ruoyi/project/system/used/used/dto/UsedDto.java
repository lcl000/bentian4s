package com.ruoyi.project.system.used.used.dto;

import java.math.BigDecimal;

import com.ruoyi.project.system.used.used.domain.CarUsed;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/8/1 10:11
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
public class UsedDto extends CarUsed {
	//品牌名称
	private String brandName;
	//车型名称
	private String carTypeName;
	//门店名称
	private String dealerName;

	//最低价
	private BigDecimal minPrice;
	//最高价
	private BigDecimal maxPrice;


	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getCarTypeName() {
		return carTypeName;
	}

	public void setCarTypeName(String carTypeName) {
		this.carTypeName = carTypeName;
	}

	public BigDecimal getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(BigDecimal minPrice) {
		this.minPrice = minPrice;
	}

	public BigDecimal getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(BigDecimal maxPrice) {
		this.maxPrice = maxPrice;
	}

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}
}
