package com.ruoyi.project.system.used.used.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.used.used.dto.UsedDto;
import com.ruoyi.project.system.used.used.mapper.CarUsedMapper;
import com.ruoyi.project.system.used.used.domain.CarUsed;
import com.ruoyi.project.system.used.used.service.ICarUsedService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 二手车Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-31
 */
@Service
public class CarUsedServiceImpl implements ICarUsedService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private CarUsedMapper carUsedMapper;

    /**
     * 查询二手车
     * 
     * @param id 二手车ID
     * @return 二手车
     */
    @Override
    public UsedDto selectCarUsedById(String id)
    {
        return carUsedMapper.selectCarUsedById(id);
    }

    /**
     * 查询二手车列表
     * 
     * @param carUsed 二手车
     * @return 二手车
     */
    @Override
    public List<UsedDto> selectCarUsedList(CarUsed carUsed)
    {
        return carUsedMapper.selectCarUsedList(carUsed);
    }

    /**
     * 新增二手车
     * 
     * @param carUsed 二手车
     * @return 结果
     */
    @Override
    public int insertCarUsed(CarUsed carUsed)
    {
        carUsed.setId(this.idGererateFactory.nextStringId());
        return carUsedMapper.insertCarUsed(carUsed);
    }

    /**
     * 修改二手车
     * 
     * @param carUsed 二手车
     * @return 结果
     */
    @Override
    public int updateCarUsed(CarUsed carUsed)
    {
        return carUsedMapper.updateCarUsed(carUsed);
    }

    /**
     * 删除二手车对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCarUsedByIds(String ids)
    {
        return carUsedMapper.deleteCarUsedByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除二手车信息
     * 
     * @param id 二手车ID
     * @return 结果
     */
    @Override
    public int deleteCarUsedById(String id)
    {
        return carUsedMapper.deleteCarUsedById(id);
    }

    @Override
    public List<CarUsed> findListByUsedDto(UsedDto car) {
        return this.carUsedMapper.findListByUsedDto(car);
    }
}
