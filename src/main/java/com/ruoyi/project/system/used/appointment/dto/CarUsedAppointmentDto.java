package com.ruoyi.project.system.used.appointment.dto;

import com.ruoyi.project.system.used.appointment.domain.CarUsedAppointment;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/8/5 16:45
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
public class CarUsedAppointmentDto extends CarUsedAppointment {

	private String carTypeName;

	private String usedName;

	public String getCarTypeName() {
		return carTypeName;
	}

	public void setCarTypeName(String carTypeName) {
		this.carTypeName = carTypeName;
	}

	public String getUsedName() {
		return usedName;
	}

	public void setUsedName(String usedName) {
		this.usedName = usedName;
	}
}
