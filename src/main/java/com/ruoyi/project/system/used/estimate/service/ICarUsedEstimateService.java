package com.ruoyi.project.system.used.estimate.service;

import java.util.List;
import com.ruoyi.project.system.used.estimate.domain.CarUsedEstimate;
import com.ruoyi.project.system.used.estimate.dto.CarUsedEstimateDto;

/**
 * 二手车评估Service接口
 * 
 * @author LCL
 * @date 2020-07-31
 */
public interface ICarUsedEstimateService 
{
    /**
     * 查询二手车评估
     * 
     * @param id 二手车评估ID
     * @return 二手车评估
     */
    public CarUsedEstimate selectCarUsedEstimateById(String id);

    /**
     * 查询二手车评估列表
     * 
     * @param carUsedEstimate 二手车评估
     * @return 二手车评估集合
     */
    public List<CarUsedEstimateDto> selectCarUsedEstimateList(CarUsedEstimate carUsedEstimate);

    /**
     * 新增二手车评估
     * 
     * @param carUsedEstimate 二手车评估
     * @return 结果
     */
    public int insertCarUsedEstimate(CarUsedEstimate carUsedEstimate);

    /**
     * 修改二手车评估
     * 
     * @param carUsedEstimate 二手车评估
     * @return 结果
     */
    public int updateCarUsedEstimate(CarUsedEstimate carUsedEstimate);

    /**
     * 批量删除二手车评估
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCarUsedEstimateByIds(String ids);

    /**
     * 删除二手车评估信息
     * 
     * @param id 二手车评估ID
     * @return 结果
     */
    public int deleteCarUsedEstimateById(String id);
}
