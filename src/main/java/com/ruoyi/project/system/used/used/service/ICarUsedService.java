package com.ruoyi.project.system.used.used.service;

import java.util.List;
import com.ruoyi.project.system.used.used.domain.CarUsed;
import com.ruoyi.project.system.used.used.dto.UsedDto;

/**
 * 二手车Service接口
 * 
 * @author LCL
 * @date 2020-07-31
 */
public interface ICarUsedService 
{
    /**
     * 查询二手车
     * 
     * @param id 二手车ID
     * @return 二手车
     */
    public UsedDto selectCarUsedById(String id);

    /**
     * 查询二手车列表
     * 
     * @param carUsed 二手车
     * @return 二手车集合
     */
    public List<UsedDto> selectCarUsedList(CarUsed carUsed);

    /**
     * 新增二手车
     * 
     * @param carUsed 二手车
     * @return 结果
     */
    public int insertCarUsed(CarUsed carUsed);

    /**
     * 修改二手车
     * 
     * @param carUsed 二手车
     * @return 结果
     */
    public int updateCarUsed(CarUsed carUsed);

    /**
     * 批量删除二手车
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCarUsedByIds(String ids);

    /**
     * 删除二手车信息
     * 
     * @param id 二手车ID
     * @return 结果
     */
    public int deleteCarUsedById(String id);

	List<CarUsed> findListByUsedDto(UsedDto car);
}
