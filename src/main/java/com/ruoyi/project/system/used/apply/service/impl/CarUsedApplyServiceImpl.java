package com.ruoyi.project.system.used.apply.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.used.apply.dto.CarUsedApplyDto;
import com.ruoyi.project.system.used.apply.mapper.CarUsedApplyMapper;
import com.ruoyi.project.system.used.apply.domain.CarUsedApply;
import com.ruoyi.project.system.used.apply.service.ICarUsedApplyService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 二手车申请Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-31
 */
@Service
public class CarUsedApplyServiceImpl implements ICarUsedApplyService 
{
    @Autowired
    private IdGererateFactory idGererateFactory;
    @Autowired
    private CarUsedApplyMapper carUsedApplyMapper;

    /**
     * 查询二手车申请
     * 
     * @param id 二手车申请ID
     * @return 二手车申请
     */
    @Override
    public CarUsedApply selectCarUsedApplyById(String id)
    {
        return carUsedApplyMapper.selectCarUsedApplyById(id);
    }

    /**
     * 查询二手车申请列表
     * 
     * @param carUsedApply 二手车申请
     * @return 二手车申请
     */
    @Override
    public List<CarUsedApplyDto> selectCarUsedApplyList(CarUsedApply carUsedApply)
    {
        return carUsedApplyMapper.selectCarUsedApplyList(carUsedApply);
    }

    /**
     * 新增二手车申请
     * 
     * @param carUsedApply 二手车申请
     * @return 结果
     */
    @Override
    public int insertCarUsedApply(CarUsedApply carUsedApply)
    {
        carUsedApply.setId(this.idGererateFactory.nextStringId());
        return carUsedApplyMapper.insertCarUsedApply(carUsedApply);
    }

    /**
     * 修改二手车申请
     * 
     * @param carUsedApply 二手车申请
     * @return 结果
     */
    @Override
    public int updateCarUsedApply(CarUsedApply carUsedApply)
    {
        return carUsedApplyMapper.updateCarUsedApply(carUsedApply);
    }

    /**
     * 删除二手车申请对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCarUsedApplyByIds(String ids)
    {
        return carUsedApplyMapper.deleteCarUsedApplyByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除二手车申请信息
     * 
     * @param id 二手车申请ID
     * @return 结果
     */
    @Override
    public int deleteCarUsedApplyById(String id)
    {
        return carUsedApplyMapper.deleteCarUsedApplyById(id);
    }
}
