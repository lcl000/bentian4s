package com.ruoyi.project.system.params.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.params.mapper.TypeParamsMapper;
import com.ruoyi.project.system.params.domain.TypeParams;
import com.ruoyi.project.system.params.service.ITypeParamsService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 商品类型详细参数Service业务层处理
 * 
 * @author LCL
 * @date 2020-06-10
 */
@Service
public class TypeParamsServiceImpl implements ITypeParamsService 
{

    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private TypeParamsMapper typeParamsMapper;

    /**
     * 查询商品类型详细参数
     * 
     * @param id 商品类型详细参数ID
     * @return 商品类型详细参数
     */
    @Override
    public TypeParams selectTypeParamsById(String id)
    {
        return typeParamsMapper.selectTypeParamsById(id);
    }

    /**
     * 查询商品类型详细参数列表
     * 
     * @param typeParams 商品类型详细参数
     * @return 商品类型详细参数
     */
    @Override
    public List<TypeParams> selectTypeParamsList(TypeParams typeParams)
    {
        return typeParamsMapper.selectTypeParamsList(typeParams);
    }

    /**
     * 新增商品类型详细参数
     * 
     * @param typeParams 商品类型详细参数
     * @return 结果
     */
    @Override
    public int insertTypeParams(TypeParams typeParams)
    {
        typeParams.setId(this.idGererateFactory.nextStringId());
        return typeParamsMapper.insertTypeParams(typeParams);
    }

    /**
     * 修改商品类型详细参数
     * 
     * @param typeParams 商品类型详细参数
     * @return 结果
     */
    @Override
    public int updateTypeParams(TypeParams typeParams)
    {
        return typeParamsMapper.updateTypeParams(typeParams);
    }

    /**
     * 删除商品类型详细参数对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteTypeParamsByIds(String ids)
    {
        return typeParamsMapper.deleteTypeParamsByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商品类型详细参数信息
     * 
     * @param id 商品类型详细参数ID
     * @return 结果
     */
    @Override
    public int deleteTypeParamsById(String id)
    {
        return typeParamsMapper.deleteTypeParamsById(id);
    }

    @Override
    public int deleteByTypeId(String typeSpecId) {
        return this.typeParamsMapper.deleteByTypeId(typeSpecId);
    }

    @Override
    public List<TypeParams> findListByTypeId(String typeId) {
        return this.typeParamsMapper.findListByTypeId(typeId);
    }
}
