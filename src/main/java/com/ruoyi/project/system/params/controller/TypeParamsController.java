package com.ruoyi.project.system.params.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.params.domain.TypeParams;
import com.ruoyi.project.system.params.service.ITypeParamsService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 商品类型详细参数Controller
 * 
 * @author LCL
 * @date 2020-06-10
 */
@Controller
@RequestMapping("/system/params")
public class TypeParamsController extends BaseController
{
    private String prefix = "system/params";

    @Autowired
    private ITypeParamsService typeParamsService;

    @RequiresPermissions("system:params:view")
    @GetMapping()
    public String params()
    {
        return prefix + "/params";
    }

    /**
     * 查询商品类型详细参数列表
     */
    @RequiresPermissions("system:params:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TypeParams typeParams)
    {
        startPage();
        List<TypeParams> list = typeParamsService.selectTypeParamsList(typeParams);
        return getDataTable(list);
    }

    /**
     * 导出商品类型详细参数列表
     */
    @RequiresPermissions("system:params:export")
    @Log(title = "商品类型详细参数", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TypeParams typeParams)
    {
        List<TypeParams> list = typeParamsService.selectTypeParamsList(typeParams);
        ExcelUtil<TypeParams> util = new ExcelUtil<TypeParams>(TypeParams.class);
        return util.exportExcel(list, "params");
    }

    /**
     * 新增商品类型详细参数
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存商品类型详细参数
     */
    @RequiresPermissions("system:params:add")
    @Log(title = "商品类型详细参数", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TypeParams typeParams)
    {
        return toAjax(typeParamsService.insertTypeParams(typeParams));
    }

    /**
     * 修改商品类型详细参数
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        TypeParams typeParams = typeParamsService.selectTypeParamsById(id);
        mmap.put("typeParams", typeParams);
        return prefix + "/edit";
    }

    /**
     * 修改保存商品类型详细参数
     */
    @RequiresPermissions("system:params:edit")
    @Log(title = "商品类型详细参数", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TypeParams typeParams)
    {
        return toAjax(typeParamsService.updateTypeParams(typeParams));
    }

    /**
     * 删除商品类型详细参数
     */
    @RequiresPermissions("system:params:remove")
    @Log(title = "商品类型详细参数", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(typeParamsService.deleteTypeParamsByIds(ids));
    }
}
