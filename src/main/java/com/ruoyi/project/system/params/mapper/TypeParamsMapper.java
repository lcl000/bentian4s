package com.ruoyi.project.system.params.mapper;

import java.util.List;
import com.ruoyi.project.system.params.domain.TypeParams;

/**
 * 商品类型详细参数Mapper接口
 * 
 * @author LCL
 * @date 2020-06-10
 */
public interface TypeParamsMapper 
{
    /**
     * 查询商品类型详细参数
     * 
     * @param id 商品类型详细参数ID
     * @return 商品类型详细参数
     */
    public TypeParams selectTypeParamsById(String id);

    /**
     * 查询商品类型详细参数列表
     * 
     * @param typeParams 商品类型详细参数
     * @return 商品类型详细参数集合
     */
    public List<TypeParams> selectTypeParamsList(TypeParams typeParams);

    /**
     * 新增商品类型详细参数
     * 
     * @param typeParams 商品类型详细参数
     * @return 结果
     */
    public int insertTypeParams(TypeParams typeParams);

    /**
     * 修改商品类型详细参数
     * 
     * @param typeParams 商品类型详细参数
     * @return 结果
     */
    public int updateTypeParams(TypeParams typeParams);

    /**
     * 删除商品类型详细参数
     * 
     * @param id 商品类型详细参数ID
     * @return 结果
     */
    public int deleteTypeParamsById(String id);

    /**
     * 批量删除商品类型详细参数
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTypeParamsByIds(String[] ids);

	int deleteByTypeId(String typeSpecId);

	List<TypeParams> findListByTypeId(String typeId);
}
