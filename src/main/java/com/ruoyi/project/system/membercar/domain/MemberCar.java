package com.ruoyi.project.system.membercar.domain;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 用户车辆信息对象 member_car
 * 
 * @author LCL
 * @date 2020-08-10
 */
public class MemberCar extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 用户主键ID */
    private String memberId;

    /** 车主姓名 */
    @Excel(name = "车主姓名",sort = 2)
    private String name;

    /** 联系方式 */
    @Excel(name = "联系方式",sort = 3)
    private String mobile;

    /** 购车时间 */
    @Excel(name = "购车时间", width = 30, dateFormat = "yyyy-MM",sort = 6)
    private Date payTime;

    /** 车型 */
    private String carType;

    /** 车牌 */
    @Excel(name = "车牌",sort = 7)
    private String licensePlate;

    /** 车架编号 */
    @Excel(name = "车架编号",sort = 8)
    private String frameNo;

    /** 状态:0 待审核 1 审核通过 2 审核不通过 */
    @Excel(name = "状态:0 待审核 1 审核通过 2 审核不通过",sort = 11)
    private Integer status;

    /** 购买门店主键ID **/
    private String dealerId;

    @Excel(name = "用户昵称",sort = 1)
    private String memberName;

    @Excel(name = "绑车时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss",sort = 9)
    private Date created;

    private Long examineMember;

    @Excel(name = "审核时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss",sort = 13)
    private Date examineTime;

    private List postList;
    //查询用 开始时间
    private String btime;
    //查询用 结束时间
    private String etime;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setMobile(String mobile) 
    {
        this.mobile = mobile;
    }

    public String getMobile() 
    {
        return mobile;
    }
    public void setPayTime(Date payTime) 
    {
        this.payTime = payTime;
    }

    public Date getPayTime() 
    {
        return payTime;
    }
    public void setCarType(String carType) 
    {
        this.carType = carType;
    }

    public String getCarType() 
    {
        return carType;
    }
    public void setLicensePlate(String licensePlate) 
    {
        this.licensePlate = licensePlate;
    }

    public String getLicensePlate() 
    {
        return licensePlate;
    }
    public void setFrameNo(String frameNo) 
    {
        this.frameNo = frameNo;
    }

    public String getFrameNo() 
    {
        return frameNo;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public List getPostList() {
        return postList;
    }

    public void setPostList(List postList) {
        this.postList = postList;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Long getExamineMember() {
        return examineMember;
    }

    public void setExamineMember(Long examineMember) {
        this.examineMember = examineMember;
    }

    public Date getExamineTime() {
        return examineTime;
    }

    public void setExamineTime(Date examineTime) {
        this.examineTime = examineTime;
    }

    public String getBtime() {
        return btime;
    }

    public void setBtime(String btime) {
        this.btime = btime;
    }

    public String getEtime() {
        return etime;
    }

    public void setEtime(String etime) {
        this.etime = etime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("memberId", getMemberId())
            .append("name", getName())
            .append("mobile", getMobile())
            .append("payTime", getPayTime())
            .append("carType", getCarType())
            .append("licensePlate", getLicensePlate())
            .append("frameNo", getFrameNo())
            .append("status", getStatus())
            .toString();
    }
}
