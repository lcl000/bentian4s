package com.ruoyi.project.system.membercar.service;

import com.ruoyi.project.system.membercar.domain.MemberCar;
import com.ruoyi.project.system.membercar.dto.MemberCarDto;

import java.util.List;

/**
 * 用户车辆信息Service接口
 * 
 * @author LCL
 * @date 2020-08-10
 */
public interface IMemberCarService 
{
    /**
     * 查询用户车辆信息
     * 
     * @param id 用户车辆信息ID
     * @return 用户车辆信息
     */
    public MemberCar selectMemberCarById(String id);

    /**
     * 查询用户车辆信息列表
     * 
     * @param memberCar 用户车辆信息
     * @return 用户车辆信息集合
     */
    public List<MemberCarDto> selectMemberCarList(MemberCar memberCar);

    /**
     * 新增用户车辆信息
     * 
     * @param memberCar 用户车辆信息
     * @return 结果
     */
    public int insertMemberCar(MemberCar memberCar);

    /**
     * 修改用户车辆信息
     * 
     * @param memberCar 用户车辆信息
     * @return 结果
     */
    public int updateMemberCar(MemberCar memberCar);

    /**
     * 批量删除用户车辆信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMemberCarByIds(String ids);

    /**
     * 删除用户车辆信息信息
     * 
     * @param id 用户车辆信息ID
     * @return 结果
     */
    public int deleteMemberCarById(String id);

    MemberCarDto findByUid(String uid);

    int deleteByUid(String memberId);

    MemberCarDto findByMobile(String mobile);
}
