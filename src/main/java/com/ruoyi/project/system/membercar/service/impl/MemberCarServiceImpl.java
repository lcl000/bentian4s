package com.ruoyi.project.system.membercar.service.impl;

import com.ruoyi.common.utils.text.Convert;
import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.membercar.domain.MemberCar;
import com.ruoyi.project.system.membercar.dto.MemberCarDto;
import com.ruoyi.project.system.membercar.mapper.MemberCarMapper;
import com.ruoyi.project.system.membercar.service.IMemberCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户车辆信息Service业务层处理
 * 
 * @author LCL
 * @date 2020-08-10
 */
@Service
public class MemberCarServiceImpl implements IMemberCarService 
{
    @Resource
    private IdGererateFactory idGererateFactory;

    @Autowired
    private MemberCarMapper memberCarMapper;

    /**
     * 查询用户车辆信息
     * 
     * @param id 用户车辆信息ID
     * @return 用户车辆信息
     */
    @Override
    public MemberCar selectMemberCarById(String id)
    {
        return memberCarMapper.selectMemberCarById(id);
    }

    /**
     * 查询用户车辆信息列表
     * 
     * @param memberCar 用户车辆信息
     * @return 用户车辆信息
     */
    @Override
    public List<MemberCarDto> selectMemberCarList(MemberCar memberCar)
    {
        return memberCarMapper.selectMemberCarList(memberCar);
    }

    /**
     * 新增用户车辆信息
     * 
     * @param memberCar 用户车辆信息
     * @return 结果
     */
    @Override
    public int insertMemberCar(MemberCar memberCar)
    {
        memberCar.setId(this.idGererateFactory.nextStringId());
        return memberCarMapper.insertMemberCar(memberCar);
    }

    /**
     * 修改用户车辆信息
     * 
     * @param memberCar 用户车辆信息
     * @return 结果
     */
    @Override
    public int updateMemberCar(MemberCar memberCar)
    {
        return memberCarMapper.updateMemberCar(memberCar);
    }

    /**
     * 删除用户车辆信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMemberCarByIds(String ids)
    {
        return memberCarMapper.deleteMemberCarByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户车辆信息信息
     * 
     * @param id 用户车辆信息ID
     * @return 结果
     */
    @Override
    public int deleteMemberCarById(String id)
    {
        return memberCarMapper.deleteMemberCarById(id);
    }

    @Override
    public MemberCarDto findByUid(String uid) {
        return this.memberCarMapper.findByUid(uid);
    }

    @Override
    public int deleteByUid(String memberId) {
        return this.memberCarMapper.deleteByUid(memberId);
    }

    @Override
    public MemberCarDto findByMobile(String mobile) {
        return this.memberCarMapper.findByMobile(mobile);
    }
}
