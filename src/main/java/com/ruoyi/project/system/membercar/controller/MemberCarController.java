package com.ruoyi.project.system.membercar.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.coupon.usercoupon.domain.UserCoupon;
import com.ruoyi.project.system.coupon.usercoupon.service.IUserCouponService;
import com.ruoyi.project.system.giftbag.giftbagtinfo.domain.GiftBagInfo;
import com.ruoyi.project.system.giftbag.giftbagtinfo.service.IGiftBagInfoService;
import com.ruoyi.project.system.member.domain.Member;
import com.ruoyi.project.system.member.service.IMemberService;
import com.ruoyi.project.system.membercar.domain.MemberCar;
import com.ruoyi.project.system.membercar.dto.MemberCarDto;
import com.ruoyi.project.system.membercar.service.IMemberCarService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.user.service.IUserService;

/**
 * 用户车辆信息Controller
 * 
 * @author LCL
 * @date 2020-08-10
 */
@Controller
@RequestMapping("/system/member/car/membercar")
public class MemberCarController extends BaseController
{
    private String prefix = "system/membercar";

    @Autowired
    private IMemberCarService memberCarService;

    @Autowired
    private IMemberService memberService;

    @Autowired
    private IGiftBagInfoService giftBagInfoService;

    @Autowired
    private IUserCouponService userCouponService;

    @Autowired
    private IUserService userService;

    @GetMapping()
    public String membercar(ModelMap map)
    {
        map.put("post",this.userService.findPostIdsByUid(getUserId()));
        return prefix + "/membercar";
    }

    /**
     * 查询用户车辆信息列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MemberCar memberCar)
    {
        startPage();
        List<MemberCarDto> list = memberCarService.selectMemberCarList(memberCar);
        return getDataTable(list);
    }

    /**
     * 导出用户车辆信息列表
     */
    @Log(title = "用户车辆信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MemberCar memberCar)
    {
        List<MemberCarDto> list = memberCarService.selectMemberCarList(memberCar);
        ExcelUtil<MemberCarDto> util = new ExcelUtil<MemberCarDto>(MemberCarDto.class);
        return util.exportExcel(list, "membercar");
    }

    /**
     * 新增用户车辆信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存用户车辆信息
     */
    @Log(title = "用户车辆信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MemberCar memberCar)
    {
        memberCar.setCreated(DateUtils.getNowDate());
        return toAjax(memberCarService.insertMemberCar(memberCar));
    }

    /**
     * 修改用户车辆信息
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        MemberCar memberCar = memberCarService.selectMemberCarById(id);
        mmap.put("memberCar", memberCar);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户车辆信息
     */
    @Log(title = "用户车辆信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MemberCar memberCar)
    {
        return toAjax(memberCarService.updateMemberCar(memberCar));
    }

    /**
     * 删除用户车辆信息
     */
    @Log(title = "用户车辆信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(memberCarService.deleteMemberCarByIds(ids));
    }


    @PostMapping("/agree/{id}")
    @ResponseBody
    @Transactional
    public AjaxResult agree(@PathVariable String id){
        MemberCar memberCar = this.memberCarService.selectMemberCarById(id);
        if(memberCar==null){
            return AjaxResult.error();
        }
        //审核通过的话 修改为审核通过
        MemberCar updateCar = new MemberCar();
        updateCar.setId(id);
        updateCar.setStatus(1);
        updateCar.setExamineTime(DateUtils.getNowDate());
        updateCar.setExamineMember(getUserId());
        this.memberCarService.updateMemberCar(updateCar);

        //查询用户信息
        Member member = this.memberService.selectMemberById(memberCar.getMemberId());
        //判断用户是否绑定车辆
        if(member.getHasCar()==0){
            Member update = new Member();
            update.setId(member.getId());
            update.setHasCar(1);
            this.memberService.updateMember(update);
            //车主第一次绑车 返券
            GiftBagInfo select = new GiftBagInfo();
            //判断用户是否是新用户
            Integer num = DateUtils.getRealMonBtn(memberCar.getPayTime(),DateUtils.getNowDate());
            if(num>=4){
                select.setGiftBagId("xxxx");
            }else{
                select.setGiftBagId("xxx");
            }
            select.setStatus(0);
            List<GiftBagInfo> giftBagInfos = this.giftBagInfoService.selectGiftBagInfoList(select);
            for(GiftBagInfo giftBagInfo:giftBagInfos){
                for (int i = 0; i <giftBagInfo.getTotal(); i++) {
                    UserCoupon userCoupon = new UserCoupon();
                    userCoupon.setStatus(0);
                    userCoupon.setType(giftBagInfo.getType());
                    userCoupon.setMemberId(member.getId());
                    userCoupon.setUseCondition(giftBagInfo.getUseCondition());
                    userCoupon.setPrice(giftBagInfo.getPrice());
                    userCoupon.setEndTime(giftBagInfo.getEndTime());
                    userCoupon.setNotes(giftBagInfo.getNotes());
                    userCoupon.setStartTime(giftBagInfo.getStartTime());
                    userCoupon.setCouponId(giftBagInfo.getId());
                    userCoupon.setName(giftBagInfo.getName());
                    userCoupon.setCreated(DateUtils.getNowDate());
                    this.userCouponService.insertUserCoupon(userCoupon);
                }
            }
        }

        return AjaxResult.success();
    }


    @PostMapping("/refuse/{id}")
    @ResponseBody
    public AjaxResult refuse(@PathVariable String id){
        MemberCar memberCar = this.memberCarService.selectMemberCarById(id);
        if(memberCar==null){
            return AjaxResult.error();
        }
        MemberCar update = new MemberCar();
        update.setId(id);
        update.setStatus(2);
        update.setExamineTime(DateUtils.getNowDate());
        update.setExamineMember(getUserId());
        this.memberCarService.updateMemberCar(update);

        return AjaxResult.success();
    }


}
