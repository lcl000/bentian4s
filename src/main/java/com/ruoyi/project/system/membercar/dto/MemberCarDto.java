package com.ruoyi.project.system.membercar.dto;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.project.system.membercar.domain.MemberCar;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/8/10 15:54
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
public class MemberCarDto extends MemberCar {
	//车型名称
	@Excel(name = "车型",sort = 4)
	private String typeName;

	//门店名称
	@Excel(name = "购买门店",sort = 5)
	private String dealerName;

	//审核人
	@Excel(name = "审核人",sort = 12)
	private String examineMemberName;

	//邀请人
	@Excel(name = "邀请人",sort = 10)
	private String parentName;


	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public String getExamineMemberName() {
		return examineMemberName;
	}

	public void setExamineMemberName(String examineMemberName) {
		this.examineMemberName = examineMemberName;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
}
