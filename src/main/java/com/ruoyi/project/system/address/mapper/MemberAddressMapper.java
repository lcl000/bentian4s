package com.ruoyi.project.system.address.mapper;

import java.util.List;
import com.ruoyi.project.system.address.domain.MemberAddress;

/**
 * 收货地址Mapper接口
 * 
 * @author LCL
 * @date 2020-06-22
 */
public interface MemberAddressMapper 
{
    /**
     * 查询收货地址
     * 
     * @param id 收货地址ID
     * @return 收货地址
     */
    public MemberAddress selectMemberAddressById(String id);

    /**
     * 查询收货地址列表
     * 
     * @param memberAddress 收货地址
     * @return 收货地址集合
     */
    public List<MemberAddress> selectMemberAddressList(MemberAddress memberAddress);

    /**
     * 新增收货地址
     * 
     * @param memberAddress 收货地址
     * @return 结果
     */
    public int insertMemberAddress(MemberAddress memberAddress);

    /**
     * 修改收货地址
     * 
     * @param memberAddress 收货地址
     * @return 结果
     */
    public int updateMemberAddress(MemberAddress memberAddress);

    /**
     * 删除收货地址
     * 
     * @param id 收货地址ID
     * @return 结果
     */
    public int deleteMemberAddressById(String id);

    /**
     * 批量删除收货地址
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMemberAddressByIds(String[] ids);

    int removeDefault(String uid);

    MemberAddress findDefault(String uid);
}
