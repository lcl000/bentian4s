package com.ruoyi.project.system.address.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.address.mapper.MemberAddressMapper;
import com.ruoyi.project.system.address.domain.MemberAddress;
import com.ruoyi.project.system.address.service.IMemberAddressService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 收货地址Service业务层处理
 * 
 * @author LCL
 * @date 2020-06-22
 */
@Service
public class MemberAddressServiceImpl implements IMemberAddressService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private MemberAddressMapper memberAddressMapper;

    /**
     * 查询收货地址
     * 
     * @param id 收货地址ID
     * @return 收货地址
     */
    @Override
    public MemberAddress selectMemberAddressById(String id)
    {
        return memberAddressMapper.selectMemberAddressById(id);
    }

    /**
     * 查询收货地址列表
     * 
     * @param memberAddress 收货地址
     * @return 收货地址
     */
    @Override
    public List<MemberAddress> selectMemberAddressList(MemberAddress memberAddress)
    {
        return memberAddressMapper.selectMemberAddressList(memberAddress);
    }

    /**
     * 新增收货地址
     * 
     * @param memberAddress 收货地址
     * @return 结果
     */
    @Override
    public int insertMemberAddress(MemberAddress memberAddress)
    {
        memberAddress.setId(this.idGererateFactory.nextStringId());
        return memberAddressMapper.insertMemberAddress(memberAddress);
    }

    /**
     * 修改收货地址
     * 
     * @param memberAddress 收货地址
     * @return 结果
     */
    @Override
    public int updateMemberAddress(MemberAddress memberAddress)
    {
        return memberAddressMapper.updateMemberAddress(memberAddress);
    }

    /**
     * 删除收货地址对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMemberAddressByIds(String ids)
    {
        return memberAddressMapper.deleteMemberAddressByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除收货地址信息
     * 
     * @param id 收货地址ID
     * @return 结果
     */
    @Override
    public int deleteMemberAddressById(String id)
    {
        return memberAddressMapper.deleteMemberAddressById(id);
    }

    @Override
    public int removeDefault(String uid) {
        return this.memberAddressMapper.removeDefault(uid);
    }

    @Override
    public MemberAddress findDefault(String uid) {
        return this.memberAddressMapper.findDefault(uid);
    }
}
