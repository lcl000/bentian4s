package com.ruoyi.project.system.address.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 收货地址对象 member_address
 * 
 * @author LCL
 * @date 2020-06-22
 */
public class MemberAddress extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 用户主键ID */
    @Excel(name = "用户主键ID")
    private String memberId;

    /** 收货人姓名 */
    @Excel(name = "收货人姓名")
    private String consigneeName;

    /** 收货人电话 */
    @Excel(name = "收货人电话")
    private String consigneeTel;

    /** 地区 */
    @Excel(name = "地区")
    private String area;

    /** 详细地址 */
    @Excel(name = "详细地址")
    private String address;

    /** 是否是默认地址: 0 是 1 不是 */
    @Excel(name = "是否是默认地址: 0 是 1 不是")
    private Integer isDefault;

    /** 状态: 0 正常 1 删除 */
    @Excel(name = "状态: 0 正常 1 删除")
    private Integer status;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setConsigneeName(String consigneeName) 
    {
        this.consigneeName = consigneeName;
    }

    public String getConsigneeName() 
    {
        return consigneeName;
    }
    public void setConsigneeTel(String consigneeTel) 
    {
        this.consigneeTel = consigneeTel;
    }

    public String getConsigneeTel() 
    {
        return consigneeTel;
    }
    public void setArea(String area) 
    {
        this.area = area;
    }

    public String getArea() 
    {
        return area;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setIsDefault(Integer isDefault) 
    {
        this.isDefault = isDefault;
    }

    public Integer getIsDefault() 
    {
        return isDefault;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("memberId", getMemberId())
            .append("consigneeName", getConsigneeName())
            .append("consigneeTel", getConsigneeTel())
            .append("area", getArea())
            .append("address", getAddress())
            .append("isDefault", getIsDefault())
            .append("status", getStatus())
            .toString();
    }
}
