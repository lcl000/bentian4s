package com.ruoyi.project.system.dealer.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ruoyi.project.system.dealer.domain.Dealer;

/**
 * 店铺Mapper接口
 * 
 * @author LCL
 * @date 2020-07-29
 */
public interface DealerMapper 
{
    /**
     * 查询店铺
     * 
     * @param id 店铺ID
     * @return 店铺
     */
    public Dealer selectDealerById(String id);

    /**
     * 查询店铺列表
     * 
     * @param dealer 店铺
     * @return 店铺集合
     */
    public List<Dealer> selectDealerList(Dealer dealer);

    /**
     * 新增店铺
     * 
     * @param dealer 店铺
     * @return 结果
     */
    public int insertDealer(Dealer dealer);

    /**
     * 修改店铺
     * 
     * @param dealer 店铺
     * @return 结果
     */
    public int updateDealer(Dealer dealer);

    /**
     * 删除店铺
     * 
     * @param id 店铺ID
     * @return 结果
     */
    public int deleteDealerById(String id);

    /**
     * 批量删除店铺
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDealerByIds(String[] ids);

	List<Dealer> findAll(@Param("name") String name);

    List<Dealer> selectDealersByUserId(Long userId);

	List<Dealer> findByUserPost(Long userId);
}
