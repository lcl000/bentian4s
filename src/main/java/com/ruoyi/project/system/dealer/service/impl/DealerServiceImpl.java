package com.ruoyi.project.system.dealer.service.impl;

import javax.annotation.Resource;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.gavaghan.geodesy.Ellipsoid;
import org.gavaghan.geodesy.GeodeticCalculator;
import org.gavaghan.geodesy.GlobalCoordinates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.dealer.mapper.DealerMapper;
import com.ruoyi.project.system.dealer.domain.Dealer;
import com.ruoyi.project.system.dealer.service.IDealerService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 店铺Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-29
 */
@Service
public class DealerServiceImpl implements IDealerService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private DealerMapper dealerMapper;

    /**
     * 查询店铺
     * 
     * @param id 店铺ID
     * @return 店铺
     */
    @Override
    public Dealer selectDealerById(String id)
    {
        return dealerMapper.selectDealerById(id);
    }

    /**
     * 查询店铺列表
     * 
     * @param dealer 店铺
     * @return 店铺
     */
    @Override
    public List<Dealer> selectDealerList(Dealer dealer)
    {
        return dealerMapper.selectDealerList(dealer);
    }

    /**
     * 新增店铺
     * 
     * @param dealer 店铺
     * @return 结果
     */
    @Override
    public int insertDealer(Dealer dealer)
    {
        dealer.setId(this.idGererateFactory.nextStringId());
        return dealerMapper.insertDealer(dealer);
    }

    /**
     * 修改店铺
     * 
     * @param dealer 店铺
     * @return 结果
     */
    @Override
    public int updateDealer(Dealer dealer)
    {
        return dealerMapper.updateDealer(dealer);
    }

    /**
     * 删除店铺对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteDealerByIds(String ids)
    {
        return dealerMapper.deleteDealerByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除店铺信息
     * 
     * @param id 店铺ID
     * @return 结果
     */
    @Override
    public int deleteDealerById(String id)
    {
        return dealerMapper.deleteDealerById(id);
    }

    @Override
    public Dealer findMinDistance(double lng, double lat) {
        //查询店铺列表
        List<Dealer> dealers = this.dealerMapper.findAll(null);
        double[] doubles = new double[dealers.size()];
        HashMap<Double, Dealer> dealerMap = new HashMap<Double, Dealer>();
        for (int i = 0; i < dealers.size(); i++) {
            Dealer dealer = dealers.get(i);
            doubles[i]=getDistance(lng,lat,dealer.getLng(),dealer.getLat());
            dealerMap.put(doubles[i],dealer);
        }
        //排序
        Arrays.sort(doubles);
        return dealerMap.get(doubles[0]);
    }

    @Override
    public List<Dealer> findAll(String name) {
        return this.dealerMapper.findAll(name);
    }

    @Override
    public List<Dealer> findByUserPost(Long userId) {
        return this.dealerMapper.findByUserPost(userId);
    }

    public double getDistance(double longitudeFrom, double latitudeFrom, double longitudeTo, double latitudeTo) {
        GlobalCoordinates source = new GlobalCoordinates(latitudeFrom, longitudeFrom);
        GlobalCoordinates target = new GlobalCoordinates(latitudeTo, longitudeTo);

        return new GeodeticCalculator().calculateGeodeticCurve(Ellipsoid.Sphere, source, target).getEllipsoidalDistance();
    }

}
