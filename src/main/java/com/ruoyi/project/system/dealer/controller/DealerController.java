package com.ruoyi.project.system.dealer.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.dealer.domain.Dealer;
import com.ruoyi.project.system.dealer.service.IDealerService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 店铺Controller
 * 
 * @author LCL
 * @date 2020-07-29
 */
@Controller
@RequestMapping("/system/dealer")
public class DealerController extends BaseController
{
    private String prefix = "system/dealer";

    @Autowired
    private IDealerService dealerService;

    @RequiresPermissions("system:dealer:view")
    @GetMapping()
    public String dealer()
    {
        return prefix + "/dealer";
    }


    @RequiresPermissions("system:dealer:view")
    @GetMapping("/map")
    public String map()
    {
        return prefix + "/map";
    }

    /**
     * 查询店铺列表
     */
    @RequiresPermissions("system:dealer:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Dealer dealer)
    {
        startPage();
        List<Dealer> list = dealerService.selectDealerList(dealer);
        return getDataTable(list);
    }

    /**
     * 导出店铺列表
     */
    @RequiresPermissions("system:dealer:export")
    @Log(title = "店铺", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Dealer dealer)
    {
        List<Dealer> list = dealerService.selectDealerList(dealer);
        ExcelUtil<Dealer> util = new ExcelUtil<Dealer>(Dealer.class);
        return util.exportExcel(list, "dealer");
    }

    /**
     * 新增店铺
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存店铺
     */
    @RequiresPermissions("system:dealer:add")
    @Log(title = "店铺", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Dealer dealer)
    {
        return toAjax(dealerService.insertDealer(dealer));
    }

    /**
     * 修改店铺
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        Dealer dealer = dealerService.selectDealerById(id);
        mmap.put("dealer", dealer);
        return prefix + "/edit";
    }

    /**
     * 修改保存店铺
     */
    @RequiresPermissions("system:dealer:edit")
    @Log(title = "店铺", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Dealer dealer)
    {
        return toAjax(dealerService.updateDealer(dealer));
    }

    /**
     * 删除店铺
     */
    @RequiresPermissions("system:dealer:remove")
    @Log(title = "店铺", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(dealerService.deleteDealerByIds(ids));
    }
}
