package com.ruoyi.project.system.dealer.service;

import java.util.List;
import com.ruoyi.project.system.dealer.domain.Dealer;

/**
 * 店铺Service接口
 * 
 * @author LCL
 * @date 2020-07-29
 */
public interface IDealerService 
{
    /**
     * 查询店铺
     * 
     * @param id 店铺ID
     * @return 店铺
     */
    public Dealer selectDealerById(String id);

    /**
     * 查询店铺列表
     * 
     * @param dealer 店铺
     * @return 店铺集合
     */
    public List<Dealer> selectDealerList(Dealer dealer);

    /**
     * 新增店铺
     * 
     * @param dealer 店铺
     * @return 结果
     */
    public int insertDealer(Dealer dealer);

    /**
     * 修改店铺
     * 
     * @param dealer 店铺
     * @return 结果
     */
    public int updateDealer(Dealer dealer);

    /**
     * 批量删除店铺
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDealerByIds(String ids);

    /**
     * 删除店铺信息
     * 
     * @param id 店铺ID
     * @return 结果
     */
    public int deleteDealerById(String id);

	Dealer findMinDistance(double lng, double lat);

    List<Dealer> findAll(String name);

	List<Dealer> findByUserPost(Long userId);
}
