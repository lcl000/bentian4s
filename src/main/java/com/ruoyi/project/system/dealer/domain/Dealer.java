package com.ruoyi.project.system.dealer.domain;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 店铺对象 dealer
 * 
 * @author LCL
 * @date 2020-07-29
 */
public class Dealer extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 店铺名称 */
    @Excel(name = "店铺名称")
    private String name;

    /** 店铺logo */
    @Excel(name = "店铺logo")
    private String logo;

    /** 主图 */
    @Excel(name = "主图")
    private String mainImg;

    /** 店铺联系人 */
    @Excel(name = "店铺联系人")
    private String contacts;

    /** 店铺联系人电话 */
    @Excel(name = "店铺联系人电话")
    private String contactsPhone;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 店铺经度 */
    private double lng;

    /** 店铺维度 */
    private double lat;

    /** 店铺介绍 */
    private String introduce;

    /** 店铺状态:0 正常 1 停用 */
    @Excel(name = "店铺状态:0 正常 1 停用")
    private Integer status;

    /** 店铺开始预约时间 */
    private Date startAppointmentTime;

    /** 店铺结束预约时间 */
    private Date endAppointmentTime;
    /** 同一时间最大预约数 **/
    private Integer maxNum;
    /** 排序 **/
    private Integer sort;

    /** 用户是否存在此岗位标识 默认不存在 */
    private boolean flag = false;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setLogo(String logo) 
    {
        this.logo = logo;
    }

    public String getLogo() 
    {
        return logo;
    }
    public void setMainImg(String mainImg) 
    {
        this.mainImg = mainImg;
    }

    public String getMainImg() 
    {
        return mainImg;
    }
    public void setContacts(String contacts) 
    {
        this.contacts = contacts;
    }

    public String getContacts() 
    {
        return contacts;
    }
    public void setContactsPhone(String contactsPhone) 
    {
        this.contactsPhone = contactsPhone;
    }

    public String getContactsPhone() 
    {
        return contactsPhone;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setLng(double lng)
    {
        this.lng = lng;
    }

    public double getLng()
    {
        return lng;
    }
    public void setLat(double lat)
    {
        this.lat = lat;
    }

    public double getLat()
    {
        return lat;
    }
    public void setIntroduce(String introduce)
    {
        this.introduce = introduce;
    }

    public String getIntroduce() 
    {
        return introduce;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setStartAppointmentTime(Date startAppointmentTime) 
    {
        this.startAppointmentTime = startAppointmentTime;
    }

    public Date getStartAppointmentTime() 
    {
        return startAppointmentTime;
    }
    public void setEndAppointmentTime(Date endAppointmentTime) 
    {
        this.endAppointmentTime = endAppointmentTime;
    }

    public Date getEndAppointmentTime() 
    {
        return endAppointmentTime;
    }

    public Integer getMaxNum() {
        return maxNum;
    }

    public void setMaxNum(Integer maxNum) {
        this.maxNum = maxNum;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("logo", getLogo())
            .append("mainImg", getMainImg())
            .append("contacts", getContacts())
            .append("contactsPhone", getContactsPhone())
            .append("address", getAddress())
            .append("lng", getLng())
            .append("lat", getLat())
            .append("introduce", getIntroduce())
            .append("remark", getRemark())
            .append("status", getStatus())
            .append("startAppointmentTime", getStartAppointmentTime())
            .append("endAppointmentTime", getEndAppointmentTime())
            .toString();
    }
}
