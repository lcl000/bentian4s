package com.ruoyi.project.system.apply.service.impl;

import javax.annotation.Resource;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.apply.dto.CoinApplyDto;
import com.ruoyi.project.system.apply.mapper.CoinApplyMapper;
import com.ruoyi.project.system.apply.domain.CoinApply;
import com.ruoyi.project.system.apply.service.ICoinApplyService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 积分增加申请Service业务层处理
 * 
 * @author LCL
 * @date 2020-08-14
 */
@Service
public class CoinApplyServiceImpl implements ICoinApplyService 
{
    @Resource
    private IdGererateFactory idGererateFactory;

    @Autowired
    private CoinApplyMapper coinApplyMapper;

    /**
     * 查询积分增加申请
     * 
     * @param id 积分增加申请ID
     * @return 积分增加申请
     */
    @Override
    public CoinApply selectCoinApplyById(String id)
    {
        return coinApplyMapper.selectCoinApplyById(id);
    }

    /**
     * 查询积分增加申请列表
     * 
     * @param coinApply 积分增加申请
     * @return 积分增加申请
     */
    @Override
    public List<CoinApplyDto> selectCoinApplyList(CoinApply coinApply)
    {
        return coinApplyMapper.selectCoinApplyList(coinApply);
    }

    /**
     * 新增积分增加申请
     * 
     * @param coinApply 积分增加申请
     * @return 结果
     */
    @Override
    public int insertCoinApply(CoinApply coinApply)
    {
        coinApply.setId(this.idGererateFactory.nextStringId());
        return coinApplyMapper.insertCoinApply(coinApply);
    }

    /**
     * 修改积分增加申请
     * 
     * @param coinApply 积分增加申请
     * @return 结果
     */
    @Override
    public int updateCoinApply(CoinApply coinApply)
    {
        coinApply.setUpdateTime(DateUtils.getNowDate());
        return coinApplyMapper.updateCoinApply(coinApply);
    }

    /**
     * 删除积分增加申请对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCoinApplyByIds(String ids)
    {
        return coinApplyMapper.deleteCoinApplyByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除积分增加申请信息
     * 
     * @param id 积分增加申请ID
     * @return 结果
     */
    @Override
    public int deleteCoinApplyById(String id)
    {
        return coinApplyMapper.deleteCoinApplyById(id);
    }
}
