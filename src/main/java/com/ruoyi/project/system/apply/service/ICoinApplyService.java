package com.ruoyi.project.system.apply.service;

import java.util.List;
import com.ruoyi.project.system.apply.domain.CoinApply;
import com.ruoyi.project.system.apply.dto.CoinApplyDto;

/**
 * 积分增加申请Service接口
 * 
 * @author LCL
 * @date 2020-08-14
 */
public interface ICoinApplyService 
{
    /**
     * 查询积分增加申请
     * 
     * @param id 积分增加申请ID
     * @return 积分增加申请
     */
    public CoinApply selectCoinApplyById(String id);

    /**
     * 查询积分增加申请列表
     * 
     * @param coinApply 积分增加申请
     * @return 积分增加申请集合
     */
    public List<CoinApplyDto> selectCoinApplyList(CoinApply coinApply);

    /**
     * 新增积分增加申请
     * 
     * @param coinApply 积分增加申请
     * @return 结果
     */
    public int insertCoinApply(CoinApply coinApply);

    /**
     * 修改积分增加申请
     * 
     * @param coinApply 积分增加申请
     * @return 结果
     */
    public int updateCoinApply(CoinApply coinApply);

    /**
     * 批量删除积分增加申请
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCoinApplyByIds(String ids);

    /**
     * 删除积分增加申请信息
     * 
     * @param id 积分增加申请ID
     * @return 结果
     */
    public int deleteCoinApplyById(String id);
}
