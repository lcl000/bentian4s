package com.ruoyi.project.system.apply.domain;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.project.system.membercar.domain.MemberCar;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 积分增加申请对象 coin_apply
 * 
 * @author LCL
 * @date 2020-08-14
 */
public class CoinApply
{
    private static final long serialVersionUID = 1L;
    private MemberCar memberCar;

    public MemberCar getMemberCar() {
        return memberCar;
    }

    public void setMemberCar(MemberCar memberCar) {
        this.memberCar = memberCar;
    }

    /** 主键ID */
    private String id;

    /** 类型 */
    private Integer type;

    /** 用户主键ID */
    private String memberId;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss",sort = 4)
    private Date created;

    /** 创建人 */
    private Long creator;

    /** 数量 */
    @Excel(name = "数量",sort = 7)
    private BigDecimal total;

    /** 状态: 0 待审批 1 通过 2 驳回 */
    @Excel(name = "状态: 0 待审批 1 通过 2 驳回",sort = 8)
    private Integer status;

    /** 备注 */
    @Excel(name = "备注",sort = 9)
    private String notes;

    /** 审核人 */
    private Long updateUser;

    /** 审核时间 */
    @Excel(name = "审核时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss",sort = 11)
    private Date updateTime;

    /** 门店主键ID **/
    private String dealerId;

    /** 金额 */
    @Excel(name = "金额",sort = 6)
    private BigDecimal price;


    private List postList;
    //查询用 开始时间
    private String btime;
    //查询用 结束时间
    private String etime;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setCreated(Date created) 
    {
        this.created = created;
    }

    public Date getCreated() 
    {
        return created;
    }
    public void setCreator(Long creator) 
    {
        this.creator = creator;
    }

    public Long getCreator() 
    {
        return creator;
    }
    public void setTotal(BigDecimal total) 
    {
        this.total = total;
    }

    public BigDecimal getTotal() 
    {
        return total;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setNotes(String notes) 
    {
        this.notes = notes;
    }

    public String getNotes() 
    {
        return notes;
    }
    public void setUpdateUser(Long updateUser) 
    {
        this.updateUser = updateUser;
    }

    public Long getUpdateUser() 
    {
        return updateUser;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public List getPostList() {
        return postList;
    }

    public void setPostList(List postList) {
        this.postList = postList;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getBtime() {
        return btime;
    }

    public void setBtime(String btime) {
        this.btime = btime;
    }

    public String getEtime() {
        return etime;
    }

    public void setEtime(String etime) {
        this.etime = etime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("type", getType())
            .append("memberId", getMemberId())
            .append("created", getCreated())
            .append("creator", getCreator())
            .append("total", getTotal())
            .append("status", getStatus())
            .append("notes", getNotes())
            .append("updateUser", getUpdateUser())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
