package com.ruoyi.project.system.apply.dto;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.project.system.apply.domain.CoinApply;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/8/14 16:14
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
public class CoinApplyDto extends CoinApply {
	//用户名称
	@Excel(name = "用户名称",sort = 3)
	private String memberName;
	//申请人
	@Excel(name = "申请人",sort = 5)
	private String creatorName;
	//审核人
	@Excel(name = "审核人",sort = 10)
	private String updateUserName;
	//门店名称
	@Excel(name = "门店名称",sort = 2)
	private String dealerName;
	//类型名称
	@Excel(name = "类型",sort = 1)
	private String typeName;
    //车牌号
    @Excel(name = "车牌号",sort = 12)
    private String licensePlate;
    //车架号
    @Excel(name = "车架号",sort = 13)
    private String frameNo;
    //姓名
    @Excel(name = "姓名",sort = 14)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getFrameNo() {
        return frameNo;
    }

    public void setFrameNo(String frameNo) {
        this.frameNo = frameNo;
    }

    public String getCreatorName() {
		return creatorName;
	}

	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	public String getUpdateUserName() {
		return updateUserName;
	}

	public void setUpdateUserName(String updateUserName) {
		this.updateUserName = updateUserName;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
}
