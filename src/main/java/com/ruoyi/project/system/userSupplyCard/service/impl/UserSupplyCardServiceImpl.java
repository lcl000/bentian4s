package com.ruoyi.project.system.userSupplyCard.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.project.system.userSupplyCard.dto.UserSupplyCardDto;
import com.ruoyi.project.system.userSupplyCard.mapper.UserSupplyCardMapper;
import com.ruoyi.project.system.userSupplyCard.domain.UserSupplyCard;
import com.ruoyi.project.system.userSupplyCard.service.IUserSupplyCardService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 用户供应Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-20
 */
@Service
public class UserSupplyCardServiceImpl implements IUserSupplyCardService 
{
    @Autowired
    private UserSupplyCardMapper userSupplyCardMapper;

    /**
     * 查询用户供应
     * 
     * @param id 用户供应ID
     * @return 用户供应
     */
    @Override
    public UserSupplyCard selectUserSupplyCardById(String id)
    {
        return userSupplyCardMapper.selectUserSupplyCardById(id);
    }

    /**
     * 查询用户供应列表
     * 
     * @param userSupplyCard 用户供应
     * @return 用户供应
     */
    @Override
    public List<UserSupplyCardDto> selectUserSupplyCardList(UserSupplyCard userSupplyCard)
    {
        return userSupplyCardMapper.selectUserSupplyCardList(userSupplyCard);
    }

    /**
     * 新增用户供应
     * 
     * @param userSupplyCard 用户供应
     * @return 结果
     */
    @Override
    public int insertUserSupplyCard(UserSupplyCard userSupplyCard)
    {
        return userSupplyCardMapper.insertUserSupplyCard(userSupplyCard);
    }

    /**
     * 修改用户供应
     * 
     * @param userSupplyCard 用户供应
     * @return 结果
     */
    @Override
    public int updateUserSupplyCard(UserSupplyCard userSupplyCard)
    {
        return userSupplyCardMapper.updateUserSupplyCard(userSupplyCard);
    }

    /**
     * 删除用户供应对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteUserSupplyCardByIds(String ids)
    {
        return userSupplyCardMapper.deleteUserSupplyCardByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户供应信息
     * 
     * @param id 用户供应ID
     * @return 结果
     */
    @Override
    public int deleteUserSupplyCardById(String id)
    {
        return userSupplyCardMapper.deleteUserSupplyCardById(id);
    }

    @Override
    public UserSupplyCard findByNo(String supplyNo) {
        return this.userSupplyCardMapper.findByNo(supplyNo);
    }

    @Override
    public List<UserSupplyCardDto> findListByUid(String uid) {
        return this.userSupplyCardMapper.findListByUid(uid);
    }
}
