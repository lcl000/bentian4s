package com.ruoyi.project.system.userSupplyCard.mapper;

import java.util.List;
import com.ruoyi.project.system.userSupplyCard.domain.UserSupplyCard;
import com.ruoyi.project.system.userSupplyCard.dto.UserSupplyCardDto;

/**
 * 用户供应Mapper接口
 * 
 * @author LCL
 * @date 2020-07-20
 */
public interface UserSupplyCardMapper 
{
    /**
     * 查询用户供应
     * 
     * @param id 用户供应ID
     * @return 用户供应
     */
    public UserSupplyCard selectUserSupplyCardById(String id);

    /**
     * 查询用户供应列表
     * 
     * @param userSupplyCard 用户供应
     * @return 用户供应集合
     */
    public List<UserSupplyCardDto> selectUserSupplyCardList(UserSupplyCard userSupplyCard);

    /**
     * 新增用户供应
     * 
     * @param userSupplyCard 用户供应
     * @return 结果
     */
    public int insertUserSupplyCard(UserSupplyCard userSupplyCard);

    /**
     * 修改用户供应
     * 
     * @param userSupplyCard 用户供应
     * @return 结果
     */
    public int updateUserSupplyCard(UserSupplyCard userSupplyCard);

    /**
     * 删除用户供应
     * 
     * @param id 用户供应ID
     * @return 结果
     */
    public int deleteUserSupplyCardById(String id);

    /**
     * 批量删除用户供应
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserSupplyCardByIds(String[] ids);

    UserSupplyCard findByNo(String supplyNo);

    List<UserSupplyCardDto> findListByUid(String uid);
}
