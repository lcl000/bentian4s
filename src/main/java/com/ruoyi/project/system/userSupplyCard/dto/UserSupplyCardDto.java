package com.ruoyi.project.system.userSupplyCard.dto;

import com.ruoyi.project.system.goods.goodscard.card.enums.SupplyType;
import com.ruoyi.project.system.userSupplyCard.domain.UserSupplyCard;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/7/22 9:55
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public class UserSupplyCardDto extends UserSupplyCard {
	//供应卡类型名称
	private String supplyTypeName;
	//供应卡类别名称
	private String typeName;
	//供应卡类型名称
	private String className;
	//剩余配送数
	private Integer surplus;
	//商品名称
	private String goodsName;

	public String getSupplyTypeName() {
		return supplyTypeName;
	}

	public void setSupplyTypeName(Integer supplyType) {
		this.supplyTypeName = SupplyType.getByValue(supplyType).getName();
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Integer getSurplus() {
		return surplus;
	}

	public void setSurplus(Integer total) {
		this.surplus = total-getSendTotal()<0?0:total-getSendTotal();
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}


}
