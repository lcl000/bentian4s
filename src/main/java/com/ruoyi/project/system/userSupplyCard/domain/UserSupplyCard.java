package com.ruoyi.project.system.userSupplyCard.domain;

import java.math.BigDecimal;
import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 用户供应对象 user_supply_card
 * 
 * @author LCL
 * @date 2020-07-20
 */
public class UserSupplyCard extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 产品ID */
    @Excel(name = "产品ID")
    private String goodsId;

    /** 分类ID */
    @Excel(name = "分类ID")
    private String typeId;

    /** 分类ID */
    @Excel(name = "分类ID")
    private String classId;

    /** 供应卡类型: 0 俩月卡 1 季卡 2 半年卡 3 年卡 */
    @Excel(name = "供应卡类型: 0 俩月卡 1 季卡 2 半年卡 3 年卡")
    private Integer supplyType;

    /** 配送方式 */
    @Excel(name = "配送方式")
    private String supplySend;

    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal price;

    /** 总量 */
    @Excel(name = "总量")
    private Integer total;

    /** 已配送的数量 */
    @Excel(name = "已配送的数量")
    private Integer sendTotal;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date created;

    /** 结束时间 */
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 供应卡编号 */
    @Excel(name = "供应卡编号")
    private String supplyNo;

    /** 配送卡状态:0 未完成 1 已完成 */
    @Excel(name = "配送卡状态:0 未完成 1 已完成")
    private Integer status;

    /** 是否催单 每次配送完毕后修改 */
    @Excel(name = "是否催单 每次配送完毕后修改")
    private Integer isUrge;

    /** 收货人名称 */
    @Excel(name = "收货人名称")
    private String consigneeName;

    /** 收货人电话 */
    @Excel(name = "收货人电话")
    private String consigneeMobile;

    /** 行政区划ID */
    @Excel(name = "地区 省-市-区")
    private String area;

    /** 收货地址 */
    @Excel(name = "收货地址")
    private String address;
    /** 用户主键Id */
    @Excel(name = "用户主键Id")
    private String memberId;
    //供应周期
    private Integer sumDay;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setGoodsId(String goodsId) 
    {
        this.goodsId = goodsId;
    }

    public String getGoodsId() 
    {
        return goodsId;
    }
    public void setTypeId(String typeId) 
    {
        this.typeId = typeId;
    }

    public String getTypeId() 
    {
        return typeId;
    }
    public void setClassId(String classId) 
    {
        this.classId = classId;
    }

    public String getClassId() 
    {
        return classId;
    }
    public void setSupplyType(Integer supplyType) 
    {
        this.supplyType = supplyType;
    }

    public Integer getSupplyType() 
    {
        return supplyType;
    }
    public void setSupplySend(String supplySend) 
    {
        this.supplySend = supplySend;
    }

    public String getSupplySend() 
    {
        return supplySend;
    }
    public void setPrice(BigDecimal price)
    {
        this.price = price;
    }

    public BigDecimal getPrice()
    {
        return price;
    }
    public void setTotal(Integer total)
    {
        this.total = total;
    }

    public Integer getTotal()
    {
        return total;
    }
    public void setSendTotal(Integer sendTotal)
    {
        this.sendTotal = sendTotal;
    }

    public Integer getSendTotal()
    {
        return sendTotal;
    }
    public void setCreated(Date created) 
    {
        this.created = created;
    }

    public Date getCreated()
    {
        return created;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setSupplyNo(String supplyNo) 
    {
        this.supplyNo = supplyNo;
    }

    public String getSupplyNo() 
    {
        return supplyNo;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setIsUrge(Integer isUrge) 
    {
        this.isUrge = isUrge;
    }

    public Integer getIsUrge() 
    {
        return isUrge;
    }
    public void setConsigneeName(String consigneeName) 
    {
        this.consigneeName = consigneeName;
    }

    public String getConsigneeName() 
    {
        return consigneeName;
    }
    public void setConsigneeMobile(String consigneeMobile) 
    {
        this.consigneeMobile = consigneeMobile;
    }

    public String getConsigneeMobile() 
    {
        return consigneeMobile;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Integer getSumDay() {
        return sumDay;
    }

    public void setSumDay(Integer sumDay) {
        this.sumDay = sumDay;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodsId", getGoodsId())
            .append("typeId", getTypeId())
            .append("classId", getClassId())
            .append("supplyType", getSupplyType())
            .append("supplySend", getSupplySend())
            .append("price", getPrice())
            .append("total", getTotal())
            .append("sendTotal", getSendTotal())
            .append("created", getCreated())
            .append("endTime", getEndTime())
            .append("supplyNo", getSupplyNo())
            .append("status", getStatus())
            .append("isUrge", getIsUrge())
            .append("consigneeName", getConsigneeName())
            .append("consigneeMobile", getConsigneeMobile())
            .append("area", getArea())
            .append("address", getAddress())
            .toString();
    }
}
