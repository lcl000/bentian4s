package com.ruoyi.project.system.coinrecord.enums;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/9/23 22:11
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
public enum CoinIncome {
	NEW_MEMBER(0,"推荐新用户"),
	CLOCK(1,"签到"),
	ACTIVITY(2,"转发活动"),
	AMWAY_CAR(3,"推荐购车"),
	ONE_PROTECT(5,"首保奖励"),
	PRIZE(6,"抽奖"),
	NEW_CAR(11,"购买新车"),
	ONE_INSURANCE(13,"单交强险"),
	TWO_INSURANCE(14,"商业险+交强险"),
	MAINTAIN(15,"维护保养"),
	PAINTING(16,"自费钣金、喷漆"),
	ACCIDENT_CAR(17,"事故车推荐"),
	BOUTIQUE(18,"精品"),
	SUBSTITUTION(19,"置换"),
	ADDITIONAL_PURCHASE(20,"加购");

	private int value;
	private String name;

	CoinIncome(int value, String name) {
		this.value = value;
		this.name = name;
	}

	/**
	 * 数据范围校验
	 *
	 * @param value
	 * @return
	 */
	public static boolean contains(int value) {
		return null == getByValue(value)?Boolean.FALSE.booleanValue():Boolean.TRUE.booleanValue();
	}
	public int getValue() {
		return this.value;
	}

	public String getName(){
		return this.name;
	}

	public static CoinIncome getByValue(int value){
		CoinIncome allVaues[] = CoinIncome.values();
		CoinIncome status;
		for (int i=0,len = allVaues.length;i<len;i++){
			status = allVaues[i];
			if(status.getValue()==value){
				return status;
			}
		}
		return null;
	}
}
