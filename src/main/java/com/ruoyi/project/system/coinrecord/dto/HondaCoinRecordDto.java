package com.ruoyi.project.system.coinrecord.dto;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.project.system.coinrecord.domain.HondaCoinRecord;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/29 21:31
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public class HondaCoinRecordDto extends HondaCoinRecord {
	//用户自己的名称
	private String username;
	//获取时相关人员名称
	private String memberName;

	// 绑车时用户姓名
	@Excel(name = "用户姓名",sort = 1)
	private String carName;
	// 绑车时用户手机号
	@Excel(name = "用户手机号",sort = 2)
	private String carMobile;
	// 车牌
	@Excel(name = "车牌",sort = 3)
	private String carLicensePlate;
	// 车架号
	@Excel(name = "车架号",sort = 4)
	private String carFrameNo;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getCarName() {
		return carName;
	}

	public void setCarName(String carName) {
		this.carName = carName;
	}

	public String getCarMobile() {
		return carMobile;
	}

	public void setCarMobile(String carMobile) {
		this.carMobile = carMobile;
	}

	public String getCarLicensePlate() {
		return carLicensePlate;
	}

	public void setCarLicensePlate(String carLicensePlate) {
		this.carLicensePlate = carLicensePlate;
	}

	public String getCarFrameNo() {
		return carFrameNo;
	}

	public void setCarFrameNo(String carFrameNo) {
		this.carFrameNo = carFrameNo;
	}
}
