package com.ruoyi.project.system.coinrecord.domain;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import com.ruoyi.project.system.coinrecord.enums.CoinExpenditure;
import com.ruoyi.project.system.coinrecord.enums.CoinIncome;
import com.ruoyi.project.system.dealer.domain.Dealer;
import com.ruoyi.project.system.membercar.domain.MemberCar;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 好奇币记录对象 haoqi_coin_record
 * 
 * @author LCL
 * @date 2020-06-29
 */
public class HondaCoinRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    private MemberCar memberCar;

    public MemberCar getMemberCar() {
        return memberCar;
    }

    public void setMemberCar(MemberCar memberCar) {
        this.memberCar = memberCar;
    }

    private Dealer dealer;

    public Dealer getDealer() {
        return dealer;
    }

    public void setDealer(Dealer dealer) {
        this.dealer = dealer;
    }

    /** 主键ID */
    private String id;

    /** 用户id */
    private String memberId;

    /** 改变的好奇币数量 */
    @Excel(name = "智通币数量",sort = 5)
    private BigDecimal num;

    /** 状态: 0 未到账 1 已到账 2 已拒绝*/
    private Integer status;

    /** 类型: 0 充值 1 提现 */
    @Excel(name = "类型: 0 收入 1 支出",sort = 6)
    private Integer type;

    /** 来源: 0 推荐的用户注册 1 打卡 2 转发活动 3 推荐人购车 4 推荐人续保 5 首保奖励 6 抽奖 */
    private Integer getType;

    /** 资源ID */
    private String resId;

    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd",sort = 7)
    private Date created;

    @Excel(name = "科目",sort = 8)
    private String getTypeName;


    //查询用 开始时间
    private String btime;
    //查询用 结束时间
    private String etime;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setNum(BigDecimal num)
    {
        this.num = num;
    }

    public BigDecimal getNum()
    {
        return num;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setGetType(Integer getType) 
    {
        this.getType = getType;
    }

    public Integer getGetType() 
    {
        return getType;
    }

    public String getResId() {
        return resId;
    }

    public void setResId(String resId) {
        this.resId = resId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getGetTypeName() {
        return getTypeName;
    }

    public void setGetTypeName(Integer type) {
        if(type==0){
            this.getTypeName = CoinIncome.getByValue(getGetType()).getName();
        }else{
            this.getTypeName = CoinExpenditure.getByValue(getGetType()).getName();
        }
    }

    public String getBtime() {
        return btime;
    }

    public void setBtime(String btime) {
        this.btime = btime;
    }

    public String getEtime() {
        return etime;
    }

    public void setEtime(String etime) {
        this.etime = etime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("memberId", getMemberId())
            .append("num", getNum())
            .append("status", getStatus())
            .append("type", getType())
            .append("getType", getGetType())
            .append("resId", getResId())
            .append("created", getCreated())
            .toString();
    }
}
