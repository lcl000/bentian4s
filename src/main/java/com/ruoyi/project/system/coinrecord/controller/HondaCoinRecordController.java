package com.ruoyi.project.system.coinrecord.controller;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.coinrecord.domain.HondaCoinRecord;
import com.ruoyi.project.system.coinrecord.dto.HondaCoinRecordDto;
import com.ruoyi.project.system.coinrecord.service.IHondaCoinRecordService;
import com.ruoyi.project.system.dealer.service.IDealerService;
import com.ruoyi.project.system.member.domain.Member;
import com.ruoyi.project.system.member.service.IMemberService;
import com.ruoyi.project.system.membercar.service.IMemberCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 收益记录Controller
 * 
 * @author LCL
 * @date 2020-06-29
 */
@Controller
@RequestMapping("/system/coinrecord")
public class HondaCoinRecordController extends BaseController
{
    private String prefix = "system/coinrecord";

    @Autowired
    private IDealerService dealerService;
    @Autowired
    private IMemberCarService memberCarService;
    @Autowired
    private IHondaCoinRecordService hondaCoinRecordService;

    @Autowired
    private IMemberService memberService;

    @GetMapping()
    public String coinrecord(ModelMap map)
    {
        map.put("memberList",this.memberService.findAll());
        return prefix + "/coinrecord";
    }

    /**
     * 查询收益记录列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(HondaCoinRecord hondaCoinRecord)
    {
        startPage();
        List<HondaCoinRecordDto> list = hondaCoinRecordService.selectHondaCoinRecordList(hondaCoinRecord);
        for(HondaCoinRecordDto mem :list){
            mem.setMemberCar(this.memberCarService.findByUid(mem.getMemberId()));
            Member byId = this.memberService.selectMemberById(mem.getMemberId());
            String byIdDealerId = byId.getDealerId();
            mem.setDealer(this.dealerService.selectDealerById(byIdDealerId));
        }
        return getDataTable(list);
    }

    /**
     * 导出收益记录列表
     */
    @Log(title = "收益记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(HondaCoinRecord hondaCoinRecord)
    {
        List<HondaCoinRecordDto> list = hondaCoinRecordService.selectHondaCoinRecordList(hondaCoinRecord);
        ExcelUtil<HondaCoinRecordDto> util = new ExcelUtil<HondaCoinRecordDto>(HondaCoinRecordDto.class);
        return util.exportExcel(list, "coinrecord");
    }

    /**
     * 新增收益记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存收益记录
     */
    @Log(title = "收益记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(HondaCoinRecord hondaCoinRecord)
    {
        return toAjax(hondaCoinRecordService.insertHondaCoinRecord(hondaCoinRecord));
    }

    /**
     * 修改收益记录
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        HondaCoinRecord hondaCoinRecord = hondaCoinRecordService.selectHondaCoinRecordById(id);
        mmap.put("hondaCoinRecord", hondaCoinRecord);
        return prefix + "/edit";
    }

    /**
     * 修改保存收益记录
     */
    @Log(title = "收益记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(HondaCoinRecord hondaCoinRecord)
    {
        return toAjax(hondaCoinRecordService.updateHondaCoinRecord(hondaCoinRecord));
    }

    /**
     * 删除收益记录
     */
    @Log(title = "收益记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(hondaCoinRecordService.deleteHondaCoinRecordByIds(ids));
    }


    /**
     * 提现完成
     * @param id
     * @return
     */
    @PostMapping("/yes/{id}")
    @Transactional
    @ResponseBody
    public AjaxResult refuse(@PathVariable String id){
        HondaCoinRecord hondaCoinRecord = new HondaCoinRecord();
        hondaCoinRecord.setId(id);
        hondaCoinRecord.setStatus(1);
        this.hondaCoinRecordService.updateHondaCoinRecord(hondaCoinRecord);
        return AjaxResult.success();
    }
    /**
     * 提现完成
     * @param id
     * @return
     */
    @PostMapping("/no/{id}")
    @Transactional
    @ResponseBody
    public AjaxResult noRefuse(@PathVariable String id){
        HondaCoinRecord oldRec = this.hondaCoinRecordService.selectHondaCoinRecordById(id);
        HondaCoinRecord hondaCoinRecord = new HondaCoinRecord();
        hondaCoinRecord.setId(id);
        hondaCoinRecord.setStatus(2);
        this.hondaCoinRecordService.updateHondaCoinRecord(hondaCoinRecord);
        //金额返回给用户
        Member member = this.memberService.selectMemberById(oldRec.getMemberId());

        Member update = new Member();
        update.setId(member.getId());
        update.setCoin(member.getCoin().add(oldRec.getNum()));
        this.memberService.updateMember(update);
        return AjaxResult.success();
    }

}
