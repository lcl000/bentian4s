package com.ruoyi.project.system.coinrecord.mapper;

import java.util.List;

import com.ruoyi.project.system.coinrecord.domain.HondaCoinRecord;
import com.ruoyi.project.system.coinrecord.dto.HondaCoinRecordDto;

/**
 * 好奇币记录Mapper接口
 * 
 * @author LCL
 * @date 2020-06-29
 */
public interface HondaCoinRecordMapper
{
    /**
     * 查询好奇币记录
     * 
     * @param id 好奇币记录ID
     * @return 好奇币记录
     */
    public HondaCoinRecord selectHondaCoinRecordById(String id);

    /**
     * 查询好奇币记录列表
     * 
     * @param hondaCoinRecord 好奇币记录
     * @return 好奇币记录集合
     */
    public List<HondaCoinRecordDto> selectHondaCoinRecordList(HondaCoinRecord hondaCoinRecord);

    /**
     * 新增好奇币记录
     * 
     * @param hondaCoinRecord 好奇币记录
     * @return 结果
     */
    public int insertHondaCoinRecord(HondaCoinRecord hondaCoinRecord);

    /**
     * 修改好奇币记录
     * 
     * @param hondaCoinRecord 好奇币记录
     * @return 结果
     */
    public int updateHondaCoinRecord(HondaCoinRecord hondaCoinRecord);

    /**
     * 删除好奇币记录
     * 
     * @param id 好奇币记录ID
     * @return 结果
     */
    public int deleteHondaCoinRecordById(String id);

    /**
     * 批量删除好奇币记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteHondaCoinRecordByIds(String[] ids);
}
