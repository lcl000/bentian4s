package com.ruoyi.project.system.coinrecord.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.coinrecord.dto.HondaCoinRecordDto;
import com.ruoyi.project.system.coinrecord.mapper.HondaCoinRecordMapper;
import com.ruoyi.project.system.coinrecord.domain.HondaCoinRecord;
import com.ruoyi.project.system.coinrecord.service.IHondaCoinRecordService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 好奇币记录Service业务层处理
 * 
 * @author LCL
 * @date 2020-06-29
 */
@Service
public class HondaCoinRecordServiceImpl implements IHondaCoinRecordService
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private HondaCoinRecordMapper hondaCoinRecordMapper;

    /**
     * 查询好奇币记录
     * 
     * @param id 好奇币记录ID
     * @return 好奇币记录
     */
    @Override
    public HondaCoinRecord selectHondaCoinRecordById(String id)
    {
        return hondaCoinRecordMapper.selectHondaCoinRecordById(id);
    }

    /**
     * 查询好奇币记录列表
     * 
     * @param hondaCoinRecord 好奇币记录
     * @return 好奇币记录
     */
    @Override
    public List<HondaCoinRecordDto> selectHondaCoinRecordList(HondaCoinRecord hondaCoinRecord)
    {
        return hondaCoinRecordMapper.selectHondaCoinRecordList(hondaCoinRecord);
    }

    /**
     * 新增好奇币记录
     * 
     * @param hondaCoinRecord 好奇币记录
     * @return 结果
     */
    @Override
    public int insertHondaCoinRecord(HondaCoinRecord hondaCoinRecord)
    {
        hondaCoinRecord.setId(this.idGererateFactory.nextStringId());
        return hondaCoinRecordMapper.insertHondaCoinRecord(hondaCoinRecord);
    }

    /**
     * 修改好奇币记录
     * 
     * @param hondaCoinRecord 好奇币记录
     * @return 结果
     */
    @Override
    public int updateHondaCoinRecord(HondaCoinRecord hondaCoinRecord)
    {
        return hondaCoinRecordMapper.updateHondaCoinRecord(hondaCoinRecord);
    }

    /**
     * 删除好奇币记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteHondaCoinRecordByIds(String ids)
    {
        return hondaCoinRecordMapper.deleteHondaCoinRecordByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除好奇币记录信息
     * 
     * @param id 好奇币记录ID
     * @return 结果
     */
    @Override
    public int deleteHondaCoinRecordById(String id)
    {
        return hondaCoinRecordMapper.deleteHondaCoinRecordById(id);
    }
}
