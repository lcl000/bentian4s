package com.ruoyi.project.system.coinrecord.enums;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/9/23 22:22
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
public enum CoinExpenditure {
	ORDER(0,"积分订单"),
	LUCK_DRAW(1,"抽奖");

	private int value;
	private String name;

	CoinExpenditure(int value, String name) {
		this.value = value;
		this.name = name;
	}

	/**
	 * 数据范围校验
	 *
	 * @param value
	 * @return
	 */
	public static boolean contains(int value) {
		return null == getByValue(value)?Boolean.FALSE.booleanValue():Boolean.TRUE.booleanValue();
	}
	public int getValue() {
		return this.value;
	}

	public String getName(){
		return this.name;
	}

	public static CoinExpenditure getByValue(int value){
		CoinExpenditure allVaues[] = CoinExpenditure.values();
		CoinExpenditure status;
		for (int i=0,len = allVaues.length;i<len;i++){
			status = allVaues[i];
			if(status.getValue()==value){
				return status;
			}
		}
		return null;
	}
}
