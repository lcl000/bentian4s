package com.ruoyi.project.system.brand.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.brand.domain.Brand;
import com.ruoyi.project.system.brand.service.IBrandService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 品牌Controller
 * 
 * @author LCL
 * @date 2020-06-12
 */
@Controller
@RequestMapping("/system/brand")
public class BrandController extends BaseController
{
    private String prefix = "system/brand";

    @Autowired
    private IBrandService brandService;

    @RequiresPermissions("system:brand:view")
    @GetMapping()
    public String brand()
    {
        return prefix + "/brand";
    }

    /**
     * 查询品牌列表
     */
    @RequiresPermissions("system:brand:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Brand brand)
    {
        startPage();
        List<Brand> list = brandService.selectBrandList(brand);
        return getDataTable(list);
    }

    /**
     * 导出品牌列表
     */
    @RequiresPermissions("system:brand:export")
    @Log(title = "品牌", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Brand brand)
    {
        List<Brand> list = brandService.selectBrandList(brand);
        ExcelUtil<Brand> util = new ExcelUtil<Brand>(Brand.class);
        return util.exportExcel(list, "brand");
    }

    /**
     * 新增品牌
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存品牌
     */
    @RequiresPermissions("system:brand:add")
    @Log(title = "品牌", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    @Transactional
    public AjaxResult addSave(Brand brand)
    {
        brand.setCreated(DateUtils.getNowDate());
        brandService.insertBrand(brand);
        //通过图片路径修改附件表中的相关资源数据
//        this.uploadFileService.updateUploadFileByRealPaths(Arrays.asList(brand.getUrlReal()),new UploadFile(null,null,null,brand.getId()));
        return AjaxResult.success();
    }

    /**
     * 修改品牌
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        Brand brand = brandService.selectBrandById(id);
        mmap.put("brand", brand);
        return prefix + "/edit";
    }

    /**
     * 修改保存品牌
     */
    @RequiresPermissions("system:brand:edit")
    @Log(title = "品牌", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    @Transactional
    public AjaxResult editSave(Brand brand)
    {
        //查询数据库获取旧数据
        Brand oldBrand = brandService.selectBrandById(brand.getId());
        //判断数据是否存在
        if(oldBrand==null){
            return AjaxResult.error("数据不存在");
        }
        //更新数据
        brandService.updateBrand(brand);
//        //删除的file集合
//        List delRealPaths = new ArrayList<String>();
//
//        //新增的file集合
//        List addRealPaths = new ArrayList<String>();
//
//        //判断1.是否是新增图片 2.是否修改背景图片
//        if(oldBrand.getUrl()==null){
//            addRealPaths.add(brand.getUrl());
//        }else if(!oldBrand.getUrl().equals(brand.getUrl())){
//            //删除原本存在的旧图
//            FileUploadUtils.delete(oldBrand.getUrlReal());
//
//            delRealPaths.add(oldBrand.getUrlReal());
//
//            addRealPaths.add(brand.getUrlReal());
//        }
//
//        if(delRealPaths.size()!=0){
//            //通过图片路径删除附件表中的数据
//            this.uploadFileService.deleteUploadFileByRealPaths(delRealPaths);
//        }
//        if(addRealPaths.size()!=0){
//            //通过图片路径修改附件表中的相关资源数据
//            this.uploadFileService.updateUploadFileByRealPaths(addRealPaths,new UploadFile(null,null,null,brand.getId()));
//        }

        return AjaxResult.success();
    }

    /**
     * 删除品牌
     */
    @RequiresPermissions("system:brand:remove")
    @Log(title = "品牌", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        //查询数据库中旧数据
        List<Brand> brandList = this.brandService.selectBrandByIds(ids);
        //删除数据
        brandService.deleteBrandByIds(ids);
//        for(Brand brand:brandList){
//            //删除原本的旧图
//            FileUploadUtils.delete(brand.getUrlReal());
//            //删除资源文件表中的数据
//            this.uploadFileService.deleteUploadFileByRealPaths(Arrays.asList(brand.getUrlReal()));
//        }
        return AjaxResult.success();
    }
}
