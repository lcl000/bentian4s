package com.ruoyi.project.system.brand.service;

import java.util.List;
import com.ruoyi.project.system.brand.domain.Brand;

/**
 * 品牌Service接口
 * 
 * @author LCL
 * @date 2020-06-12
 */
public interface IBrandService 
{
    /**
     * 查询品牌
     * 
     * @param id 品牌ID
     * @return 品牌
     */
    public Brand selectBrandById(String id);

    /**
     * 查询品牌列表
     * 
     * @param brand 品牌
     * @return 品牌集合
     */
    public List<Brand> selectBrandList(Brand brand);

    /**
     * 新增品牌
     * 
     * @param brand 品牌
     * @return 结果
     */
    public int insertBrand(Brand brand);

    /**
     * 修改品牌
     * 
     * @param brand 品牌
     * @return 结果
     */
    public int updateBrand(Brand brand);

    /**
     * 批量删除品牌
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBrandByIds(String ids);

    /**
     * 删除品牌信息
     * 
     * @param id 品牌ID
     * @return 结果
     */
    public int deleteBrandById(String id);

	List<Brand> selectBrandByIds(String ids);

	List<Brand> findAll();
}
