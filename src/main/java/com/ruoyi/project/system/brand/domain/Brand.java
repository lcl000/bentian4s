package com.ruoyi.project.system.brand.domain;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 品牌对象 brand
 * 
 * @author LCL
 * @date 2020-06-12
 */
public class Brand extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 品牌名称 */
    @Excel(name = "品牌名称")
    private String name;

    /** 品牌图标访问路径 */
    @Excel(name = "品牌图标访问路径")
    private String url;

    /** 品牌图标真实路径 */
    private String urlReal;

    /** 介绍 */
    private String note;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date created;

    /** 品牌地址 */
    @Excel(name = "品牌地址")
    private String httpUrl;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }
    public void setUrlReal(String urlReal) 
    {
        this.urlReal = urlReal;
    }

    public String getUrlReal() 
    {
        return urlReal;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setCreated(Date created) 
    {
        this.created = created;
    }

    public Date getCreated() 
    {
        return created;
    }
    public void setHttpUrl(String httpUrl) 
    {
        this.httpUrl = httpUrl;
    }

    public String getHttpUrl() 
    {
        return httpUrl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("url", getUrl())
            .append("urlReal", getUrlReal())
            .append("note", getNote())
            .append("sort", getSort())
            .append("created", getCreated())
            .append("httpUrl", getHttpUrl())
            .toString();
    }
}
