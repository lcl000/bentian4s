package com.ruoyi.project.system.brand.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.brand.mapper.BrandMapper;
import com.ruoyi.project.system.brand.domain.Brand;
import com.ruoyi.project.system.brand.service.IBrandService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 品牌Service业务层处理
 * 
 * @author LCL
 * @date 2020-06-12
 */
@Service
public class BrandServiceImpl implements IBrandService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private BrandMapper brandMapper;

    /**
     * 查询品牌
     * 
     * @param id 品牌ID
     * @return 品牌
     */
    @Override
    public Brand selectBrandById(String id)
    {
        return brandMapper.selectBrandById(id);
    }

    /**
     * 查询品牌列表
     * 
     * @param brand 品牌
     * @return 品牌
     */
    @Override
    public List<Brand> selectBrandList(Brand brand)
    {
        return brandMapper.selectBrandList(brand);
    }

    /**
     * 新增品牌
     * 
     * @param brand 品牌
     * @return 结果
     */
    @Override
    public int insertBrand(Brand brand)
    {
        brand.setId(this.idGererateFactory.nextStringId());
        return brandMapper.insertBrand(brand);
    }

    /**
     * 修改品牌
     * 
     * @param brand 品牌
     * @return 结果
     */
    @Override
    public int updateBrand(Brand brand)
    {
        return brandMapper.updateBrand(brand);
    }

    /**
     * 删除品牌对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBrandByIds(String ids)
    {
        return brandMapper.deleteBrandByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除品牌信息
     * 
     * @param id 品牌ID
     * @return 结果
     */
    @Override
    public int deleteBrandById(String id)
    {
        return brandMapper.deleteBrandById(id);
    }

    @Override
    public List<Brand> selectBrandByIds(String ids) {
        return brandMapper.selectBrandByIds(Convert.toStrArray(ids));
    }

    @Override
    public List<Brand> findAll() {
        return this.brandMapper.findAll();
    }
}
