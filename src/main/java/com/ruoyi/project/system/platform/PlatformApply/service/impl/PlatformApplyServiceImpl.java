package com.ruoyi.project.system.platform.PlatformApply.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.platform.PlatformApply.dto.PlatformApplyDto;
import com.ruoyi.project.system.platform.PlatformApply.mapper.PlatformApplyMapper;
import com.ruoyi.project.system.platform.PlatformApply.domain.PlatformApply;
import com.ruoyi.project.system.platform.PlatformApply.service.IPlatformApplyService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 平台申请Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-31
 */
@Service
public class PlatformApplyServiceImpl implements IPlatformApplyService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private PlatformApplyMapper platformApplyMapper;

    /**
     * 查询平台申请
     * 
     * @param id 平台申请ID
     * @return 平台申请
     */
    @Override
    public PlatformApply selectPlatformApplyById(String id)
    {
        return platformApplyMapper.selectPlatformApplyById(id);
    }

    /**
     * 查询平台申请列表
     * 
     * @param platformApply 平台申请
     * @return 平台申请
     */
    @Override
    public List<PlatformApplyDto> selectPlatformApplyList(PlatformApply platformApply)
    {
        return platformApplyMapper.selectPlatformApplyList(platformApply);
    }

    /**
     * 新增平台申请
     * 
     * @param platformApply 平台申请
     * @return 结果
     */
    @Override
    public int insertPlatformApply(PlatformApply platformApply)
    {
        platformApply.setId(this.idGererateFactory.nextStringId());
        return platformApplyMapper.insertPlatformApply(platformApply);
    }

    /**
     * 修改平台申请
     * 
     * @param platformApply 平台申请
     * @return 结果
     */
    @Override
    public int updatePlatformApply(PlatformApply platformApply)
    {
        return platformApplyMapper.updatePlatformApply(platformApply);
    }

    /**
     * 删除平台申请对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePlatformApplyByIds(String ids)
    {
        return platformApplyMapper.deletePlatformApplyByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除平台申请信息
     * 
     * @param id 平台申请ID
     * @return 结果
     */
    @Override
    public int deletePlatformApplyById(String id)
    {
        return platformApplyMapper.deletePlatformApplyById(id);
    }
}
