package com.ruoyi.project.system.platform.platform.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.platform.platform.domain.Platform;
import com.ruoyi.project.system.platform.platform.service.IPlatformService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 平台列Controller
 * 
 * @author LCL
 * @date 2020-07-31
 */
@Controller
@RequestMapping("/system/platform")
public class PlatformController extends BaseController
{
    private String prefix = "system/platform/platform";

    @Autowired
    private IPlatformService platformService;

    @RequiresPermissions("system:platform:view")
    @GetMapping()
    public String platform()
    {
        return prefix + "/platform";
    }

    /**
     * 查询平台列列表
     */
    @RequiresPermissions("system:platform:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Platform platform)
    {
        startPage();
        List<Platform> list = platformService.selectPlatformList(platform);
        return getDataTable(list);
    }

    /**
     * 导出平台列列表
     */
    @RequiresPermissions("system:platform:export")
    @Log(title = "平台列", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Platform platform)
    {
        List<Platform> list = platformService.selectPlatformList(platform);
        ExcelUtil<Platform> util = new ExcelUtil<Platform>(Platform.class);
        return util.exportExcel(list, "platform");
    }

    /**
     * 新增平台列
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存平台列
     */
    @RequiresPermissions("system:platform:add")
    @Log(title = "平台列", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Platform platform)
    {
        return toAjax(platformService.insertPlatform(platform));
    }

    /**
     * 修改平台列
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        Platform platform = platformService.selectPlatformById(id);
        mmap.put("platform", platform);
        return prefix + "/edit";
    }

    /**
     * 修改保存平台列
     */
    @RequiresPermissions("system:platform:edit")
    @Log(title = "平台列", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Platform platform)
    {
        return toAjax(platformService.updatePlatform(platform));
    }

    /**
     * 删除平台列
     */
    @RequiresPermissions("system:platform:remove")
    @Log(title = "平台列", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(platformService.deletePlatformByIds(ids));
    }
}
