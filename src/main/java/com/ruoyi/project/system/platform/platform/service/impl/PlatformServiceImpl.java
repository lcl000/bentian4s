package com.ruoyi.project.system.platform.platform.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.platform.platform.mapper.PlatformMapper;
import com.ruoyi.project.system.platform.platform.domain.Platform;
import com.ruoyi.project.system.platform.platform.service.IPlatformService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 平台列Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-31
 */
@Service
public class PlatformServiceImpl implements IPlatformService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private PlatformMapper platformMapper;

    /**
     * 查询平台列
     * 
     * @param id 平台列ID
     * @return 平台列
     */
    @Override
    public Platform selectPlatformById(String id)
    {
        return platformMapper.selectPlatformById(id);
    }

    /**
     * 查询平台列列表
     * 
     * @param platform 平台列
     * @return 平台列
     */
    @Override
    public List<Platform> selectPlatformList(Platform platform)
    {
        return platformMapper.selectPlatformList(platform);
    }

    /**
     * 新增平台列
     * 
     * @param platform 平台列
     * @return 结果
     */
    @Override
    public int insertPlatform(Platform platform)
    {
        platform.setId(this.idGererateFactory.nextStringId());
        return platformMapper.insertPlatform(platform);
    }

    /**
     * 修改平台列
     * 
     * @param platform 平台列
     * @return 结果
     */
    @Override
    public int updatePlatform(Platform platform)
    {
        return platformMapper.updatePlatform(platform);
    }

    /**
     * 删除平台列对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePlatformByIds(String ids)
    {
        return platformMapper.deletePlatformByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除平台列信息
     * 
     * @param id 平台列ID
     * @return 结果
     */
    @Override
    public int deletePlatformById(String id)
    {
        return platformMapper.deletePlatformById(id);
    }

    @Override
    public List<Platform> findAll() {
        return this.platformMapper.findAll();
    }
}
