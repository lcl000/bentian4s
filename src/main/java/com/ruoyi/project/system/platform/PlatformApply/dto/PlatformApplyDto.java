package com.ruoyi.project.system.platform.PlatformApply.dto;

import com.ruoyi.project.system.platform.PlatformApply.domain.PlatformApply;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/8/3 17:48
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
public class PlatformApplyDto extends PlatformApply {

	//平台名称
	private String platformName;

	public String getPlatformName() {
		return platformName;
	}

	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}
}
