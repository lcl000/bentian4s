package com.ruoyi.project.system.platform.PlatformApply.mapper;

import java.util.List;
import com.ruoyi.project.system.platform.PlatformApply.domain.PlatformApply;
import com.ruoyi.project.system.platform.PlatformApply.dto.PlatformApplyDto;

/**
 * 平台申请Mapper接口
 * 
 * @author LCL
 * @date 2020-07-31
 */
public interface PlatformApplyMapper 
{
    /**
     * 查询平台申请
     * 
     * @param id 平台申请ID
     * @return 平台申请
     */
    public PlatformApply selectPlatformApplyById(String id);

    /**
     * 查询平台申请列表
     * 
     * @param platformApply 平台申请
     * @return 平台申请集合
     */
    public List<PlatformApplyDto> selectPlatformApplyList(PlatformApply platformApply);

    /**
     * 新增平台申请
     * 
     * @param platformApply 平台申请
     * @return 结果
     */
    public int insertPlatformApply(PlatformApply platformApply);

    /**
     * 修改平台申请
     * 
     * @param platformApply 平台申请
     * @return 结果
     */
    public int updatePlatformApply(PlatformApply platformApply);

    /**
     * 删除平台申请
     * 
     * @param id 平台申请ID
     * @return 结果
     */
    public int deletePlatformApplyById(String id);

    /**
     * 批量删除平台申请
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePlatformApplyByIds(String[] ids);
}
