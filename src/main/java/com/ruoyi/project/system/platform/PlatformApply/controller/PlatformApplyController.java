package com.ruoyi.project.system.platform.PlatformApply.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.platform.PlatformApply.domain.PlatformApply;
import com.ruoyi.project.system.platform.PlatformApply.dto.PlatformApplyDto;
import com.ruoyi.project.system.platform.PlatformApply.service.IPlatformApplyService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.platform.platform.service.IPlatformService;

/**
 * 平台申请Controller
 * 
 * @author LCL
 * @date 2020-07-31
 */
@Controller
@RequestMapping("/system/platform/apply")
public class PlatformApplyController extends BaseController
{
    private String prefix = "system/platform/PlatformApply";

    @Autowired
    private IPlatformApplyService platformApplyService;
    @Autowired
    private IPlatformService platformService;

    @RequiresPermissions("system:PlatformApply:view")
    @GetMapping()
    public String PlatformApply(ModelMap map)
    {
        map.put("platformList",this.platformService.findAll());
        return prefix + "/PlatformApply";
    }

    /**
     * 查询平台申请列表
     */
    @RequiresPermissions("system:PlatformApply:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PlatformApply platformApply)
    {
        startPage();
        List<PlatformApplyDto> list = platformApplyService.selectPlatformApplyList(platformApply);
        return getDataTable(list);
    }

    /**
     * 导出平台申请列表
     */
    @RequiresPermissions("system:PlatformApply:export")
    @Log(title = "平台申请", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PlatformApply platformApply)
    {
        List<PlatformApplyDto> list = platformApplyService.selectPlatformApplyList(platformApply);
        ExcelUtil<PlatformApplyDto> util = new ExcelUtil<PlatformApplyDto>(PlatformApplyDto.class);
        return util.exportExcel(list, "PlatformApply");
    }

    /**
     * 新增平台申请
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存平台申请
     */
    @RequiresPermissions("system:PlatformApply:add")
    @Log(title = "平台申请", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PlatformApply platformApply)
    {
        return toAjax(platformApplyService.insertPlatformApply(platformApply));
    }

    /**
     * 修改平台申请
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        PlatformApply platformApply = platformApplyService.selectPlatformApplyById(id);
        mmap.put("platformApply", platformApply);
        return prefix + "/edit";
    }

    /**
     * 修改保存平台申请
     */
    @RequiresPermissions("system:PlatformApply:edit")
    @Log(title = "平台申请", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PlatformApply platformApply)
    {
        return toAjax(platformApplyService.updatePlatformApply(platformApply));
    }

    /**
     * 删除平台申请
     */
    @RequiresPermissions("system:PlatformApply:remove")
    @Log(title = "平台申请", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(platformApplyService.deletePlatformApplyByIds(ids));
    }



    @PostMapping("/agree/{id}")
    @ResponseBody
    public AjaxResult agree(@PathVariable String id){
        PlatformApply platformApply = this.platformApplyService.selectPlatformApplyById(id);
        if(platformApply==null){
            return AjaxResult.error();
        }
        PlatformApply update = new PlatformApply();
        update.setId(id);
        update.setStatus(1);
        this.platformApplyService.updatePlatformApply(update);

        return AjaxResult.success();
    }
}
