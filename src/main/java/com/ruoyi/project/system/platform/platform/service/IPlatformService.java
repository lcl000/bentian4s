package com.ruoyi.project.system.platform.platform.service;

import java.util.List;
import com.ruoyi.project.system.platform.platform.domain.Platform;

/**
 * 平台列Service接口
 * 
 * @author LCL
 * @date 2020-07-31
 */
public interface IPlatformService 
{
    /**
     * 查询平台列
     * 
     * @param id 平台列ID
     * @return 平台列
     */
    public Platform selectPlatformById(String id);

    /**
     * 查询平台列列表
     * 
     * @param platform 平台列
     * @return 平台列集合
     */
    public List<Platform> selectPlatformList(Platform platform);

    /**
     * 新增平台列
     * 
     * @param platform 平台列
     * @return 结果
     */
    public int insertPlatform(Platform platform);

    /**
     * 修改平台列
     * 
     * @param platform 平台列
     * @return 结果
     */
    public int updatePlatform(Platform platform);

    /**
     * 批量删除平台列
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePlatformByIds(String ids);

    /**
     * 删除平台列信息
     * 
     * @param id 平台列ID
     * @return 结果
     */
    public int deletePlatformById(String id);

    List<Platform> findAll();
}
