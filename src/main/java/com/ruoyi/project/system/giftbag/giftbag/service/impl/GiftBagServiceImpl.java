package com.ruoyi.project.system.giftbag.giftbag.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.giftbag.giftbag.mapper.GiftBagMapper;
import com.ruoyi.project.system.giftbag.giftbag.domain.GiftBag;
import com.ruoyi.project.system.giftbag.giftbag.service.IGiftBagService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 礼包模板Service业务层处理
 * 
 * @author LCL
 * @date 2020-08-13
 */
@Service
public class GiftBagServiceImpl implements IGiftBagService 
{
    @Autowired
    private GiftBagMapper giftBagMapper;

    /**
     * 查询礼包模板
     * 
     * @param id 礼包模板ID
     * @return 礼包模板
     */
    @Override
    public GiftBag selectGiftBagById(String id)
    {
        return giftBagMapper.selectGiftBagById(id);
    }

    /**
     * 查询礼包模板列表
     * 
     * @param giftBag 礼包模板
     * @return 礼包模板
     */
    @Override
    public List<GiftBag> selectGiftBagList(GiftBag giftBag)
    {
        return giftBagMapper.selectGiftBagList(giftBag);
    }

    /**
     * 新增礼包模板
     * 
     * @param giftBag 礼包模板
     * @return 结果
     */
    @Override
    public int insertGiftBag(GiftBag giftBag)
    {
        return giftBagMapper.insertGiftBag(giftBag);
    }

    /**
     * 修改礼包模板
     * 
     * @param giftBag 礼包模板
     * @return 结果
     */
    @Override
    public int updateGiftBag(GiftBag giftBag)
    {
        return giftBagMapper.updateGiftBag(giftBag);
    }

    /**
     * 删除礼包模板对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteGiftBagByIds(String ids)
    {
        return giftBagMapper.deleteGiftBagByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除礼包模板信息
     * 
     * @param id 礼包模板ID
     * @return 结果
     */
    @Override
    public int deleteGiftBagById(String id)
    {
        return giftBagMapper.deleteGiftBagById(id);
    }
}
