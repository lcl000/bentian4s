package com.ruoyi.project.system.giftbag.giftbagtinfo.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ruoyi.common.utils.text.Convert;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.giftbag.giftbagtinfo.domain.GiftBagInfo;
import com.ruoyi.project.system.giftbag.giftbagtinfo.service.IGiftBagInfoService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 礼包详情Controller
 * 
 * @author LCL
 * @date 2020-08-13
 */
@Controller
@RequestMapping("/system/giftbag/giftbagtinfo")
public class GiftBagInfoController extends BaseController
{
    private String prefix = "system/giftbag/giftbagtinfo";

    @Autowired
    private IGiftBagInfoService giftBagInfoService;

    @RequiresPermissions("system/giftbag:giftbagtinfo:view")
    @GetMapping()
    public String giftbagtinfo()
    {
        return prefix + "/giftbagtinfo";
    }

    /**
     * 查询礼包详情列表
     */
    @RequiresPermissions("system/giftbag:giftbagtinfo:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(GiftBagInfo giftBagInfo)
    {
        startPage();
        List<GiftBagInfo> list = giftBagInfoService.selectGiftBagInfoList(giftBagInfo);
        return getDataTable(list);
    }

    /**
     * 导出礼包详情列表
     */
    @RequiresPermissions("system/giftbag:giftbagtinfo:export")
    @Log(title = "礼包详情", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(GiftBagInfo giftBagInfo)
    {
        List<GiftBagInfo> list = giftBagInfoService.selectGiftBagInfoList(giftBagInfo);
        ExcelUtil<GiftBagInfo> util = new ExcelUtil<GiftBagInfo>(GiftBagInfo.class);
        return util.exportExcel(list, "giftbagtinfo");
    }

    /**
     * 新增礼包详情
     */
    @GetMapping("/add/{id}")
    public String add(@PathVariable String id,ModelMap map)
    {
        map.put("id",id);
        return prefix + "/add";
    }

    /**
     * 新增保存礼包详情
     */
    @RequiresPermissions("system/giftbag:giftbagtinfo:add")
    @Log(title = "礼包详情", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    @Transactional
    public AjaxResult addSave(GiftBagInfo giftBagInfo)
    {
        giftBagInfoService.insertGiftBagInfo(giftBagInfo);
        //更新礼包总数
        this.giftBagInfoService.updateGiftBagSum(giftBagInfo.getGiftBagId());
        return AjaxResult.success();
    }

    /**
     * 修改礼包详情
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        GiftBagInfo giftBagInfo = giftBagInfoService.selectGiftBagInfoById(id);
        mmap.put("giftBagInfo", giftBagInfo);
        return prefix + "/edit";
    }

    /**
     * 修改保存礼包详情
     */
    @RequiresPermissions("system/giftbag:giftbagtinfo:edit")
    @Log(title = "礼包详情", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    @Transactional
    public AjaxResult editSave(GiftBagInfo giftBagInfo)
    {
        giftBagInfoService.updateGiftBagInfo(giftBagInfo);
        //更新礼包总数
        this.giftBagInfoService.updateGiftBagSum(giftBagInfo.getGiftBagId());
        return AjaxResult.success();
    }

    /**
     * 删除礼包详情
     */
    @RequiresPermissions("system/giftbag:giftbagtinfo:remove")
    @Log(title = "礼包详情", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        GiftBagInfo giftBagInfo = this.giftBagInfoService.selectGiftBagInfoById(Convert.toStrArray(ids)[0]);
        giftBagInfoService.deleteGiftBagInfoByIds(ids);
        //更新礼包总数
        this.giftBagInfoService.updateGiftBagSum(giftBagInfo.getGiftBagId());
        return AjaxResult.success();
    }
}
