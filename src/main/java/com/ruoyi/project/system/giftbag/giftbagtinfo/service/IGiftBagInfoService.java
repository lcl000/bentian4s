package com.ruoyi.project.system.giftbag.giftbagtinfo.service;

import java.util.List;
import com.ruoyi.project.system.giftbag.giftbagtinfo.domain.GiftBagInfo;

/**
 * 礼包详情Service接口
 * 
 * @author LCL
 * @date 2020-08-13
 */
public interface IGiftBagInfoService 
{
    /**
     * 查询礼包详情
     * 
     * @param id 礼包详情ID
     * @return 礼包详情
     */
    public GiftBagInfo selectGiftBagInfoById(String id);

    /**
     * 查询礼包详情列表
     * 
     * @param giftBagInfo 礼包详情
     * @return 礼包详情集合
     */
    public List<GiftBagInfo> selectGiftBagInfoList(GiftBagInfo giftBagInfo);

    /**
     * 新增礼包详情
     * 
     * @param giftBagInfo 礼包详情
     * @return 结果
     */
    public int insertGiftBagInfo(GiftBagInfo giftBagInfo);

    /**
     * 修改礼包详情
     * 
     * @param giftBagInfo 礼包详情
     * @return 结果
     */
    public int updateGiftBagInfo(GiftBagInfo giftBagInfo);

    /**
     * 批量删除礼包详情
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGiftBagInfoByIds(String ids);

    /**
     * 删除礼包详情信息
     * 
     * @param id 礼包详情ID
     * @return 结果
     */
    public int deleteGiftBagInfoById(String id);

	int updateGiftBagSum(String giftBagId);
}
