package com.ruoyi.project.system.giftbag.giftbagtinfo.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.giftbag.giftbagtinfo.mapper.GiftBagInfoMapper;
import com.ruoyi.project.system.giftbag.giftbagtinfo.domain.GiftBagInfo;
import com.ruoyi.project.system.giftbag.giftbagtinfo.service.IGiftBagInfoService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 礼包详情Service业务层处理
 * 
 * @author LCL
 * @date 2020-08-13
 */
@Service
public class GiftBagInfoServiceImpl implements IGiftBagInfoService 
{
    @Resource
    private IdGererateFactory idGererateFactory;

    @Autowired
    private GiftBagInfoMapper giftBagInfoMapper;

    /**
     * 查询礼包详情
     * 
     * @param id 礼包详情ID
     * @return 礼包详情
     */
    @Override
    public GiftBagInfo selectGiftBagInfoById(String id)
    {
        return giftBagInfoMapper.selectGiftBagInfoById(id);
    }

    /**
     * 查询礼包详情列表
     * 
     * @param giftBagInfo 礼包详情
     * @return 礼包详情
     */
    @Override
    public List<GiftBagInfo> selectGiftBagInfoList(GiftBagInfo giftBagInfo)
    {
        return giftBagInfoMapper.selectGiftBagInfoList(giftBagInfo);
    }

    /**
     * 新增礼包详情
     * 
     * @param giftBagInfo 礼包详情
     * @return 结果
     */
    @Override
    public int insertGiftBagInfo(GiftBagInfo giftBagInfo)
    {
        giftBagInfo.setId(this.idGererateFactory.nextStringId());
        return giftBagInfoMapper.insertGiftBagInfo(giftBagInfo);
    }

    /**
     * 修改礼包详情
     * 
     * @param giftBagInfo 礼包详情
     * @return 结果
     */
    @Override
    public int updateGiftBagInfo(GiftBagInfo giftBagInfo)
    {
        return giftBagInfoMapper.updateGiftBagInfo(giftBagInfo);
    }

    /**
     * 删除礼包详情对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteGiftBagInfoByIds(String ids)
    {
        return giftBagInfoMapper.deleteGiftBagInfoByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除礼包详情信息
     * 
     * @param id 礼包详情ID
     * @return 结果
     */
    @Override
    public int deleteGiftBagInfoById(String id)
    {
        return giftBagInfoMapper.deleteGiftBagInfoById(id);
    }

    @Override
    public int updateGiftBagSum(String giftBagId) {
        return this.giftBagInfoMapper.updateGiftBagSum(giftBagId);
    }
}
