package com.ruoyi.project.system.giftbag.giftbag.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.giftbag.giftbag.domain.GiftBag;
import com.ruoyi.project.system.giftbag.giftbag.service.IGiftBagService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.giftbag.giftbagtinfo.service.IGiftBagInfoService;

/**
 * 礼包模板Controller
 * 
 * @author LCL
 * @date 2020-08-13
 */
@Controller
@RequestMapping("/system/giftbag/giftbag")
public class GiftBagController extends BaseController
{
    private String prefix = "system/giftbag/giftbag";

    @Autowired
    private IGiftBagService giftBagService;

    @Autowired
    private IGiftBagInfoService giftBagInfoService;

    @RequiresPermissions("system/giftbag:giftbag:view")
    @GetMapping()
    public String giftbag()
    {
        return prefix + "/giftbag";
    }

    /**
     * 查询礼包模板列表
     */
    @RequiresPermissions("system/giftbag:giftbag:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(GiftBag giftBag)
    {
        startPage();
        List<GiftBag> list = giftBagService.selectGiftBagList(giftBag);
        return getDataTable(list);
    }

    /**
     * 查询字典详细
     */
    @GetMapping("/detail/{giftbagId}")
    public String detail(@PathVariable("giftbagId") String giftbagId, ModelMap mmap)
    {
        mmap.put("giftbag", this.giftBagService.selectGiftBagById(giftbagId));
        return prefix + "/data";
    }


    /**
     * 导出礼包模板列表
     */
    @RequiresPermissions("system/giftbag:giftbag:export")
    @Log(title = "礼包模板", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(GiftBag giftBag)
    {
        List<GiftBag> list = giftBagService.selectGiftBagList(giftBag);
        ExcelUtil<GiftBag> util = new ExcelUtil<GiftBag>(GiftBag.class);
        return util.exportExcel(list, "giftbag");
    }

    /**
     * 新增礼包模板
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存礼包模板
     */
    @RequiresPermissions("system/giftbag:giftbag:add")
    @Log(title = "礼包模板", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(GiftBag giftBag)
    {
        return toAjax(giftBagService.insertGiftBag(giftBag));
    }

    /**
     * 修改礼包模板
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        GiftBag giftBag = giftBagService.selectGiftBagById(id);
        mmap.put("giftBag", giftBag);
        return prefix + "/edit";
    }

    /**
     * 修改保存礼包模板
     */
    @RequiresPermissions("system/giftbag:giftbag:edit")
    @Log(title = "礼包模板", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(GiftBag giftBag)
    {
        return toAjax(giftBagService.updateGiftBag(giftBag));
    }

    /**
     * 删除礼包模板
     */
    @RequiresPermissions("system/giftbag:giftbag:remove")
    @Log(title = "礼包模板", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(giftBagService.deleteGiftBagByIds(ids));
    }
}
