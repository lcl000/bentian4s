package com.ruoyi.project.system.giftbag.giftbagtinfo.domain;

import java.math.BigDecimal;
import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 礼包详情对象 gift_bag_info
 * 
 * @author LCL
 * @date 2020-08-13
 */
public class GiftBagInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 0 正常 1 下架 */
    @Excel(name = "0 正常 1 下架")
    private Integer status;

    /** 0 优惠券 1 抵用券 */
    @Excel(name = "0 优惠券 1 抵用券")
    private Integer type;

    /** 使用条件 */
    @Excel(name = "使用条件")
    private String useCondition;

    /** 免减金额 */
    @Excel(name = "免减金额")
    private BigDecimal price;

    /** 开始时间 */
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 结束时间 */
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 补充说明 */
    @Excel(name = "补充说明")
    private String notes;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 数量 */
    @Excel(name = "数量")
    private Integer total;

    /** 礼包主键ID */
    @Excel(name = "礼包主键ID")
    private String giftBagId;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setUseCondition(String useCondition) 
    {
        this.useCondition = useCondition;
    }

    public String getUseCondition() 
    {
        return useCondition;
    }
    public void setPrice(BigDecimal price)
    {
        this.price = price;
    }

    public BigDecimal getPrice()
    {
        return price;
    }
    public void setStartTime(Date startTime)
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setNotes(String notes) 
    {
        this.notes = notes;
    }

    public String getNotes() 
    {
        return notes;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setTotal(Integer total)
    {
        this.total = total;
    }

    public Integer getTotal()
    {
        return total;
    }
    public void setGiftBagId(String giftBagId) 
    {
        this.giftBagId = giftBagId;
    }

    public String getGiftBagId() 
    {
        return giftBagId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("status", getStatus())
            .append("type", getType())
            .append("useCondition", getUseCondition())
            .append("price", getPrice())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("notes", getNotes())
            .append("sort", getSort())
            .append("total", getTotal())
            .append("giftBagId", getGiftBagId())
            .toString();
    }
}
