package com.ruoyi.project.system.giftbag.giftbag.mapper;

import java.util.List;
import com.ruoyi.project.system.giftbag.giftbag.domain.GiftBag;

/**
 * 礼包模板Mapper接口
 * 
 * @author LCL
 * @date 2020-08-13
 */
public interface GiftBagMapper 
{
    /**
     * 查询礼包模板
     * 
     * @param id 礼包模板ID
     * @return 礼包模板
     */
    public GiftBag selectGiftBagById(String id);

    /**
     * 查询礼包模板列表
     * 
     * @param giftBag 礼包模板
     * @return 礼包模板集合
     */
    public List<GiftBag> selectGiftBagList(GiftBag giftBag);

    /**
     * 新增礼包模板
     * 
     * @param giftBag 礼包模板
     * @return 结果
     */
    public int insertGiftBag(GiftBag giftBag);

    /**
     * 修改礼包模板
     * 
     * @param giftBag 礼包模板
     * @return 结果
     */
    public int updateGiftBag(GiftBag giftBag);

    /**
     * 删除礼包模板
     * 
     * @param id 礼包模板ID
     * @return 结果
     */
    public int deleteGiftBagById(String id);

    /**
     * 批量删除礼包模板
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGiftBagByIds(String[] ids);
}
