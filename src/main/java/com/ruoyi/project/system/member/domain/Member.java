package com.ruoyi.project.system.member.domain;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import com.ruoyi.project.system.membercar.domain.MemberCar;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 小程序用户对象 member
 *
 * @author LCL
 * @date 2020-06-09
 */
public class Member extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 角色 */
    @Excel(name = "角色")
    private Integer role;

    /** 手机号 */
    @Excel(name = "手机号")
    private String mobile;

    /** 微信号 */
    @Excel(name = "微信号")
    private String wechat;

    /** 性别 */
    @Excel(name = "性别")
    private Integer sex;

    /** 唯一的openId */
    private String openid;

    /** 用户头像 */
    @Excel(name = "用户头像")
    private String headpic;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String username;



    /** 用户密码(授权登录的话 暂时没用) */
    private String password;

    /** token */
    private String token;

    /** 账户状态 */
    @Excel(name = "账户状态")
    private Integer status;

    /** 推荐人ID */
    @Excel(name = "推荐人ID")
    private String introUser;

    /** 推荐人数量 */
    @Excel(name = "推荐人数量")
    private Long recommendNum;

    /** 收益 */
    @Excel(name = "收益")
    private BigDecimal coin;

    /** 注册时间 */
    @Excel(name = "注册时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Excel.Type.EXPORT)
    private Date created;

    /** 居住城市 **/
    private String city;

    /** 付款单数 **/
    private Integer orderNum;

    /** 历史收益 **/
    private BigDecimal sumCoin;

    /** 生日 **/
    private Date birthday;

    /** 会员等级 **/
    private Integer vipLv;

    /** 是否绑定车辆 **/
    private Integer hasCar;

    /** 分享时的二维码 **/
    private String qrCode;

    /** 工号 **/
    private String memeberNo;

    private MemberCar memberCar;
    /** 门店主键ID **/
    private String dealerId;

    private String frameNo;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setRole(Integer role)
    {
        this.role = role;
    }

    public Integer getRole()
    {
        return role;
    }
    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }

    public String getMobile()
    {
        return mobile;
    }
    public void setWechat(String wechat)
    {
        this.wechat = wechat;
    }

    public String getWechat()
    {
        return wechat;
    }
    public void setSex(Integer sex)
    {
        this.sex = sex;
    }

    public Integer getSex()
    {
        return sex;
    }
    public void setOpenid(String openid)
    {
        this.openid = openid;
    }

    public String getOpenid()
    {
        return openid;
    }
    public void setHeadpic(String headpic)
    {
        this.headpic = headpic;
    }

    public String getHeadpic()
    {
        return headpic;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPassword()
    {
        return password;
    }
    public void setToken(String token)
    {
        this.token = token;
    }

    public String getToken()
    {
        return token;
    }
    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Integer getStatus()
    {
        return status;
    }
    public void setIntroUser(String introUser)
    {
        this.introUser = introUser;
    }

    public String getIntroUser()
    {
        return introUser;
    }
    public void setRecommendNum(Long recommendNum)
    {
        this.recommendNum = recommendNum;
    }

    public Long getRecommendNum()
    {
        return recommendNum;
    }
    public void setCoin(BigDecimal coin)
    {
        this.coin = coin;
    }

    public BigDecimal getCoin()
    {
        return coin;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }


    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public BigDecimal getSumCoin() {
        return sumCoin;
    }

    public void setSumCoin(BigDecimal sumCoin) {
        this.sumCoin = sumCoin;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Integer getVipLv() {
        return vipLv;
    }

    public void setVipLv(Integer vipLv) {
        this.vipLv = vipLv;
    }

    public Integer getHasCar() {
        return hasCar;
    }

    public void setHasCar(Integer hasCar) {
        this.hasCar = hasCar;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getMemeberNo() {
        return memeberNo;
    }

    public void setMemeberNo(String memeberNo) {
        this.memeberNo = memeberNo;
    }

    public MemberCar getMemberCar() {
        return memberCar;
    }

    public void setMemberCar(MemberCar memberCar) {
        this.memberCar = memberCar;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getFrameNo() {
        return frameNo;
    }

    public void setFrameNo(String frameNo) {
        this.frameNo = frameNo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("role", getRole())
            .append("mobile", getMobile())
            .append("wechat", getWechat())
            .append("sex", getSex())
            .append("openid", getOpenid())
            .append("headpic", getHeadpic())

            .append("password", getPassword())
            .append("token", getToken())
            .append("status", getStatus())
            .append("introUser", getIntroUser())
            .append("recommendNum", getRecommendNum())
            .append("coin", getCoin())
            .append("username", getUsername())
            .append("created",getCreated())
            .append("city",getCity())
            .append("orderNum",getOrderNum())
            .append("sumCoin",getSumCoin())
            .toString();
    }
}
