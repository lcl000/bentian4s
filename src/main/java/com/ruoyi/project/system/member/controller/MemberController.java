package com.ruoyi.project.system.member.controller;

import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.car.type.service.ICarTypeService;
import com.ruoyi.project.system.dealer.service.IDealerService;
import com.ruoyi.project.system.member.domain.Member;
import com.ruoyi.project.system.member.enums.MemberStatus;
import com.ruoyi.project.system.member.service.IMemberService;
import com.ruoyi.project.system.membercar.service.IMemberCarService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 小程序用户Controller
 * 
 * @author LCL
 * @date 2020-06-09
 */
@Controller
@RequestMapping("/system/member")
public class MemberController extends BaseController
{
    private String prefix = "system/member";

    @Autowired
    private IMemberService memberService;

    @Autowired
    private IMemberCarService memberCarService;

    @Autowired
    private ICarTypeService carTypeService;

    @Autowired
    private IDealerService dealerService;

    @RequiresPermissions("system:member:view")
    @GetMapping()
    public String member()
    {
        return prefix + "/member";
    }

    @RequiresPermissions("system:member:view")
    @GetMapping("/staff")
    public String staff()
    {
        return prefix + "/staff";
    }

    @GetMapping("/examine")
    public String examine()
    {
        return prefix + "/examine";
    }


    @GetMapping("/shopo")
    public String shopo()
    {
        return prefix + "/shopowner";
    }

    /**
     * 查询小程序用户列表
     */
    @RequiresPermissions("system:member:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Member member)
    {
        startPage();
        List<Member> list = memberService.selectMemberList(member);
        for(Member mem :list){
            mem.setMemberCar(this.memberCarService.findByUid(mem.getId()));
        }
        return getDataTable(list);
    }

    /**
     * 发送优惠券
     */
    @GetMapping("/couponlist/{id}")
    public String couponlist(@PathVariable String id,ModelMap map)
    {
        map.put("member",this.memberService.selectMemberById(id));
        return prefix + "/couponlist";
    }
    /**
     * 车辆信息
     */
    @GetMapping("/car/{id}")
    public String car(@PathVariable String id,ModelMap map)
    {
        map.put("car",this.memberCarService.findByUid(id));
        map.put("type",this.carTypeService.findAll());
        map.put("dealerList",this.dealerService.findAll(null));
        return prefix + "/car";
    }

    /**
     * 查询店长信息
     */
    @PostMapping("/shopowner")
    @ResponseBody
    public TableDataInfo shopowner(Member member)
    {
        startPage();
        List list = new ArrayList<HashMap<String,Object>>();
        //查询店长列表
        Member select = new Member();
        select.setStatus(MemberStatus.NORMAL.getValue());
        select.setRole(0);
        List<Member> members = this.memberService.selectMemberList(select);
        for(Member memb:members){
            HashMap<String, Object> map = new HashMap<>();
            map.put("id",memb.getId());
            map.put("name",memb.getUsername());
            map.put("mobile",memb.getMobile());
            //查询该店长推荐的人
            String[] personIds = this.memberService.findListPerson(memb.getId());
            if(personIds.length!=0){
                //查询这些人下面的所有订单数量
                Integer orderSum = this.memberService.findOrderSumByIds(personIds);
                map.put("orderNum",orderSum);
                //查询这些人多有订单的总金额
                Double sunPrice = this.memberService.findOrderPriceSumByIds(personIds);
                map.put("orderPrice",sunPrice);
                //查询这些人下有多少人是会员
                Integer vipNum = this.memberService.findSumVipByIds(personIds);
                map.put("vipNum",vipNum);
                //推荐人数量
                map.put("memberNum",personIds.length);
            }else {
                map.put("orderNum",0);
                map.put("orderPrice",0);
                map.put("vipNum",0);
                map.put("memberNum",0);
            }
            list.add(map);
        }


        return getDataTable(list);
    }

    /**
     * 查看详细
     */
    @GetMapping("/userlist/{userId}")
    public String detail(@PathVariable("userId") String userId, ModelMap mmap)
    {
        //查询该店长推荐的人
        List<Member> memberListPerson = this.memberService.findMemberListPerson(userId);
        mmap.put("member",this.memberService.selectMemberById(userId));
        mmap.put("userlist", memberListPerson);
        return "system/member/userlist";
    }
    /**
     * 查看详细
     */
    @PostMapping("/userlist/list")
    @ResponseBody
    public TableDataInfo detaillist(Member member)
    {
        startPage();
        //查询该店长推荐的人
        List<Member> memberListPerson = this.memberService.findMemberListPerson(member.getId());

        return getDataTable(memberListPerson);
    }
    /**
     * 导出小程序用户列表
     */
    @RequiresPermissions("system:member:export")
    @Log(title = "小程序用户", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Member member)
    {
        List<Member> list = memberService.selectMemberList(member);
        ExcelUtil<Member> util = new ExcelUtil<Member>(Member.class);
        return util.exportExcel(list, "member");
    }

    /**
     * 新增小程序用户
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存小程序用户
     */
    @RequiresPermissions("system:member:add")
    @Log(title = "小程序用户", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Member member)
    {
        return toAjax(memberService.insertMember(member));
    }

    /**
     * 修改小程序用户
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        Member member = memberService.selectMemberById(id);
        mmap.put("member", member);
        mmap.put("userList",this.memberService.findAll());
        mmap.put("dealerList",this.dealerService.findAll(null));
        return prefix + "/edit";
    }

    /**
     * 修改小程序用户
     */
    @GetMapping("/editstaff/{id}")
    public String editStaff(@PathVariable("id") String id, ModelMap mmap)
    {
        Member member = memberService.selectMemberById(id);
        mmap.put("member", member);
        mmap.put("dealerList",this.dealerService.findAll(null));
        return prefix + "/editstaff";
    }

    /**
     * 跳转审核用户详情
     */
    @GetMapping("/examine/{id}")
    public String examine(@PathVariable("id") String id, ModelMap mmap)
    {
        Member member = memberService.selectMemberById(id);
        mmap.put("member", member);
        return prefix + "/examinemember";
    }

    /**
     * 修改保存小程序用户
     */
    @RequiresPermissions("system:member:edit")
    @Log(title = "小程序用户", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    @Transactional
    public AjaxResult editSave(Member member)
    {
        Member oldMember = this.memberService.selectMemberById(member.getId());
        if(member.getIntroUser()!=null&&oldMember.getIntroUser()!=null&&!oldMember.getIntroUser().equals(member.getIntroUser())){
            //更新原本的推荐人的信息
            Member oldIntroUser = this.memberService.selectMemberById(oldMember.getIntroUser());
            if(oldIntroUser!=null){
                Member updateOldIntroUser = new Member();
                updateOldIntroUser.setId(oldIntroUser.getId());
                updateOldIntroUser.setRecommendNum(oldIntroUser.getRecommendNum()-1);
                this.memberService.updateMember(updateOldIntroUser);
            }
            //更新新的推荐人的信息
            Member nowIntroUser = this.memberService.selectMemberById(member.getIntroUser());
            Member updateNowIntroUser = new Member();
            updateNowIntroUser.setId(nowIntroUser.getId());
            updateNowIntroUser.setRecommendNum(nowIntroUser.getRecommendNum()+1);
            this.memberService.updateMember(updateNowIntroUser);

        }
        return toAjax(memberService.updateMember(member));
    }

    /**
     * 审核用户
     */
    @Log(title = "审核用户", businessType = BusinessType.UPDATE)
    @PostMapping("/examine/edit")
    @ResponseBody
    public AjaxResult examineMember(Member member)
    {
        return toAjax(memberService.updateMember(member));
    }

    /**
     * 删除小程序用户
     */
    @RequiresPermissions("system:member:remove")
    @Log(title = "小程序用户", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(memberService.deleteMemberByIds(ids));
    }

    @GetMapping("/parentinfo/{id}")
    @ResponseBody
    public AjaxResult findMemberParentinfoInfo(@PathVariable String id){
        Member byUid = this.memberService.selectMemberById(id);
        if(!StringUtils.isBlank(byUid.getIntroUser())){
            return AjaxResult.success(this.memberService.selectMemberById(byUid.getIntroUser()));
        }
        return AjaxResult.success();
    }

    /**
     * 查询用户详情
     */
    @GetMapping("/userinfo/{id}")
    public String userinfo(@PathVariable("id") String id,ModelMap map)
    {
        map.put("id",memberService.selectMemberById(id));
        return prefix + "/userinfo";
    }
}
