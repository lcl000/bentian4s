package com.ruoyi.project.system.member.enums;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/22 11:05
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public enum MemberExamine {
	INCOMPLETE(0,"未完善"),
	TO_BE_REVIEWED(1,"待审核"),
	ADOPT(2,"通过"),
	FAIL(3,"未通过");

	private int value;
	private String name;

	MemberExamine(int value, String name) {
		this.value = value;
		this.name = name;
	}

	/**
	 * 数据范围校验
	 *
	 * @param value
	 * @return
	 */
	public static boolean contains(int value) {
		return null == getByValue(value)?Boolean.FALSE.booleanValue():Boolean.TRUE.booleanValue();
	}
	public int getValue() {
		return this.value;
	}

	public String getName(){
		return this.name;
	}

	public static MemberExamine getByValue(int value){
		MemberExamine allVaues[] = MemberExamine.values();
		MemberExamine status;
		for (int i=0,len = allVaues.length;i<len;i++){
			status = allVaues[i];
			if(status.getValue()==value){
				return status;
			}
		}
		return null;
	}
}
