package com.ruoyi.project.system.member.enums;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/22 11:08
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public enum MemberIsVip {
	NO(0,"不是"),
	YES(1,"是");

	private int value;
	private String name;

	MemberIsVip(int value, String name) {
		this.value = value;
		this.name = name;
	}

	/**
	 * 数据范围校验
	 *
	 * @param value
	 * @return
	 */
	public static boolean contains(int value) {
		return null == getByValue(value)?Boolean.FALSE.booleanValue():Boolean.TRUE.booleanValue();
	}
	public int getValue() {
		return this.value;
	}

	public String getName(){
		return this.name;
	}

	public static MemberIsVip getByValue(int value){
		MemberIsVip allVaues[] = MemberIsVip.values();
		MemberIsVip status;
		for (int i=0,len = allVaues.length;i<len;i++){
			status = allVaues[i];
			if(status.getValue()==value){
				return status;
			}
		}
		return null;
	}
}
