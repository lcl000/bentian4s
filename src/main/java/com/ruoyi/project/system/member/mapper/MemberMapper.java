package com.ruoyi.project.system.member.mapper;

import java.math.BigDecimal;
import java.util.List;
import com.ruoyi.project.system.member.domain.Member;

/**
 * 小程序用户Mapper接口
 * 
 * @author LCL
 * @date 2020-06-09
 */
public interface MemberMapper 
{
    /**
     * 查询小程序用户
     * 
     * @param id 小程序用户ID
     * @return 小程序用户
     */
    public Member selectMemberById(String id);

    /**
     * 查询小程序用户列表
     * 
     * @param member 小程序用户
     * @return 小程序用户集合
     */
    public List<Member> selectMemberList(Member member);

    /**
     * 新增小程序用户
     * 
     * @param member 小程序用户
     * @return 结果
     */
    public int insertMember(Member member);

    /**
     * 修改小程序用户
     * 
     * @param member 小程序用户
     * @return 结果
     */
    public int updateMember(Member member);

    /**
     * 删除小程序用户
     * 
     * @param id 小程序用户ID
     * @return 结果
     */
    public int deleteMemberById(String id);

    /**
     * 批量删除小程序用户
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMemberByIds(String[] ids);

	Member findOneByOpenId(String openid);

	String[] findListPerson(String id);

    Integer findOrderSumByIds(String[] personIds);

    Double findOrderPriceSumByIds(String[] personIds);

    Integer findSumVipByIds(String[] personIds);

	List<Member> findMemberListPerson(String userId);

	List<Member> findAll();

	List<Member> findAllHasCar();
}
