package com.ruoyi.project.system.member.service.impl;

import javax.annotation.Resource;

import java.math.BigDecimal;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.member.mapper.MemberMapper;
import com.ruoyi.project.system.member.domain.Member;
import com.ruoyi.project.system.member.service.IMemberService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 小程序用户Service业务层处理
 * 
 * @author LCL
 * @date 2020-06-09
 */
@Service
public class MemberServiceImpl implements IMemberService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private MemberMapper memberMapper;

    /**
     * 查询小程序用户
     * 
     * @param id 小程序用户ID
     * @return 小程序用户
     */
    @Override
    public Member selectMemberById(String id)
    {
        return memberMapper.selectMemberById(id);
    }

    /**
     * 查询小程序用户列表
     * 
     * @param member 小程序用户
     * @return 小程序用户
     */
    @Override
    public List<Member> selectMemberList(Member member)
    {
        return memberMapper.selectMemberList(member);
    }

    /**
     * 新增小程序用户
     * 
     * @param member 小程序用户
     * @return 结果
     */
    @Override
    public int insertMember(Member member)
    {
        member.setId(this.idGererateFactory.nextStringId());
        return memberMapper.insertMember(member);
    }

    /**
     * 修改小程序用户
     * 
     * @param member 小程序用户
     * @return 结果
     */
    @Override
    public int updateMember(Member member)
    {
        return memberMapper.updateMember(member);
    }

    /**
     * 删除小程序用户对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMemberByIds(String ids)
    {
        return memberMapper.deleteMemberByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除小程序用户信息
     * 
     * @param id 小程序用户ID
     * @return 结果
     */
    @Override
    public int deleteMemberById(String id)
    {
        return memberMapper.deleteMemberById(id);
    }

    @Override
    public Member findOneByOpenId(String openid) {
        return this.memberMapper.findOneByOpenId(openid);
    }

    @Override
    public String[] findListPerson(String id) {
        return this.memberMapper.findListPerson(id);
    }

    @Override
    public Integer findOrderSumByIds(String[] personIds) {
        return this.memberMapper.findOrderSumByIds(personIds);
    }

    @Override
    public Double findOrderPriceSumByIds(String[] personIds) {
        return this.memberMapper.findOrderPriceSumByIds(personIds);
    }

    @Override
    public Integer findSumVipByIds(String[] personIds) {
        return this.memberMapper.findSumVipByIds(personIds);
    }

    @Override
    public List<Member> findMemberListPerson(String userId) {
        return this.memberMapper.findMemberListPerson(userId);
    }

    @Override
    public List<Member> findAll() {
        return this.memberMapper.findAll();
    }

    @Override
    public List<Member> findAllHasCar() {
        return this.memberMapper.findAllHasCar();
    }

    @Override
    public int updateCoinMember(Member update, Member member) {
        Integer[] vipCoin = new Integer[]{50001,20001,8001,0};
        int vipLv = 1;
        for(int i=0;i<vipCoin.length;i++){
            if(update.getCoin().compareTo(new BigDecimal(vipCoin[i]))!=-1){
                vipLv=vipCoin.length-i;
                break;
            }
        }
        if(member.getVipLv()<vipLv){
            update.setVipLv(vipLv);
        }
        return this.memberMapper.updateMember(update);
    }
}
