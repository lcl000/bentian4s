package com.ruoyi.project.system.goods.goodscard.send.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.goods.goodscard.send.domain.SupplySend;
import com.ruoyi.project.system.goods.goodscard.send.service.ISupplySendService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 配送方式Controller
 * 
 * @author LCL
 * @date 2020-07-08
 */
@Controller
@RequestMapping("/system/send")
public class SupplySendController extends BaseController
{
    private String prefix = "system/send";

    @Autowired
    private ISupplySendService supplySendService;

    @RequiresPermissions("system:send:view")
    @GetMapping()
    public String send()
    {
        return prefix + "/send";
    }

    /**
     * 查询配送方式列表
     */
    @RequiresPermissions("system:send:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SupplySend supplySend)
    {
        startPage();
        List<SupplySend> list = supplySendService.selectSupplySendList(supplySend);
        return getDataTable(list);
    }

    /**
     * 导出配送方式列表
     */
    @RequiresPermissions("system:send:export")
    @Log(title = "配送方式", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SupplySend supplySend)
    {
        List<SupplySend> list = supplySendService.selectSupplySendList(supplySend);
        ExcelUtil<SupplySend> util = new ExcelUtil<SupplySend>(SupplySend.class);
        return util.exportExcel(list, "send");
    }

    /**
     * 新增配送方式
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存配送方式
     */
    @RequiresPermissions("system:send:add")
    @Log(title = "配送方式", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SupplySend supplySend)
    {
        return toAjax(supplySendService.insertSupplySend(supplySend));
    }

    /**
     * 修改配送方式
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        SupplySend supplySend = supplySendService.selectSupplySendById(id);
        mmap.put("supplySend", supplySend);
        return prefix + "/edit";
    }

    /**
     * 修改保存配送方式
     */
    @RequiresPermissions("system:send:edit")
    @Log(title = "配送方式", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SupplySend supplySend)
    {
        return toAjax(supplySendService.updateSupplySend(supplySend));
    }

    /**
     * 删除配送方式
     */
    @RequiresPermissions("system:send:remove")
    @Log(title = "配送方式", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(supplySendService.deleteSupplySendByIds(ids));
    }
}
