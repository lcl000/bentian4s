package com.ruoyi.project.system.goods.goods.service;

import java.util.List;

import com.ruoyi.project.system.goods.goods.domain.GoodsDto;
import com.ruoyi.project.system.goods.goods.domain.GoodsGoods;

/**
 * 商品Service接口
 * 
 * @author LCL
 * @date 2020-06-12
 */
public interface IGoodsGoodsService 
{
    /**
     * 查询商品
     * 
     * @param id 商品ID
     * @return 商品
     */
    public GoodsGoods selectGoodsGoodsById(String id);

    /**
     * 查询商品列表
     * 
     * @param goodsGoods 商品
     * @return 商品集合
     */
    public List<GoodsDto> selectGoodsGoodsList(GoodsGoods goodsGoods);

    /**
     * 新增商品
     * 
     * @param goodsGoods 商品
     * @return 结果
     */
    public int insertGoodsGoods(GoodsGoods goodsGoods);

    /**
     * 修改商品
     * 
     * @param goodsGoods 商品
     * @return 结果
     */
    public int updateGoodsGoods(GoodsGoods goodsGoods);

    /**
     * 批量删除商品
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGoodsGoodsByIds(String ids);

    /**
     * 删除商品信息
     * 
     * @param id 商品ID
     * @return 结果
     */
    public int deleteGoodsGoodsById(String id);

    GoodsDto findGoodsDtoById(String id);

	int remove(String ids);

	List<GoodsDto> findGoodsDtoList(GoodsGoods goodsGoods);

	GoodsDto findGoodsDtoByPId(String product);
}
