package com.ruoyi.project.system.goods.product.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.goods.product.mapper.GoodsProductMapper;
import com.ruoyi.project.system.goods.product.domain.GoodsProduct;
import com.ruoyi.project.system.goods.product.service.IGoodsProductService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 商品 产品Service业务层处理
 * 
 * @author LCL
 * @date 2020-06-12
 */
@Service
public class GoodsProductServiceImpl implements IGoodsProductService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private GoodsProductMapper goodsProductMapper;

    /**
     * 查询商品 产品
     * 
     * @param id 商品 产品ID
     * @return 商品 产品
     */
    @Override
    public GoodsProduct selectGoodsProductById(String id)
    {
        return goodsProductMapper.selectGoodsProductById(id);
    }

    /**
     * 查询商品 产品列表
     * 
     * @param goodsProduct 商品 产品
     * @return 商品 产品
     */
    @Override
    public List<GoodsProduct> selectGoodsProductList(GoodsProduct goodsProduct)
    {
        return goodsProductMapper.selectGoodsProductList(goodsProduct);
    }

    /**
     * 新增商品 产品
     * 
     * @param goodsProduct 商品 产品
     * @return 结果
     */
    @Override
    public int insertGoodsProduct(GoodsProduct goodsProduct)
    {
        goodsProduct.setId(this.idGererateFactory.nextStringId());
        return goodsProductMapper.insertGoodsProduct(goodsProduct);
    }

    /**
     * 修改商品 产品
     * 
     * @param goodsProduct 商品 产品
     * @return 结果
     */
    @Override
    public int updateGoodsProduct(GoodsProduct goodsProduct)
    {
        return goodsProductMapper.updateGoodsProduct(goodsProduct);
    }

    /**
     * 删除商品 产品对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteGoodsProductByIds(String ids)
    {
        return goodsProductMapper.deleteGoodsProductByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商品 产品信息
     * 
     * @param id 商品 产品ID
     * @return 结果
     */
    @Override
    public int deleteGoodsProductById(String id)
    {
        return goodsProductMapper.deleteGoodsProductById(id);
    }

    @Override
    public List<GoodsProduct> findListByGoodsId(String goodsId) {
        return goodsProductMapper.findListByGoodsId(goodsId);
    }

    @Override
    public int removeByGoodsId(String goodsId) {
        return goodsProductMapper.removeByGoodsId(goodsId);
    }

    @Override
    public GoodsProduct findByName(String id, String spec) {
        return this.goodsProductMapper.findByName(id,spec);
    }
}
