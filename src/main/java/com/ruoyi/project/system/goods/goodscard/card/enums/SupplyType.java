package com.ruoyi.project.system.goods.goodscard.card.enums;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/7/21 15:38
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public enum SupplyType {
	TWO(0,"俩月卡"),
	THREE(1,"季卡"),
	SIX(2,"半年卡"),
	YEAR(3,"年卡");

	private int value;
	private String name;

	SupplyType(int value, String name) {
		this.value = value;
		this.name = name;
	}

	/**
	 * 数据范围校验
	 *
	 * @param value
	 * @return
	 */
	public static boolean contains(int value) {
		return null == getByValue(value)?Boolean.FALSE.booleanValue():Boolean.TRUE.booleanValue();
	}
	public int getValue() {
		return this.value;
	}

	public String getName(){
		return this.name;
	}

	public static SupplyType getByValue(int value){
		SupplyType allVaues[] = SupplyType.values();
		SupplyType status;
		for (int i=0,len = allVaues.length;i<len;i++){
			status = allVaues[i];
			if(status.getValue()==value){
				return status;
			}
		}
		return null;
	}
}
