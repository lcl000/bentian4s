package com.ruoyi.project.system.goods.cart.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 购物车对象 shopping_cart
 * 
 * @author LCL
 * @date 2020-06-27
 */
public class ShoppingCart extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 用户主键ID */
    @Excel(name = "用户主键ID")
    private String memberId;

    /** 产品主键ID */
    @Excel(name = "产品主键ID")
    private String goodsId;

    /** 规格主键ID */
    @Excel(name = "规格主键ID")
    private String productId;

    /** 数量 */
    @Excel(name = "数量")
    private Long total;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setMemberId(String memberId) 
    {
        this.memberId = memberId;
    }

    public String getMemberId() 
    {
        return memberId;
    }
    public void setGoodsId(String goodsId) 
    {
        this.goodsId = goodsId;
    }

    public String getGoodsId() 
    {
        return goodsId;
    }
    public void setProductId(String productId) 
    {
        this.productId = productId;
    }

    public String getProductId() 
    {
        return productId;
    }
    public void setTotal(Long total) 
    {
        this.total = total;
    }

    public Long getTotal() 
    {
        return total;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("memberId", getMemberId())
            .append("goodsId", getGoodsId())
            .append("productId", getProductId())
            .append("total", getTotal())
            .toString();
    }
}
