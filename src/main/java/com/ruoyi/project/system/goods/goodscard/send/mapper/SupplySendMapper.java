package com.ruoyi.project.system.goods.goodscard.send.mapper;

import java.util.List;
import com.ruoyi.project.system.goods.goodscard.send.domain.SupplySend;

/**
 * 配送方式Mapper接口
 * 
 * @author LCL
 * @date 2020-07-08
 */
public interface SupplySendMapper 
{
    /**
     * 查询配送方式
     * 
     * @param id 配送方式ID
     * @return 配送方式
     */
    public SupplySend selectSupplySendById(String id);

    /**
     * 查询配送方式列表
     * 
     * @param supplySend 配送方式
     * @return 配送方式集合
     */
    public List<SupplySend> selectSupplySendList(SupplySend supplySend);

    /**
     * 新增配送方式
     * 
     * @param supplySend 配送方式
     * @return 结果
     */
    public int insertSupplySend(SupplySend supplySend);

    /**
     * 修改配送方式
     * 
     * @param supplySend 配送方式
     * @return 结果
     */
    public int updateSupplySend(SupplySend supplySend);

    /**
     * 删除配送方式
     * 
     * @param id 配送方式ID
     * @return 结果
     */
    public int deleteSupplySendById(String id);

    /**
     * 批量删除配送方式
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSupplySendByIds(String[] ids);

	int deleteBySid(String id);

    List<SupplySend> findListByPid(String id);
}
