package com.ruoyi.project.system.goods.goodscard.card.mapper;

import java.util.List;
import com.ruoyi.project.system.goods.goodscard.card.domain.GoodsSupplyCard;
import com.ruoyi.project.system.goods.goodscard.card.dto.SupplyCardDto;

/**
 * 商品供应卡Mapper接口
 * 
 * @author LCL
 * @date 2020-07-08
 */
public interface GoodsSupplyCardMapper 
{
    /**
     * 查询商品供应卡
     * 
     * @param id 商品供应卡ID
     * @return 商品供应卡
     */
    public GoodsSupplyCard selectGoodsSupplyCardById(String id);

    /**
     * 查询商品供应卡列表
     * 
     * @param goodsSupplyCard 商品供应卡
     * @return 商品供应卡集合
     */
    public List<GoodsSupplyCard> selectGoodsSupplyCardList(GoodsSupplyCard goodsSupplyCard);

    /**
     * 新增商品供应卡
     * 
     * @param goodsSupplyCard 商品供应卡
     * @return 结果
     */
    public int insertGoodsSupplyCard(GoodsSupplyCard goodsSupplyCard);

    /**
     * 修改商品供应卡
     * 
     * @param goodsSupplyCard 商品供应卡
     * @return 结果
     */
    public int updateGoodsSupplyCard(GoodsSupplyCard goodsSupplyCard);

    /**
     * 删除商品供应卡
     * 
     * @param id 商品供应卡ID
     * @return 结果
     */
    public int deleteGoodsSupplyCardById(String id);

    /**
     * 批量删除商品供应卡
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGoodsSupplyCardByIds(String[] ids);

	SupplyCardDto findById(String id);

    SupplyCardDto findByCId(String supplyCardId);
}
