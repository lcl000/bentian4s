package com.ruoyi.project.system.goods.cart.service.impl;

import javax.annotation.Resource;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.goods.cart.dto.CartDto;
import com.ruoyi.project.system.goods.cart.mapper.ShoppingCartMapper;
import com.ruoyi.project.system.goods.cart.domain.ShoppingCart;
import com.ruoyi.project.system.goods.cart.service.IShoppingCartService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 购物车Service业务层处理
 * 
 * @author LCL
 * @date 2020-06-27
 */
@Service
public class ShoppingCartServiceImpl implements IShoppingCartService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private ShoppingCartMapper shoppingCartMapper;

    /**
     * 查询购物车
     * 
     * @param id 购物车ID
     * @return 购物车
     */
    @Override
    public ShoppingCart selectShoppingCartById(String id)
    {
        return shoppingCartMapper.selectShoppingCartById(id);
    }

    /**
     * 查询购物车列表
     * 
     * @param shoppingCart 购物车
     * @return 购物车
     */
    @Override
    public List<ShoppingCart> selectShoppingCartList(ShoppingCart shoppingCart)
    {
        return shoppingCartMapper.selectShoppingCartList(shoppingCart);
    }

    /**
     * 新增购物车
     * 
     * @param shoppingCart 购物车
     * @return 结果
     */
    @Override
    public int insertShoppingCart(ShoppingCart shoppingCart)
    {
        shoppingCart.setId(this.idGererateFactory.nextStringId());
        return shoppingCartMapper.insertShoppingCart(shoppingCart);
    }

    /**
     * 修改购物车
     * 
     * @param shoppingCart 购物车
     * @return 结果
     */
    @Override
    public int updateShoppingCart(ShoppingCart shoppingCart)
    {
        return shoppingCartMapper.updateShoppingCart(shoppingCart);
    }

    /**
     * 删除购物车对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteShoppingCartByIds(ArrayList ids)
    {
        return shoppingCartMapper.deleteShoppingCartByIds(ids);
    }

    /**
     * 删除购物车信息
     * 
     * @param id 购物车ID
     * @return 结果
     */
    @Override
    public int deleteShoppingCartById(String id)
    {
        return shoppingCartMapper.deleteShoppingCartById(id);
    }

    @Override
    public List<CartDto> findListByUid(String uid) {
        return this.shoppingCartMapper.findListByUid(uid);
    }

    @Override
    public int deleteByIds(ArrayList cartIds) {
        return this.shoppingCartMapper.deleteShoppingCartByIds(cartIds);
    }

    @Override
    public ShoppingCart findByGidAndPid(String goodsId, String productId,String uid) {
        return this.shoppingCartMapper.findByGidAndPid(goodsId,productId,uid);
    }
}
