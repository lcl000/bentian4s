package com.ruoyi.project.system.goods.goods.domain;

import java.math.BigDecimal;
import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 商品对象 goods_goods
 * 
 * @author LCL
 * @date 2020-06-12
 */
public class GoodsGoods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String name;

    /** 商品副标题 */
    @Excel(name = "商品副标题")
    private String title;

    /** 商品分类ID */
    @Excel(name = "商品分类ID")
    private String classId;

    /** 商品类型ID */
    @Excel(name = "商品类型ID")
    private String typeId;

    /** 主图访问路径 */
    @Excel(name = "主图访问路径")
    private String mainUrl;

    /** 默认邮费 */
    @Excel(name = "默认邮费")
    private BigDecimal postFee;

    /** 月售 */
    @Excel(name = "月售")
    private Long monthlySales;

    /** 库存 */
    @Excel(name = "库存")
    private Long stock;

    /** 规格详情 */
    private String spec;

    /** 参数详情 */
    private String param;

    /** 启用规格 0启用 1 不启用 */
    private Integer hasSpec;

    /** 启用秒杀 0 不开启 1开启 */
    @Excel(name = "启用秒杀 0 不开启 1开启")
    private Integer hasSpike;

    /** 秒杀开始时间 */
    private Date spikeStart;

    /** 秒杀结束时间 */
    private Date spikeEnd;

    /** 是否是品牌大牌 0 不是 1 是 */
    private Integer isBrand;

    /** 是否是境外产品 0 不是 1 是 */
    private Integer isAbroad;

    /** 是否是推荐产品 0 不是 1 是 */
    private Integer isRecommend;

    /** 是否是热卖产品 0 不是 1 是 */
    private Integer isHot;

    /** 能否积分折扣 0 不是 1 是 */
    private Integer isPointDiscount;

    /** 状态 0 上架 1 下架 2 删除 */
    @Excel(name = "状态 0 上架 1 下架 2 删除")
    private Integer status;

    /** 标签: 1 直邮 2 正品 3 保税 */
    private Integer tag;

    /** 排序 */
    private Integer sort;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setClassId(String classId) 
    {
        this.classId = classId;
    }

    public String getClassId() 
    {
        return classId;
    }
    public void setTypeId(String typeId) 
    {
        this.typeId = typeId;
    }

    public String getTypeId() 
    {
        return typeId;
    }
    public void setMainUrl(String mainUrl) 
    {
        this.mainUrl = mainUrl;
    }

    public String getMainUrl() 
    {
        return mainUrl;
    }
    public void setPostFee(BigDecimal postFee) 
    {
        this.postFee = postFee;
    }

    public BigDecimal getPostFee() 
    {
        return postFee;
    }
    public void setMonthlySales(Long monthlySales) 
    {
        this.monthlySales = monthlySales;
    }

    public Long getMonthlySales() 
    {
        return monthlySales;
    }
    public void setStock(Long stock) 
    {
        this.stock = stock;
    }

    public Long getStock() 
    {
        return stock;
    }
    public void setSpec(String spec) 
    {
        this.spec = spec;
    }

    public String getSpec() 
    {
        return spec;
    }
    public void setParam(String param) 
    {
        this.param = param;
    }

    public String getParam() 
    {
        return param;
    }
    public void setHasSpec(Integer hasSpec) 
    {
        this.hasSpec = hasSpec;
    }

    public Integer getHasSpec() 
    {
        return hasSpec;
    }
    public void setHasSpike(Integer hasSpike) 
    {
        this.hasSpike = hasSpike;
    }

    public Integer getHasSpike() 
    {
        return hasSpike;
    }
    public void setSpikeStart(Date spikeStart) 
    {
        this.spikeStart = spikeStart;
    }

    public Date getSpikeStart() 
    {
        return spikeStart;
    }
    public void setSpikeEnd(Date spikeEnd) 
    {
        this.spikeEnd = spikeEnd;
    }

    public Date getSpikeEnd() 
    {
        return spikeEnd;
    }
    public void setIsBrand(Integer isBrand) 
    {
        this.isBrand = isBrand;
    }

    public Integer getIsBrand() 
    {
        return isBrand;
    }
    public void setIsAbroad(Integer isAbroad) 
    {
        this.isAbroad = isAbroad;
    }

    public Integer getIsAbroad() 
    {
        return isAbroad;
    }
    public void setIsRecommend(Integer isRecommend) 
    {
        this.isRecommend = isRecommend;
    }

    public Integer getIsRecommend() 
    {
        return isRecommend;
    }
    public void setIsHot(Integer isHot) 
    {
        this.isHot = isHot;
    }

    public Integer getIsHot() 
    {
        return isHot;
    }
    public void setIsPointDiscount(Integer isPointDiscount) 
    {
        this.isPointDiscount = isPointDiscount;
    }

    public Integer getIsPointDiscount() 
    {
        return isPointDiscount;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    public Integer getTag() {
        return tag;
    }

    public void setTag(Integer tag) {
        this.tag = tag;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("title", getTitle())
            .append("classId", getClassId())
            .append("typeId", getTypeId())
            .append("mainUrl", getMainUrl())
            .append("postFee", getPostFee())
            .append("monthlySales", getMonthlySales())
            .append("stock", getStock())
            .append("spec", getSpec())
            .append("param", getParam())
            .append("hasSpec", getHasSpec())
            .append("hasSpike", getHasSpike())
            .append("spikeStart", getSpikeStart())
            .append("spikeEnd", getSpikeEnd())
            .append("isBrand", getIsBrand())
            .append("isAbroad", getIsAbroad())
            .append("isRecommend", getIsRecommend())
            .append("isHot", getIsHot())
            .append("isPointDiscount", getIsPointDiscount())
            .append("status", getStatus())
            .append("tag", getTag())
            .append("sort", getSort())
            .toString();
    }
}
