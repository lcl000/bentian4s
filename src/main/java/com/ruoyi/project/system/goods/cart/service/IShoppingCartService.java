package com.ruoyi.project.system.goods.cart.service;

import java.util.ArrayList;
import java.util.List;
import com.ruoyi.project.system.goods.cart.domain.ShoppingCart;
import com.ruoyi.project.system.goods.cart.dto.CartDto;

/**
 * 购物车Service接口
 * 
 * @author LCL
 * @date 2020-06-27
 */
public interface IShoppingCartService 
{
    /**
     * 查询购物车
     * 
     * @param id 购物车ID
     * @return 购物车
     */
    public ShoppingCart selectShoppingCartById(String id);

    /**
     * 查询购物车列表
     * 
     * @param shoppingCart 购物车
     * @return 购物车集合
     */
    public List<ShoppingCart> selectShoppingCartList(ShoppingCart shoppingCart);

    /**
     * 新增购物车
     * 
     * @param shoppingCart 购物车
     * @return 结果
     */
    public int insertShoppingCart(ShoppingCart shoppingCart);

    /**
     * 修改购物车
     * 
     * @param shoppingCart 购物车
     * @return 结果
     */
    public int updateShoppingCart(ShoppingCart shoppingCart);

    /**
     * 批量删除购物车
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteShoppingCartByIds(ArrayList ids);

    /**
     * 删除购物车信息
     * 
     * @param id 购物车ID
     * @return 结果
     */
    public int deleteShoppingCartById(String id);

	List<CartDto> findListByUid(String uid);

	int deleteByIds(ArrayList cartIds);

	ShoppingCart findByGidAndPid(String goodsId, String productId,String uid);
}
