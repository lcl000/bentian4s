package com.ruoyi.project.system.goods.goodscard.product.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.goods.goodscard.product.dto.SupplyCardProductDto;
import com.ruoyi.project.system.goods.goodscard.product.mapper.SupplyCardProductMapper;
import com.ruoyi.project.system.goods.goodscard.product.domain.SupplyCardProduct;
import com.ruoyi.project.system.goods.goodscard.product.service.ISupplyCardProductService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 供应卡规格Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-08
 */
@Service
public class SupplyCardProductServiceImpl implements ISupplyCardProductService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private SupplyCardProductMapper supplyCardProductMapper;

    /**
     * 查询供应卡规格
     * 
     * @param id 供应卡规格ID
     * @return 供应卡规格
     */
    @Override
    public SupplyCardProduct selectSupplyCardProductById(String id)
    {
        return supplyCardProductMapper.selectSupplyCardProductById(id);
    }

    /**
     * 查询供应卡规格列表
     * 
     * @param supplyCardProduct 供应卡规格
     * @return 供应卡规格
     */
    @Override
    public List<SupplyCardProduct> selectSupplyCardProductList(SupplyCardProduct supplyCardProduct)
    {
        return supplyCardProductMapper.selectSupplyCardProductList(supplyCardProduct);
    }

    /**
     * 新增供应卡规格
     * 
     * @param supplyCardProduct 供应卡规格
     * @return 结果
     */
    @Override
    public int insertSupplyCardProduct(SupplyCardProduct supplyCardProduct)
    {
        supplyCardProduct.setId(this.idGererateFactory.nextStringId());
        return supplyCardProductMapper.insertSupplyCardProduct(supplyCardProduct);
    }

    /**
     * 修改供应卡规格
     * 
     * @param supplyCardProduct 供应卡规格
     * @return 结果
     */
    @Override
    public int updateSupplyCardProduct(SupplyCardProduct supplyCardProduct)
    {
        return supplyCardProductMapper.updateSupplyCardProduct(supplyCardProduct);
    }

    /**
     * 删除供应卡规格对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSupplyCardProductByIds(String ids)
    {
        return supplyCardProductMapper.deleteSupplyCardProductByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除供应卡规格信息
     * 
     * @param id 供应卡规格ID
     * @return 结果
     */
    @Override
    public int deleteSupplyCardProductById(String id)
    {
        return supplyCardProductMapper.deleteSupplyCardProductById(id);
    }

    @Override
    public List<SupplyCardProductDto> findBySId(String id) {
        return this.supplyCardProductMapper.findBySId(id);
    }

    @Override
    public int deleteBySid(String id) {
        return this.supplyCardProductMapper.deleteBySid(id);
    }

    @Override
    public List<SupplyCardProductDto> findByGid(String id) {
        return this.supplyCardProductMapper.findByGid(id);
    }
}
