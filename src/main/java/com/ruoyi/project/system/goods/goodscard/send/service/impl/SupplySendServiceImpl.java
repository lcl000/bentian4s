package com.ruoyi.project.system.goods.goodscard.send.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.goods.goodscard.send.mapper.SupplySendMapper;
import com.ruoyi.project.system.goods.goodscard.send.domain.SupplySend;
import com.ruoyi.project.system.goods.goodscard.send.service.ISupplySendService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 配送方式Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-08
 */
@Service
public class SupplySendServiceImpl implements ISupplySendService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private SupplySendMapper supplySendMapper;

    /**
     * 查询配送方式
     * 
     * @param id 配送方式ID
     * @return 配送方式
     */
    @Override
    public SupplySend selectSupplySendById(String id)
    {
        return supplySendMapper.selectSupplySendById(id);
    }

    /**
     * 查询配送方式列表
     * 
     * @param supplySend 配送方式
     * @return 配送方式
     */
    @Override
    public List<SupplySend> selectSupplySendList(SupplySend supplySend)
    {
        return supplySendMapper.selectSupplySendList(supplySend);
    }

    /**
     * 新增配送方式
     * 
     * @param supplySend 配送方式
     * @return 结果
     */
    @Override
    public int insertSupplySend(SupplySend supplySend)
    {
        supplySend.setId(this.idGererateFactory.nextStringId());
        return supplySendMapper.insertSupplySend(supplySend);
    }

    /**
     * 修改配送方式
     * 
     * @param supplySend 配送方式
     * @return 结果
     */
    @Override
    public int updateSupplySend(SupplySend supplySend)
    {
        return supplySendMapper.updateSupplySend(supplySend);
    }

    /**
     * 删除配送方式对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSupplySendByIds(String ids)
    {
        return supplySendMapper.deleteSupplySendByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除配送方式信息
     * 
     * @param id 配送方式ID
     * @return 结果
     */
    @Override
    public int deleteSupplySendById(String id)
    {
        return supplySendMapper.deleteSupplySendById(id);
    }

    @Override
    public int deleteBySid(String id) {
        return this.supplySendMapper.deleteBySid(id);
    }

    @Override
    public List<SupplySend> findListByPid(String id) {
        return this.supplySendMapper.findListByPid(id);
    }
}
