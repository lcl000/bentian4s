package com.ruoyi.project.system.goods.img.enums;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/22 13:47
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public enum GoodsImgType {
	BANNER(0,"轮播图"),
	DETAILS(1,"详情图");

	private int value;
	private String name;

	GoodsImgType(int value, String name) {
		this.value = value;
		this.name = name;
	}

	/**
	 * 数据范围校验
	 *
	 * @param value
	 * @return
	 */
	public static boolean contains(int value) {
		return null == getByValue(value)?Boolean.FALSE.booleanValue():Boolean.TRUE.booleanValue();
	}
	public int getValue() {
		return this.value;
	}

	public String getName(){
		return this.name;
	}

	public static GoodsImgType getByValue(int value){
		GoodsImgType allVaues[] = GoodsImgType.values();
		GoodsImgType status;
		for (int i=0,len = allVaues.length;i<len;i++){
			status = allVaues[i];
			if(status.getValue()==value){
				return status;
			}
		}
		return null;
	}

}
