package com.ruoyi.project.system.goods.goodscard.product.service;

import java.util.List;
import com.ruoyi.project.system.goods.goodscard.product.domain.SupplyCardProduct;
import com.ruoyi.project.system.goods.goodscard.product.dto.SupplyCardProductDto;

/**
 * 供应卡规格Service接口
 * 
 * @author LCL
 * @date 2020-07-08
 */
public interface ISupplyCardProductService 
{
    /**
     * 查询供应卡规格
     * 
     * @param id 供应卡规格ID
     * @return 供应卡规格
     */
    public SupplyCardProduct selectSupplyCardProductById(String id);

    /**
     * 查询供应卡规格列表
     * 
     * @param supplyCardProduct 供应卡规格
     * @return 供应卡规格集合
     */
    public List<SupplyCardProduct> selectSupplyCardProductList(SupplyCardProduct supplyCardProduct);

    /**
     * 新增供应卡规格
     * 
     * @param supplyCardProduct 供应卡规格
     * @return 结果
     */
    public int insertSupplyCardProduct(SupplyCardProduct supplyCardProduct);

    /**
     * 修改供应卡规格
     * 
     * @param supplyCardProduct 供应卡规格
     * @return 结果
     */
    public int updateSupplyCardProduct(SupplyCardProduct supplyCardProduct);

    /**
     * 批量删除供应卡规格
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSupplyCardProductByIds(String ids);

    /**
     * 删除供应卡规格信息
     * 
     * @param id 供应卡规格ID
     * @return 结果
     */
    public int deleteSupplyCardProductById(String id);

	List<SupplyCardProductDto> findBySId(String id);

	int deleteBySid(String id);

	List<SupplyCardProductDto> findByGid(String id);
}
