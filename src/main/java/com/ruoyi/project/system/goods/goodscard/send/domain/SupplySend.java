package com.ruoyi.project.system.goods.goodscard.send.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 配送方式对象 supply_send
 * 
 * @author LCL
 * @date 2020-07-08
 */
public class SupplySend extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 供应卡主键ID */
    @Excel(name = "供应卡主键ID")
    private String supplyId;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 次数 */
    @Excel(name = "次数")
    private Integer num;

    /** 数量 */
    @Excel(name = "数量")
    private Integer total;

    /** 供应卡规格ID */
    @Excel(name = "供应卡规格ID")
    private String supplyProductId;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setSupplyId(String supplyId) 
    {
        this.supplyId = supplyId;
    }

    public String getSupplyId() 
    {
        return supplyId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setNum(Integer num)
    {
        this.num = num;
    }

    public Integer getNum()
    {
        return num;
    }
    public void setTotal(Integer total)
    {
        this.total = total;
    }

    public Integer getTotal()
    {
        return total;
    }
    public void setSupplyProductId(String supplyProductId) 
    {
        this.supplyProductId = supplyProductId;
    }

    public String getSupplyProductId() 
    {
        return supplyProductId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("supplyId", getSupplyId())
            .append("name", getName())
            .append("num", getNum())
            .append("total", getTotal())
            .append("supplyProductId", getSupplyProductId())
            .toString();
    }
}
