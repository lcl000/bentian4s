package com.ruoyi.project.system.goods.product.mapper;

import java.util.List;
import com.ruoyi.project.system.goods.product.domain.GoodsProduct;

/**
 * 商品 产品Mapper接口
 * 
 * @author LCL
 * @date 2020-06-12
 */
public interface GoodsProductMapper 
{
    /**
     * 查询商品 产品
     * 
     * @param id 商品 产品ID
     * @return 商品 产品
     */
    public GoodsProduct selectGoodsProductById(String id);

    /**
     * 查询商品 产品列表
     * 
     * @param goodsProduct 商品 产品
     * @return 商品 产品集合
     */
    public List<GoodsProduct> selectGoodsProductList(GoodsProduct goodsProduct);

    /**
     * 新增商品 产品
     * 
     * @param goodsProduct 商品 产品
     * @return 结果
     */
    public int insertGoodsProduct(GoodsProduct goodsProduct);

    /**
     * 修改商品 产品
     * 
     * @param goodsProduct 商品 产品
     * @return 结果
     */
    public int updateGoodsProduct(GoodsProduct goodsProduct);

    /**
     * 删除商品 产品
     * 
     * @param id 商品 产品ID
     * @return 结果
     */
    public int deleteGoodsProductById(String id);

    /**
     * 批量删除商品 产品
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGoodsProductByIds(String[] ids);

	List<GoodsProduct> findListByGoodsId(String goodsId);

    int removeByGoodsId(String goodsId);

	GoodsProduct findByName(String id, String spec);
}
