package com.ruoyi.project.system.goods.goodscard.supplytype.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.goods.goodscard.supplytype.mapper.SupplyTypeMapper;
import com.ruoyi.project.system.goods.goodscard.supplytype.domain.SupplyType;
import com.ruoyi.project.system.goods.goodscard.supplytype.service.ISupplyTypeService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 供应卡类型Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-09
 */
@Service
public class SupplyTypeServiceImpl implements ISupplyTypeService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private SupplyTypeMapper supplyTypeMapper;

    /**
     * 查询供应卡类型
     * 
     * @param id 供应卡类型ID
     * @return 供应卡类型
     */
    @Override
    public SupplyType selectSupplyTypeById(String id)
    {
        return supplyTypeMapper.selectSupplyTypeById(id);
    }

    /**
     * 查询供应卡类型列表
     * 
     * @param supplyType 供应卡类型
     * @return 供应卡类型
     */
    @Override
    public List<SupplyType> selectSupplyTypeList(SupplyType supplyType)
    {
        return supplyTypeMapper.selectSupplyTypeList(supplyType);
    }

    /**
     * 新增供应卡类型
     * 
     * @param supplyType 供应卡类型
     * @return 结果
     */
    @Override
    public int insertSupplyType(SupplyType supplyType)
    {
        supplyType.setId(this.idGererateFactory.nextStringId());
        return supplyTypeMapper.insertSupplyType(supplyType);
    }

    /**
     * 修改供应卡类型
     * 
     * @param supplyType 供应卡类型
     * @return 结果
     */
    @Override
    public int updateSupplyType(SupplyType supplyType)
    {
        return supplyTypeMapper.updateSupplyType(supplyType);
    }

    /**
     * 删除供应卡类型对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSupplyTypeByIds(String ids)
    {
        return supplyTypeMapper.deleteSupplyTypeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除供应卡类型信息
     * 
     * @param id 供应卡类型ID
     * @return 结果
     */
    @Override
    public int deleteSupplyTypeById(String id)
    {
        return supplyTypeMapper.deleteSupplyTypeById(id);
    }

    @Override
    public List<SupplyType> findBySId(String id) {
        return this.supplyTypeMapper.findBySId(id);
    }

    @Override
    public int deleteBySid(String id) {
        return this.supplyTypeMapper.deleteBySid(id);
    }
}
