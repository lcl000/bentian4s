package com.ruoyi.project.system.goods.goods.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.goods.goods.domain.GoodsDto;
import com.ruoyi.project.system.goods.goods.mapper.GoodsGoodsMapper;
import com.ruoyi.project.system.goods.goods.domain.GoodsGoods;
import com.ruoyi.project.system.goods.goods.service.IGoodsGoodsService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 商品Service业务层处理
 * 
 * @author LCL
 * @date 2020-06-12
 */
@Service
public class GoodsGoodsServiceImpl implements IGoodsGoodsService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private GoodsGoodsMapper goodsGoodsMapper;

    /**
     * 查询商品
     * 
     * @param id 商品ID
     * @return 商品
     */
    @Override
    public GoodsGoods selectGoodsGoodsById(String id)
    {
        return goodsGoodsMapper.selectGoodsGoodsById(id);
    }

    /**
     * 查询商品列表
     * 
     * @param goodsGoods 商品
     * @return 商品
     */
    @Override
    public List<GoodsDto> selectGoodsGoodsList(GoodsGoods goodsGoods)
    {
        return goodsGoodsMapper.selectGoodsGoodsList(goodsGoods);
    }

    /**
     * 新增商品
     * 
     * @param goodsGoods 商品
     * @return 结果
     */
    @Override
    public int insertGoodsGoods(GoodsGoods goodsGoods)
    {
        goodsGoods.setId(this.idGererateFactory.nextStringId());
        return goodsGoodsMapper.insertGoodsGoods(goodsGoods);
    }

    /**
     * 修改商品
     * 
     * @param goodsGoods 商品
     * @return 结果
     */
    @Override
    public int updateGoodsGoods(GoodsGoods goodsGoods)
    {
        return goodsGoodsMapper.updateGoodsGoods(goodsGoods);
    }

    /**
     * 删除商品对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteGoodsGoodsByIds(String ids)
    {
        return goodsGoodsMapper.deleteGoodsGoodsByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商品信息
     * 
     * @param id 商品ID
     * @return 结果
     */
    @Override
    public int deleteGoodsGoodsById(String id)
    {
        return goodsGoodsMapper.deleteGoodsGoodsById(id);
    }

    @Override
    public GoodsDto findGoodsDtoById(String id) {
        return this.goodsGoodsMapper.findGoodsDtoById(id);
    }

    @Override
    public int remove(String ids) {
        return this.goodsGoodsMapper.remove(Convert.toStrArray(ids));
    }

    @Override
    public List<GoodsDto> findGoodsDtoList(GoodsGoods goodsGoods) {
        return this.goodsGoodsMapper.findGoodsDtoList(goodsGoods);
    }

    @Override
    public GoodsDto findGoodsDtoByPId(String product) {
        return this.goodsGoodsMapper.findGoodsDtoByPId(product);
    }
}
