package com.ruoyi.project.system.goods.goodscard.supplytype.mapper;

import java.util.List;
import com.ruoyi.project.system.goods.goodscard.supplytype.domain.SupplyType;

/**
 * 供应卡类型Mapper接口
 * 
 * @author LCL
 * @date 2020-07-09
 */
public interface SupplyTypeMapper 
{
    /**
     * 查询供应卡类型
     * 
     * @param id 供应卡类型ID
     * @return 供应卡类型
     */
    public SupplyType selectSupplyTypeById(String id);

    /**
     * 查询供应卡类型列表
     * 
     * @param supplyType 供应卡类型
     * @return 供应卡类型集合
     */
    public List<SupplyType> selectSupplyTypeList(SupplyType supplyType);

    /**
     * 新增供应卡类型
     * 
     * @param supplyType 供应卡类型
     * @return 结果
     */
    public int insertSupplyType(SupplyType supplyType);

    /**
     * 修改供应卡类型
     * 
     * @param supplyType 供应卡类型
     * @return 结果
     */
    public int updateSupplyType(SupplyType supplyType);

    /**
     * 删除供应卡类型
     * 
     * @param id 供应卡类型ID
     * @return 结果
     */
    public int deleteSupplyTypeById(String id);

    /**
     * 批量删除供应卡类型
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSupplyTypeByIds(String[] ids);

	List<SupplyType> findBySId(String id);

	int deleteBySid(String id);
}
