package com.ruoyi.project.system.goods.goodscard.card.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 商品供应卡对象 goods_supply_card
 * 
 * @author LCL
 * @date 2020-07-08
 */
public class GoodsSupplyCard extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 产品ID */
    @Excel(name = "产品ID")
    private String goodsId;

    /** 分类id: 蒙牛,伊利  作用: 存储规格,详参  */
    @Excel(name = "分类id: 蒙牛,伊利  作用: 存储规格,详参 ")
    private String typeId;

    /** 产品类型ID */
    @Excel(name = "产品类型ID")
    private String classId;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setGoodsId(String goodsId) 
    {
        this.goodsId = goodsId;
    }

    public String getGoodsId() 
    {
        return goodsId;
    }
    public void setTypeId(String typeId) 
    {
        this.typeId = typeId;
    }

    public String getTypeId() 
    {
        return typeId;
    }
    public void setClassId(String classId) 
    {
        this.classId = classId;
    }

    public String getClassId() 
    {
        return classId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodsId", getGoodsId())
            .append("typeId", getTypeId())
            .append("classId", getClassId())
            .toString();
    }
}
