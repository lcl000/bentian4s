package com.ruoyi.project.system.goods.goods.domain;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/16 17:03
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public class GoodsDto extends GoodsGoods {
	//类型名称
	private String typeName;
	//原价
	private BigDecimal priceO;
	//现价
	private BigDecimal priceN;
	//秒杀开始时间戳
	private Long spikeStartL;
	//秒杀结束时间戳
	private Long spikeEndL;
	//产品规格
	private String productSpec;
	//数量
	private Integer total;
	//规格ID
	private String specId;

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public BigDecimal getPriceO() {
		return priceO;
	}

	public void setPriceO(BigDecimal priceO) {
		this.priceO = priceO;
	}

	public BigDecimal getPriceN() {
		return priceN;
	}

	public void setPriceN(BigDecimal priceN) {
		this.priceN = priceN;
	}

	public Long getSpikeStartL() {
		return spikeStartL;
	}

	public void setSpikeStartL(Date spikeStart) {
		this.spikeStartL = spikeStart.getTime();
	}

	public Long getSpikeEndL() {
		return spikeEndL;
	}

	public void setSpikeEndL(Date spikeEnd) {
		this.spikeEndL = spikeEnd.getTime();
	}

	public String getProductSpec() {
		return productSpec;
	}

	public void setProductSpec(String productSpec) {
		this.productSpec = productSpec;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public String getSpecId() {
		return specId;
	}

	public void setSpecId(String specId) {
		this.specId = specId;
	}
}
