package com.ruoyi.project.system.goods.goodscard.card.dto;

import java.util.List;

import com.ruoyi.project.system.goods.goodscard.card.domain.GoodsSupplyCard;
import com.ruoyi.project.system.goods.goodscard.product.domain.SupplyCardProduct;
import com.ruoyi.project.system.goods.goodscard.product.dto.SupplyCardProductDto;
import com.ruoyi.project.system.goods.goodscard.supplytype.domain.SupplyType;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/7/9 10:39
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public class SupplyCardDto extends GoodsSupplyCard {

	private List<SupplyType> supplyTypes;

	private List<SupplyCardProductDto> supplyCardProducts;

	private String className;

	private String typeName;

	public List<SupplyType> getSupplyTypes() {
		return supplyTypes;
	}

	public void setSupplyTypes(List<SupplyType> supplyTypes) {
		this.supplyTypes = supplyTypes;
	}

	public List<SupplyCardProductDto> getSupplyCardProducts() {
		return supplyCardProducts;
	}

	public void setSupplyCardProducts(List<SupplyCardProductDto> supplyCardProducts) {
		this.supplyCardProducts = supplyCardProducts;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
}
