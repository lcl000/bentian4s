package com.ruoyi.project.system.goods.img.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.goods.img.mapper.GoodsImgMapper;
import com.ruoyi.project.system.goods.img.domain.GoodsImg;
import com.ruoyi.project.system.goods.img.service.IGoodsImgService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 商品 主图Service业务层处理
 * 
 * @author LCL
 * @date 2020-06-12
 */
@Service
public class GoodsImgServiceImpl implements IGoodsImgService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private GoodsImgMapper goodsImgMapper;

    /**
     * 查询商品 主图
     * 
     * @param id 商品 主图ID
     * @return 商品 主图
     */
    @Override
    public GoodsImg selectGoodsImgById(String id)
    {
        return goodsImgMapper.selectGoodsImgById(id);
    }

    /**
     * 查询商品 主图列表
     * 
     * @param goodsImg 商品 主图
     * @return 商品 主图
     */
    @Override
    public List<GoodsImg> selectGoodsImgList(GoodsImg goodsImg)
    {
        return goodsImgMapper.selectGoodsImgList(goodsImg);
    }

    /**
     * 新增商品 主图
     * 
     * @param goodsImg 商品 主图
     * @return 结果
     */
    @Override
    public int insertGoodsImg(GoodsImg goodsImg)
    {
        goodsImg.setId(this.idGererateFactory.nextStringId());
        return goodsImgMapper.insertGoodsImg(goodsImg);
    }

    /**
     * 修改商品 主图
     * 
     * @param goodsImg 商品 主图
     * @return 结果
     */
    @Override
    public int updateGoodsImg(GoodsImg goodsImg)
    {
        return goodsImgMapper.updateGoodsImg(goodsImg);
    }

    /**
     * 删除商品 主图对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteGoodsImgByIds(String ids)
    {
        return goodsImgMapper.deleteGoodsImgByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商品 主图信息
     * 
     * @param id 商品 主图ID
     * @return 结果
     */
    @Override
    public int deleteGoodsImgById(String id)
    {
        return goodsImgMapper.deleteGoodsImgById(id);
    }

    @Override
    public List<GoodsImg> findListByGoodsIdAndType(String goodsId,Integer type) {
        return goodsImgMapper.findListByGoodsIdAndType(goodsId,type);
    }

    @Override
    public int deleteByGoodsId(String goodsId) {
        return goodsImgMapper.deleteByGoodsId(goodsId);
    }
}
