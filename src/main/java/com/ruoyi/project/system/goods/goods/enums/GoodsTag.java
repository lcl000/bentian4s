package com.ruoyi.project.system.goods.goods.enums;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/19 14:02
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public enum GoodsTag {
	NULL(0,"无"),
	DIRECT_MAIL(1,"直邮"),
	QUALITY_GOODS(2,"正品"),
	BONDED(3,"保税");

	private int value;
	private String name;

	GoodsTag(int value, String name) {
		this.value = value;
		this.name = name;
	}

	/**
	 * 数据范围校验
	 *
	 * @param value
	 * @return
	 */
	public static boolean contains(int value) {
		return null == getByValue(value)?Boolean.FALSE.booleanValue():Boolean.TRUE.booleanValue();
	}
	public int getValue() {
		return this.value;
	}

	public String getName(){
		return this.name;
	}

	public static GoodsTag getByValue(int value){
		GoodsTag allVaues[] = GoodsTag.values();
		GoodsTag status;
		for (int i=0,len = allVaues.length;i<len;i++){
			status = allVaues[i];
			if(status.getValue()==value){
				return status;
			}
		}
		return null;
	}
}
