package com.ruoyi.project.system.goods.goodscard.supplytype.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 供应卡类型对象 supply_type
 * 
 * @author LCL
 * @date 2020-07-09
 */
public class SupplyType extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 商品ID */
    @Excel(name = "商品ID")
    private String goodsId;

    /** 商品供应卡ID */
    @Excel(name = "商品供应卡ID")
    private String supplyId;

    /** 类型: 0 二月卡 1 季卡 2 半年卡 3 年卡 */
    @Excel(name = "类型: 0 二月卡 1 季卡 2 半年卡 3 年卡")
    private Integer type;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setGoodsId(String goodsId) 
    {
        this.goodsId = goodsId;
    }

    public String getGoodsId() 
    {
        return goodsId;
    }
    public void setSupplyId(String supplyId) 
    {
        this.supplyId = supplyId;
    }

    public String getSupplyId() 
    {
        return supplyId;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodsId", getGoodsId())
            .append("supplyId", getSupplyId())
            .append("type", getType())
            .toString();
    }
}
