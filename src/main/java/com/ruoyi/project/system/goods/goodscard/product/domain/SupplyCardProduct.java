package com.ruoyi.project.system.goods.goodscard.product.domain;

import java.math.BigDecimal;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 供应卡规格对象 supply_card_product
 * 
 * @author LCL
 * @date 2020-07-08
 */
public class SupplyCardProduct extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 供应卡主键ID */
    @Excel(name = "供应卡主键ID")
    private String supplyCardId;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 卡的价格 */
    @Excel(name = "卡的价格")
    private BigDecimal price;

    /** 单价 */
    @Excel(name = "单价")
    private BigDecimal priceOne;

    /** 类型: 0 俩月卡 1 季卡 2 半年卡 3 年卡 */
    @Excel(name = "类型: 0 俩月卡 1 季卡 2 半年卡 3 年卡")
    private Integer type;

    /** 供应周期 */
    @Excel(name = "供应周期")
    private Long sumDay;

    /** 数量 */
    @Excel(name = "数量")
    private Long total;

    /** 备注 */
    @Excel(name = "备注")
    private String notes;

    private String sends;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setSupplyCardId(String supplyCardId) 
    {
        this.supplyCardId = supplyCardId;
    }

    public String getSupplyCardId() 
    {
        return supplyCardId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPrice(BigDecimal price)
    {
        this.price = price;
    }

    public BigDecimal getPrice()
    {
        return price;
    }
    public void setPriceOne(BigDecimal priceOne)
    {
        this.priceOne = priceOne;
    }

    public BigDecimal getPriceOne()
    {
        return priceOne;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setSumDay(Long sumDay) 
    {
        this.sumDay = sumDay;
    }

    public Long getSumDay() 
    {
        return sumDay;
    }
    public void setTotal(Long total) 
    {
        this.total = total;
    }

    public Long getTotal() 
    {
        return total;
    }
    public void setNotes(String notes) 
    {
        if(notes==null){
            notes = "";
        }
        this.notes = notes;
    }

    public String getNotes() 
    {
        return notes;
    }

    public String getSends() {
        return sends;
    }

    public void setSends(String sends) {
        this.sends = sends;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("supplyCardId", getSupplyCardId())
            .append("name", getName())
            .append("price", getPrice())
            .append("priceOne", getPriceOne())
            .append("type", getType())
            .append("sumDay", getSumDay())
            .append("total", getTotal())
            .append("notes", getNotes())
            .toString();
    }
}
