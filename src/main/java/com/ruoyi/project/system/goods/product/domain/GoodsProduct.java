package com.ruoyi.project.system.goods.product.domain;

import java.math.BigDecimal;
import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 商品 产品对象 goods_product
 * 
 * @author LCL
 * @date 2020-06-12
 */
public class GoodsProduct extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 产品ID */
    @Excel(name = "产品ID")
    private String goodsId;

    /** 是否默认 0 不是 1 是 */
    @Excel(name = "是否默认 0 不是 1 是")
    private Integer isDefault;

    /** 货品名 */
    @Excel(name = "货品名")
    private String name;

    /** 产品图片访问路径 */
    @Excel(name = "产品图片访问路径")
    private String icon;

    /** 货品规格 */
    @Excel(name = "货品规格")
    private String spec;

    /** 原价 */
    @Excel(name = "原价")
    private BigDecimal priceO;

    /** 现价 */
    @Excel(name = "现价")
    private BigDecimal priceN;

    /** 上架时间 */
    @Excel(name = "上架时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date upTime;

    /** 下架时间 */
    @Excel(name = "下架时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date downTime;

    /** 销售量 */
    @Excel(name = "销售量")
    private Long numSale;

    /** 状态 0 正常 1 删除 */
    @Excel(name = "状态 0 正常 1 删除")
    private Integer status;

    /** 库存 */
    @Excel(name = "库存")
    private Integer stock;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setGoodsId(String goodsId) 
    {
        this.goodsId = goodsId;
    }

    public String getGoodsId() 
    {
        return goodsId;
    }
    public void setIsDefault(Integer isDefault) 
    {
        this.isDefault = isDefault;
    }

    public Integer getIsDefault() 
    {
        return isDefault;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setIcon(String icon) 
    {
        this.icon = icon;
    }

    public String getIcon() 
    {
        return icon;
    }
    public void setSpec(String spec) 
    {
        this.spec = spec;
    }

    public String getSpec() 
    {
        return spec;
    }
    public void setPriceO(BigDecimal priceO) 
    {
        this.priceO = priceO;
    }

    public BigDecimal getPriceO() 
    {
        return priceO;
    }
    public void setPriceN(BigDecimal priceN) 
    {
        this.priceN = priceN;
    }

    public BigDecimal getPriceN() 
    {
        return priceN;
    }
    public void setUpTime(Date upTime) 
    {
        this.upTime = upTime;
    }

    public Date getUpTime() 
    {
        return upTime;
    }
    public void setDownTime(Date downTime) 
    {
        this.downTime = downTime;
    }

    public Date getDownTime() 
    {
        return downTime;
    }
    public void setNumSale(Long numSale) 
    {
        this.numSale = numSale;
    }

    public Long getNumSale() 
    {
        return numSale;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodsId", getGoodsId())
            .append("isDefault", getIsDefault())
            .append("name", getName())
            .append("icon", getIcon())
            .append("spec", getSpec())
            .append("priceO", getPriceO())
            .append("priceN", getPriceN())
            .append("upTime", getUpTime())
            .append("downTime", getDownTime())
            .append("numSale", getNumSale())
            .append("status", getStatus())
            .append("stock", getStock())
            .toString();
    }
}
