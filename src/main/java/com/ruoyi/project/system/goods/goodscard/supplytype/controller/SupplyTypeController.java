package com.ruoyi.project.system.goods.goodscard.supplytype.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.goods.goodscard.supplytype.domain.SupplyType;
import com.ruoyi.project.system.goods.goodscard.supplytype.service.ISupplyTypeService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 供应卡类型Controller
 * 
 * @author LCL
 * @date 2020-07-09
 */
@Controller
@RequestMapping("/system/supplytype")
public class SupplyTypeController extends BaseController
{
    private String prefix = "system/supplytype";

    @Autowired
    private ISupplyTypeService supplyTypeService;

    @RequiresPermissions("system:supplytype:view")
    @GetMapping()
    public String supplytype()
    {
        return prefix + "/supplytype";
    }

    /**
     * 查询供应卡类型列表
     */
    @RequiresPermissions("system:supplytype:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SupplyType supplyType)
    {
        startPage();
        List<SupplyType> list = supplyTypeService.selectSupplyTypeList(supplyType);
        return getDataTable(list);
    }

    /**
     * 导出供应卡类型列表
     */
    @RequiresPermissions("system:supplytype:export")
    @Log(title = "供应卡类型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SupplyType supplyType)
    {
        List<SupplyType> list = supplyTypeService.selectSupplyTypeList(supplyType);
        ExcelUtil<SupplyType> util = new ExcelUtil<SupplyType>(SupplyType.class);
        return util.exportExcel(list, "supplytype");
    }

    /**
     * 新增供应卡类型
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存供应卡类型
     */
    @RequiresPermissions("system:supplytype:add")
    @Log(title = "供应卡类型", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SupplyType supplyType)
    {
        return toAjax(supplyTypeService.insertSupplyType(supplyType));
    }

    /**
     * 修改供应卡类型
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        SupplyType supplyType = supplyTypeService.selectSupplyTypeById(id);
        mmap.put("supplyType", supplyType);
        return prefix + "/edit";
    }

    /**
     * 修改保存供应卡类型
     */
    @RequiresPermissions("system:supplytype:edit")
    @Log(title = "供应卡类型", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SupplyType supplyType)
    {
        return toAjax(supplyTypeService.updateSupplyType(supplyType));
    }

    /**
     * 删除供应卡类型
     */
    @RequiresPermissions("system:supplytype:remove")
    @Log(title = "供应卡类型", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(supplyTypeService.deleteSupplyTypeByIds(ids));
    }
}
