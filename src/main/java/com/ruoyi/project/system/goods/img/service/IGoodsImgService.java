package com.ruoyi.project.system.goods.img.service;

import java.util.List;
import com.ruoyi.project.system.goods.img.domain.GoodsImg;

/**
 * 商品 主图Service接口
 * 
 * @author LCL
 * @date 2020-06-12
 */
public interface IGoodsImgService 
{
    /**
     * 查询商品 主图
     * 
     * @param id 商品 主图ID
     * @return 商品 主图
     */
    public GoodsImg selectGoodsImgById(String id);

    /**
     * 查询商品 主图列表
     * 
     * @param goodsImg 商品 主图
     * @return 商品 主图集合
     */
    public List<GoodsImg> selectGoodsImgList(GoodsImg goodsImg);

    /**
     * 新增商品 主图
     * 
     * @param goodsImg 商品 主图
     * @return 结果
     */
    public int insertGoodsImg(GoodsImg goodsImg);

    /**
     * 修改商品 主图
     * 
     * @param goodsImg 商品 主图
     * @return 结果
     */
    public int updateGoodsImg(GoodsImg goodsImg);

    /**
     * 批量删除商品 主图
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGoodsImgByIds(String ids);

    /**
     * 删除商品 主图信息
     * 
     * @param id 商品 主图ID
     * @return 结果
     */
    public int deleteGoodsImgById(String id);

	List<GoodsImg> findListByGoodsIdAndType(String goodsId,Integer type);

	int deleteByGoodsId(String goodsId);
}
