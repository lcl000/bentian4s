package com.ruoyi.project.system.goods.product.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.goods.product.domain.GoodsProduct;
import com.ruoyi.project.system.goods.product.service.IGoodsProductService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 商品 产品Controller
 * 
 * @author LCL
 * @date 2020-06-12
 */
@Controller
@RequestMapping("/system/product")
public class GoodsProductController extends BaseController
{
    private String prefix = "system/product";

    @Autowired
    private IGoodsProductService goodsProductService;

    @RequiresPermissions("system:product:view")
    @GetMapping()
    public String product()
    {
        return prefix + "/product";
    }

    /**
     * 查询商品 产品列表
     */
    @RequiresPermissions("system:product:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(GoodsProduct goodsProduct)
    {
        startPage();
        List<GoodsProduct> list = goodsProductService.selectGoodsProductList(goodsProduct);
        return getDataTable(list);
    }

    /**
     * 导出商品 产品列表
     */
    @RequiresPermissions("system:product:export")
    @Log(title = "商品 产品", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(GoodsProduct goodsProduct)
    {
        List<GoodsProduct> list = goodsProductService.selectGoodsProductList(goodsProduct);
        ExcelUtil<GoodsProduct> util = new ExcelUtil<GoodsProduct>(GoodsProduct.class);
        return util.exportExcel(list, "product");
    }

    /**
     * 新增商品 产品
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存商品 产品
     */
    @RequiresPermissions("system:product:add")
    @Log(title = "商品 产品", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(GoodsProduct goodsProduct)
    {
        return toAjax(goodsProductService.insertGoodsProduct(goodsProduct));
    }

    /**
     * 修改商品 产品
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        GoodsProduct goodsProduct = goodsProductService.selectGoodsProductById(id);
        mmap.put("goodsProduct", goodsProduct);
        return prefix + "/edit";
    }

    /**
     * 修改保存商品 产品
     */
    @RequiresPermissions("system:product:edit")
    @Log(title = "商品 产品", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(GoodsProduct goodsProduct)
    {
        return toAjax(goodsProductService.updateGoodsProduct(goodsProduct));
    }

    /**
     * 删除商品 产品
     */
    @RequiresPermissions("system:product:remove")
    @Log(title = "商品 产品", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(goodsProductService.deleteGoodsProductByIds(ids));
    }
}
