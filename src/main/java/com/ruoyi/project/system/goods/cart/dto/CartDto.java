package com.ruoyi.project.system.goods.cart.dto;

import com.ruoyi.project.system.goods.cart.domain.ShoppingCart;

import java.math.BigDecimal;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/27 22:26
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public class CartDto extends ShoppingCart {
	//品牌名称
	private String brandName;
	//原价
	private BigDecimal priceO;
	//现价
	private BigDecimal priceN;
	//产品规格
	private String productSpec;
	//该产品状态
	private Integer goodsStatus;
	//产品图片
	private String mainUrl;
	//产品标签
	private Integer tag;
	//产品名称
	private String goodsName;
	//规格状态
	private Integer productStatus;

	private String className;

    private String typeName;


	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public BigDecimal getPriceO() {
		return priceO;
	}

	public void setPriceO(BigDecimal priceO) {
		this.priceO = priceO;
	}

	public BigDecimal getPriceN() {
		return priceN;
	}

	public void setPriceN(BigDecimal priceN) {
		this.priceN = priceN;
	}



	public String getProductSpec() {
		return productSpec;
	}

	public void setProductSpec(String productSpec) {
		this.productSpec = productSpec;
	}

	public Integer getGoodsStatus() {
		return goodsStatus;
	}

	public void setGoodsStatus(Integer goodsStatus) {
		this.goodsStatus = goodsStatus;
	}

	public String getMainUrl() {
		return mainUrl;
	}

	public void setMainUrl(String mainUrl) {
		this.mainUrl = mainUrl;
	}

	public Integer getTag() {
		return tag;
	}

	public void setTag(Integer tag) {
		this.tag = tag;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public Integer getProductStatus() {
		return productStatus;
	}

	public void setProductStatus(Integer productStatus) {
		this.productStatus = productStatus;
	}

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
