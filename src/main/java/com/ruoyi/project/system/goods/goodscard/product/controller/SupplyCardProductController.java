package com.ruoyi.project.system.goods.goodscard.product.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.goods.goodscard.product.domain.SupplyCardProduct;
import com.ruoyi.project.system.goods.goodscard.product.service.ISupplyCardProductService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 供应卡规格Controller
 * 
 * @author LCL
 * @date 2020-07-08
 */
@Controller
@RequestMapping("/system/goodscard/product")
public class SupplyCardProductController extends BaseController
{
    private String prefix = "system/product";

    @Autowired
    private ISupplyCardProductService supplyCardProductService;

    @RequiresPermissions("system:product:view")
    @GetMapping
    public String product()
    {
        return prefix + "/product";
    }

    /**
     * 查询供应卡规格列表
     */
    @RequiresPermissions("system:product:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SupplyCardProduct supplyCardProduct)
    {
        startPage();
        List<SupplyCardProduct> list = supplyCardProductService.selectSupplyCardProductList(supplyCardProduct);
        return getDataTable(list);
    }

    /**
     * 导出供应卡规格列表
     */
    @RequiresPermissions("system:product:export")
    @Log(title = "供应卡规格", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SupplyCardProduct supplyCardProduct)
    {
        List<SupplyCardProduct> list = supplyCardProductService.selectSupplyCardProductList(supplyCardProduct);
        ExcelUtil<SupplyCardProduct> util = new ExcelUtil<SupplyCardProduct>(SupplyCardProduct.class);
        return util.exportExcel(list, "product");
    }

    /**
     * 新增供应卡规格
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存供应卡规格
     */
    @RequiresPermissions("system:product:add")
    @Log(title = "供应卡规格", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SupplyCardProduct supplyCardProduct)
    {
        return toAjax(supplyCardProductService.insertSupplyCardProduct(supplyCardProduct));
    }

    /**
     * 修改供应卡规格
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        SupplyCardProduct supplyCardProduct = supplyCardProductService.selectSupplyCardProductById(id);
        mmap.put("supplyCardProduct", supplyCardProduct);
        return prefix + "/edit";
    }

    /**
     * 修改保存供应卡规格
     */
    @RequiresPermissions("system:product:edit")
    @Log(title = "供应卡规格", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SupplyCardProduct supplyCardProduct)
    {
        return toAjax(supplyCardProductService.updateSupplyCardProduct(supplyCardProduct));
    }

    /**
     * 删除供应卡规格
     */
    @RequiresPermissions("system:product:remove")
    @Log(title = "供应卡规格", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(supplyCardProductService.deleteSupplyCardProductByIds(ids));
    }
}
