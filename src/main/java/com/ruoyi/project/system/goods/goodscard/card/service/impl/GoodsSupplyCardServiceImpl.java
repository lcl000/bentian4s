package com.ruoyi.project.system.goods.goodscard.card.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.goods.goodscard.card.dto.SupplyCardDto;
import com.ruoyi.project.system.goods.goodscard.card.mapper.GoodsSupplyCardMapper;
import com.ruoyi.project.system.goods.goodscard.card.domain.GoodsSupplyCard;
import com.ruoyi.project.system.goods.goodscard.card.service.IGoodsSupplyCardService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 商品供应卡Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-08
 */
@Service
public class GoodsSupplyCardServiceImpl implements IGoodsSupplyCardService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private GoodsSupplyCardMapper goodsSupplyCardMapper;

    /**
     * 查询商品供应卡
     * 
     * @param id 商品供应卡ID
     * @return 商品供应卡
     */
    @Override
    public GoodsSupplyCard selectGoodsSupplyCardById(String id)
    {
        return goodsSupplyCardMapper.selectGoodsSupplyCardById(id);
    }

    /**
     * 查询商品供应卡列表
     * 
     * @param goodsSupplyCard 商品供应卡
     * @return 商品供应卡
     */
    @Override
    public List<GoodsSupplyCard> selectGoodsSupplyCardList(GoodsSupplyCard goodsSupplyCard)
    {
        return goodsSupplyCardMapper.selectGoodsSupplyCardList(goodsSupplyCard);
    }

    /**
     * 新增商品供应卡
     * 
     * @param goodsSupplyCard 商品供应卡
     * @return 结果
     */
    @Override
    public int insertGoodsSupplyCard(GoodsSupplyCard goodsSupplyCard)
    {
        goodsSupplyCard.setId(this.idGererateFactory.nextStringId());
        return goodsSupplyCardMapper.insertGoodsSupplyCard(goodsSupplyCard);
    }

    /**
     * 修改商品供应卡
     * 
     * @param goodsSupplyCard 商品供应卡
     * @return 结果
     */
    @Override
    public int updateGoodsSupplyCard(GoodsSupplyCard goodsSupplyCard)
    {
        return goodsSupplyCardMapper.updateGoodsSupplyCard(goodsSupplyCard);
    }

    /**
     * 删除商品供应卡对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteGoodsSupplyCardByIds(String ids)
    {
        return goodsSupplyCardMapper.deleteGoodsSupplyCardByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商品供应卡信息
     * 
     * @param id 商品供应卡ID
     * @return 结果
     */
    @Override
    public int deleteGoodsSupplyCardById(String id)
    {
        return goodsSupplyCardMapper.deleteGoodsSupplyCardById(id);
    }

    @Override
    public SupplyCardDto findById(String id) {
        return this.goodsSupplyCardMapper.findById(id);
    }

    @Override
    public SupplyCardDto findByCId(String supplyCardId) {
        return this.goodsSupplyCardMapper.findByCId(supplyCardId);
    }
}
