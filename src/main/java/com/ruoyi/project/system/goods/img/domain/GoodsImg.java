package com.ruoyi.project.system.goods.img.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 商品 主图对象 goods_img
 * 
 * @author LCL
 * @date 2020-06-12
 */
public class GoodsImg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 图片访问路径 */
    @Excel(name = "图片访问路径")
    private String url;

    /** 商品主键ID */
    @Excel(name = "商品主键ID")
    private String goodsId;

    //类型:0 主图 1 详情
    private Integer type;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }
    public void setGoodsId(String goodsId) 
    {
        this.goodsId = goodsId;
    }

    public String getGoodsId() 
    {
        return goodsId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("sort", getSort())
            .append("url", getUrl())
            .append("goodsId", getGoodsId())
            .append("type",getType())
            .toString();
    }
}
