package com.ruoyi.project.system.goods.goodscard.product.dto;

import java.util.List;

import com.ruoyi.project.system.goods.goodscard.product.domain.SupplyCardProduct;
import com.ruoyi.project.system.goods.goodscard.send.domain.SupplySend;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/7/14 15:22
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public class SupplyCardProductDto extends SupplyCardProduct {
	private List<SupplySend> sendList;

	public List<SupplySend> getSendList() {
		return sendList;
	}

	public void setSendList(List<SupplySend> sendList) {
		this.sendList = sendList;
	}
}
