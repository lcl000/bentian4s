package com.ruoyi.project.system.goods.goodscard.card.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.goods.goodscard.card.domain.GoodsSupplyCard;
import com.ruoyi.project.system.goods.goodscard.card.dto.SupplyCardDto;
import com.ruoyi.project.system.goods.goodscard.card.service.IGoodsSupplyCardService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.goods.goodscard.product.dto.SupplyCardProductDto;
import com.ruoyi.project.system.goods.goodscard.product.service.ISupplyCardProductService;
import com.ruoyi.project.system.goods.goodscard.send.domain.SupplySend;
import com.ruoyi.project.system.goods.goodscard.send.service.ISupplySendService;
import com.ruoyi.project.system.goods.goodscard.supplytype.domain.SupplyType;
import com.ruoyi.project.system.goods.goodscard.supplytype.service.ISupplyTypeService;

/**
 * 商品供应卡Controller
 * 
 * @author LCL
 * @date 2020-07-08
 */
@Controller
@RequestMapping("/system/card")
public class GoodsSupplyCardController extends BaseController
{
    private String prefix = "system/goods";

    @Autowired
    private IGoodsSupplyCardService goodsSupplyCardService;

    @Autowired
    private ISupplyTypeService supplyTypeService;

    @Autowired
    private ISupplyCardProductService supplyCardProductService;

    @Autowired
    private ISupplySendService supplySendService;

    @RequiresPermissions("system:card:view")
    @GetMapping()
    public String card()
    {
        return prefix + "/card";
    }

    /**
     * 查询商品供应卡列表
     */
    @RequiresPermissions("system:card:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(GoodsSupplyCard goodsSupplyCard)
    {
        startPage();
        List<GoodsSupplyCard> list = goodsSupplyCardService.selectGoodsSupplyCardList(goodsSupplyCard);
        return getDataTable(list);
    }

    /**
     * 导出商品供应卡列表
     */
    @RequiresPermissions("system:card:export")
    @Log(title = "商品供应卡", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(GoodsSupplyCard goodsSupplyCard)
    {
        List<GoodsSupplyCard> list = goodsSupplyCardService.selectGoodsSupplyCardList(goodsSupplyCard);
        ExcelUtil<GoodsSupplyCard> util = new ExcelUtil<GoodsSupplyCard>(GoodsSupplyCard.class);
        return util.exportExcel(list, "card");
    }

    /**
     * 新增商品供应卡
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存商品供应卡
     */
    @RequiresPermissions("system:card:add")
    @Log(title = "商品供应卡", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(GoodsSupplyCard goodsSupplyCard)
    {
        return toAjax(goodsSupplyCardService.insertGoodsSupplyCard(goodsSupplyCard));
    }

    /**
     * 修改商品供应卡
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        GoodsSupplyCard goodsSupplyCard = goodsSupplyCardService.selectGoodsSupplyCardById(id);
        mmap.put("goodsSupplyCard", goodsSupplyCard);
        return prefix + "/edit";
    }

    /**
     * 修改保存商品供应卡
     */
    @RequiresPermissions("system:card:edit")
    @Log(title = "商品供应卡", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(GoodsSupplyCard goodsSupplyCard)
    {
        return toAjax(goodsSupplyCardService.updateGoodsSupplyCard(goodsSupplyCard));
    }

    /**
     * 删除商品供应卡
     */
    @RequiresPermissions("system:card:remove")
    @Log(title = "商品供应卡", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(goodsSupplyCardService.deleteGoodsSupplyCardByIds(ids));
    }

    /**
     * 修改供应卡
     * @param id
     * @param mmap
     * @return
     */
    @GetMapping("/goSupplyCard/{id}")
    public String goSupplyCart(@PathVariable String id,ModelMap mmap){
        SupplyCardDto supplyCardDto = this.goodsSupplyCardService.findById(id);

        if(supplyCardDto!=null){

            supplyCardDto.setSupplyTypes(this.supplyTypeService.findBySId(supplyCardDto.getId()));
            List<SupplyCardProductDto> cardProductDtos = this.supplyCardProductService.findBySId(supplyCardDto.getId());
            for(SupplyCardProductDto supplyCardProductDto :cardProductDtos){
                supplyCardProductDto.setSendList(this.supplySendService.findListByPid(supplyCardProductDto.getId()));
            }
            supplyCardDto.setSupplyCardProducts(cardProductDtos);

        }

        mmap.put("supplyCardDto",supplyCardDto);

        return prefix + "/goSupplyCard";
    }

    @PostMapping("/goSupplyCard")
    @ResponseBody
    @Transactional
    public AjaxResult updateSupplyCard(SupplyCardDto supplyCardDto){
        //查询供应卡
        GoodsSupplyCard oldCard = this.goodsSupplyCardService.selectGoodsSupplyCardById(supplyCardDto.getId());
        if(oldCard==null){
            return AjaxResult.error("供应卡不存在");
        }

        //清除原本存在的供应卡类别
        this.supplyTypeService.deleteBySid(supplyCardDto.getId());
        //清除原本存在的供应卡规格
        this.supplyCardProductService.deleteBySid(supplyCardDto.getId());
        //清除原本存在的供应卡配送方式
        this.supplySendService.deleteBySid(supplyCardDto.getId());
        //新增供应卡规格
        for(SupplyCardProductDto supplyCardProductDto :supplyCardDto.getSupplyCardProducts()){
            supplyCardProductDto.setSupplyCardId(supplyCardDto.getId());
            this.supplyCardProductService.insertSupplyCardProduct(supplyCardProductDto);
            //新增该规格下的配送方式
            String sends = supplyCardProductDto.getSends().replaceAll(" ","");
            if(!StringUtils.isBlank(sends)){
                String[] split = sends.split("瓶");
                for(String s :split){
                    String[] sendInfo = s.replaceAll("次","").split("/");
                    SupplySend supplySend = new SupplySend();
                    supplySend.setName(s+"瓶");
                    supplySend.setSupplyId(supplyCardDto.getId());
                    supplySend.setNum(Integer.parseInt(sendInfo[0]));
                    supplySend.setTotal(Integer.parseInt(sendInfo[1]));
                    supplySend.setSupplyProductId(supplyCardProductDto.getId());
                    this.supplySendService.insertSupplySend(supplySend);
                }
            }
            //新增供应卡类别
            SupplyType supplyType = new SupplyType();
            supplyType.setSupplyId(supplyCardDto.getId());
            supplyType.setGoodsId(oldCard.getGoodsId());
            supplyType.setType(supplyCardProductDto.getType());
            this.supplyTypeService.insertSupplyType(supplyType);
        }
        return AjaxResult.success();
    }

}
