package com.ruoyi.project.system.type.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.type.mapper.GoodsTypeMapper;
import com.ruoyi.project.system.type.domain.GoodsType;
import com.ruoyi.project.system.type.service.IGoodsTypeService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 商品类别Service业务层处理
 * 
 * @author LCL
 * @date 2020-06-10
 */
@Service
public class GoodsTypeServiceImpl implements IGoodsTypeService 
{

    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private GoodsTypeMapper goodsTypeMapper;

    /**
     * 查询商品类别
     * 
     * @param id 商品类别ID
     * @return 商品类别
     */
    @Override
    public GoodsType selectGoodsTypeById(String id)
    {
        return goodsTypeMapper.selectGoodsTypeById(id);
    }

    /**
     * 查询商品类别列表
     * 
     * @param goodsType 商品类别
     * @return 商品类别
     */
    @Override
    public List<GoodsType> selectGoodsTypeList(GoodsType goodsType)
    {
        return goodsTypeMapper.selectGoodsTypeList(goodsType);
    }

    /**
     * 新增商品类别
     * 
     * @param goodsType 商品类别
     * @return 结果
     */
    @Override
    public int insertGoodsType(GoodsType goodsType)
    {
        goodsType.setId(this.idGererateFactory.nextStringId());
        return goodsTypeMapper.insertGoodsType(goodsType);
    }

    /**
     * 修改商品类别
     * 
     * @param goodsType 商品类别
     * @return 结果
     */
    @Override
    public int updateGoodsType(GoodsType goodsType)
    {
        return goodsTypeMapper.updateGoodsType(goodsType);
    }

    /**
     * 删除商品类别对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteGoodsTypeByIds(String ids)
    {
        return goodsTypeMapper.deleteGoodsTypeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商品类别信息
     * 
     * @param id 商品类别ID
     * @return 结果
     */
    @Override
    public int deleteGoodsTypeById(String id)
    {
        return goodsTypeMapper.deleteGoodsTypeById(id);
    }

    @Override
    public List<GoodsType> findAll() {
        return this.goodsTypeMapper.findAll();
    }
}
