package com.ruoyi.project.system.type.service;

import java.util.List;
import com.ruoyi.project.system.type.domain.GoodsType;

/**
 * 商品类别Service接口
 * 
 * @author LCL
 * @date 2020-06-10
 */
public interface IGoodsTypeService 
{
    /**
     * 查询商品类别
     * 
     * @param id 商品类别ID
     * @return 商品类别
     */
    public GoodsType selectGoodsTypeById(String id);

    /**
     * 查询商品类别列表
     * 
     * @param goodsType 商品类别
     * @return 商品类别集合
     */
    public List<GoodsType> selectGoodsTypeList(GoodsType goodsType);

    /**
     * 新增商品类别
     * 
     * @param goodsType 商品类别
     * @return 结果
     */
    public int insertGoodsType(GoodsType goodsType);

    /**
     * 修改商品类别
     * 
     * @param goodsType 商品类别
     * @return 结果
     */
    public int updateGoodsType(GoodsType goodsType);

    /**
     * 批量删除商品类别
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGoodsTypeByIds(String ids);

    /**
     * 删除商品类别信息
     * 
     * @param id 商品类别ID
     * @return 结果
     */
    public int deleteGoodsTypeById(String id);

	List<GoodsType> findAll();
}
