package com.ruoyi.project.system.type.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.text.Convert;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.params.domain.TypeParams;
import com.ruoyi.project.system.params.service.ITypeParamsService;
import com.ruoyi.project.system.spec.service.IGoodsSpecService;
import com.ruoyi.project.system.tspec.domain.TypeSpec;
import com.ruoyi.project.system.tspec.service.ITypeSpecService;
import com.ruoyi.project.system.tsvalue.domain.TypeSpecValue;
import com.ruoyi.project.system.tsvalue.service.ITypeSpecValueService;
import com.ruoyi.project.system.type.domain.GoodsType;
import com.ruoyi.project.system.type.service.IGoodsTypeService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 商品类别Controller
 * 
 * @author LCL
 * @date 2020-06-10
 */
@Controller
@RequestMapping("/system/type")
public class GoodsTypeController extends BaseController
{
    private String prefix = "system/type";

    @Autowired
    private IGoodsTypeService goodsTypeService;
    @Autowired
    private ITypeSpecService typeSpecService;
    @Autowired
    private ITypeSpecValueService typeSpecValueService;
    @Autowired
    private ITypeParamsService typeParamsService;
    @Autowired
    private IGoodsSpecService goodsSpecService;

    @RequiresPermissions("system:type:view")
    @GetMapping()
    public String type()
    {
        return prefix + "/type";
    }

    /**
     * 查询商品类别列表
     */
    @RequiresPermissions("system:type:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(GoodsType goodsType)
    {
        startPage();
        List<GoodsType> list = goodsTypeService.selectGoodsTypeList(goodsType);
        return getDataTable(list);
    }

    /**
     * 导出商品类别列表
     */
    @RequiresPermissions("system:type:export")
    @Log(title = "商品类别", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(GoodsType goodsType)
    {
        List<GoodsType> list = goodsTypeService.selectGoodsTypeList(goodsType);
        ExcelUtil<GoodsType> util = new ExcelUtil<GoodsType>(GoodsType.class);
        return util.exportExcel(list, "type");
    }

    /**
     * 新增商品类别
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存商品类别
     */
    @RequiresPermissions("system:type:add")
    @Log(title = "商品类别", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    @Transactional
    public AjaxResult addSave(GoodsType goodsType,
                              @RequestParam(value = "specId",required = false) String[] specId,
                              @RequestParam(value = "specValIds",required = false) String[] specValIds,
                              @RequestParam(value = "group_params",required = false) String[] group_params)
    {
        //添加分类信息
        goodsType.setCreated(DateUtils.getNowDate());
        goodsTypeService.insertGoodsType(goodsType);
        //添加分类下的规格信息
        TypeSpec typeSpec = new TypeSpec();
        typeSpec.setTypeId(goodsType.getId());
        if(specId!=null){
            for(int i=0;i<specId.length;i++){
                typeSpec.setSpecId(specId[i]);
                this.typeSpecService.insertTypeSpec(typeSpec);
                TypeSpecValue typeSpecValue = new TypeSpecValue();
                typeSpecValue.setTypeSpecId(typeSpec.getId());
                //添加规格下的 规格值信息
                if(specId.length==1){
                    for(int j=0;j<specValIds.length;j++){
                        if(!StringUtils.isBlank(specValIds[j])){
                            typeSpecValue.setSpecValueId(specValIds[j]);
                            this.typeSpecValueService.insertTypeSpecValue(typeSpecValue);
                        }
                    }
                    break;
                }else{
                    String[] valueId = specValIds[i].split(",");
                    for(int j=0;j<valueId.length;j++){
                        typeSpecValue.setSpecValueId(valueId[j]);
                        this.typeSpecValueService.insertTypeSpecValue(typeSpecValue);
                    }
                }
            }
        }
        //添加详细参数信息
        TypeParams typeParams = new TypeParams();
        typeParams.setStatus(0);
        typeParams.setTypeId(goodsType.getId());
        if(group_params!=null){
            for (String param:group_params){
                typeParams.setName(param);
                this.typeParamsService.insertTypeParams(typeParams);
            }
        }
        return AjaxResult.success();
    }

    /**
     * 修改商品类别
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        //类别信息
        GoodsType goodsType = goodsTypeService.selectGoodsTypeById(id);
        mmap.put("goodsType", goodsType);
        //类别规格信息
        TypeSpec typeSpec = new TypeSpec();
        typeSpec.setTypeId(id);
        List<TypeSpec> typeSpecs = this.typeSpecService.selectTypeSpecList(typeSpec);
        for(TypeSpec spec:typeSpecs){
            spec.setGoodsSpec(this.goodsSpecService.selectGoodsSpecById(spec.getSpecId()));
            spec.setTypeSpecValueDtoList(this.typeSpecValueService.findListByTypeSpecId(spec.getId()));
        }
        mmap.put("specList",typeSpecs);
        //类别详细参数信息
        TypeParams typeParams = new TypeParams();
        typeParams.setTypeId(id);
        mmap.put("paramsList",this.typeParamsService.selectTypeParamsList(typeParams));
        return prefix + "/edit";
    }

    /**
     * 修改保存商品类别
     */
    @RequiresPermissions("system:type:edit")
    @Log(title = "商品类别", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    @Transactional
    public AjaxResult editSave(GoodsType goodsType,
                               @RequestParam(value = "specId",required = false) String[] specId,
                               @RequestParam(value = "specValIds",required = false) String[] specValIds,
                               @RequestParam(value = "group_params",required = false) String[] group_params)
    {
        goodsTypeService.updateGoodsType(goodsType);

        //删除原本类别下存在的规格 规格值 详参
        TypeSpec typeSpec = new TypeSpec();
        typeSpec.setTypeId(goodsType.getId());
        List<TypeSpec> typeSpecList = this.typeSpecService.selectTypeSpecList(typeSpec);
        for(TypeSpec tSpec:typeSpecList){
            this.typeSpecValueService.deleteByTypeSpecId(tSpec.getId());
            this.typeSpecService.deleteTypeSpecById(tSpec.getId());
        }

        this.typeParamsService.deleteByTypeId(goodsType.getId());

        //添加分类下的规格信息
        if(specId!=null){
            for(int i=0;i<specId.length;i++){
                typeSpec.setSpecId(specId[i]);
                this.typeSpecService.insertTypeSpec(typeSpec);
                TypeSpecValue typeSpecValue = new TypeSpecValue();
                typeSpecValue.setTypeSpecId(typeSpec.getId());
                //添加规格下的 规格值信息
                if(specId.length==1){
                    for(int j=0;j<specValIds.length;j++){
                        if(!StringUtils.isBlank(specValIds[j])){
                            typeSpecValue.setSpecValueId(specValIds[j]);
                            this.typeSpecValueService.insertTypeSpecValue(typeSpecValue);
                        }
                    }
                    break;
                }else{
                    String[] valueId = specValIds[i].split(",");
                    for(int j=0;j<valueId.length;j++){
                        typeSpecValue.setSpecValueId(valueId[j]);
                        this.typeSpecValueService.insertTypeSpecValue(typeSpecValue);
                    }
                }
            }
            //添加详细参数信息
            TypeParams typeParams = new TypeParams();
            typeParams.setStatus(0);
            typeParams.setTypeId(goodsType.getId());
            if(group_params!=null){
                for (String param:group_params){
                    typeParams.setName(param);
                    this.typeParamsService.insertTypeParams(typeParams);
                }
            }
        }

        return AjaxResult.success();
    }

    /**
     * 删除商品类别
     */
    @RequiresPermissions("system:type:remove")
    @Log(title = "商品类别", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    @Transactional
    public AjaxResult remove(String ids)
    {
        //todo: 判断分类下是否存在商品

        for(String id: Convert.toStrArray(ids)){
            //删除分类下的规格 规格值 详参
            TypeSpec typeSpec = new TypeSpec();
            typeSpec.setTypeId(id);
            List<TypeSpec> typeSpecList = this.typeSpecService.selectTypeSpecList(typeSpec);
            for(TypeSpec tSpec:typeSpecList){
                this.typeSpecValueService.deleteByTypeSpecId(tSpec.getId());
                this.typeSpecService.deleteTypeSpecById(tSpec.getId());
            }

            this.typeParamsService.deleteByTypeId(id);
            //删除类别
            this.goodsTypeService.deleteGoodsTypeById(id);
        }

        return AjaxResult.success();
    }
}
