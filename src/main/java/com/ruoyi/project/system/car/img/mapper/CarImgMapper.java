package com.ruoyi.project.system.car.img.mapper;

import java.util.List;
import com.ruoyi.project.system.car.img.domain.CarImg;

/**
 * 汽车图片Mapper接口
 * 
 * @author LCL
 * @date 2020-07-29
 */
public interface CarImgMapper 
{
    /**
     * 查询汽车图片
     * 
     * @param id 汽车图片ID
     * @return 汽车图片
     */
    public CarImg selectCarImgById(String id);

    /**
     * 查询汽车图片列表
     * 
     * @param carImg 汽车图片
     * @return 汽车图片集合
     */
    public List<CarImg> selectCarImgList(CarImg carImg);

    /**
     * 新增汽车图片
     * 
     * @param carImg 汽车图片
     * @return 结果
     */
    public int insertCarImg(CarImg carImg);

    /**
     * 修改汽车图片
     * 
     * @param carImg 汽车图片
     * @return 结果
     */
    public int updateCarImg(CarImg carImg);

    /**
     * 删除汽车图片
     * 
     * @param id 汽车图片ID
     * @return 结果
     */
    public int deleteCarImgById(String id);

    /**
     * 批量删除汽车图片
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCarImgByIds(String[] ids);

	int delByCid(String cid);
}
