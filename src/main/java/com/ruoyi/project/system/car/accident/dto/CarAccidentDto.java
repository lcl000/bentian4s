package com.ruoyi.project.system.car.accident.dto;

import java.util.List;

import com.ruoyi.project.system.car.accident.domain.CarAccident;
import com.ruoyi.project.system.car.img.domain.CarImg;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/8/11 14:06
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
public class CarAccidentDto extends CarAccident {
	//图片列表
	private List<CarImg> carImgs;
	//推荐人姓名
	private String memberName;
	//推荐人电话
	private String memberMobile;

	public List<CarImg> getCarImgs() {
		return carImgs;
	}

	public void setCarImgs(List<CarImg> carImgs) {
		this.carImgs = carImgs;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberMobile() {
		return memberMobile;
	}

	public void setMemberMobile(String memberMobile) {
		this.memberMobile = memberMobile;
	}
}
