package com.ruoyi.project.system.car.accident.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.car.accident.dto.CarAccidentDto;
import com.ruoyi.project.system.car.accident.mapper.CarAccidentMapper;
import com.ruoyi.project.system.car.accident.domain.CarAccident;
import com.ruoyi.project.system.car.accident.service.ICarAccidentService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 事故车推荐Service业务层处理
 * 
 * @author LCL
 * @date 2020-08-11
 */
@Service
public class CarAccidentServiceImpl implements ICarAccidentService 
{
    @Resource
    private IdGererateFactory idGererateFactory;

    @Autowired
    private CarAccidentMapper carAccidentMapper;

    /**
     * 查询事故车推荐
     * 
     * @param id 事故车推荐ID
     * @return 事故车推荐
     */
    @Override
    public CarAccidentDto selectCarAccidentById(String id)
    {
        return carAccidentMapper.selectCarAccidentById(id);
    }

    /**
     * 查询事故车推荐列表
     * 
     * @param carAccident 事故车推荐
     * @return 事故车推荐
     */
    @Override
    public List<CarAccidentDto> selectCarAccidentList(CarAccident carAccident)
    {
        return carAccidentMapper.selectCarAccidentList(carAccident);
    }

    /**
     * 新增事故车推荐
     * 
     * @param carAccident 事故车推荐
     * @return 结果
     */
    @Override
    public int insertCarAccident(CarAccident carAccident)
    {
        carAccident.setId(this.idGererateFactory.nextStringId());
        return carAccidentMapper.insertCarAccident(carAccident);
    }

    /**
     * 修改事故车推荐
     * 
     * @param carAccident 事故车推荐
     * @return 结果
     */
    @Override
    public int updateCarAccident(CarAccident carAccident)
    {
        return carAccidentMapper.updateCarAccident(carAccident);
    }

    /**
     * 删除事故车推荐对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCarAccidentByIds(String ids)
    {
        return carAccidentMapper.deleteCarAccidentByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除事故车推荐信息
     * 
     * @param id 事故车推荐ID
     * @return 结果
     */
    @Override
    public int deleteCarAccidentById(String id)
    {
        return carAccidentMapper.deleteCarAccidentById(id);
    }
}
