package com.ruoyi.project.system.car.car.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.car.car.dto.CarDto;
import com.ruoyi.project.system.car.car.mapper.CarMapper;
import com.ruoyi.project.system.car.car.domain.Car;
import com.ruoyi.project.system.car.car.service.ICarService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 汽车Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-29
 */
@Service
public class CarServiceImpl implements ICarService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private CarMapper carMapper;

    /**
     * 查询汽车
     * 
     * @param id 汽车ID
     * @return 汽车
     */
    @Override
    public Car selectCarById(String id)
    {
        return carMapper.selectCarById(id);
    }

    /**
     * 查询汽车列表
     * 
     * @param car 汽车
     * @return 汽车
     */
    @Override
    public List<CarDto> selectCarList(Car car)
    {
        return carMapper.selectCarList(car);
    }

    /**
     * 新增汽车
     * 
     * @param car 汽车
     * @return 结果
     */
    @Override
    public int insertCar(Car car)
    {
        car.setId(this.idGererateFactory.nextStringId());
        return carMapper.insertCar(car);
    }

    /**
     * 修改汽车
     * 
     * @param car 汽车
     * @return 结果
     */
    @Override
    public int updateCar(Car car)
    {
        return carMapper.updateCar(car);
    }

    /**
     * 删除汽车对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCarByIds(String ids)
    {
        return carMapper.deleteCarByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除汽车信息
     * 
     * @param id 汽车ID
     * @return 结果
     */
    @Override
    public int deleteCarById(String id)
    {
        return carMapper.deleteCarById(id);
    }

    @Override
    public List<Car> findListByCarDto(CarDto car) {
        return this.carMapper.findListByCarDto(car);
    }

    @Override
    public CarDto findById(String id) {
        return this.carMapper.findById(id);
    }
}
