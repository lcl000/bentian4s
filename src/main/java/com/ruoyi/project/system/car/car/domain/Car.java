package com.ruoyi.project.system.car.car.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 汽车对象 car
 * 
 * @author LCL
 * @date 2020-07-29
 */
public class Car extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 品牌ID */
    @Excel(name = "品牌ID")
    private String brandId;

    /** 车型ID */
    @Excel(name = "车型ID")
    private String carType;

    /** 价格/万 */
    @Excel(name = "价格/万")
    private Double price;

    /** 主图 */
    @Excel(name = "主图")
    private String mainUrl;

    /** 活动主键ID */
    @Excel(name = "活动主键ID")
    private String activityId;

    /** 店铺ID */
    @Excel(name = "店铺ID")
    private String dealerId;

    /** 排序 */
    @Excel(name = "排序")
    private Integer sort;

    /** 状态: 0 正常 1 下架 */
    @Excel(name = "状态: 0 正常 1 下架")
    private Integer status;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setBrandId(String brandId) 
    {
        this.brandId = brandId;
    }

    public String getBrandId() 
    {
        return brandId;
    }
    public void setCarType(String carType) 
    {
        this.carType = carType;
    }

    public String getCarType() 
    {
        return carType;
    }
    public void setPrice(Double price) 
    {
        this.price = price;
    }

    public Double getPrice() 
    {
        return price;
    }
    public void setMainUrl(String mainUrl) 
    {
        this.mainUrl = mainUrl;
    }

    public String getMainUrl() 
    {
        return mainUrl;
    }
    public void setActivityId(String activityId) 
    {
        this.activityId = activityId;
    }

    public String getActivityId() 
    {
        return activityId;
    }
    public void setDealerId(String dealerId) 
    {
        this.dealerId = dealerId;
    }

    public String getDealerId() 
    {
        return dealerId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("brandId", getBrandId())
            .append("carType", getCarType())
            .append("price", getPrice())
            .append("mainUrl", getMainUrl())
            .append("activityId", getActivityId())
            .append("dealerId", getDealerId())
            .append("status", getStatus())
            .append("sort", getSort())
            .toString();
    }
}
