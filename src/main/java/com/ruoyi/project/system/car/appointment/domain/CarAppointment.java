package com.ruoyi.project.system.car.appointment.domain;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 预约试驾对象 car_appointment
 * 
 * @author LCL
 * @date 2020-07-31
 */
public class CarAppointment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 店铺主键ID */
    @Excel(name = "店铺主键ID")
    private String dealer;

    /** 客户姓名 */
    @Excel(name = "客户姓名")
    private String name;

    /** 客户电话 */
    @Excel(name = "客户电话")
    private String mobile;

    /** 预约时间 */
    @Excel(name = "预约时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date appointmentTime;

    /** 车型ID */
    @Excel(name = "车型ID")
    private String carType;

    /** 车辆ID */
    @Excel(name = "车辆ID")
    private String carId;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date created;

    /** 0 未体验 1 已体验 2 废弃 */
    @Excel(name = "0 未体验 1 已体验 2 废弃")
    private Integer status;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDealer(String dealer) 
    {
        this.dealer = dealer;
    }

    public String getDealer() 
    {
        return dealer;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setMobile(String mobile) 
    {
        this.mobile = mobile;
    }

    public String getMobile() 
    {
        return mobile;
    }
    public void setAppointmentTime(Date appointmentTime) 
    {
        this.appointmentTime = appointmentTime;
    }

    public Date getAppointmentTime() 
    {
        return appointmentTime;
    }
    public void setCarType(String carType) 
    {
        this.carType = carType;
    }

    public String getCarType() 
    {
        return carType;
    }
    public void setCarId(String carId) 
    {
        this.carId = carId;
    }

    public String getCarId() 
    {
        return carId;
    }
    public void setCreated(Date created) 
    {
        this.created = created;
    }

    public Date getCreated() 
    {
        return created;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dealer", getDealer())
            .append("name", getName())
            .append("mobile", getMobile())
            .append("appointmentTime", getAppointmentTime())
            .append("carType", getCarType())
            .append("carId", getCarId())
            .append("created", getCreated())
            .append("status", getStatus())
            .toString();
    }
}
