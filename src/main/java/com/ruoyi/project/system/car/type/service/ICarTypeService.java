package com.ruoyi.project.system.car.type.service;

import java.util.List;
import com.ruoyi.project.system.car.type.domain.CarType;

/**
 * 汽车类别Service接口
 * 
 * @author LCL
 * @date 2020-07-30
 */
public interface ICarTypeService 
{
    /**
     * 查询汽车类别
     * 
     * @param id 汽车类别ID
     * @return 汽车类别
     */
    public CarType selectCarTypeById(String id);

    /**
     * 查询汽车类别列表
     * 
     * @param carType 汽车类别
     * @return 汽车类别集合
     */
    public List<CarType> selectCarTypeList(CarType carType);

    /**
     * 新增汽车类别
     * 
     * @param carType 汽车类别
     * @return 结果
     */
    public int insertCarType(CarType carType);

    /**
     * 修改汽车类别
     * 
     * @param carType 汽车类别
     * @return 结果
     */
    public int updateCarType(CarType carType);

    /**
     * 批量删除汽车类别
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCarTypeByIds(String ids);

    /**
     * 删除汽车类别信息
     * 
     * @param id 汽车类别ID
     * @return 结果
     */
    public int deleteCarTypeById(String id);

    List<CarType> findAll();
}
