package com.ruoyi.project.system.car.appointment.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.car.appointment.domain.CarAppointment;
import com.ruoyi.project.system.car.appointment.dto.CarAppointmentDto;
import com.ruoyi.project.system.car.appointment.service.ICarAppointmentService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.dealer.service.IDealerService;

/**
 * 预约试驾Controller
 * 
 * @author LCL
 * @date 2020-07-31
 */
@Controller
@RequestMapping("/system/car/appointment")
public class CarAppointmentController extends BaseController
{
    private String prefix = "system/car/appointment";

    @Autowired
    private ICarAppointmentService carAppointmentService;
    @Autowired
    private IDealerService dealerService;

    @RequiresPermissions("system:appointment:view")
    @GetMapping()
    public String appointment(ModelMap map)
    {
        map.put("dealerList",this.dealerService.findAll(null));
        return prefix + "/appointment";
    }

    /**
     * 查询预约试驾列表
     */
    @RequiresPermissions("system:appointment:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CarAppointment carAppointment)
    {
        startPage();
        List<CarAppointmentDto> list = carAppointmentService.selectCarAppointmentList(carAppointment);
        return getDataTable(list);
    }

    /**
     * 导出预约试驾列表
     */
    @RequiresPermissions("system:appointment:export")
    @Log(title = "预约试驾", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CarAppointment carAppointment)
    {
        List<CarAppointmentDto> list = carAppointmentService.selectCarAppointmentList(carAppointment);
        ExcelUtil<CarAppointmentDto> util = new ExcelUtil<CarAppointmentDto>(CarAppointmentDto.class);
        return util.exportExcel(list, "appointment");
    }

    /**
     * 新增预约试驾
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存预约试驾
     */
    @RequiresPermissions("system:appointment:add")
    @Log(title = "预约试驾", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CarAppointment carAppointment)
    {
        return toAjax(carAppointmentService.insertCarAppointment(carAppointment));
    }

    /**
     * 修改预约试驾
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        CarAppointment carAppointment = carAppointmentService.selectCarAppointmentById(id);
        mmap.put("carAppointment", carAppointment);
        return prefix + "/edit";
    }

    /**
     * 修改保存预约试驾
     */
    @RequiresPermissions("system:appointment:edit")
    @Log(title = "预约试驾", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CarAppointment carAppointment)
    {
        return toAjax(carAppointmentService.updateCarAppointment(carAppointment));
    }

    /**
     * 删除预约试驾
     */
    @RequiresPermissions("system:appointment:remove")
    @Log(title = "预约试驾", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(carAppointmentService.deleteCarAppointmentByIds(ids));
    }




    @PostMapping("/agree/{id}")
    @ResponseBody
    public AjaxResult agree(@PathVariable String id){
        CarAppointment carAppointment = this.carAppointmentService.selectCarAppointmentById(id);
        if(carAppointment==null){
            return AjaxResult.error();
        }
        CarAppointment update = new CarAppointment();
        update.setId(id);
        update.setStatus(1);
        this.carAppointmentService.updateCarAppointment(update);

        return AjaxResult.success();
    }

    @PostMapping("/refuse/{id}")
    @ResponseBody
    public AjaxResult refuse(@PathVariable String id){
        CarAppointment carAppointment = this.carAppointmentService.selectCarAppointmentById(id);
        if(carAppointment==null){
            return AjaxResult.error();
        }
        CarAppointment update = new CarAppointment();
        update.setId(id);
        update.setStatus(2);
        this.carAppointmentService.updateCarAppointment(update);

        return AjaxResult.success();
    }


}
