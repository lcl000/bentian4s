package com.ruoyi.project.system.car.car.dto;

import java.math.BigDecimal;

import com.ruoyi.project.system.car.car.domain.Car;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/7/30 15:55
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
public class CarDto extends Car {
	//品牌名称
	private String brandName;
	//车型名称
	private String carTypeName;
	//活动名称
	private String activityName;

	//最低价
	private BigDecimal minPrice;
	//最高价
	private BigDecimal maxPrice;


	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getCarTypeName() {
		return carTypeName;
	}

	public void setCarTypeName(String carTypeName) {
		this.carTypeName = carTypeName;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	public BigDecimal getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(BigDecimal minPrice) {
		this.minPrice = minPrice;
	}

	public BigDecimal getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(BigDecimal maxPrice) {
		this.maxPrice = maxPrice;
	}
}
