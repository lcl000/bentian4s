package com.ruoyi.project.system.car.appointment.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.car.appointment.dto.CarAppointmentDto;
import com.ruoyi.project.system.car.appointment.mapper.CarAppointmentMapper;
import com.ruoyi.project.system.car.appointment.domain.CarAppointment;
import com.ruoyi.project.system.car.appointment.service.ICarAppointmentService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 预约试驾Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-31
 */
@Service
public class CarAppointmentServiceImpl implements ICarAppointmentService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private CarAppointmentMapper carAppointmentMapper;

    /**
     * 查询预约试驾
     * 
     * @param id 预约试驾ID
     * @return 预约试驾
     */
    @Override
    public CarAppointment selectCarAppointmentById(String id)
    {
        return carAppointmentMapper.selectCarAppointmentById(id);
    }

    /**
     * 查询预约试驾列表
     * 
     * @param carAppointment 预约试驾
     * @return 预约试驾
     */
    @Override
    public List<CarAppointmentDto> selectCarAppointmentList(CarAppointment carAppointment)
    {
        return carAppointmentMapper.selectCarAppointmentList(carAppointment);
    }

    /**
     * 新增预约试驾
     * 
     * @param carAppointment 预约试驾
     * @return 结果
     */
    @Override
    public int insertCarAppointment(CarAppointment carAppointment)
    {
        carAppointment.setId(this.idGererateFactory.nextStringId());
        return carAppointmentMapper.insertCarAppointment(carAppointment);
    }

    /**
     * 修改预约试驾
     * 
     * @param carAppointment 预约试驾
     * @return 结果
     */
    @Override
    public int updateCarAppointment(CarAppointment carAppointment)
    {
        return carAppointmentMapper.updateCarAppointment(carAppointment);
    }

    /**
     * 删除预约试驾对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCarAppointmentByIds(String ids)
    {
        return carAppointmentMapper.deleteCarAppointmentByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除预约试驾信息
     * 
     * @param id 预约试驾ID
     * @return 结果
     */
    @Override
    public int deleteCarAppointmentById(String id)
    {
        return carAppointmentMapper.deleteCarAppointmentById(id);
    }
}
