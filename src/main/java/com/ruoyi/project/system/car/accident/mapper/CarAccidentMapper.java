package com.ruoyi.project.system.car.accident.mapper;

import java.util.List;
import com.ruoyi.project.system.car.accident.domain.CarAccident;
import com.ruoyi.project.system.car.accident.dto.CarAccidentDto;

/**
 * 事故车推荐Mapper接口
 * 
 * @author LCL
 * @date 2020-08-11
 */
public interface CarAccidentMapper 
{
    /**
     * 查询事故车推荐
     * 
     * @param id 事故车推荐ID
     * @return 事故车推荐
     */
    public CarAccidentDto selectCarAccidentById(String id);

    /**
     * 查询事故车推荐列表
     * 
     * @param carAccident 事故车推荐
     * @return 事故车推荐集合
     */
    public List<CarAccidentDto> selectCarAccidentList(CarAccident carAccident);

    /**
     * 新增事故车推荐
     * 
     * @param carAccident 事故车推荐
     * @return 结果
     */
    public int insertCarAccident(CarAccident carAccident);

    /**
     * 修改事故车推荐
     * 
     * @param carAccident 事故车推荐
     * @return 结果
     */
    public int updateCarAccident(CarAccident carAccident);

    /**
     * 删除事故车推荐
     * 
     * @param id 事故车推荐ID
     * @return 结果
     */
    public int deleteCarAccidentById(String id);

    /**
     * 批量删除事故车推荐
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCarAccidentByIds(String[] ids);
}
