package com.ruoyi.project.system.car.type.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.brand.service.IBrandService;
import com.ruoyi.project.system.car.type.domain.CarType;
import com.ruoyi.project.system.car.type.service.ICarTypeService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 汽车类别Controller
 * 
 * @author LCL
 * @date 2020-07-30
 */
@Controller
@RequestMapping("/system/car/type")
public class CarTypeController extends BaseController
{
    private String prefix = "system/car/type";

    @Autowired
    private ICarTypeService carTypeService;

    @Autowired
    private IBrandService brandService;

    @RequiresPermissions("system:type:view")
    @GetMapping()
    public String type(ModelMap map)
    {
        map.put("brandList",this.brandService.findAll());
        return prefix + "/type";
    }

    /**
     * 查询汽车类别列表
     */
    @RequiresPermissions("system:type:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CarType carType)
    {
        startPage();
        List<CarType> list = carTypeService.selectCarTypeList(carType);
        return getDataTable(list);
    }

    /**
     * 导出汽车类别列表
     */
    @RequiresPermissions("system:type:export")
    @Log(title = "汽车类别", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CarType carType)
    {
        List<CarType> list = carTypeService.selectCarTypeList(carType);
        ExcelUtil<CarType> util = new ExcelUtil<CarType>(CarType.class);
        return util.exportExcel(list, "type");
    }

    /**
     * 新增汽车类别
     */
    @GetMapping("/add")
    public String add(ModelMap map)
    {
        map.put("brandList",this.brandService.findAll());
        return prefix + "/add";
    }

    /**
     * 新增保存汽车类别
     */
    @RequiresPermissions("system:type:add")
    @Log(title = "汽车类别", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CarType carType)
    {
        carType.setCreated(DateUtils.getNowDate());
        return toAjax(carTypeService.insertCarType(carType));
    }

    /**
     * 修改汽车类别
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        CarType carType = carTypeService.selectCarTypeById(id);
        mmap.put("carType", carType);
        mmap.put("brandList",this.brandService.findAll());
        return prefix + "/edit";
    }

    /**
     * 修改保存汽车类别
     */
    @RequiresPermissions("system:type:edit")
    @Log(title = "汽车类别", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CarType carType)
    {
        return toAjax(carTypeService.updateCarType(carType));
    }

    /**
     * 删除汽车类别
     */
    @RequiresPermissions("system:type:remove")
    @Log(title = "汽车类别", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(carTypeService.deleteCarTypeByIds(ids));
    }

    @GetMapping("/{brandId}/all")
    @ResponseBody
    public AjaxResult findAllByBrandId(@PathVariable String brandId){
        CarType carType = new CarType();
        carType.setBrandId(brandId);
        return AjaxResult.success(this.carTypeService.selectCarTypeList(carType));
    }
}
