package com.ruoyi.project.system.car.type.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.car.type.mapper.CarTypeMapper;
import com.ruoyi.project.system.car.type.domain.CarType;
import com.ruoyi.project.system.car.type.service.ICarTypeService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 汽车类别Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-30
 */
@Service
public class CarTypeServiceImpl implements ICarTypeService 
{
    @Resource
    private IdGererateFactory idGererateFactory;

    @Autowired
    private CarTypeMapper carTypeMapper;

    /**
     * 查询汽车类别
     * 
     * @param id 汽车类别ID
     * @return 汽车类别
     */
    @Override
    public CarType selectCarTypeById(String id)
    {
        return carTypeMapper.selectCarTypeById(id);
    }

    /**
     * 查询汽车类别列表
     * 
     * @param carType 汽车类别
     * @return 汽车类别
     */
    @Override
    public List<CarType> selectCarTypeList(CarType carType)
    {
        return carTypeMapper.selectCarTypeList(carType);
    }

    /**
     * 新增汽车类别
     * 
     * @param carType 汽车类别
     * @return 结果
     */
    @Override
    public int insertCarType(CarType carType)
    {
        carType.setId(this.idGererateFactory.nextStringId());
        return carTypeMapper.insertCarType(carType);
    }

    /**
     * 修改汽车类别
     * 
     * @param carType 汽车类别
     * @return 结果
     */
    @Override
    public int updateCarType(CarType carType)
    {
        return carTypeMapper.updateCarType(carType);
    }

    /**
     * 删除汽车类别对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCarTypeByIds(String ids)
    {
        return carTypeMapper.deleteCarTypeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除汽车类别信息
     * 
     * @param id 汽车类别ID
     * @return 结果
     */
    @Override
    public int deleteCarTypeById(String id)
    {
        return carTypeMapper.deleteCarTypeById(id);
    }

    @Override
    public List<CarType> findAll() {
        return this.carTypeMapper.findAll();
    }
}
