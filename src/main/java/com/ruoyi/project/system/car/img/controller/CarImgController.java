package com.ruoyi.project.system.car.img.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.car.img.domain.CarImg;
import com.ruoyi.project.system.car.img.service.ICarImgService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 汽车图片Controller
 * 
 * @author LCL
 * @date 2020-07-29
 */
@Controller
@RequestMapping("/system/img")
public class CarImgController extends BaseController
{
    private String prefix = "system/car/img";

    @Autowired
    private ICarImgService carImgService;

    @RequiresPermissions("system:img:view")
    @GetMapping()
    public String img()
    {
        return prefix + "/img";
    }

    /**
     * 查询汽车图片列表
     */
    @RequiresPermissions("system:img:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CarImg carImg)
    {
        startPage();
        List<CarImg> list = carImgService.selectCarImgList(carImg);
        return getDataTable(list);
    }

    /**
     * 导出汽车图片列表
     */
    @RequiresPermissions("system:img:export")
    @Log(title = "汽车图片", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CarImg carImg)
    {
        List<CarImg> list = carImgService.selectCarImgList(carImg);
        ExcelUtil<CarImg> util = new ExcelUtil<CarImg>(CarImg.class);
        return util.exportExcel(list, "img");
    }

    /**
     * 新增汽车图片
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存汽车图片
     */
    @RequiresPermissions("system:img:add")
    @Log(title = "汽车图片", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CarImg carImg)
    {
        return toAjax(carImgService.insertCarImg(carImg));
    }

    /**
     * 修改汽车图片
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        CarImg carImg = carImgService.selectCarImgById(id);
        mmap.put("carImg", carImg);
        return prefix + "/edit";
    }

    /**
     * 修改保存汽车图片
     */
    @RequiresPermissions("system:img:edit")
    @Log(title = "汽车图片", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CarImg carImg)
    {
        return toAjax(carImgService.updateCarImg(carImg));
    }

    /**
     * 删除汽车图片
     */
    @RequiresPermissions("system:img:remove")
    @Log(title = "汽车图片", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(carImgService.deleteCarImgByIds(ids));
    }
}
