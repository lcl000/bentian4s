package com.ruoyi.project.system.car.appointment.service;

import java.util.List;
import com.ruoyi.project.system.car.appointment.domain.CarAppointment;
import com.ruoyi.project.system.car.appointment.dto.CarAppointmentDto;

/**
 * 预约试驾Service接口
 * 
 * @author LCL
 * @date 2020-07-31
 */
public interface ICarAppointmentService 
{
    /**
     * 查询预约试驾
     * 
     * @param id 预约试驾ID
     * @return 预约试驾
     */
    public CarAppointment selectCarAppointmentById(String id);

    /**
     * 查询预约试驾列表
     * 
     * @param carAppointment 预约试驾
     * @return 预约试驾集合
     */
    public List<CarAppointmentDto> selectCarAppointmentList(CarAppointment carAppointment);

    /**
     * 新增预约试驾
     * 
     * @param carAppointment 预约试驾
     * @return 结果
     */
    public int insertCarAppointment(CarAppointment carAppointment);

    /**
     * 修改预约试驾
     * 
     * @param carAppointment 预约试驾
     * @return 结果
     */
    public int updateCarAppointment(CarAppointment carAppointment);

    /**
     * 批量删除预约试驾
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCarAppointmentByIds(String ids);

    /**
     * 删除预约试驾信息
     * 
     * @param id 预约试驾ID
     * @return 结果
     */
    public int deleteCarAppointmentById(String id);
}
