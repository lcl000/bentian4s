package com.ruoyi.project.system.car.appointment.dto;

import com.ruoyi.project.system.car.appointment.domain.CarAppointment;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/8/3 15:40
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
public class CarAppointmentDto extends CarAppointment {

	//店铺名称
	private String dealerName;

	//车辆名称
	private String carName;

	//车型
	private String carTypeName;

	public String getDealerName() {
		return dealerName;
	}

	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	public String getCarName() {
		return carName;
	}

	public void setCarName(String carName) {
		this.carName = carName;
	}

	public String getCarTypeName() {
		return carTypeName;
	}

	public void setCarTypeName(String carTypeName) {
		this.carTypeName = carTypeName;
	}
}
