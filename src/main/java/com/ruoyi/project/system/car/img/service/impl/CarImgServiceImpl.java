package com.ruoyi.project.system.car.img.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.car.img.mapper.CarImgMapper;
import com.ruoyi.project.system.car.img.domain.CarImg;
import com.ruoyi.project.system.car.img.service.ICarImgService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 汽车图片Service业务层处理
 * 
 * @author LCL
 * @date 2020-07-29
 */
@Service
public class CarImgServiceImpl implements ICarImgService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private CarImgMapper carImgMapper;

    /**
     * 查询汽车图片
     * 
     * @param id 汽车图片ID
     * @return 汽车图片
     */
    @Override
    public CarImg selectCarImgById(String id)
    {
        return carImgMapper.selectCarImgById(id);
    }

    /**
     * 查询汽车图片列表
     * 
     * @param carImg 汽车图片
     * @return 汽车图片
     */
    @Override
    public List<CarImg> selectCarImgList(CarImg carImg)
    {
        return carImgMapper.selectCarImgList(carImg);
    }

    /**
     * 新增汽车图片
     * 
     * @param carImg 汽车图片
     * @return 结果
     */
    @Override
    public int insertCarImg(CarImg carImg)
    {
        carImg.setId(this.idGererateFactory.nextStringId());
        return carImgMapper.insertCarImg(carImg);
    }

    /**
     * 修改汽车图片
     * 
     * @param carImg 汽车图片
     * @return 结果
     */
    @Override
    public int updateCarImg(CarImg carImg)
    {
        return carImgMapper.updateCarImg(carImg);
    }

    /**
     * 删除汽车图片对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCarImgByIds(String ids)
    {
        return carImgMapper.deleteCarImgByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除汽车图片信息
     * 
     * @param id 汽车图片ID
     * @return 结果
     */
    @Override
    public int deleteCarImgById(String id)
    {
        return carImgMapper.deleteCarImgById(id);
    }

    @Override
    public int delByCid(String cid) {
        return this.carImgMapper.delByCid(cid);
    }
}
