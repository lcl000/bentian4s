package com.ruoyi.project.system.car.img.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 汽车图片对象 car_img
 * 
 * @author LCL
 * @date 2020-07-29
 */
public class CarImg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 图片路径 */
    @Excel(name = "图片路径")
    private String url;

    /** 汽车主键ID */
    @Excel(name = "汽车主键ID")
    private String carId;

    /** 类别: 0 轮播图 1 外观 2 内饰 3 科技 4 配置 5 权益选择 */
    @Excel(name = "类别: 0 轮播图 1 外观 2 内饰 3 科技 4 配置 5 权益选择")
    private Integer type;

    private Integer sort;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }
    public void setCarId(String carId) 
    {
        this.carId = carId;
    }

    public String getCarId() 
    {
        return carId;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("url", getUrl())
            .append("carId", getCarId())
            .append("type", getType())
            .toString();
    }
}
