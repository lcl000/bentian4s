package com.ruoyi.project.system.car.accident.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.car.accident.domain.CarAccident;
import com.ruoyi.project.system.car.accident.dto.CarAccidentDto;
import com.ruoyi.project.system.car.accident.service.ICarAccidentService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.car.img.domain.CarImg;
import com.ruoyi.project.system.car.img.service.ICarImgService;

/**
 * 事故车推荐Controller
 * 
 * @author LCL
 * @date 2020-08-11
 */
@Controller
@RequestMapping("/system/car/accident")
public class CarAccidentController extends BaseController
{
    private String prefix = "system/car/accident";

    @Autowired
    private ICarAccidentService carAccidentService;

    @Autowired
    private ICarImgService carImgService;

    @RequiresPermissions("system/car:accident:view")
    @GetMapping()
    public String accident()
    {
        return prefix + "/accident";
    }

    /**
     * 查询事故车推荐列表
     */
    @RequiresPermissions("system/car:accident:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CarAccident carAccident)
    {
        startPage();
        List<CarAccidentDto> list = carAccidentService.selectCarAccidentList(carAccident);
        for(CarAccidentDto carAccidentDto:list){
            CarImg carImg = new CarImg();
            carImg.setCarId(carAccidentDto.getId());
            carAccidentDto.setCarImgs(this.carImgService.selectCarImgList(carImg));
        }
        return getDataTable(list);
    }

    /**
     * 导出事故车推荐列表
     */
    @RequiresPermissions("system/car:accident:export")
    @Log(title = "事故车推荐", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CarAccident carAccident)
    {
        List<CarAccidentDto> list = carAccidentService.selectCarAccidentList(carAccident);
        ExcelUtil<CarAccidentDto> util = new ExcelUtil<CarAccidentDto>(CarAccidentDto.class);
        return util.exportExcel(list, "accident");
    }

    /**
     * 新增事故车推荐
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存事故车推荐
     */
    @RequiresPermissions("system/car:accident:add")
    @Log(title = "事故车推荐", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CarAccident carAccident)
    {
        return toAjax(carAccidentService.insertCarAccident(carAccident));
    }

    /**
     * 修改事故车推荐
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        CarAccidentDto carAccident = carAccidentService.selectCarAccidentById(id);
        CarImg carImg = new CarImg();
        carImg.setCarId(id);
        carAccident.setCarImgs(this.carImgService.selectCarImgList(carImg));
        mmap.put("carAccident", carAccident);
        return prefix + "/edit";
    }

    /**
     * 修改保存事故车推荐
     */
    @RequiresPermissions("system/car:accident:edit")
    @Log(title = "事故车推荐", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CarAccident carAccident)
    {
        return toAjax(carAccidentService.updateCarAccident(carAccident));
    }

    /**
     * 删除事故车推荐
     */
    @RequiresPermissions("system/car:accident:remove")
    @Log(title = "事故车推荐", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(carAccidentService.deleteCarAccidentByIds(ids));
    }
}
