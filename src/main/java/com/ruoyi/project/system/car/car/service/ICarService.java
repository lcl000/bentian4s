package com.ruoyi.project.system.car.car.service;

import java.util.List;
import com.ruoyi.project.system.car.car.domain.Car;
import com.ruoyi.project.system.car.car.dto.CarDto;

/**
 * 汽车Service接口
 * 
 * @author LCL
 * @date 2020-07-29
 */
public interface ICarService 
{
    /**
     * 查询汽车
     * 
     * @param id 汽车ID
     * @return 汽车
     */
    public Car selectCarById(String id);

    /**
     * 查询汽车列表
     * 
     * @param car 汽车
     * @return 汽车集合
     */
    public List<CarDto> selectCarList(Car car);

    /**
     * 新增汽车
     * 
     * @param car 汽车
     * @return 结果
     */
    public int insertCar(Car car);

    /**
     * 修改汽车
     * 
     * @param car 汽车
     * @return 结果
     */
    public int updateCar(Car car);

    /**
     * 批量删除汽车
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCarByIds(String ids);

    /**
     * 删除汽车信息
     * 
     * @param id 汽车ID
     * @return 结果
     */
    public int deleteCarById(String id);

	List<Car> findListByCarDto(CarDto car);

    CarDto findById(String id);
}
