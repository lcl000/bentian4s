package com.ruoyi.project.system.car.car.controller;

import java.util.Arrays;
import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.activity.service.IActivityService;
import com.ruoyi.project.system.brand.service.IBrandService;
import com.ruoyi.project.system.car.car.domain.Car;
import com.ruoyi.project.system.car.car.dto.CarDto;
import com.ruoyi.project.system.car.car.service.ICarService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.car.img.domain.CarImg;
import com.ruoyi.project.system.car.img.service.ICarImgService;
import com.ruoyi.project.system.car.type.service.ICarTypeService;

/**
 * 汽车Controller
 * 
 * @author LCL
 * @date 2020-07-29
 */
@Controller
@RequestMapping("/system/car")
public class CarController extends BaseController
{
    private String prefix = "system/car/car";

    @Autowired
    private ICarService carService;
    @Autowired
    private IBrandService brandService;
    @Autowired
    private ICarTypeService carTypeService;
    @Autowired
    private IActivityService activityService;
    @Autowired
    private ICarImgService carImgService;

    @RequiresPermissions("system:car:view")
    @GetMapping()
    public String car(ModelMap map)
    {
        map.put("brandList",this.brandService.findAll());
        map.put("carTypeList",this.carTypeService.findAll());
        return prefix + "/car";
    }

    /**
     * 查询汽车列表
     */
    @RequiresPermissions("system:car:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Car car)
    {
        startPage();
        List<CarDto> list = carService.selectCarList(car);
        return getDataTable(list);
    }

    /**
     * 导出汽车列表
     */
    @RequiresPermissions("system:car:export")
    @Log(title = "汽车", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Car car)
    {
        List<CarDto> list = carService.selectCarList(car);
        ExcelUtil<CarDto> util = new ExcelUtil<CarDto>(CarDto.class);
        return util.exportExcel(list, "car");
    }

    /**
     * 新增汽车
     */
    @GetMapping("/add")
    public String add(ModelMap map)
    {
        map.put("brand",this.brandService.findAll());
        map.put("type",this.carTypeService.findAll());
        map.put("activity",this.activityService.findList());
        return prefix + "/add";
    }

    /**
     * 新增保存汽车
     */
    @RequiresPermissions("system:car:add")
    @Log(title = "汽车", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    @Transactional
    public AjaxResult addSave(Car car,String[] carImg0,String[] carImg1,
                              String[] carImg2,String[] carImg3,
                              String[] carImg4,String[] carImg5)
    {
        //添加汽车信息
        carService.insertCar(car);
        //添加图片信息
        addCarImg(car.getId(),Arrays.asList(carImg0,carImg1,carImg2,carImg3,carImg4,carImg5));
        return AjaxResult.success();
    }

    private void addCarImg(String id, List<String[]> imgList) {
        for (int i = 0; i <imgList.size(); i++) {
            if(imgList.get(i)!=null){
                String[] imgs= imgList.get(i);
                for (int j = 0; j <imgs.length ; j++) {
                    CarImg carImg = new CarImg();
                    carImg.setSort(j);
                    carImg.setCarId(id);
                    carImg.setUrl(imgs[j]);
                    carImg.setType(i);
                    this.carImgService.insertCarImg(carImg);
                }
            }
        }
    }

    /**
     * 修改汽车
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        Car car = carService.selectCarById(id);
        mmap.put("car", car);
        CarImg carImg = new CarImg();
        carImg.setCarId(id);
        carImg.setType(0);
        mmap.put("carImg0", this.carImgService.selectCarImgList(carImg));
        carImg.setType(1);
        mmap.put("carImg1", this.carImgService.selectCarImgList(carImg));
        carImg.setType(2);
        mmap.put("carImg2", this.carImgService.selectCarImgList(carImg));
        carImg.setType(3);
        mmap.put("carImg3", this.carImgService.selectCarImgList(carImg));
        carImg.setType(4);
        mmap.put("carImg4", this.carImgService.selectCarImgList(carImg));
        carImg.setType(5);
        mmap.put("carImg5", this.carImgService.selectCarImgList(carImg));
        mmap.put("brand",this.brandService.findAll());
        mmap.put("type",this.carTypeService.findAll());
        mmap.put("activity",this.activityService.findList());
        return prefix + "/edit";
    }

    /**
     * 修改保存汽车
     */
    @RequiresPermissions("system:car:edit")
    @Log(title = "汽车", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Car car,String[] carImg0,String[] carImg1,
                               String[] carImg2,String[] carImg3,
                               String[] carImg4,String[] carImg5)
    {
        //判断数据是否存在
        Car oldCar = this.carService.selectCarById(car.getId());
        if(oldCar==null){
            return AjaxResult.error("数据不存在");
        }
        //清除原本存在的图片数据
        this.carImgService.delByCid(car.getId());
        //更新汽车信息
        this.carService.updateCar(car);
        //添加图片信息
        addCarImg(car.getId(),Arrays.asList(carImg0,carImg1,carImg2,carImg3,carImg4,carImg5));
        return AjaxResult.success();
    }

    /**
     * 删除汽车
     */
    @RequiresPermissions("system:car:remove")
    @Log(title = "汽车", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(carService.deleteCarByIds(ids));
    }
}
