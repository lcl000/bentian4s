package com.ruoyi.project.system.share.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.share.domain.ActivityShare;
import com.ruoyi.project.system.share.service.IActivityShareService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 活动分享Controller
 * 
 * @author LCL
 * @date 2020-08-15
 */
@Controller
@RequestMapping("/system/share")
public class ActivityShareController extends BaseController
{
    private String prefix = "system/share";

    @Autowired
    private IActivityShareService activityShareService;

    @RequiresPermissions("system:share:view")
    @GetMapping()
    public String share()
    {
        return prefix + "/share";
    }

    /**
     * 查询活动分享列表
     */
    @RequiresPermissions("system:share:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ActivityShare activityShare)
    {
        startPage();
        List<ActivityShare> list = activityShareService.selectActivityShareList(activityShare);
        return getDataTable(list);
    }

    /**
     * 导出活动分享列表
     */
    @RequiresPermissions("system:share:export")
    @Log(title = "活动分享", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ActivityShare activityShare)
    {
        List<ActivityShare> list = activityShareService.selectActivityShareList(activityShare);
        ExcelUtil<ActivityShare> util = new ExcelUtil<ActivityShare>(ActivityShare.class);
        return util.exportExcel(list, "share");
    }

    /**
     * 新增活动分享
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存活动分享
     */
    @RequiresPermissions("system:share:add")
    @Log(title = "活动分享", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ActivityShare activityShare)
    {
        return toAjax(activityShareService.insertActivityShare(activityShare));
    }

    /**
     * 修改活动分享
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        ActivityShare activityShare = activityShareService.selectActivityShareById(id);
        mmap.put("activityShare", activityShare);
        return prefix + "/edit";
    }

    /**
     * 修改保存活动分享
     */
    @RequiresPermissions("system:share:edit")
    @Log(title = "活动分享", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ActivityShare activityShare)
    {
        return toAjax(activityShareService.updateActivityShare(activityShare));
    }

    /**
     * 删除活动分享
     */
    @RequiresPermissions("system:share:remove")
    @Log(title = "活动分享", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(activityShareService.deleteActivityShareByIds(ids));
    }
}
