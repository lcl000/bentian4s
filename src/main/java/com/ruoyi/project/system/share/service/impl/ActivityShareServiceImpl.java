package com.ruoyi.project.system.share.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.share.mapper.ActivityShareMapper;
import com.ruoyi.project.system.share.domain.ActivityShare;
import com.ruoyi.project.system.share.service.IActivityShareService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 活动分享Service业务层处理
 * 
 * @author LCL
 * @date 2020-08-15
 */
@Service
public class ActivityShareServiceImpl implements IActivityShareService 
{
    @Resource
    private IdGererateFactory idGererateFactory;

    @Autowired
    private ActivityShareMapper activityShareMapper;

    /**
     * 查询活动分享
     * 
     * @param id 活动分享ID
     * @return 活动分享
     */
    @Override
    public ActivityShare selectActivityShareById(String id)
    {
        return activityShareMapper.selectActivityShareById(id);
    }

    /**
     * 查询活动分享列表
     * 
     * @param activityShare 活动分享
     * @return 活动分享
     */
    @Override
    public List<ActivityShare> selectActivityShareList(ActivityShare activityShare)
    {
        return activityShareMapper.selectActivityShareList(activityShare);
    }

    /**
     * 新增活动分享
     * 
     * @param activityShare 活动分享
     * @return 结果
     */
    @Override
    public int insertActivityShare(ActivityShare activityShare)
    {
        activityShare.setId(this.idGererateFactory.nextStringId());
        return activityShareMapper.insertActivityShare(activityShare);
    }

    /**
     * 修改活动分享
     * 
     * @param activityShare 活动分享
     * @return 结果
     */
    @Override
    public int updateActivityShare(ActivityShare activityShare)
    {
        return activityShareMapper.updateActivityShare(activityShare);
    }

    /**
     * 删除活动分享对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteActivityShareByIds(String ids)
    {
        return activityShareMapper.deleteActivityShareByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除活动分享信息
     * 
     * @param id 活动分享ID
     * @return 结果
     */
    @Override
    public int deleteActivityShareById(String id)
    {
        return activityShareMapper.deleteActivityShareById(id);
    }
}
