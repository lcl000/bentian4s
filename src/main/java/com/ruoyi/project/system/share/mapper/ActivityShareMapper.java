package com.ruoyi.project.system.share.mapper;

import java.util.List;
import com.ruoyi.project.system.share.domain.ActivityShare;

/**
 * 活动分享Mapper接口
 * 
 * @author LCL
 * @date 2020-08-15
 */
public interface ActivityShareMapper 
{
    /**
     * 查询活动分享
     * 
     * @param id 活动分享ID
     * @return 活动分享
     */
    public ActivityShare selectActivityShareById(String id);

    /**
     * 查询活动分享列表
     * 
     * @param activityShare 活动分享
     * @return 活动分享集合
     */
    public List<ActivityShare> selectActivityShareList(ActivityShare activityShare);

    /**
     * 新增活动分享
     * 
     * @param activityShare 活动分享
     * @return 结果
     */
    public int insertActivityShare(ActivityShare activityShare);

    /**
     * 修改活动分享
     * 
     * @param activityShare 活动分享
     * @return 结果
     */
    public int updateActivityShare(ActivityShare activityShare);

    /**
     * 删除活动分享
     * 
     * @param id 活动分享ID
     * @return 结果
     */
    public int deleteActivityShareById(String id);

    /**
     * 批量删除活动分享
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteActivityShareByIds(String[] ids);
}
