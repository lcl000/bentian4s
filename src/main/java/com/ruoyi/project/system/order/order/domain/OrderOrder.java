package com.ruoyi.project.system.order.order.domain;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import com.ruoyi.project.system.membercar.domain.MemberCar;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单对象 order_order
 * 
 * @author LCL
 * @date 2020-06-18
 */
public class OrderOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    private MemberCar memberCar;

    public MemberCar getMemberCar() {
        return memberCar;
    }

    public void setMemberCar(MemberCar memberCar) {
        this.memberCar = memberCar;
    }

    /** 主键ID */
    private String id;

    /** 订单号 */
    @Excel(name = "订单号")
    private String orderNo;

    /** 会员ID */
    private String userId;

    /** 邮费 */
    @Excel(name = "邮费")
    private BigDecimal postFee;

    /** 订单原价 */
    @Excel(name = "订单原价")
    private BigDecimal pricePay;

    /** 订单实付金额 */
    @Excel(name = "订单实付金额")
    private BigDecimal priceRealPay;

    /** 0 待支付 1 待发货 2 待收货 3 待评论 4 已完成 */
    private Integer status;

    @Excel(name = "订单状态")
    private String statusName;

    /** 订单创建时间 */
    @Excel(name = "订单创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date created;

    /** 订单修改时间 */
    @Excel(name = "订单修改时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updateTime;

    /** 退货时间 */
    private Date returnTime;

    /** 发货时间 */
    @Excel(name = "发货时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date consignTime;

    /** 订单确认收货时间 */
    @Excel(name = "订单确认收货时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date closeTime;

    /** 支付类型: 0 支付宝 1 微信 */
    private Integer payType;

    /** 配送人名称 */
    @Excel(name = "配送人名称")
    private String shippingName;

    /** 配送人电话 */
    @Excel(name = "配送人电话")
    private String shippingMobile;

    /** 买家留言 */
    @Excel(name = "买家留言")
    private String buyerMsg;

    /** 删除标记 0 正常 1 删除 */
    private Integer delFlag;

    /** 年份 */
    private String year;

    /** 月份 */
    private String month;

    /** 年月日 */
    private String day;

    /** 收货人姓名 */
    private String consigneeName;

    /** 收货人电话 */
    private String consigneeTel;

    /** 地区 */
    private String area;

    /** 地址 */
    private String address;
    /** 支付单号 */
    private String outTradeNo;
    /** 上级订单编号 */
    private String superOrderId;
    /** 上级订单编号 */
    private String shippingCompanyCode;
    /** 核销员ID */
    private String usedMemberId;


    //查询用 开始时间
    private String btime;
    //查询用 结束时间
    private String etime;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setOrderNo(String orderNo) 
    {
        this.orderNo = orderNo;
    }

    public String getOrderNo() 
    {
        return orderNo;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setPostFee(BigDecimal postFee)
    {
        this.postFee = postFee;
    }

    public BigDecimal getPostFee()
    {
        return postFee;
    }
    public void setPricePay(BigDecimal pricePay)
    {
        this.pricePay = pricePay;
    }

    public BigDecimal getPricePay()
    {
        return pricePay;
    }
    public void setPriceRealPay(BigDecimal priceRealPay)
    {
        this.priceRealPay = priceRealPay;
    }

    public BigDecimal getPriceRealPay()
    {
        return priceRealPay;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }
    public void setCreated(Date created) 
    {
        this.created = created;
    }

    public Date getCreated() 
    {
        return created;
    }
    public void setReturnTime(Date returnTime) 
    {
        this.returnTime = returnTime;
    }

    public Date getReturnTime() 
    {
        return returnTime;
    }
    public void setConsignTime(Date consignTime) 
    {
        this.consignTime = consignTime;
    }

    public Date getConsignTime() 
    {
        return consignTime;
    }
    public void setCloseTime(Date closeTime) 
    {
        this.closeTime = closeTime;
    }

    public Date getCloseTime() 
    {
        return closeTime;
    }
    public void setPayType(Integer payType) 
    {
        this.payType = payType;
    }

    public Integer getPayType() 
    {
        return payType;
    }
    public void setShippingName(String shippingName) 
    {
        this.shippingName = shippingName;
    }

    public String getShippingName() 
    {
        return shippingName;
    }
    public void setShippingMobile(String shippingMobile)
    {
        this.shippingMobile = shippingMobile;
    }

    public String getShippingMobile()
    {
        return shippingMobile;
    }
    public void setBuyerMsg(String buyerMsg) 
    {
        this.buyerMsg = buyerMsg;
    }

    public String getBuyerMsg() 
    {
        return buyerMsg;
    }
    public void setDelFlag(Integer delFlag) 
    {
        this.delFlag = delFlag;
    }

    public Integer getDelFlag() 
    {
        return delFlag;
    }
    public void setYear(String year) 
    {
        this.year = year;
    }

    public String getYear() 
    {
        return year;
    }
    public void setMonth(String month) 
    {
        this.month = month;
    }

    public String getMonth() 
    {
        return month;
    }
    public void setDay(String day) 
    {
        this.day = day;
    }

    public String getDay() 
    {
        return day;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsigneeTel() {
        return consigneeTel;
    }

    public void setConsigneeTel(String consigneeTel) {
        this.consigneeTel = consigneeTel;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getSuperOrderId() {
        return superOrderId;
    }

    public void setSuperOrderId(String superOrderId) {
        this.superOrderId = superOrderId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getShippingCompanyCode() {
        return shippingCompanyCode;
    }

    public void setShippingCompanyCode(String shippingCompanyCode) {
        this.shippingCompanyCode = shippingCompanyCode;
    }

    public String getUsedMemberId() {
        return usedMemberId;
    }

    public void setUsedMemberId(String usedMemberId) {
        this.usedMemberId = usedMemberId;
    }

    public String getBtime() {
        return btime;
    }

    public void setBtime(String btime) {
        this.btime = btime;
    }

    public String getEtime() {
        return etime;
    }

    public void setEtime(String etime) {
        this.etime = etime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("orderNo", getOrderNo())
            .append("userId", getUserId())
            .append("postFee", getPostFee())
            .append("pricePay", getPricePay())
            .append("priceRealPay", getPriceRealPay())
            .append("status", getStatus())
            .append("created", getCreated())
            .append("updateTime", getUpdateTime())
            .append("returnTime", getReturnTime())
            .append("consignTime", getConsignTime())
            .append("closeTime", getCloseTime())
            .append("payType", getPayType())
            .append("shippingName", getShippingName())
            .append("shippingMobile", getShippingMobile())
            .append("buyerMsg", getBuyerMsg())
            .append("delFlag", getDelFlag())
            .append("year", getYear())
            .append("month", getMonth())
            .append("day", getDay())
            .append("consigneeName", getConsigneeName())
            .append("consigneeTel", getConsigneeTel())
            .append("area", getArea())
            .append("address", getAddress())
            .append("outTradeNo", getOutTradeNo())
            .append("superOrderId", getSuperOrderId())
            .toString();
    }
}
