package com.ruoyi.project.system.order.order.enums;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/23 14:52
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public enum OrderPayType {
	TO_BE_PAID(0,"待支付"),
	ALIPAY(1,"支付宝"),
	WEIXIN(2,"微信");

	private int value;
	private String name;

	OrderPayType(int value, String name) {
		this.value = value;
		this.name = name;
	}

	/**
	 * 数据范围校验
	 *
	 * @param value
	 * @return
	 */
	public static boolean contains(int value) {
		return null == getByValue(value)?Boolean.FALSE.booleanValue():Boolean.TRUE.booleanValue();
	}
	public int getValue() {
		return this.value;
	}

	public String getName(){
		return this.name;
	}

	public static OrderPayType getByValue(int value){
		OrderPayType allVaues[] = OrderPayType.values();
		OrderPayType status;
		for (int i=0,len = allVaues.length;i<len;i++){
			status = allVaues[i];
			if(status.getValue()==value){
				return status;
			}
		}
		return null;
	}
}
