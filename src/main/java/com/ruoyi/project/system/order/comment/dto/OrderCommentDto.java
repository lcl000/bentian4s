package com.ruoyi.project.system.order.comment.dto;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.ruoyi.project.system.order.comment.domain.OrderComment;
import com.ruoyi.project.system.order.commentimg.domain.CommentImg;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/20 11:29
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public class OrderCommentDto extends OrderComment {
	//商品名称
	private String goodsName;
	//产品规格
	private String productSpec;
	//回复人名称
	private String opByName;
	//买家昵称
	private String userName;
	//订单编号
	private String orderNo;
	//买家头像
	private String headpic;
	//评论图片
	private List<CommentImg> commentImgs;
	//上传图片数组
	private List<CommentImg> multipartFiles;

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getProductSpec() {
		return productSpec;
	}

	public void setProductSpec(String productSpec) {
		this.productSpec = productSpec;
	}

	public String getOpByName() {
		return opByName;
	}

	public void setOpByName(String opByName) {
		this.opByName = opByName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public List<CommentImg> getCommentImgs() {
		return commentImgs;
	}

	public void setCommentImgs(List<CommentImg> commentImgs) {
		this.commentImgs = commentImgs;
	}

	public String getHeadpic() {
		return headpic;
	}

	public void setHeadpic(String headpic) {
		this.headpic = headpic;
	}

	public List<CommentImg> getMultipartFiles() {
		return multipartFiles;
	}

	public void setMultipartFiles(List<CommentImg> multipartFiles) {
		this.multipartFiles = multipartFiles;
	}
}
