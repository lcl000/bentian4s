package com.ruoyi.project.system.order.commentimg.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.order.commentimg.domain.CommentImg;
import com.ruoyi.project.system.order.commentimg.service.ICommentImgService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 评论图片Controller
 * 
 * @author LCL
 * @date 2020-06-18
 */
@Controller
@RequestMapping("/system/commentimg")
public class CommentImgController extends BaseController
{
    private String prefix = "system/commentimg";

    @Autowired
    private ICommentImgService commentImgService;

    @RequiresPermissions("system:commentimg:view")
    @GetMapping()
    public String commentimg()
    {
        return prefix + "/commentimg";
    }

    /**
     * 查询评论图片列表
     */
    @RequiresPermissions("system:commentimg:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CommentImg commentImg)
    {
        startPage();
        List<CommentImg> list = commentImgService.selectCommentImgList(commentImg);
        return getDataTable(list);
    }

    /**
     * 导出评论图片列表
     */
    @RequiresPermissions("system:commentimg:export")
    @Log(title = "评论图片", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CommentImg commentImg)
    {
        List<CommentImg> list = commentImgService.selectCommentImgList(commentImg);
        ExcelUtil<CommentImg> util = new ExcelUtil<CommentImg>(CommentImg.class);
        return util.exportExcel(list, "commentimg");
    }

    /**
     * 新增评论图片
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存评论图片
     */
    @RequiresPermissions("system:commentimg:add")
    @Log(title = "评论图片", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CommentImg commentImg)
    {
        return toAjax(commentImgService.insertCommentImg(commentImg));
    }

    /**
     * 修改评论图片
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        CommentImg commentImg = commentImgService.selectCommentImgById(id);
        mmap.put("commentImg", commentImg);
        return prefix + "/edit";
    }

    /**
     * 修改保存评论图片
     */
    @RequiresPermissions("system:commentimg:edit")
    @Log(title = "评论图片", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CommentImg commentImg)
    {
        return toAjax(commentImgService.updateCommentImg(commentImg));
    }

    /**
     * 删除评论图片
     */
    @RequiresPermissions("system:commentimg:remove")
    @Log(title = "评论图片", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(commentImgService.deleteCommentImgByIds(ids));
    }
}
