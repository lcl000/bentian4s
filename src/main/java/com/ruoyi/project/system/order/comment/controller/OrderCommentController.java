package com.ruoyi.project.system.order.comment.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.order.comment.domain.OrderComment;
import com.ruoyi.project.system.order.comment.dto.OrderCommentDto;
import com.ruoyi.project.system.order.comment.service.IOrderCommentService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.order.commentimg.domain.CommentImg;
import com.ruoyi.project.system.order.commentimg.service.ICommentImgService;

/**
 * 商品评论Controller
 * 
 * @author LCL
 * @date 2020-06-18
 */
@Controller
@RequestMapping("/system/comment")
public class OrderCommentController extends BaseController
{
    private String prefix = "system/comment";

    @Autowired
    private IOrderCommentService orderCommentService;
    @Autowired
    private ICommentImgService commentImgService;

    @RequiresPermissions("system:comment:view")
    @GetMapping()
    public String comment()
    {
        return prefix + "/comment";
    }

    /**
     * 查询商品评论列表
     */
    @RequiresPermissions("system:comment:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(OrderCommentDto orderComment)
    {
        startPage();
        List<OrderCommentDto> list = orderCommentService.selectOrderCommentList(orderComment);
        return getDataTable(list);
    }

//    /**
//     * 导出商品评论列表
//     */
//    @RequiresPermissions("system:comment:export")
//    @Log(title = "商品评论", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    @ResponseBody
//    public AjaxResult export(OrderComment orderComment)
//    {
//        List<OrderCommentDto> list = orderCommentService.selectOrderCommentList(orderComment);
//        ExcelUtil<OrderComment> util = new ExcelUtil<OrderComment>(OrderComment.class);
//        return util.exportExcel(list, "comment");
//    }

    /**
     * 新增商品评论
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存商品评论
     */
    @RequiresPermissions("system:comment:add")
    @Log(title = "商品评论", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(OrderComment orderComment)
    {
        return toAjax(orderCommentService.insertOrderComment(orderComment));
    }

    /**
     * 修改商品评论
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        OrderComment orderComment = orderCommentService.selectOrderCommentById(id);
        mmap.put("orderComment", orderComment);
        CommentImg commentImg = new CommentImg();
        commentImg.setCommentId(id);
        mmap.put("imgList",this.commentImgService.selectCommentImgList(commentImg));
        return prefix + "/edit";
    }

    /**
     * 修改保存商品评论
     */
    @RequiresPermissions("system:comment:edit")
    @Log(title = "商品评论", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    @Transactional
    public AjaxResult editSave(OrderComment orderComment)
    {
//        orderComment.setOpCreated(DateUtils.getNowDate());
//        orderComment.setOpBy(getUserId());
//        orderCommentService.updateOrderComment(orderComment);
        return AjaxResult.success();
    }

    /**
     * 删除商品评论
     */
    @RequiresPermissions("system:comment:remove")
    @Log(title = "商品评论", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(orderCommentService.deleteOrderCommentByIds(ids));
    }
}
