package com.ruoyi.project.system.order.ordergoods.dto;

import com.ruoyi.project.system.order.ordergoods.domain.OrderGoods;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/18 16:00
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public class OrderGoodsDto extends OrderGoods {
	//商品名称
	private String goodsName;
	//商品规格图
	private String goodsProductUrl;
	//商品规格
	private String goodsProductSpec;
	//商品规格
	private Integer tag;


	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getGoodsProductUrl() {
		return goodsProductUrl;
	}

	public void setGoodsProductUrl(String goodsProductUrl) {
		this.goodsProductUrl = goodsProductUrl;
	}

	public String getGoodsProductSpec() {
		return goodsProductSpec;
	}

	public void setGoodsProductSpec(String goodsProductSpec) {
		this.goodsProductSpec = goodsProductSpec;
	}

	public Integer getTag() {
		return tag;
	}

	public void setTag(Integer tag) {
		this.tag = tag;
	}
}
