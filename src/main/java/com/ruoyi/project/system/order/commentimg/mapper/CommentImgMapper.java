package com.ruoyi.project.system.order.commentimg.mapper;

import java.util.List;
import com.ruoyi.project.system.order.commentimg.domain.CommentImg;

/**
 * 评论图片Mapper接口
 * 
 * @author LCL
 * @date 2020-06-18
 */
public interface CommentImgMapper 
{
    /**
     * 查询评论图片
     * 
     * @param id 评论图片ID
     * @return 评论图片
     */
    public CommentImg selectCommentImgById(String id);

    /**
     * 查询评论图片列表
     * 
     * @param commentImg 评论图片
     * @return 评论图片集合
     */
    public List<CommentImg> selectCommentImgList(CommentImg commentImg);

    /**
     * 新增评论图片
     * 
     * @param commentImg 评论图片
     * @return 结果
     */
    public int insertCommentImg(CommentImg commentImg);

    /**
     * 修改评论图片
     * 
     * @param commentImg 评论图片
     * @return 结果
     */
    public int updateCommentImg(CommentImg commentImg);

    /**
     * 删除评论图片
     * 
     * @param id 评论图片ID
     * @return 结果
     */
    public int deleteCommentImgById(String id);

    /**
     * 批量删除评论图片
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCommentImgByIds(String[] ids);
}
