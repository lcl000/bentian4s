package com.ruoyi.project.system.order.order.enums;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/23 10:41
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public enum OrderStatus {
	TO_BE_PAID(0,"待兑换"),
	TO_BE_DELIVERED(1,"待发货"),
	TO_BE_RECEIVED(2,"待收货"),
	TO_BE_COMMENT(3,"待评论"),
	COMPLETE(4,"已完成"),
	TO_BE_RETURN(5,"退货中"),
	RETUREN_SUCCESS(6,"退货成功"),
	TO_BE_EXCHANGE(7,"换货中"),
	CANCEL(8,"已取消");

	private int value;
	private String name;

	OrderStatus(int value, String name) {
		this.value = value;
		this.name = name;
	}

	/**
	 * 数据范围校验
	 *
	 * @param value
	 * @return
	 */
	public static boolean contains(int value) {
		return null == getByValue(value)?Boolean.FALSE.booleanValue():Boolean.TRUE.booleanValue();
	}
	public int getValue() {
		return this.value;
	}

	public String getName(){
		return this.name;
	}

	public static OrderStatus getByValue(int value){
		OrderStatus allVaues[] = OrderStatus.values();
		OrderStatus status;
		for (int i=0,len = allVaues.length;i<len;i++){
			status = allVaues[i];
			if(status.getValue()==value){
				return status;
			}
		}
		return null;
	}
}
