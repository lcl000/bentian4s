package com.ruoyi.project.system.order.commentimg.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 评论图片对象 comment_img
 * 
 * @author LCL
 * @date 2020-06-18
 */
public class CommentImg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 商品评论主键ID */
    @Excel(name = "商品评论主键ID")
    private String commentId;

    /** 图片访问路径 */
    @Excel(name = "图片访问路径")
    private String url;



    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setCommentId(String commentId) 
    {
        this.commentId = commentId;
    }

    public String getCommentId() 
    {
        return commentId;
    }
    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("commentId", getCommentId())
            .append("url", getUrl())

            .toString();
    }
}
