package com.ruoyi.project.system.order.ordergoods.service;

import java.util.List;
import com.ruoyi.project.system.order.ordergoods.domain.OrderGoods;
import com.ruoyi.project.system.order.ordergoods.dto.OrderGoodsDto;

/**
 * 订单商品Service接口
 * 
 * @author LCL
 * @date 2020-06-18
 */
public interface IOrderGoodsService 
{
    /**
     * 查询订单商品
     * 
     * @param id 订单商品ID
     * @return 订单商品
     */
    public OrderGoods selectOrderGoodsById(String id);

    /**
     * 查询订单商品列表
     * 
     * @param orderGoods 订单商品
     * @return 订单商品集合
     */
    public List<OrderGoods> selectOrderGoodsList(OrderGoods orderGoods);

    /**
     * 新增订单商品
     * 
     * @param orderGoods 订单商品
     * @return 结果
     */
    public int insertOrderGoods(OrderGoods orderGoods);

    /**
     * 修改订单商品
     * 
     * @param orderGoods 订单商品
     * @return 结果
     */
    public int updateOrderGoods(OrderGoods orderGoods);

    /**
     * 批量删除订单商品
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOrderGoodsByIds(String ids);

    /**
     * 删除订单商品信息
     * 
     * @param id 订单商品ID
     * @return 结果
     */
    public int deleteOrderGoodsById(String id);

	List<OrderGoodsDto> findListByOrderId(String id);
}
