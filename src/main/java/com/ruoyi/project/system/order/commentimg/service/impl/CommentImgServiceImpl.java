package com.ruoyi.project.system.order.commentimg.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.order.commentimg.mapper.CommentImgMapper;
import com.ruoyi.project.system.order.commentimg.domain.CommentImg;
import com.ruoyi.project.system.order.commentimg.service.ICommentImgService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 评论图片Service业务层处理
 * 
 * @author LCL
 * @date 2020-06-18
 */
@Service
public class CommentImgServiceImpl implements ICommentImgService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private CommentImgMapper commentImgMapper;

    /**
     * 查询评论图片
     * 
     * @param id 评论图片ID
     * @return 评论图片
     */
    @Override
    public CommentImg selectCommentImgById(String id)
    {
        return commentImgMapper.selectCommentImgById(id);
    }

    /**
     * 查询评论图片列表
     * 
     * @param commentImg 评论图片
     * @return 评论图片
     */
    @Override
    public List<CommentImg> selectCommentImgList(CommentImg commentImg)
    {
        return commentImgMapper.selectCommentImgList(commentImg);
    }

    /**
     * 新增评论图片
     * 
     * @param commentImg 评论图片
     * @return 结果
     */
    @Override
    public int insertCommentImg(CommentImg commentImg)
    {
        commentImg.setId(this.idGererateFactory.nextStringId());
        return commentImgMapper.insertCommentImg(commentImg);
    }

    /**
     * 修改评论图片
     * 
     * @param commentImg 评论图片
     * @return 结果
     */
    @Override
    public int updateCommentImg(CommentImg commentImg)
    {
        return commentImgMapper.updateCommentImg(commentImg);
    }

    /**
     * 删除评论图片对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCommentImgByIds(String ids)
    {
        return commentImgMapper.deleteCommentImgByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除评论图片信息
     * 
     * @param id 评论图片ID
     * @return 结果
     */
    @Override
    public int deleteCommentImgById(String id)
    {
        return commentImgMapper.deleteCommentImgById(id);
    }
}
