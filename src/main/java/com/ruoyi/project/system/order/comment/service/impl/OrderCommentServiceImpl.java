package com.ruoyi.project.system.order.comment.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.order.comment.dto.OrderCommentDto;
import com.ruoyi.project.system.order.comment.mapper.OrderCommentMapper;
import com.ruoyi.project.system.order.comment.domain.OrderComment;
import com.ruoyi.project.system.order.comment.service.IOrderCommentService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 商品评论Service业务层处理
 * 
 * @author LCL
 * @date 2020-06-18
 */
@Service
public class OrderCommentServiceImpl implements IOrderCommentService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private OrderCommentMapper orderCommentMapper;

    /**
     * 查询商品评论
     * 
     * @param id 商品评论ID
     * @return 商品评论
     */
    @Override
    public OrderComment selectOrderCommentById(String id)
    {
        return orderCommentMapper.selectOrderCommentById(id);
    }

    /**
     * 查询商品评论列表
     * 
     * @param orderComment 商品评论
     * @return 商品评论
     */
    @Override
    public List<OrderCommentDto> selectOrderCommentList(OrderCommentDto orderComment)
    {
        return orderCommentMapper.selectOrderCommentList(orderComment);
    }

    /**
     * 新增商品评论
     * 
     * @param orderComment 商品评论
     * @return 结果
     */
    @Override
    public int insertOrderComment(OrderComment orderComment)
    {
        orderComment.setId(this.idGererateFactory.nextStringId());
        return orderCommentMapper.insertOrderComment(orderComment);
    }

    /**
     * 修改商品评论
     * 
     * @param orderComment 商品评论
     * @return 结果
     */
    @Override
    public int updateOrderComment(OrderComment orderComment)
    {
        return orderCommentMapper.updateOrderComment(orderComment);
    }

    /**
     * 删除商品评论对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteOrderCommentByIds(String ids)
    {
        return orderCommentMapper.deleteOrderCommentByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商品评论信息
     * 
     * @param id 商品评论ID
     * @return 结果
     */
    @Override
    public int deleteOrderCommentById(String id)
    {
        return orderCommentMapper.deleteOrderCommentById(id);
    }

    @Override
    public List<OrderCommentDto> findListByGoodsId(String goodsId) {
        return this.orderCommentMapper.findListByGoodsId(goodsId);
    }
}
