package com.ruoyi.project.system.order.order.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.project.system.order.order.domain.OrderOrder;
import com.ruoyi.project.system.order.ordergoods.domain.OrderGoods;
import com.ruoyi.project.system.order.ordergoods.dto.OrderGoodsDto;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/18 15:40
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public class OrderOrderDto extends OrderOrder {
	//买家昵称
	@Excel(name = "买家昵称")
	private String userName;

	// 用户手机号
	private String userMobile;


	//商品集合
	private List<OrderGoodsDto> goodsList;

	//购物车
	private ArrayList<String> cartIds;


	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<OrderGoodsDto> getGoodsList() {
		return goodsList;
	}

	public void setGoodsList(List<OrderGoodsDto> goodsList) {
		this.goodsList = goodsList;
	}

	public ArrayList<String> getCartIds() {
		return cartIds;
	}

	public void setCartIds(ArrayList<String> cartIds) {
		this.cartIds = cartIds;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}
}
