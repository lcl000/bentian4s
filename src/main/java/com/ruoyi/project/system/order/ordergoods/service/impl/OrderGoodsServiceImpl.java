package com.ruoyi.project.system.order.ordergoods.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.order.ordergoods.dto.OrderGoodsDto;
import com.ruoyi.project.system.order.ordergoods.mapper.OrderGoodsMapper;
import com.ruoyi.project.system.order.ordergoods.domain.OrderGoods;
import com.ruoyi.project.system.order.ordergoods.service.IOrderGoodsService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 订单商品Service业务层处理
 * 
 * @author LCL
 * @date 2020-06-18
 */
@Service
public class OrderGoodsServiceImpl implements IOrderGoodsService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private OrderGoodsMapper orderGoodsMapper;

    /**
     * 查询订单商品
     * 
     * @param id 订单商品ID
     * @return 订单商品
     */
    @Override
    public OrderGoods selectOrderGoodsById(String id)
    {
        return orderGoodsMapper.selectOrderGoodsById(id);
    }

    /**
     * 查询订单商品列表
     * 
     * @param orderGoods 订单商品
     * @return 订单商品
     */
    @Override
    public List<OrderGoods> selectOrderGoodsList(OrderGoods orderGoods)
    {
        return orderGoodsMapper.selectOrderGoodsList(orderGoods);
    }

    /**
     * 新增订单商品
     * 
     * @param orderGoods 订单商品
     * @return 结果
     */
    @Override
    public int insertOrderGoods(OrderGoods orderGoods)
    {
        orderGoods.setId(this.idGererateFactory.nextStringId());
        return orderGoodsMapper.insertOrderGoods(orderGoods);
    }

    /**
     * 修改订单商品
     * 
     * @param orderGoods 订单商品
     * @return 结果
     */
    @Override
    public int updateOrderGoods(OrderGoods orderGoods)
    {
        return orderGoodsMapper.updateOrderGoods(orderGoods);
    }

    /**
     * 删除订单商品对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteOrderGoodsByIds(String ids)
    {
        return orderGoodsMapper.deleteOrderGoodsByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除订单商品信息
     * 
     * @param id 订单商品ID
     * @return 结果
     */
    @Override
    public int deleteOrderGoodsById(String id)
    {
        return orderGoodsMapper.deleteOrderGoodsById(id);
    }

    @Override
    public List<OrderGoodsDto> findListByOrderId(String id) {
        return this.orderGoodsMapper.findListByOrderId(id);
    }
}
