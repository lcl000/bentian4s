package com.ruoyi.project.system.order.order.service.impl;

import javax.annotation.Resource;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.order.order.dto.OrderOrderDto;
import com.ruoyi.project.system.order.order.mapper.OrderOrderMapper;
import com.ruoyi.project.system.order.order.domain.OrderOrder;
import com.ruoyi.project.system.order.order.service.IOrderOrderService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 订单Service业务层处理
 * 
 * @author LCL
 * @date 2020-06-18
 */
@Service
public class OrderOrderServiceImpl implements IOrderOrderService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private OrderOrderMapper orderOrderMapper;

    /**
     * 查询订单
     * 
     * @param id 订单ID
     * @return 订单
     */
    @Override
    public OrderOrder selectOrderOrderById(String id)
    {
        return orderOrderMapper.selectOrderOrderById(id);
    }

    /**
     * 查询订单列表
     * 
     * @param orderOrder 订单
     * @return 订单
     */
    @Override
    public List<OrderOrderDto> selectOrderOrderList(OrderOrder orderOrder)
    {
        return orderOrderMapper.selectOrderOrderList(orderOrder);
    }

    /**
     * 新增订单
     * 
     * @param orderOrder 订单
     * @return 结果
     */
    @Override
    public int insertOrderOrder(OrderOrder orderOrder)
    {
        orderOrder.setId(this.idGererateFactory.nextStringId());
        return orderOrderMapper.insertOrderOrder(orderOrder);
    }

    /**
     * 修改订单
     * 
     * @param orderOrder 订单
     * @return 结果
     */
    @Override
    public int updateOrderOrder(OrderOrder orderOrder)
    {
        return orderOrderMapper.updateOrderOrder(orderOrder);
    }

    /**
     * 删除订单对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteOrderOrderByIds(String ids)
    {
        return orderOrderMapper.deleteOrderOrderByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除订单信息
     * 
     * @param id 订单ID
     * @return 结果
     */
    @Override
    public int deleteOrderOrderById(String id)
    {
        return orderOrderMapper.deleteOrderOrderById(id);
    }

    @Override
    public OrderOrderDto findByOrderId(String id) {
        return this.orderOrderMapper.findByOrderId(id);
    }
    @Override
    public OrderOrderDto findByOrderNo(String orderNo) {
        return this.orderOrderMapper.findByOrderNo(orderNo);
    }

    @Override
    public List<OrderOrderDto> findListByStatusAndUid(Integer[] status, String uid) {
        return this.orderOrderMapper.findListByStatusAndUid(status,uid);
    }

    @Override
    public List<OrderOrderDto> findListBySuperOId(String id) {
        return this.orderOrderMapper.findListBySuperOId(id);
    }

    @Override
    public List<OrderOrderDto> findListByUsedMember(String uid) {
        return this.orderOrderMapper.findListByUsedMember(uid);
    }
}
