package com.ruoyi.project.system.order.ordergoods.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.order.ordergoods.domain.OrderGoods;
import com.ruoyi.project.system.order.ordergoods.service.IOrderGoodsService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 订单商品Controller
 * 
 * @author LCL
 * @date 2020-06-18
 */
@Controller
@RequestMapping("/system/ordergoods")
public class OrderGoodsController extends BaseController
{
    private String prefix = "system/ordergoods";

    @Autowired
    private IOrderGoodsService orderGoodsService;

    @RequiresPermissions("system:ordergoods:view")
    @GetMapping()
    public String ordergoods()
    {
        return prefix + "/ordergoods";
    }

    /**
     * 查询订单商品列表
     */
    @RequiresPermissions("system:ordergoods:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(OrderGoods orderGoods)
    {
        startPage();
        List<OrderGoods> list = orderGoodsService.selectOrderGoodsList(orderGoods);
        return getDataTable(list);
    }

    /**
     * 导出订单商品列表
     */
    @RequiresPermissions("system:ordergoods:export")
    @Log(title = "订单商品", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(OrderGoods orderGoods)
    {
        List<OrderGoods> list = orderGoodsService.selectOrderGoodsList(orderGoods);
        ExcelUtil<OrderGoods> util = new ExcelUtil<OrderGoods>(OrderGoods.class);
        return util.exportExcel(list, "ordergoods");
    }

    /**
     * 新增订单商品
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存订单商品
     */
    @RequiresPermissions("system:ordergoods:add")
    @Log(title = "订单商品", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(OrderGoods orderGoods)
    {
        return toAjax(orderGoodsService.insertOrderGoods(orderGoods));
    }

    /**
     * 修改订单商品
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        OrderGoods orderGoods = orderGoodsService.selectOrderGoodsById(id);
        mmap.put("orderGoods", orderGoods);
        return prefix + "/edit";
    }

    /**
     * 修改保存订单商品
     */
    @RequiresPermissions("system:ordergoods:edit")
    @Log(title = "订单商品", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(OrderGoods orderGoods)
    {
        return toAjax(orderGoodsService.updateOrderGoods(orderGoods));
    }

    /**
     * 删除订单商品
     */
    @RequiresPermissions("system:ordergoods:remove")
    @Log(title = "订单商品", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(orderGoodsService.deleteOrderGoodsByIds(ids));
    }
}
