package com.ruoyi.project.system.order.ordergoods.domain;

import java.math.BigDecimal;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 订单商品对象 order_goods
 * 
 * @author LCL
 * @date 2020-06-18
 */
public class OrderGoods extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 订单主键ID */
    @Excel(name = "订单主键ID")
    private String orderId;

    /** 商品主键ID */
    @Excel(name = "商品主键ID")
    private String goodsId;

    /** 商品规格ID */
    @Excel(name = "商品规格ID")
    private String productId;

    /** 数量 */
    @Excel(name = "数量")
    private Long total;

    /** 删除标记 0 正常 1 删除 */
    private Integer delFlag;

    /** 是否已评论 0 未评论 1 已评论 */
    @Excel(name = "是否已评论 0 未评论 1 已评论")
    private Integer hasComent;

    /** 商品原价 */
    @Excel(name = "商品原价")
    private BigDecimal priceO;

    /** 商品现价 */
    @Excel(name = "商品现价")
    private BigDecimal priceN;

    /** 运费 */
    @Excel(name = "运费")
    private BigDecimal postFee;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setOrderId(String orderId) 
    {
        this.orderId = orderId;
    }

    public String getOrderId() 
    {
        return orderId;
    }
    public void setGoodsId(String goodsId) 
    {
        this.goodsId = goodsId;
    }

    public String getGoodsId() 
    {
        return goodsId;
    }
    public void setProductId(String productId) 
    {
        this.productId = productId;
    }

    public String getProductId() 
    {
        return productId;
    }
    public void setTotal(Long total) 
    {
        this.total = total;
    }

    public Long getTotal() 
    {
        return total;
    }
    public void setDelFlag(Integer delFlag) 
    {
        this.delFlag = delFlag;
    }

    public Integer getDelFlag() 
    {
        return delFlag;
    }
    public void setHasComent(Integer hasComent) 
    {
        this.hasComent = hasComent;
    }

    public Integer getHasComent() 
    {
        return hasComent;
    }
    public void setPriceO(BigDecimal priceO)
    {
        this.priceO = priceO;
    }

    public BigDecimal getPriceO()
    {
        return priceO;
    }
    public void setPriceN(BigDecimal priceN)
    {
        this.priceN = priceN;
    }

    public BigDecimal getPriceN()
    {
        return priceN;
    }
    public void setPostFee(BigDecimal postFee)
    {
        this.postFee = postFee;
    }

    public BigDecimal getPostFee()
    {
        return postFee;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("orderId", getOrderId())
            .append("goodsId", getGoodsId())
            .append("productId", getProductId())
            .append("total", getTotal())
            .append("delFlag", getDelFlag())
            .append("hasComent", getHasComent())
            .append("priceO", getPriceO())
            .append("priceN", getPriceN())
            .append("postFee", getPostFee())
            .toString();
    }
}
