package com.ruoyi.project.system.order.comment.domain;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 商品评论对象 order_comment
 * 
 * @author LCL
 * @date 2020-06-18
 */
public class OrderComment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 商品主键ID */
    @Excel(name = "商品主键ID")
    private String goodsId;

    /** 商品规格ID */
    @Excel(name = "商品规格ID")
    private String productId;

    /** 用户ID */
    @Excel(name = "用户ID")
    private String userId;

    /** 订单主键ID */
    @Excel(name = "订单主键ID")
    private String orderId;

    /** 评分 */
    @Excel(name = "评分")
    private Integer grade;

    /** 评分内容 */
    @Excel(name = "评分内容")
    private String context;

    /** 评论时间 */
    @Excel(name = "评论时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date created;

    /** 追加评论内容 */
    @Excel(name = "追加评论内容", width = 30, dateFormat = "yyyy-MM-dd")
    private Date addContext;

    /** 追加评论时间 */
    @Excel(name = "追加评论时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date addContextTime;

    /** 回复内容 */
    @Excel(name = "回复内容")
    private String reply;

    /** 回复人主键ID */
    @Excel(name = "回复人主键ID")
    private Long opBy;

    /** 回复时间 */
    @Excel(name = "回复时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date opCreated;

    /** 状态: 0 正常 1 删除 */
    @Excel(name = "状态: 0 正常 1 删除")
    private Integer status;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setGoodsId(String goodsId) 
    {
        this.goodsId = goodsId;
    }

    public String getGoodsId() 
    {
        return goodsId;
    }
    public void setProductId(String productId) 
    {
        this.productId = productId;
    }

    public String getProductId() 
    {
        return productId;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setOrderId(String orderId) 
    {
        this.orderId = orderId;
    }

    public String getOrderId() 
    {
        return orderId;
    }
    public void setGrade(Integer grade) 
    {
        this.grade = grade;
    }

    public Integer getGrade() 
    {
        return grade;
    }
    public void setContext(String context) 
    {
        this.context = context;
    }

    public String getContext() 
    {
        return context;
    }
    public void setCreated(Date created) 
    {
        this.created = created;
    }

    public Date getCreated() 
    {
        return created;
    }
    public void setAddContext(Date addContext) 
    {
        this.addContext = addContext;
    }

    public Date getAddContext() 
    {
        return addContext;
    }
    public void setAddContextTime(Date addContextTime) 
    {
        this.addContextTime = addContextTime;
    }

    public Date getAddContextTime() 
    {
        return addContextTime;
    }
    public void setReply(String reply) 
    {
        this.reply = reply;
    }

    public String getReply() 
    {
        return reply;
    }
    public void setOpBy(Long opBy) 
    {
        this.opBy = opBy;
    }

    public Long getOpBy() 
    {
        return opBy;
    }
    public void setOpCreated(Date opCreated) 
    {
        this.opCreated = opCreated;
    }

    public Date getOpCreated() 
    {
        return opCreated;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodsId", getGoodsId())
            .append("productId", getProductId())
            .append("userId", getUserId())
            .append("orderId", getOrderId())
            .append("grade", getGrade())
            .append("context", getContext())
            .append("created", getCreated())
            .append("addContext", getAddContext())
            .append("addContextTime", getAddContextTime())
            .append("reply", getReply())
            .append("opBy", getOpBy())
            .append("opCreated", getOpCreated())
            .append("status", getStatus())
            .toString();
    }
}
