package com.ruoyi.project.system.order.order.mapper;

import java.util.List;
import com.ruoyi.project.system.order.order.domain.OrderOrder;
import com.ruoyi.project.system.order.order.dto.OrderOrderDto;

/**
 * 订单Mapper接口
 * 
 * @author LCL
 * @date 2020-06-18
 */
public interface OrderOrderMapper 
{
    /**
     * 查询订单
     * 
     * @param id 订单ID
     * @return 订单
     */
    public OrderOrder selectOrderOrderById(String id);

    /**
     * 查询订单列表
     * 
     * @param orderOrder 订单
     * @return 订单集合
     */
    public List<OrderOrderDto> selectOrderOrderList(OrderOrder orderOrder);

    /**
     * 新增订单
     * 
     * @param orderOrder 订单
     * @return 结果
     */
    public int insertOrderOrder(OrderOrder orderOrder);

    /**
     * 修改订单
     * 
     * @param orderOrder 订单
     * @return 结果
     */
    public int updateOrderOrder(OrderOrder orderOrder);

    /**
     * 删除订单
     * 
     * @param id 订单ID
     * @return 结果
     */
    public int deleteOrderOrderById(String id);

    /**
     * 批量删除订单
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOrderOrderByIds(String[] ids);

	OrderOrderDto findByOrderId(String id);

    OrderOrderDto findByOrderNo(String orderNo);

	List<OrderOrderDto> findListByStatusAndUid(Integer[] status, String uid);

	List<OrderOrderDto> findListBySuperOId(String id);

    List<OrderOrderDto> findListByUsedMember(String uid);
}
