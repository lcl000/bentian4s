package com.ruoyi.project.system.order.comment.service;

import java.util.List;
import com.ruoyi.project.system.order.comment.domain.OrderComment;
import com.ruoyi.project.system.order.comment.dto.OrderCommentDto;

/**
 * 商品评论Service接口
 * 
 * @author LCL
 * @date 2020-06-18
 */
public interface IOrderCommentService 
{
    /**
     * 查询商品评论
     * 
     * @param id 商品评论ID
     * @return 商品评论
     */
    public OrderComment selectOrderCommentById(String id);

    /**
     * 查询商品评论列表
     * 
     * @param orderComment 商品评论
     * @return 商品评论集合
     */
    public List<OrderCommentDto> selectOrderCommentList(OrderCommentDto orderComment);

    /**
     * 新增商品评论
     * 
     * @param orderComment 商品评论
     * @return 结果
     */
    public int insertOrderComment(OrderComment orderComment);

    /**
     * 修改商品评论
     * 
     * @param orderComment 商品评论
     * @return 结果
     */
    public int updateOrderComment(OrderComment orderComment);

    /**
     * 批量删除商品评论
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOrderCommentByIds(String ids);

    /**
     * 删除商品评论信息
     * 
     * @param id 商品评论ID
     * @return 结果
     */
    public int deleteOrderCommentById(String id);

	List<OrderCommentDto> findListByGoodsId(String goodsId);
}
