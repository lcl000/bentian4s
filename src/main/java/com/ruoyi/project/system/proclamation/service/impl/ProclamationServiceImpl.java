package com.ruoyi.project.system.proclamation.service.impl;

import javax.annotation.Resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.proclamation.mapper.ProclamationMapper;
import com.ruoyi.project.system.proclamation.domain.Proclamation;
import com.ruoyi.project.system.proclamation.service.IProclamationService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 公告Service业务层处理
 * 
 * @author LCL
 * @date 2020-06-08
 */
@Service
public class ProclamationServiceImpl implements IProclamationService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private ProclamationMapper proclamationMapper;

    /**
     * 查询公告
     * 
     * @param id 公告ID
     * @return 公告
     */
    @Override
    public Proclamation selectProclamationById(String id)
    {
        return proclamationMapper.selectProclamationById(id);
    }

    /**
     * 查询公告列表
     * 
     * @param proclamation 公告
     * @return 公告
     */
    @Override
    public List<Proclamation> selectProclamationList(Proclamation proclamation)
    {
        return proclamationMapper.selectProclamationList(proclamation);
    }

    /**
     * 新增公告
     * 
     * @param proclamation 公告
     * @return 结果
     */
    @Override
    public int insertProclamation(Proclamation proclamation)
    {
        proclamation.setId(this.idGererateFactory.nextStringId());
        return proclamationMapper.insertProclamation(proclamation);
    }

    /**
     * 修改公告
     * 
     * @param proclamation 公告
     * @return 结果
     */
    @Override
    public int updateProclamation(Proclamation proclamation)
    {
        return proclamationMapper.updateProclamation(proclamation);
    }

    /**
     * 删除公告对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteProclamationByIds(String ids)
    {
        return proclamationMapper.deleteProclamationByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除公告信息
     * 
     * @param id 公告ID
     * @return 结果
     */
    @Override
    public int deleteProclamationById(String id)
    {
        return proclamationMapper.deleteProclamationById(id);
    }
}
