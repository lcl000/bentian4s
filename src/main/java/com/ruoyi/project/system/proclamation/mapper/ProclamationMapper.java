package com.ruoyi.project.system.proclamation.mapper;

import java.util.List;
import com.ruoyi.project.system.proclamation.domain.Proclamation;

/**
 * 公告Mapper接口
 * 
 * @author LCL
 * @date 2020-06-08
 */
public interface ProclamationMapper 
{
    /**
     * 查询公告
     * 
     * @param id 公告ID
     * @return 公告
     */
    public Proclamation selectProclamationById(String id);

    /**
     * 查询公告列表
     * 
     * @param proclamation 公告
     * @return 公告集合
     */
    public List<Proclamation> selectProclamationList(Proclamation proclamation);

    /**
     * 新增公告
     * 
     * @param proclamation 公告
     * @return 结果
     */
    public int insertProclamation(Proclamation proclamation);

    /**
     * 修改公告
     * 
     * @param proclamation 公告
     * @return 结果
     */
    public int updateProclamation(Proclamation proclamation);

    /**
     * 删除公告
     * 
     * @param id 公告ID
     * @return 结果
     */
    public int deleteProclamationById(String id);

    /**
     * 批量删除公告
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteProclamationByIds(String[] ids);
}
