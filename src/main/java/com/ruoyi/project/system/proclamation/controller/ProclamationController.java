package com.ruoyi.project.system.proclamation.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.proclamation.domain.Proclamation;
import com.ruoyi.project.system.proclamation.service.IProclamationService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 公告Controller
 * 
 * @author LCL
 * @date 2020-06-08
 */
@Controller
@RequestMapping("/system/proclamation")
public class ProclamationController extends BaseController
{
    private String prefix = "system/proclamation";

    @Autowired
    private IProclamationService proclamationService;

    @RequiresPermissions("system:proclamation:view")
    @GetMapping()
    public String proclamation()
    {
        return prefix + "/proclamation";
    }

    /**
     * 查询公告列表
     */
    @RequiresPermissions("system:proclamation:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Proclamation proclamation)
    {
        startPage();
        List<Proclamation> list = proclamationService.selectProclamationList(proclamation);
        return getDataTable(list);
    }

    /**
     * 导出公告列表
     */
    @RequiresPermissions("system:proclamation:export")
    @Log(title = "公告", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Proclamation proclamation)
    {
        List<Proclamation> list = proclamationService.selectProclamationList(proclamation);
        ExcelUtil<Proclamation> util = new ExcelUtil<Proclamation>(Proclamation.class);
        return util.exportExcel(list, "proclamation");
    }

    /**
     * 新增公告
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存公告
     */
    @RequiresPermissions("system:proclamation:add")
    @Log(title = "公告", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Proclamation proclamation)
    {
        proclamation.setCreated(DateUtils.getNowDate());
        return toAjax(proclamationService.insertProclamation(proclamation));
    }

    /**
     * 修改公告
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        Proclamation proclamation = proclamationService.selectProclamationById(id);
        mmap.put("proclamation", proclamation);
        return prefix + "/edit";
    }

    /**
     * 修改保存公告
     */
    @RequiresPermissions("system:proclamation:edit")
    @Log(title = "公告", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Proclamation proclamation)
    {
        return toAjax(proclamationService.updateProclamation(proclamation));
    }

    /**
     * 删除公告
     */
    @RequiresPermissions("system:proclamation:remove")
    @Log(title = "公告", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(proclamationService.deleteProclamationByIds(ids));
    }
}
