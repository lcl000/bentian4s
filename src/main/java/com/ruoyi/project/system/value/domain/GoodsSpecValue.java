package com.ruoyi.project.system.value.domain;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 商品规格值对象 goods_spec_value
 * 
 * @author LCL
 * @date 2020-06-09
 */
public class GoodsSpecValue extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 商品规格表主键ID */
    private String specId;

    /** 规格值 */
    @Excel(name = "规格值")
    private String value;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date created;

    /** 状态: 0 正常 1 删除 */
    private Integer status;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setSpecId(String specId) 
    {
        this.specId = specId;
    }

    public String getSpecId() 
    {
        return specId;
    }
    public void setValue(String value) 
    {
        this.value = value;
    }

    public String getValue() 
    {
        return value;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setCreated(Date created) 
    {
        this.created = created;
    }

    public Date getCreated() 
    {
        return created;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("specId", getSpecId())
            .append("value", getValue())
            .append("sort", getSort())
            .append("created", getCreated())
            .append("updateTime", getUpdateTime())
            .append("status", getStatus())
            .toString();
    }
}
