package com.ruoyi.project.system.value.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.value.domain.GoodsSpecValue;
import com.ruoyi.project.system.value.service.IGoodsSpecValueService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 商品规格值Controller
 * 
 * @author LCL
 * @date 2020-06-09
 */
@Controller
@RequestMapping("/system/value")
public class GoodsSpecValueController extends BaseController
{
    private String prefix = "system/value";

    @Autowired
    private IGoodsSpecValueService goodsSpecValueService;

    @RequiresPermissions("system:value:view")
    @GetMapping()
    public String value()
    {
        return prefix + "/value";
    }

    /**
     * 查询商品规格值列表
     */
    @RequiresPermissions("system:value:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(GoodsSpecValue goodsSpecValue)
    {
        startPage();
        List<GoodsSpecValue> list = goodsSpecValueService.selectGoodsSpecValueList(goodsSpecValue);
        return getDataTable(list);
    }

    /**
     * 导出商品规格值列表
     */
    @RequiresPermissions("system:value:export")
    @Log(title = "商品规格值", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(GoodsSpecValue goodsSpecValue)
    {
        List<GoodsSpecValue> list = goodsSpecValueService.selectGoodsSpecValueList(goodsSpecValue);
        ExcelUtil<GoodsSpecValue> util = new ExcelUtil<GoodsSpecValue>(GoodsSpecValue.class);
        return util.exportExcel(list, "value");
    }

    /**
     * 新增商品规格值
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存商品规格值
     */
    @RequiresPermissions("system:value:add")
    @Log(title = "商品规格值", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(GoodsSpecValue goodsSpecValue)
    {
        return toAjax(goodsSpecValueService.insertGoodsSpecValue(goodsSpecValue));
    }

    /**
     * 修改商品规格值
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        GoodsSpecValue goodsSpecValue = goodsSpecValueService.selectGoodsSpecValueById(id);
        mmap.put("goodsSpecValue", goodsSpecValue);
        return prefix + "/edit";
    }

    /**
     * 修改保存商品规格值
     */
    @RequiresPermissions("system:value:edit")
    @Log(title = "商品规格值", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(GoodsSpecValue goodsSpecValue)
    {
        return toAjax(goodsSpecValueService.updateGoodsSpecValue(goodsSpecValue));
    }

    /**
     * 删除商品规格值
     */
    @RequiresPermissions("system:value:remove")
    @Log(title = "商品规格值", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(goodsSpecValueService.deleteGoodsSpecValueByIds(ids));
    }
}
