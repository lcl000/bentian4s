package com.ruoyi.project.system.value.mapper;

import java.util.HashMap;
import java.util.List;
import com.ruoyi.project.system.value.domain.GoodsSpecValue;

/**
 * 商品规格值Mapper接口
 * 
 * @author LCL
 * @date 2020-06-09
 */
public interface GoodsSpecValueMapper 
{
    /**
     * 查询商品规格值
     * 
     * @param id 商品规格值ID
     * @return 商品规格值
     */
    public GoodsSpecValue selectGoodsSpecValueById(String id);

    /**
     * 查询商品规格值列表
     * 
     * @param goodsSpecValue 商品规格值
     * @return 商品规格值集合
     */
    public List<GoodsSpecValue> selectGoodsSpecValueList(GoodsSpecValue goodsSpecValue);

    /**
     * 新增商品规格值
     * 
     * @param goodsSpecValue 商品规格值
     * @return 结果
     */
    public int insertGoodsSpecValue(GoodsSpecValue goodsSpecValue);

    /**
     * 修改商品规格值
     * 
     * @param goodsSpecValue 商品规格值
     * @return 结果
     */
    public int updateGoodsSpecValue(GoodsSpecValue goodsSpecValue);

    /**
     * 删除商品规格值
     * 
     * @param id 商品规格值ID
     * @return 结果
     */
    public int deleteGoodsSpecValueById(String id);

    /**
     * 批量删除商品规格值
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGoodsSpecValueByIds(String[] ids);

	int updateBySpecId(HashMap<String, Object> map);

    int deleteBySpecIds(String[] ids);

    List<GoodsSpecValue> findAllBySpecId(String id);
}
