package com.ruoyi.project.system.value.service.impl;

import javax.annotation.Resource;

import java.util.HashMap;
import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.value.mapper.GoodsSpecValueMapper;
import com.ruoyi.project.system.value.domain.GoodsSpecValue;
import com.ruoyi.project.system.value.service.IGoodsSpecValueService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 商品规格值Service业务层处理
 * 
 * @author LCL
 * @date 2020-06-09
 */
@Service
public class GoodsSpecValueServiceImpl implements IGoodsSpecValueService 
{
    @Resource
    private IdGererateFactory idGererateFactory;
    @Autowired
    private GoodsSpecValueMapper goodsSpecValueMapper;

    /**
     * 查询商品规格值
     * 
     * @param id 商品规格值ID
     * @return 商品规格值
     */
    @Override
    public GoodsSpecValue selectGoodsSpecValueById(String id)
    {
        return goodsSpecValueMapper.selectGoodsSpecValueById(id);
    }

    /**
     * 查询商品规格值列表
     * 
     * @param goodsSpecValue 商品规格值
     * @return 商品规格值
     */
    @Override
    public List<GoodsSpecValue> selectGoodsSpecValueList(GoodsSpecValue goodsSpecValue)
    {
        return goodsSpecValueMapper.selectGoodsSpecValueList(goodsSpecValue);
    }

    /**
     * 新增商品规格值
     * 
     * @param goodsSpecValue 商品规格值
     * @return 结果
     */
    @Override
    public int insertGoodsSpecValue(GoodsSpecValue goodsSpecValue)
    {
        goodsSpecValue.setId(this.idGererateFactory.nextStringId());
        return goodsSpecValueMapper.insertGoodsSpecValue(goodsSpecValue);
    }

    /**
     * 修改商品规格值
     * 
     * @param goodsSpecValue 商品规格值
     * @return 结果
     */
    @Override
    public int updateGoodsSpecValue(GoodsSpecValue goodsSpecValue)
    {
        goodsSpecValue.setUpdateTime(DateUtils.getNowDate());
        return goodsSpecValueMapper.updateGoodsSpecValue(goodsSpecValue);
    }

    /**
     * 删除商品规格值对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteGoodsSpecValueByIds(String ids)
    {
        return goodsSpecValueMapper.deleteGoodsSpecValueByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商品规格值信息
     * 
     * @param id 商品规格值ID
     * @return 结果
     */
    @Override
    public int deleteGoodsSpecValueById(String id)
    {
        return goodsSpecValueMapper.deleteGoodsSpecValueById(id);
    }

    @Override
    public int updateBySpecId(HashMap<String, Object> map) {
        return this.goodsSpecValueMapper.updateBySpecId(map);
    }

    @Override
    public int deleteBySpecIds(String ids) {
        return this.goodsSpecValueMapper.deleteBySpecIds(Convert.toStrArray(ids));
    }

    @Override
    public List<GoodsSpecValue> findAllBySpecId(String id) {
        return this.goodsSpecValueMapper.findAllBySpecId(id);
    }
}
