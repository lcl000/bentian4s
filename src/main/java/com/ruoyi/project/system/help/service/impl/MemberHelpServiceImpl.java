package com.ruoyi.project.system.help.service.impl;

import javax.annotation.Resource;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;
import com.ruoyi.project.system.help.mapper.MemberHelpMapper;
import com.ruoyi.project.system.help.domain.MemberHelp;
import com.ruoyi.project.system.help.service.IMemberHelpService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 用户求救Service业务层处理
 * 
 * @author LCL
 * @date 2020-08-19
 */
@Service
public class MemberHelpServiceImpl implements IMemberHelpService 
{
    @Resource
    private IdGererateFactory idGererateFactory;

    @Autowired
    private MemberHelpMapper memberHelpMapper;

    /**
     * 查询用户求救
     * 
     * @param id 用户求救ID
     * @return 用户求救
     */
    @Override
    public MemberHelp selectMemberHelpById(String id)
    {
        return memberHelpMapper.selectMemberHelpById(id);
    }

    /**
     * 查询用户求救列表
     * 
     * @param memberHelp 用户求救
     * @return 用户求救
     */
    @Override
    public List<MemberHelp> selectMemberHelpList(MemberHelp memberHelp)
    {
        return memberHelpMapper.selectMemberHelpList(memberHelp);
    }

    /**
     * 新增用户求救
     * 
     * @param memberHelp 用户求救
     * @return 结果
     */
    @Override
    public int insertMemberHelp(MemberHelp memberHelp)
    {
        memberHelp.setId(this.idGererateFactory.nextStringId());
        return memberHelpMapper.insertMemberHelp(memberHelp);
    }

    /**
     * 修改用户求救
     * 
     * @param memberHelp 用户求救
     * @return 结果
     */
    @Override
    public int updateMemberHelp(MemberHelp memberHelp)
    {
        memberHelp.setUpdateTime(DateUtils.getNowDate());
        return memberHelpMapper.updateMemberHelp(memberHelp);
    }

    /**
     * 删除用户求救对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteMemberHelpByIds(String ids)
    {
        return memberHelpMapper.deleteMemberHelpByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户求救信息
     * 
     * @param id 用户求救ID
     * @return 结果
     */
    @Override
    public int deleteMemberHelpById(String id)
    {
        return memberHelpMapper.deleteMemberHelpById(id);
    }
}
