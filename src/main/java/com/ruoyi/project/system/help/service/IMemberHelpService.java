package com.ruoyi.project.system.help.service;

import java.util.List;
import com.ruoyi.project.system.help.domain.MemberHelp;

/**
 * 用户求救Service接口
 * 
 * @author LCL
 * @date 2020-08-19
 */
public interface IMemberHelpService 
{
    /**
     * 查询用户求救
     * 
     * @param id 用户求救ID
     * @return 用户求救
     */
    public MemberHelp selectMemberHelpById(String id);

    /**
     * 查询用户求救列表
     * 
     * @param memberHelp 用户求救
     * @return 用户求救集合
     */
    public List<MemberHelp> selectMemberHelpList(MemberHelp memberHelp);

    /**
     * 新增用户求救
     * 
     * @param memberHelp 用户求救
     * @return 结果
     */
    public int insertMemberHelp(MemberHelp memberHelp);

    /**
     * 修改用户求救
     * 
     * @param memberHelp 用户求救
     * @return 结果
     */
    public int updateMemberHelp(MemberHelp memberHelp);

    /**
     * 批量删除用户求救
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMemberHelpByIds(String ids);

    /**
     * 删除用户求救信息
     * 
     * @param id 用户求救ID
     * @return 结果
     */
    public int deleteMemberHelpById(String id);
}
