package com.ruoyi.project.system.help.controller;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.car.img.domain.CarImg;
import com.ruoyi.project.system.car.img.service.ICarImgService;
import com.ruoyi.project.system.help.domain.MemberHelp;
import com.ruoyi.project.system.help.service.IMemberHelpService;
import com.ruoyi.project.system.membercar.service.IMemberCarService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户求救Controller
 * 
 * @author LCL
 * @date 2020-08-19
 */
@Controller
@RequestMapping("/system/help")
public class MemberHelpController extends BaseController
{
    private String prefix = "system/help";

    @Autowired
    private IMemberCarService memberCarService;

    @Autowired
    private IMemberHelpService memberHelpService;

    @Autowired
    private ICarImgService carImgService;

    @RequiresPermissions("system:help:view")
    @GetMapping()
    public String help()
    {
        return prefix + "/help";
    }

    /**
     * 查询用户求救列表
     */
    @RequiresPermissions("system:help:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MemberHelp memberHelp)
    {
        startPage();
        List<MemberHelp> list = memberHelpService.selectMemberHelpList(memberHelp);
        for(MemberHelp mem :list){
            mem.setMemberCar(this.memberCarService.findByMobile(mem.getMobile()));
        }
        return getDataTable(list);
    }

    /**
     * 导出用户求救列表
     */
    @RequiresPermissions("system:help:export")
    @Log(title = "用户求救", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MemberHelp memberHelp)
    {
        List<MemberHelp> list = memberHelpService.selectMemberHelpList(memberHelp);
        ExcelUtil<MemberHelp> util = new ExcelUtil<MemberHelp>(MemberHelp.class);
        return util.exportExcel(list, "help");
    }

    /**
     * 新增用户求救
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存用户求救
     */
    @RequiresPermissions("system:help:add")
    @Log(title = "用户求救", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MemberHelp memberHelp)
    {
        return toAjax(memberHelpService.insertMemberHelp(memberHelp));
    }

    /**
     * 修改用户求救
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        MemberHelp memberHelp = memberHelpService.selectMemberHelpById(id);
        CarImg carImg = new CarImg();
        carImg.setCarId(id);
        memberHelp.setCarImgs(this.carImgService.selectCarImgList(carImg));
        mmap.put("memberHelp", memberHelp);
        return prefix + "/edit";
    }

    /**
     * 修改保存用户求救
     */
    @RequiresPermissions("system:help:edit")
    @Log(title = "用户求救", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MemberHelp memberHelp)
    {
        if(memberHelp.getStatus()!=0){
            memberHelp.setUpdateTime(DateUtils.getNowDate());
        }
        return toAjax(memberHelpService.updateMemberHelp(memberHelp));
    }

    /**
     * 删除用户求救
     */
    @RequiresPermissions("system:help:remove")
    @Log(title = "用户求救", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(memberHelpService.deleteMemberHelpByIds(ids));
    }
}
