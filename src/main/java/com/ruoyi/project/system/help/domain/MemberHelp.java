package com.ruoyi.project.system.help.domain;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import com.ruoyi.project.system.car.img.domain.CarImg;
import com.ruoyi.project.system.membercar.domain.MemberCar;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;
import java.util.List;

/**
 * 用户求救对象 member_help
 * 
 * @author LCL
 * @date 2020-08-19
 */
public class MemberHelp extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private MemberCar memberCar;

    public MemberCar getMemberCar() {
        return memberCar;
    }

    public void setMemberCar(MemberCar memberCar) {
        this.memberCar = memberCar;
    }

    /** 主键ID */
    private String id;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String name;

    /** 电话 */
    @Excel(name = "电话")
    private String mobile;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date created;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;
    //图片列表
    private List<CarImg> carImgs;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setMobile(String mobile) 
    {
        this.mobile = mobile;
    }

    public String getMobile() 
    {
        return mobile;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setCreated(Date created) 
    {
        this.created = created;
    }

    public Date getCreated() 
    {
        return created;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    public List<CarImg> getCarImgs() {
        return carImgs;
    }

    public void setCarImgs(List<CarImg> carImgs) {
        this.carImgs = carImgs;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("mobile", getMobile())
            .append("address", getAddress())
            .append("created", getCreated())
            .append("status", getStatus())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
