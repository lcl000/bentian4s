package com.ruoyi.project.mobile.car;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.R;
import com.ruoyi.common.utils.RFactory;
import com.ruoyi.common.utils.mobile.weixin.template.AddCarServerMsg;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.project.AppBeanInjector;
import com.ruoyi.project.system.brand.service.IBrandService;
import com.ruoyi.project.system.car.appointment.domain.CarAppointment;
import com.ruoyi.project.system.car.appointment.service.ICarAppointmentService;
import com.ruoyi.project.system.car.car.domain.Car;
import com.ruoyi.project.system.car.car.dto.CarDto;
import com.ruoyi.project.system.car.car.service.ICarService;
import com.ruoyi.project.system.car.img.domain.CarImg;
import com.ruoyi.project.system.car.img.service.ICarImgService;
import com.ruoyi.project.system.car.type.domain.CarType;
import com.ruoyi.project.system.car.type.service.ICarTypeService;
import com.ruoyi.project.system.dealer.domain.Dealer;
import com.ruoyi.project.system.dealer.service.IDealerService;
import com.ruoyi.project.system.hondaconfig.service.IHondaConfigService;
import com.ruoyi.project.system.member.service.IMemberService;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/7/31 9:22
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
@RestController
@RequestMapping("/api/car")
public class ApiCarController extends BaseController {


	@Autowired
	private ICarService carService;
	@Autowired
	private ICarImgService carImgService;
	@Autowired
	private IDealerService dealerService;
	@Autowired
	private ICarAppointmentService carAppointmentService;
	@Autowired
	private IBrandService brandService;
	@Autowired
	private ICarTypeService carTypeService;
	@Autowired
	private IHondaConfigService hondaConfigService;

	@Autowired
	private IMemberService memberService;

	/**
	 * 查询车辆列表
	 * @param uid
	 * @param brandId
	 * @param carType
	 * @param minPrice
	 * @param maxPrice
	 * @param name
	 * @return
	 */
	@GetMapping
	public R carList(@RequestParam String uid,@RequestParam(required = false) String brandId,
					  @RequestParam(required = false) String carType,@RequestParam(required = false) BigDecimal minPrice,
					  @RequestParam(required = false) BigDecimal maxPrice,@RequestParam(required = false) String name){
		startPage();
		CarDto car = new CarDto();
		car.setBrandId(brandId);
		car.setStatus(0);
		car.setCarType(carType);
		car.setName(name);
		car.setMaxPrice(maxPrice);
		car.setMinPrice(minPrice);
		return RFactory.generateR(getDataTable(this.carService.findListByCarDto(car)));
	}

	/**
	 * 查询车辆详情
	 * @param uid
	 * @param id
	 * @return
	 */
	@GetMapping("/info")
	public R carInfo(@RequestParam String uid,@RequestParam String id){
		CarDto carDto= this.carService.findById(id);
		HashMap<String, Object> map = new HashMap<>();
		map.put("car",carDto);

		CarImg carImg = new CarImg();
		carImg.setCarId(id);
		carImg.setType(0);
		map.put("carImg0", this.carImgService.selectCarImgList(carImg));
		carImg.setType(1);
		map.put("carImg1", this.carImgService.selectCarImgList(carImg));
		carImg.setType(2);
		map.put("carImg2", this.carImgService.selectCarImgList(carImg));
		carImg.setType(3);
		map.put("carImg3", this.carImgService.selectCarImgList(carImg));
		carImg.setType(4);
		map.put("carImg4", this.carImgService.selectCarImgList(carImg));
		carImg.setType(5);
		map.put("carImg5", this.carImgService.selectCarImgList(carImg));
		map.put("mobile",this.hondaConfigService.findAll().get(0).getCustomerMobile());
		return RFactory.generateR(map);
	}

	/**
	 * 预约试驾
	 * @param uid
	 * @param id
	 * @param lng
	 * @param lat
	 * @return
	 */
	@GetMapping("/appointment")
	public R carAppointment(@RequestParam String uid,@RequestParam(required = false) String id,
							@RequestParam Double lng,@RequestParam Double lat){
		HashMap<String, Object> map = new HashMap<>();
		if(id!=null){
			map.put("carTypeName",this.carService.findById(id).getCarTypeName());
		}
		if(lng!=null){
			map.put("dealer",this.dealerService.findMinDistance(lng,lat));
		}else{
			map.put("dealer",this.dealerService.selectDealerList(new Dealer()).get(0));
		}
		return RFactory.generateR(map);
	}

	/**
	 * 提交预约
	 * @param carAppointment
	 * @return
	 */
	@PostMapping("/appointment")
	public R carAppointmentSave(@RequestBody CarAppointment carAppointment){
		carAppointment.setStatus(0);
		carAppointment.setCreated(DateUtils.getNowDate());
		this.carAppointmentService.insertCarAppointment(carAppointment);


		//消息接收人的数组
		List<String> receiveList = new ArrayList<String>();

		Map<String,Map<String,List<String>>> dealerList = AppBeanInjector.receive.getDealer();
		Map<String,List<String>> all = dealerList.get("all");
		//无论门店 无论服务 全部推送
		List<String> dealerAll = all.get("all");
		if(dealerAll!=null){
			receiveList.addAll(dealerAll);
		}


		Map<String, List<String>> byDId = dealerList.get(carAppointment.getDealer());
		//相关门店 无论服务 全部推送
		List<String> byDidAll = byDId.get("all");
		if(byDidAll!=null){
			receiveList.addAll(byDidAll);
		}
		//相关门店 相关服务 全部推送
		List<String> byDidByType = byDId.get("shijia");
		if(byDidByType!=null){
			receiveList.addAll(byDidByType);
		}
		//通知相关人员
		for(int i=0;i<receiveList.size();i++){
			AddCarServerMsg addCarServerMsg = new AddCarServerMsg();
			addCarServerMsg.setToOpenId(receiveList.get(i));
			HashMap<String, String> data = new HashMap<>();
			data.put("name",carAppointment.getName());
			data.put("created",DateUtils.parseDateToStr("yyyy-MM-dd",carAppointment.getAppointmentTime()));
			data.put("mobile",carAppointment.getMobile());
			data.put("typeName","预约试驾");
			addCarServerMsg.setContentMsg(data);
			AppBeanInjector.weixinTemplateMsgService.pushMsg(addCarServerMsg);
		}



		return R.ok();
	}


	/**
	 * 查询品牌列表
	 * @param uid
	 * @return
	 */
	@GetMapping("/brands")
	private R findBrandsList(@RequestParam String uid){
		return RFactory.generateR(this.brandService.findAll());
	}

	/**
	 * 查询车型列表
	 * @param uid
	 * @return
	 */
	@GetMapping("/types")
	private R findCarTypeList(@RequestParam String uid,@RequestParam String id){
		CarType carType = new CarType();
		carType.setBrandId(id);
		return RFactory.generateR(this.carTypeService.selectCarTypeList(carType));
	}

}
