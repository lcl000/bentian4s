package com.ruoyi.project.mobile.used;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.R;
import com.ruoyi.common.utils.RFactory;
import com.ruoyi.common.utils.mobile.weixin.template.AddCarServerMsg;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.project.AppBeanInjector;
import com.ruoyi.project.system.car.img.domain.CarImg;
import com.ruoyi.project.system.car.img.service.ICarImgService;
import com.ruoyi.project.system.dealer.service.IDealerService;
import com.ruoyi.project.system.used.apply.domain.CarUsedApply;
import com.ruoyi.project.system.used.apply.service.ICarUsedApplyService;
import com.ruoyi.project.system.used.appointment.domain.CarUsedAppointment;
import com.ruoyi.project.system.used.appointment.service.ICarUsedAppointmentService;
import com.ruoyi.project.system.used.estimate.domain.CarUsedEstimate;
import com.ruoyi.project.system.used.estimate.service.ICarUsedEstimateService;
import com.ruoyi.project.system.used.used.domain.CarUsed;
import com.ruoyi.project.system.used.used.dto.UsedDto;
import com.ruoyi.project.system.used.used.service.ICarUsedService;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/8/1 10:02
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
@RestController
@RequestMapping("/api/used")
public class ApiCarUsedController extends BaseController{


	@Autowired
	private ICarUsedService carUsedService;


	@Autowired
	private ICarImgService carImgService;

	@Autowired
	private ICarUsedApplyService carUsedApplyService;

	@Autowired
	private ICarUsedEstimateService carUsedEstimateService;

	@Autowired
	private ICarUsedAppointmentService carUsedAppointmentService;

	@Autowired
	private IDealerService dealerService;

	/**
	 * 查询二手车列表
	 * @param uid
	 * @param brandId
	 * @param carType
	 * @param minPrice
	 * @param maxPrice
	 * @param name
	 * @return
	 */
	@GetMapping
	private R carUsedList(@RequestParam String uid,@RequestParam(required = false) String brandId,
						  @RequestParam(required = false) String carType,@RequestParam(required = false) BigDecimal minPrice,
						  @RequestParam(required = false) BigDecimal maxPrice,@RequestParam(required = false) String name){
		startPage();
		UsedDto car = new UsedDto();
		car.setBrandId(brandId);
		car.setStatus(0);
		car.setCarType(carType);
		car.setName(name);
		car.setMaxPrice(maxPrice);
		car.setMinPrice(minPrice);
		return RFactory.generateR(getDataTable(this.carUsedService.findListByUsedDto(car)));
	}

	/**
	 * 查询二手车详情
	 * @param uid
	 * @param id
	 * @return
	 */
	@GetMapping("/info")
	private R carUsedInfo(@RequestParam String uid,@RequestParam String id){
		UsedDto carUsed = this.carUsedService.selectCarUsedById(id);
		HashMap<String, Object> map = new HashMap<>();
		map.put("used",carUsed);
		CarImg carImg = new CarImg();
		carImg.setCarId(carUsed.getId());
		carImg.setType(0);
		map.put("banner",this.carImgService.selectCarImgList(carImg));
		map.put("dealer",this.dealerService.selectDealerById(carUsed.getDealerId()));
		return RFactory.generateR(map);
	}

	/**
	 * 二手车卖车
	 * @param carUsedApply
	 * @return
	 */
	@PostMapping("/apply")
	private R carUsedApply(@RequestBody CarUsedApply carUsedApply){
		carUsedApply.setStatus(0);
		this.carUsedApplyService.insertCarUsedApply(carUsedApply);


		//消息接收人的数组
		List<String> receiveList = new ArrayList<String>();

		Map<String,Map<String,List<String>>> dealerList = AppBeanInjector.receive.getDealer();
		Map<String,List<String>> all = dealerList.get("all");
		//无论门店 无论服务 全部推送
		List<String> dealerAll = all.get("all");
		if(dealerAll!=null){
			receiveList.addAll(dealerAll);
		}


		List<String> byDidAll = all.get("used");
		if(byDidAll!=null){
			receiveList.addAll(byDidAll);
		}
		//通知相关人员
		for(int i=0;i<receiveList.size();i++){
			AddCarServerMsg addCarServerMsg = new AddCarServerMsg();
			addCarServerMsg.setToOpenId(receiveList.get(i));
			HashMap<String, String> data = new HashMap<>();
			data.put("name",carUsedApply.getName());
			data.put("created",DateUtils.dateTimeNow("yyyy-MM-dd HH:mm:ss"));
			data.put("mobile",carUsedApply.getMobile());
			data.put("typeName","二手车卖车");
			addCarServerMsg.setContentMsg(data);
			AppBeanInjector.weixinTemplateMsgService.pushMsg(addCarServerMsg);
		}

		return R.ok();
	}


	/**
	 * 二手车评估
	 * @param carUsedEstimate
	 * @return
	 */
	@PostMapping("/estimate")
	private R carUsedEstimate(@RequestBody CarUsedEstimate carUsedEstimate){
		carUsedEstimate.setStatus(0);
		this.carUsedEstimateService.insertCarUsedEstimate(carUsedEstimate);


		//消息接收人的数组
		List<String> receiveList = new ArrayList<String>();

		Map<String,Map<String,List<String>>> dealerList = AppBeanInjector.receive.getDealer();
		Map<String,List<String>> all = dealerList.get("all");
		//无论门店 无论服务 全部推送
		List<String> dealerAll = all.get("all");
		if(dealerAll!=null){
			receiveList.addAll(dealerAll);
		}


		List<String> byDidAll = all.get("used");
		if(byDidAll!=null){
			receiveList.addAll(byDidAll);
		}
		//通知相关人员
		for(int i=0;i<receiveList.size();i++){
			AddCarServerMsg addCarServerMsg = new AddCarServerMsg();
			addCarServerMsg.setToOpenId(receiveList.get(i));
			HashMap<String, String> data = new HashMap<>();
			data.put("name","未留");
			data.put("created",DateUtils.dateTimeNow("yyyy-MM-dd HH:mm:ss"));
			data.put("mobile",carUsedEstimate.getMobile());
			data.put("typeName","二手车评估");
			addCarServerMsg.setContentMsg(data);
			AppBeanInjector.weixinTemplateMsgService.pushMsg(addCarServerMsg);
		}

		return R.ok();
	}

	/**
	 * 二手车预约
	 * @param carUsedAppointment
	 * @return
	 */
	@PostMapping("/appointment")
	private R carUsedAppointment(@RequestBody CarUsedAppointment carUsedAppointment){
		carUsedAppointment.setStatus(0);
		this.carUsedAppointmentService.insertCarUsedAppointment(carUsedAppointment);


		//消息接收人的数组
		List<String> receiveList = new ArrayList<String>();

		Map<String,Map<String,List<String>>> dealerList = AppBeanInjector.receive.getDealer();
		Map<String,List<String>> all = dealerList.get("all");
		//无论门店 无论服务 全部推送
		List<String> dealerAll = all.get("all");
		if(dealerAll!=null){
			receiveList.addAll(dealerAll);
		}

		List<String> byDidAll = all.get("used");
		if(byDidAll!=null){
			receiveList.addAll(byDidAll);
		}
		//通知相关人员
		for(int i=0;i<receiveList.size();i++){
			AddCarServerMsg addCarServerMsg = new AddCarServerMsg();
			addCarServerMsg.setToOpenId(receiveList.get(i));
			HashMap<String, String> data = new HashMap<>();
			data.put("name",carUsedAppointment.getName());
			data.put("created",DateUtils.parseDateToStr("yyyy-MM-dd",carUsedAppointment.getAppointmentTime()));
			data.put("mobile",carUsedAppointment.getMobile());
			data.put("typeName","二手车预约");
			addCarServerMsg.setContentMsg(data);
			AppBeanInjector.weixinTemplateMsgService.pushMsg(addCarServerMsg);
		}

		return R.ok();
	}



}
