package com.ruoyi.project.mobile.activity;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.R;
import com.ruoyi.common.utils.RFactory;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.project.system.activity.domain.Activity;
import com.ruoyi.project.system.activity.service.IActivityService;
import com.ruoyi.project.system.coinrecord.domain.HondaCoinRecord;
import com.ruoyi.project.system.coinrecord.service.IHondaCoinRecordService;
import com.ruoyi.project.system.member.domain.Member;
import com.ruoyi.project.system.member.service.IMemberService;
import com.ruoyi.project.system.share.domain.ActivityShare;
import com.ruoyi.project.system.share.service.IActivityShareService;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/8/3 10:49
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
@RestController
@RequestMapping("/api/activity")
public class ApiActivityController extends BaseController {

	@Autowired
	private IActivityService activityService;

	@Autowired
	private IActivityShareService activityShareService;

	@Autowired
	private IMemberService memberService;

	@Autowired
	private IHondaCoinRecordService hondaCoinRecordService;
	/**
	 * 查询活动列表
	 * @param uid
	 * @return
	 */
	@GetMapping
	private R findActivityList(@RequestParam String uid){
		startPage();
		return RFactory.generateR(getDataTable(this.activityService.findList()));
	}

	/**
	 * 查询活动详情
	 * @param uid
	 * @param id
	 * @return
	 */
	@GetMapping("/info")
	private R activityInfo(@RequestParam String uid,@RequestParam String id){
		Activity activity = this.activityService.selectActivityById(id);
		return RFactory.generateR(activity);
	}

	/**
	 * 用户分享活动
	 * @return
	 */
	@PostMapping("/share")
	@Transactional
	private R share(@RequestBody ActivityShare activityShare){
		ActivityShare select = new ActivityShare();
		select.setActivityId(activityShare.getActivityId());
		select.setMemberId(activityShare.getMemberId());
		//查询用户今天是否是第一次分享这篇文章
		List<ActivityShare> activityShares = this.activityShareService.selectActivityShareList(select);
		if(activityShares.size()==0){
			//判断今天分享是否超过三次
			select.setActivityId(null);
			List<ActivityShare> oldShare = this.activityShareService.selectActivityShareList(select);
			if(oldShare.size()<3){
				//生成分享记录
				activityShare.setCreated(DateUtils.getNowDate());
				this.activityShareService.insertActivityShare(activityShare);
				//生成积分记录
				HondaCoinRecord hondaCoinRecord = new HondaCoinRecord();
				hondaCoinRecord.setNum(new BigDecimal(10));
				hondaCoinRecord.setCreated(DateUtils.getNowDate());
				hondaCoinRecord.setStatus(1);
				hondaCoinRecord.setType(0);
				hondaCoinRecord.setMemberId(activityShare.getMemberId());
				hondaCoinRecord.setGetType(2);
				this.hondaCoinRecordService.insertHondaCoinRecord(hondaCoinRecord);

				//用户积分变更
				Member member = this.memberService.selectMemberById(activityShare.getMemberId());

				Member update = new Member();
				update.setId(activityShare.getMemberId());
				update.setCoin(member.getCoin().add(new BigDecimal(10)));
				update.setSumCoin(member.getSumCoin().add(new BigDecimal(10)));
				this.memberService.updateCoinMember(update,member);

			}
		}
		return R.ok();
	}

}
