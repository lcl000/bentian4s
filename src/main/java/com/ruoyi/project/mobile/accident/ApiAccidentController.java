package com.ruoyi.project.mobile.accident;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.R;
import com.ruoyi.common.utils.mobile.weixin.template.CarAccidentMsg;
import com.ruoyi.project.AppBeanInjector;
import com.ruoyi.project.system.car.accident.domain.CarAccident;
import com.ruoyi.project.system.car.accident.dto.CarAccidentDto;
import com.ruoyi.project.system.car.accident.service.ICarAccidentService;
import com.ruoyi.project.system.car.img.domain.CarImg;
import com.ruoyi.project.system.car.img.service.ICarImgService;
import com.ruoyi.project.system.member.domain.Member;
import com.ruoyi.project.system.member.service.IMemberService;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/8/11 14:03
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
@RestController
@RequestMapping("/api/accident")
public class ApiAccidentController {

	@Autowired
	private ICarAccidentService carAccidentService;

	@Autowired
	private ICarImgService carImgService;

	@Autowired
	private IMemberService memberService;
	/**
	 * 提交事故车信息
	 * @param carAccidentDto
	 * @return
	 */
	@PostMapping
	@Transactional
	public R savaAccident(@RequestBody CarAccidentDto carAccidentDto){
		carAccidentDto.setStatus(0);
		carAccidentDto.setCreated(DateUtils.getNowDate());
		//添加事故车信息
		this.carAccidentService.insertCarAccident(carAccidentDto);
		//添加图片信息
		for (int i = 0; i < carAccidentDto.getCarImgs().size(); i++) {
			CarImg carImg = carAccidentDto.getCarImgs().get(i);
			carImg.setSort(i);
			carImg.setCarId(carAccidentDto.getId());
			this.carImgService.insertCarImg(carImg);
		}
		List<String> jieshou = AppBeanInjector.weixinConfig.getAccident();
		//通知相关人员
		for(int i=0;i<jieshou.size();i++){
			CarAccidentMsg memberHelpMsg = new CarAccidentMsg();
			memberHelpMsg.setToOpenId(jieshou.get(i));
			HashMap<String, String> data = new HashMap<>();
			data.put("address",carAccidentDto.getAddress());
			data.put("created",DateUtils.parseDateToStr("yyyy-MM-dd HH:mm:ss",carAccidentDto.getCreated()));
			Member member = this.memberService.selectMemberById(carAccidentDto.getMemberId());
			data.put("mobile",member.getMobile());
			memberHelpMsg.setContentMsg(data);
			AppBeanInjector.weixinTemplateMsgService.pushMsg(memberHelpMsg);
		}
		return R.ok();
	}

}
