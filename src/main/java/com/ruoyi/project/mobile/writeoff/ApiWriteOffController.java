package com.ruoyi.project.mobile.writeoff;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.R;
import com.ruoyi.common.utils.RFactory;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.project.system.coupon.usercoupon.domain.UserCoupon;
import com.ruoyi.project.system.coupon.usercoupon.dto.UserCouponDto;
import com.ruoyi.project.system.coupon.usercoupon.service.IUserCouponService;
import com.ruoyi.project.system.member.domain.Member;
import com.ruoyi.project.system.member.service.IMemberService;
import com.ruoyi.project.system.order.order.domain.OrderOrder;
import com.ruoyi.project.system.order.order.dto.OrderOrderDto;
import com.ruoyi.project.system.order.order.enums.OrderStatus;
import com.ruoyi.project.system.order.order.service.IOrderOrderService;
import com.ruoyi.project.system.order.ordergoods.service.IOrderGoodsService;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/8/13 9:51
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
@RestController
@RequestMapping("/api/writeoff")
public class ApiWriteOffController extends BaseController {

	@Autowired
	private IUserCouponService userCouponService;

	@Autowired
	private IMemberService memberService;

	@Autowired
	private IOrderOrderService orderOrderService;

	@Autowired
	private IOrderGoodsService orderGoodsService;

	/**
	 * 核销优惠券
	 * @param userCoupon
	 * @return
	 */
	@PostMapping("/coupon")
	public R writeOffCoupon(@RequestBody UserCoupon userCoupon){
		UserCoupon oldUserCoupon = this.userCouponService.selectUserCouponById(userCoupon.getId());
		//判断优惠券是否已使用
		if(oldUserCoupon.getStatus()==1){
			return R.error("优惠券已使用");
		}
		//判断优惠券是否在使用期内
		if(!(oldUserCoupon.getStartTime().getTime()<DateUtils.getNowDate().getTime()&&oldUserCoupon.getEndTime().getTime()>DateUtils.getNowDate().getTime())){
			return R.error("不在有效使用期内");
		}
		//判断核销人是否是核销员
		Member member = this.memberService.selectMemberById(userCoupon.getUsedMemberId());
		if(member.getRole()!=0){
			return R.error("非核销员 请勿操作");
		}
		//修改优惠券为已使用
		userCoupon.setStatus(1);
		userCoupon.setUsedTime(DateUtils.getNowDate());
		this.userCouponService.updateUserCoupon(userCoupon);
		return R.ok();
	}

	/**
	 * 订单核销
	 * @param orderOrder
	 * @return
	 */
	@PostMapping("/order")
	public R writeOffOrder(@RequestBody OrderOrder orderOrder){
		//判断订单是否存在 是否可核销
		OrderOrder order = this.orderOrderService.selectOrderOrderById(orderOrder.getId());
		if(order.getStatus()!=0){
			return R.error("订单错误,不可自提");
		}
		//判断核销人是否是核销员
		Member member = this.memberService.selectMemberById(orderOrder.getUsedMemberId());
		if(member.getRole()!=0){
			return R.error("非核销员 请勿操作");
		}
		//修改订单信息
		orderOrder.setStatus(OrderStatus.COMPLETE.getValue());
		orderOrder.setUpdateTime(DateUtils.getNowDate());
		this.orderOrderService.updateOrderOrder(orderOrder);
		return R.ok();
	}
	/**
	 * 查询核销订单列表
	 * @param uid
	 * @param uid
	 * @return
	 */
	@GetMapping("/order")
	public R orderList(@RequestParam String uid){
		startPage();
		List<OrderOrderDto> orderOrderDtos = this.orderOrderService.findListByUsedMember(uid);
		if(orderOrderDtos!=null){
			for(OrderOrderDto orderDto:orderOrderDtos){
				orderDto.setStatusName(OrderStatus.getByValue(orderDto.getStatus()).getName());
				orderDto.setGoodsList(this.orderGoodsService.findListByOrderId(orderDto.getId()));
			}
		}
		return RFactory.generateR(getDataTable(orderOrderDtos));
	}


	/**
	 * 查询核销优惠券列表
	 * @param uid
	 * @param type
	 * @return
	 */
	@GetMapping("/coupon")
	public R couponList(@RequestParam String uid,@RequestParam Integer type){
		startPage();
		List<UserCouponDto> userCouponDtoList = this.userCouponService.findListByUsedMemberIdAndType(uid,type);
		return RFactory.generateR(getDataTable(userCouponDtoList));
	}

	/**
	 * 获取今日核销总数
	 * @param uid
	 * @return
	 */
	@GetMapping("/count")
	public R count(@RequestParam String uid){
		List<UserCouponDto> userCouponDtoList1 = this.userCouponService.findListByUsedMemberIdAndType(uid,0);
		List<UserCouponDto> userCouponDtoList2 = this.userCouponService.findListByUsedMemberIdAndType(uid,1);
		List<OrderOrderDto> orderOrderDtos = this.orderOrderService.findListByUsedMember(uid);
		return RFactory.generateR(userCouponDtoList1.size()+userCouponDtoList2.size()+orderOrderDtos.size());
	}

}
