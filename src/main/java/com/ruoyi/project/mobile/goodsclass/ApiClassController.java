package com.ruoyi.project.mobile.goodsclass;

import com.ruoyi.common.utils.R;
import com.ruoyi.common.utils.RFactory;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.project.system.goods.goods.domain.GoodsGoods;
import com.ruoyi.project.system.goods.goods.service.IGoodsGoodsService;
import com.ruoyi.project.system.goodsclass.domain.GoodsClass;
import com.ruoyi.project.system.goodsclass.enums.ClassStatus;
import com.ruoyi.project.system.goodsclass.service.IGoodsClassService;
import com.ruoyi.project.system.type.domain.GoodsType;
import com.ruoyi.project.system.type.service.IGoodsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/21 21:17
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
@RestController
@RequestMapping("/api/class")
public class ApiClassController extends BaseController {

	@Autowired
	private IGoodsClassService goodsClassService;
	@Autowired
	private IGoodsGoodsService goodsGoodsService;
    @Autowired
    private IGoodsTypeService goodsTypeService;

	/**
	 * 查询一级分类列表
	 * @param uid
	 * @return
	 */
	@GetMapping
	private R findAll(@RequestParam String uid){
		return RFactory.generateR(this.goodsClassService.findApiList());
	}

    /**
     * 查询类型（儿童型、成人型...）
     * @param uid
     * @return
     */
    @GetMapping("/class")
    private R homeClass(@RequestParam String uid){
        GoodsClass goodsClass = new GoodsClass();
        goodsClass.setStatus(ClassStatus.NORMAL.getValue());
        return RFactory.generateR(this.goodsClassService.selectGoodsClassList(goodsClass));
    }

    /**
     * 查询类别（品牌：伊利、蒙牛...）
     * @param uid
     * @return
     */
    @GetMapping("/type")
    private R homeType(@RequestParam String uid){
        GoodsType goodsType = new GoodsType();
        goodsType.setStatus(ClassStatus.NORMAL.getValue());
        return RFactory.generateR(this.goodsTypeService.selectGoodsTypeList(goodsType));
    }

	/**
	 * 查询分类下的商品列表
	 * @param classId 商品类型ID
     * @param typeId 商品类别ID
	 * @return
	 */
	@GetMapping("/goods")
	private R findGoodsList(@RequestParam String uid,@RequestParam String classId,@RequestParam String typeId){
		startPage();
		GoodsGoods goodsGoods = new GoodsGoods();
		goodsGoods.setClassId(classId);
		goodsGoods.setTypeId(typeId);
		return RFactory.generateR(getDataTable(this.goodsGoodsService.findGoodsDtoList(goodsGoods)));
	}
}
