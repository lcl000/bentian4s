package com.ruoyi.project.mobile.dealer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ruoyi.common.utils.R;
import com.ruoyi.common.utils.RFactory;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.project.system.dealer.service.IDealerService;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/7/31 15:11
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
@RestController
@RequestMapping("/api/dealer")
public class ApiDealerController{


	@Autowired
	private IDealerService dealerService;


	/**
	 * 查询店铺列表
	 * @param uid
	 * @return
	 */
	@GetMapping
	private R dealerList(@RequestParam String uid,@RequestParam(required = false) String name){
		return RFactory.generateR(this.dealerService.findAll(name));
	}


}
