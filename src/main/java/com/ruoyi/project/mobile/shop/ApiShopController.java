package com.ruoyi.project.mobile.shop;

import javax.annotation.Resource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageHelper;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.R;
import com.ruoyi.common.utils.RFactory;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.project.system.banner.shop.domain.BannerShop;
import com.ruoyi.project.system.banner.shop.service.IBannerShopService;
import com.ruoyi.project.system.coinrecord.domain.HondaCoinRecord;
import com.ruoyi.project.system.coinrecord.dto.HondaCoinRecordDto;
import com.ruoyi.project.system.coinrecord.service.IHondaCoinRecordService;
import com.ruoyi.project.system.coupon.coupon.domain.Coupon;
import com.ruoyi.project.system.coupon.coupon.service.ICouponService;
import com.ruoyi.project.system.coupon.usercoupon.domain.UserCoupon;
import com.ruoyi.project.system.coupon.usercoupon.dto.UserCouponDto;
import com.ruoyi.project.system.coupon.usercoupon.service.IUserCouponService;
import com.ruoyi.project.system.goods.goods.domain.GoodsDto;
import com.ruoyi.project.system.goods.goods.domain.GoodsGoods;
import com.ruoyi.project.system.goods.goods.enums.GoodsStatus;
import com.ruoyi.project.system.goods.goods.service.IGoodsGoodsService;
import com.ruoyi.project.system.member.domain.Member;
import com.ruoyi.project.system.member.service.IMemberService;
import com.ruoyi.project.system.sign.domain.MemberSign;
import com.ruoyi.project.system.sign.service.IMemberSignService;
import com.ruoyi.project.system.type.domain.GoodsType;
import com.ruoyi.project.system.type.service.IGoodsTypeService;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/8/7 10:10
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
@RestController
@RequestMapping("/api/shop")
public class ApiShopController extends BaseController {

	@Resource
	private IBannerShopService bannerShopService;

	@Resource
	private IGoodsGoodsService goodsGoodsService;

	@Resource
	private IGoodsTypeService goodsTypeService;

	@Resource
	private IMemberService memberService;

	@Resource
	private IHondaCoinRecordService hondaCoinRecordService;

	@Resource
	private ICouponService couponService;

	@Resource
	private IUserCouponService userCouponService;

	@Resource
	private IMemberSignService memberSignService;
	/**
	 * 查询轮播
	 * @param uid
	 * @return
	 */
	@GetMapping("/banners")
	public R shopBannerList(@RequestParam String uid){
		List<BannerShop> bannerShops = this.bannerShopService.findAll();
		return RFactory.generateR(bannerShops);
	}

	/**
	 * 查询秒杀优惠券列表
	 * @param uid
	 * @return
	 */
	@GetMapping("/coupons")
	public R shopCouponList(@RequestParam String uid){
		Coupon coupon = new Coupon();
		coupon.setHasSeckill(1);
		//查询未过期
		coupon.setOverdue(0);
		//查询正常
		coupon.setStatus(0);
		coupon.setHasSeckill(1);
		List<Coupon> coupons = this.couponService.selectCouponList(coupon);
		for(Coupon cou:coupons){
			UserCoupon userCoupon = new UserCoupon();
			userCoupon.setMemberId(uid);
			userCoupon.setCouponId(cou.getId());
			List<UserCouponDto> userCoupons = this.userCouponService.selectUserCouponList(userCoupon);
			if(userCoupons.size()>0){
				cou.setReceiveStatus(1);
			}else{
				cou.setReceiveStatus(0);
			}
		}
		return RFactory.generateR(coupons);
	}

	/**
	 * 领取优惠券
	 * @param userCoupon
	 * @return
	 */
	@PostMapping("/coupons")
	@Transactional
	public R userCoupons(@RequestBody UserCoupon userCoupon){
		Coupon coupon = this.couponService.selectCouponById(userCoupon.getCouponId());
		//判断优惠券数量是否为0
		if(coupon.getTotal()==0){
			return R.error("您来晚了,被抢光了..");
		}
		//判断是否领取过
		UserCoupon oldCoupon = new UserCoupon();
		oldCoupon.setMemberId(userCoupon.getMemberId());
		oldCoupon.setCouponId(userCoupon.getCouponId());
		List<UserCouponDto> userCoupons = this.userCouponService.selectUserCouponList(userCoupon);
		if(userCoupons.size()>0){
			return R.error("你已领取过");
		}else{
			//更新优惠券数量
			Coupon update = new Coupon();
			update.setId(coupon.getId());
			update.setTotal(coupon.getTotal()-1);
			this.couponService.updateCoupon(update);
			//添加用户优惠券
			userCoupon.setStatus(0);
			userCoupon.setType(coupon.getType());
			userCoupon.setStartTime(coupon.getStartTime());
			userCoupon.setNotes(coupon.getNotes());
			userCoupon.setEndTime(coupon.getEndTime());
			userCoupon.setPrice(coupon.getPrice());
			userCoupon.setUseCondition(coupon.getUseCondition());
			userCoupon.setName(coupon.getName());
			userCoupon.setCreated(DateUtils.getNowDate());
			this.userCouponService.insertUserCoupon(userCoupon);
			return R.ok();
		}
	}

	/**
	 * 查询商品列表
	 * @param uid
	 * @return
	 */
	@GetMapping("/goods")
	public R shopGoodsList(@RequestParam String uid,@RequestParam(required = false) String typeId,@RequestParam(required = false) String name){
		startPage();
		GoodsGoods goodsGoods = new GoodsGoods();
		goodsGoods.setStatus(GoodsStatus.UP_SHELF.getValue());
		goodsGoods.setTypeId(typeId);
		goodsGoods.setName(name);
		List<GoodsDto> goodsDtos = this.goodsGoodsService.selectGoodsGoodsList(goodsGoods);
		return RFactory.generateR(getDataTable(goodsDtos));
	}

	/**
	 * 查询全部分类列表
	 * @param uid
	 * @return
	 */
	@GetMapping("/typelist")
	public R typeList(@RequestParam String uid){
		List<GoodsType> all = this.goodsTypeService.findAll();
		return RFactory.generateR(all);
	}
	/**
	 * 查询分类列表(喊商品)
	 * @param uid
	 * @return
	 */
	@GetMapping("/types")
	public R shopTypeList(@RequestParam String uid){
		List<GoodsType> all = this.goodsTypeService.findAll();
		List list= new ArrayList<>();
		GoodsGoods goodsGoods = new GoodsGoods();
		goodsGoods.setStatus(GoodsStatus.UP_SHELF.getValue());
		for(GoodsType goodsType :all){
			HashMap<String, Object> data = new HashMap<String, Object>();
			PageHelper.startPage(1, 2, "");
			goodsGoods.setTypeId(goodsType.getId());
			List<GoodsDto> goodsDtos = this.goodsGoodsService.selectGoodsGoodsList(goodsGoods);
			if(goodsDtos.size()!=0){
				data.put("goodsList",goodsDtos);
			}else{
				continue;
			}
			data.put("type",goodsType);
			list.add(data);
		}
		return RFactory.generateR(list);
	}

	/**
	 * 查询用户积分
	 * @param uid
	 * @return
	 */
	@GetMapping("/coin")
	public R memberCoin(@RequestParam String uid){
		Member member = this.memberService.selectMemberById(uid);
		HashMap<String, Object> map = new HashMap<>();
		map.put("sumCoin",member.getSumCoin());
		map.put("coin",member.getCoin());
		return RFactory.generateR(map);
	}

	/**
	 * 查询收益记录
	 * @param uid
	 * @param type
	 * @return
	 */
	@GetMapping("/coinrecord")
	public R coinRecordList(@RequestParam String uid,@RequestParam Integer type){
		startPage();
		HondaCoinRecord hondaCoinRecord = new HondaCoinRecord();
		hondaCoinRecord.setType(type);
		hondaCoinRecord.setMemberId(uid);
		List<HondaCoinRecordDto> hondaCoinRecordDtos = this.hondaCoinRecordService.selectHondaCoinRecordList(hondaCoinRecord);
		return RFactory.generateR(getDataTable(hondaCoinRecordDtos));
	}

	/**
	 * 查询打卡情况
	 * @param uid
	 * @return
	 */
	@GetMapping("/sign")
	public R memberSign(@RequestParam String uid){
		MemberSign oldSign = this.memberSignService.selectMemberSignById(uid);
		HashMap<String, Object> map = new HashMap<>();
		if(oldSign==null){
			MemberSign memberSign = new MemberSign();
			memberSign.setContinuity(0);
			memberSign.setSumCoin(new BigDecimal(0));
			memberSign.setTotal(0);
			memberSign.setId(uid);
			this.memberSignService.insertMemberSign(memberSign);
			map.put("sign",memberSign);
			map.put("today",0);
			map.put("continuity",0);
			map.put("nowDay",0);
			return RFactory.generateR(map);
		}
		map.put("sign",oldSign);
		if(oldSign.getUpdateTime()!=null){
			String updateTime = DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD, oldSign.getUpdateTime());
			String nowData = DateUtils.getDate();
			map.put("today",updateTime.equals(nowData)?1:0);
			if(DateUtils.IsYesterday(oldSign.getUpdateTime())||DateUtils.IsToday(oldSign.getUpdateTime())){
				map.put("continuity",oldSign.getContinuity());
				map.put("nowDay",oldSign.getContinuity()%7);
			}else{
				map.put("continuity",0);
				map.put("nowDay",0);
			}
		}else{
			map.put("today",0);
			map.put("continuity",0);
			map.put("nowDay",0);
		}
		return RFactory.generateR(map);
	}

	/**
	 * 用户打卡
	 * @param memberSign
	 * @return
	 */
	@PostMapping("/sign")
	@Transactional
	public R sign(@RequestBody MemberSign memberSign){
		//查询用户打卡信息
		MemberSign sign = this.memberSignService.selectMemberSignById(memberSign.getId());


		memberSign.setTotal(sign.getTotal()+1);
		memberSign.setUpdateTime(DateUtils.getNowDate());
		if(sign.getUpdateTime()!=null){
			//判断用户今天是否已经打卡
			if(DateUtils.IsToday(sign.getUpdateTime())){
				return R.error("请勿重复打卡");
			}
			//判断最新打卡时间是不是昨天
			memberSign.setContinuity(DateUtils.IsYesterday(sign.getUpdateTime())?sign.getContinuity()+1:1);
		}else {
			memberSign.setContinuity(1);
		}
		//判断今天是否是打卡第7天
		BigDecimal coin = new BigDecimal(0);
		if(memberSign.getContinuity()%7==0){
			coin=coin.add(new BigDecimal(5));
			memberSign.setSumCoin(sign.getSumCoin().add(coin));
		}else{
			coin=coin.add(new BigDecimal(1));
			memberSign.setSumCoin(sign.getSumCoin().add(coin));
		}
		this.memberSignService.updateMemberSign(memberSign);
		//生成积分记录
		HondaCoinRecord hondaCoinRecord = new HondaCoinRecord();
		hondaCoinRecord.setNum(coin);
		hondaCoinRecord.setCreated(DateUtils.getNowDate());
		hondaCoinRecord.setStatus(1);
		hondaCoinRecord.setType(0);
		hondaCoinRecord.setMemberId(memberSign.getId());
		hondaCoinRecord.setGetType(1);
		this.hondaCoinRecordService.insertHondaCoinRecord(hondaCoinRecord);

		//用户积分变更
		Member member = this.memberService.selectMemberById(memberSign.getId());

		Member update = new Member();
		update.setId(member.getId());
		update.setCoin(member.getCoin().add(coin));
		update.setSumCoin(member.getSumCoin().add(coin));
		this.memberService.updateCoinMember(update,member);

		return R.ok();
	}

}
