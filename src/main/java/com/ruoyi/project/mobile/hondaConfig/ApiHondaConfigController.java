package com.ruoyi.project.mobile.hondaConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ruoyi.common.utils.R;
import com.ruoyi.common.utils.RFactory;
import com.ruoyi.project.system.hondaconfig.service.IHondaConfigService;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/8/11 14:37
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
@RestController
@RequestMapping("/api/hondaconfig")
public class ApiHondaConfigController {

	@Autowired
	private IHondaConfigService hondaConfigService;

	/**
	 * 查询配置信息
	 * @return
	 */
	@GetMapping
	private R hondaConfig(){
		return RFactory.generateR(this.hondaConfigService.findAll().get(0));
	}

}
