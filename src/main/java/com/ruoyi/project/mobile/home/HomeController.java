package com.ruoyi.project.mobile.home;

import java.util.List;

import com.ruoyi.common.utils.R;
import com.ruoyi.common.utils.RFactory;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.project.system.activity.domain.Activity;
import com.ruoyi.project.system.activity.service.IActivityService;
import com.ruoyi.project.system.banner.banner.service.IBannerService;
import com.ruoyi.project.system.banner.platform.service.IBannerPlatformService;
import com.ruoyi.project.system.banner.used.service.IBannerUsedService;
import com.ruoyi.project.system.brand.service.IBrandService;
import com.ruoyi.project.system.car.car.domain.Car;
import com.ruoyi.project.system.car.car.service.ICarService;
import com.ruoyi.project.system.dealer.domain.Dealer;
import com.ruoyi.project.system.dealer.service.IDealerService;
import com.ruoyi.project.system.goods.goods.domain.GoodsGoods;
import com.ruoyi.project.system.goods.goods.enums.GoodsStatus;
import com.ruoyi.project.system.goods.goods.service.IGoodsGoodsService;
import com.ruoyi.project.system.goodsclass.domain.GoodsClass;
import com.ruoyi.project.system.goodsclass.enums.ClassStatus;
import com.ruoyi.project.system.goodsclass.service.IGoodsClassService;
import com.ruoyi.project.system.hondaconfig.service.IHondaConfigService;
import com.ruoyi.project.system.proclamation.domain.Proclamation;
import com.ruoyi.project.system.proclamation.enums.ProclamationStatus;
import com.ruoyi.project.system.proclamation.service.IProclamationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/18 18:19
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
@RestController
@RequestMapping("/api/home")
public class HomeController extends BaseController {

	@Autowired
	private IBannerService bannerService;
	@Autowired
	private IBannerPlatformService bannerPlatformService;
	@Autowired
	private IBannerUsedService bannerUsedService;
	@Autowired
	private IBrandService brandService;
	@Autowired
	private IGoodsClassService goodsClassService;
	@Autowired
	private IProclamationService proclamationService;
	@Autowired
	private IGoodsGoodsService goodsGoodsService;
	@Autowired
	private IHondaConfigService hondaConfigService;
	@Autowired
	private IActivityService activityService;
	@Autowired
	private ICarService carService;
	@Autowired
	private IDealerService dealerService;

	/**
	 * 查询轮播图
	 * @param uid
	 * @return
	 */
	@GetMapping("/banners")
	private R homeBanners(@RequestParam String uid){
		return RFactory.generateR(this.bannerService.findBannerList());
	}

	/**
	 * 查询二手车轮播图
	 * @param uid
	 * @return
	 */
	@GetMapping("/usedbanners")
	private R homeUsedBanners(@RequestParam String uid){
		return RFactory.generateR(this.bannerUsedService.findBannerList());
	}

	/**
	 * 查询平台轮播图
	 * @param uid
	 * @return
	 */
	@GetMapping("/platformbanners")
	private R homePlatformBanners(@RequestParam String uid){
		return RFactory.generateR(this.bannerPlatformService.findBannerList());
	}


	/**
	 * 查询品牌列表
	 * @param uid
	 * @return
	 */
	@GetMapping("/brands")
	private R homeBrand(@RequestParam String uid){
		return RFactory.generateR(this.brandService.findAll());
	}

	/**
	 * 查询活动
	 * @param uid
	 * @return
	 */
	@GetMapping("/activities")
	private R homeActivity(@RequestParam String uid){
		startPage();
		return RFactory.generateR(getDataTable(this.activityService.findList()));
	}

	/**
	 * 获取经销商
	 * @param uid
	 * @param lng
	 * @param lat
	 * @return
	 */
	@GetMapping("/dealer")
	private R homeDealer(@RequestParam String uid,
			@RequestParam(required = false) Double lng,@RequestParam(required = false) Double lat){
		if (lat!=null){
			return RFactory.generateR(this.dealerService.findMinDistance(lng,lat));
		}
		List<Dealer> dealers = this.dealerService.selectDealerList(new Dealer());
		if(dealers.size()!=0){
			return RFactory.generateR(dealers.get(0));
		}
		return RFactory.generateR(null);
	}

	/**
	 * 查询汽车列表
	 * @param uid
	 * @param brandId
	 * @return
	 */

	@GetMapping("/cars")
	private R homeCars(@RequestParam String uid,@RequestParam String brandId){
		startPage();
		Car car = new Car();
		car.setStatus(0);
		car.setBrandId(brandId);
		return RFactory.generateR(this.getDataTable(this.carService.selectCarList(car)));
	}

	/**
	 * 查询公告
	 * @param uid
	 * @return
	 */
	@GetMapping("/proclamations")
	private R homeProclamations(@RequestParam String uid){
		Proclamation proclamation = new Proclamation();
		proclamation.setStatus(ProclamationStatus.SHOW.getValue());
		return RFactory.generateR(this.proclamationService.selectProclamationList(proclamation));
	}


	/**
	 *
	 * @param brand 是否是品牌大牌 0 不是 1 是
	 * @param abroad 是否是境外产品 0 不是 1 是
	 * @param recommend 是否是推荐产品 0 不是 1 是
	 * @param hot 是否是热卖产品 0 不是 1 是
	 * @param spike 是否是秒杀产品 0 不是 1 是
	 * @return
	 */
	@GetMapping("/goods")
	private R homeGoods(@RequestParam String uid,
						@RequestParam(required = false,defaultValue = "") Integer brand,
						@RequestParam(required = false,defaultValue = "") Integer abroad,
						@RequestParam(required = false,defaultValue = "") Integer recommend,
						@RequestParam(required = false,defaultValue = "") Integer hot,
						@RequestParam(required = false,defaultValue = "") Integer spike,
						@RequestParam(required = false,defaultValue = "") String name){
		startPage();
		GoodsGoods goodsGoods = new GoodsGoods();
		goodsGoods.setIsAbroad(abroad);
		goodsGoods.setIsBrand(brand);
		goodsGoods.setIsHot(hot);
		goodsGoods.setIsRecommend(recommend);
		goodsGoods.setHasSpike(spike);
		goodsGoods.setStatus(GoodsStatus.UP_SHELF.getValue());
		goodsGoods.setName(name);
		return RFactory.generateR(getDataTable(this.goodsGoodsService.findGoodsDtoList(goodsGoods)));
	}

    /**
     * 搜索
     * @param name 商品名
     * @return
     */
    @GetMapping("/search")
    private R homeSearch(@RequestParam(required = false,defaultValue = "") String name) {
        startPage();
        GoodsGoods goodsGoods = new GoodsGoods();
        goodsGoods.setName(name);
        return RFactory.generateR(getDataTable(this.goodsGoodsService.selectGoodsGoodsList(goodsGoods)));
    }

}
