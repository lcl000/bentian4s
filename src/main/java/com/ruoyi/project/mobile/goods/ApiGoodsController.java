package com.ruoyi.project.mobile.goods;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.R;
import com.ruoyi.common.utils.RFactory;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.project.system.address.service.IMemberAddressService;
import com.ruoyi.project.system.goods.cart.domain.ShoppingCart;
import com.ruoyi.project.system.goods.cart.service.IShoppingCartService;
import com.ruoyi.project.system.goods.goods.domain.GoodsDto;
import com.ruoyi.project.system.goods.goods.service.IGoodsGoodsService;
import com.ruoyi.project.system.goods.goodscard.card.dto.SupplyCardDto;
import com.ruoyi.project.system.goods.goodscard.card.enums.SupplyType;
import com.ruoyi.project.system.goods.goodscard.card.service.IGoodsSupplyCardService;
import com.ruoyi.project.system.goods.goodscard.product.domain.SupplyCardProduct;
import com.ruoyi.project.system.goods.goodscard.product.service.ISupplyCardProductService;
import com.ruoyi.project.system.goods.goodscard.send.service.ISupplySendService;
import com.ruoyi.project.system.goods.goodscard.supplytype.service.ISupplyTypeService;
import com.ruoyi.project.system.goods.img.enums.GoodsImgType;
import com.ruoyi.project.system.goods.img.service.IGoodsImgService;
import com.ruoyi.project.system.goods.product.service.IGoodsProductService;
import com.ruoyi.project.system.order.comment.dto.OrderCommentDto;
import com.ruoyi.project.system.order.comment.service.IOrderCommentService;
import com.ruoyi.project.system.order.commentimg.domain.CommentImg;
import com.ruoyi.project.system.order.commentimg.service.ICommentImgService;
import com.ruoyi.project.system.spec.service.IGoodsSpecService;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/22 11:39
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
@RestController
@RequestMapping("/api/goods")
public class ApiGoodsController extends BaseController {

	@Autowired
	private IGoodsGoodsService goodsGoodsService;
	@Autowired
	private IOrderCommentService commentService;
	@Autowired
	private IGoodsImgService goodsImgService;
	@Autowired
	private IGoodsProductService goodsProductService;
	@Autowired
	private ICommentImgService commentImgService;
	@Autowired
	private IShoppingCartService shoppingCartService;
	@Autowired
	private ISupplyCardProductService supplyCardProductService;
	@Autowired
	private IGoodsSupplyCardService goodsSupplyCardService;
	@Autowired
	private ISupplyTypeService supplyTypeService;
	@Autowired
	private ISupplySendService supplySendService;
	@Autowired
	private IMemberAddressService memberAddressService;

	/**
	 * 查询商品详情
	 * @param id
	 * @return
	 */
	@GetMapping
	private R findGoodsInfo(@RequestParam String uid,@RequestParam String id){
		GoodsDto goodsDto = goodsGoodsService.findGoodsDtoById(id);
		HashMap<String, Object> map = new HashMap<>();
		//商品信息
		map.put("goods",goodsDto);
		//轮播信息
		map.put("bannerImg",this.goodsImgService.findListByGoodsIdAndType(id,GoodsImgType.BANNER.getValue()));
		//详情信息
		map.put("detailsImg",this.goodsImgService.findListByGoodsIdAndType(id,GoodsImgType.DETAILS.getValue()));
		return RFactory.generateR(map);
	}


	/**
	 * 查询商品规格
	 * @param uid
	 * @param id
	 * @param spec
	 * @return
	 */
	@GetMapping("/spec")
	private R findGoodsSpec(@RequestParam String uid,@RequestParam String id,
							@RequestParam String spec){
		return RFactory.generateR(this.goodsProductService.findByName(id,spec));
	}


}
