package com.ruoyi.project.mobile.user;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.commons.lang3.StringUtils;

import com.ruoyi.common.utils.R;
import com.ruoyi.common.utils.RFactory;
import com.ruoyi.project.system.address.domain.MemberAddress;
import com.ruoyi.project.system.address.service.IMemberAddressService;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/22 17:37
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
@RestController
@RequestMapping("/api/address")
public class ApiAddressController {

	@Autowired
	private IMemberAddressService memberAddressService;

	/**
	 * 查询用户地址
	 *
	 * @param uid
	 * @return
	 */
	@GetMapping
	private R memeberAddress(@RequestParam String uid) {
		MemberAddress memberAddress = new MemberAddress();
		memberAddress.setMemberId(uid);
		memberAddress.setStatus(0);
		return RFactory.generateR(this.memberAddressService.selectMemberAddressList(memberAddress));
	}

	/**
	 * 新增用户地址
	 *
	 * @param memberAddress
	 * @return
	 */
	@PostMapping
	private R addAddress(@RequestBody MemberAddress memberAddress) {
		if (memberAddress.getIsDefault() == 0) {
			//取消原本的默认地址
			this.memberAddressService.removeDefault(memberAddress.getMemberId());
		}

		memberAddress.setStatus(0);
		this.memberAddressService.insertMemberAddress(memberAddress);
		return R.ok();
	}

	/**
	 * 删除地址
	 *
	 * @return
	 */
	@PostMapping("/del")
	private R deleteAddress(@RequestBody Map<String, Object> map) {
		MemberAddress memberAddress = this.memberAddressService.selectMemberAddressById((String) map.get("id"));
		if (!memberAddress.getMemberId().equals((String) map.get("uid"))) {
			return R.ok();
		}
		this.memberAddressService.deleteMemberAddressById((String) map.get("id"));
		return R.ok();
	}

	/**
	 * 修改默认地址
	 *
	 * @param map
	 * @return
	 */
	@PostMapping("/default")
	private R updateDefault(@RequestBody Map<String, Object> map) {
		MemberAddress oldAddress = this.memberAddressService.selectMemberAddressById((String) map.get("id"));
		if (oldAddress == null || !oldAddress.getMemberId().equals((String) map.get("uid"))) {
			return R.ok();
		}
		//取消原本的默认地址
		this.memberAddressService.removeDefault((String) map.get("c"));
		MemberAddress memberAddress = new MemberAddress();
		memberAddress.setIsDefault(0);
		memberAddress.setId((String) map.get("id"));
		this.memberAddressService.updateMemberAddress(memberAddress);
		return R.ok();
	}

	/**
	 * 查询地址详情
	 *
	 * @param uid
	 * @param id
	 * @return
	 */
	@GetMapping("/info")
	private R addressInfo(@RequestParam String uid, @RequestParam String id) {
		return RFactory.generateR(this.memberAddressService.selectMemberAddressById(id));
	}

	/**
	 * 修改用户地址信息
	 *
	 * @param memberAddress
	 * @return
	 */
	@PostMapping("/update")
	private R updateAddress(@RequestBody MemberAddress memberAddress) {
		if (memberAddress.getIsDefault() == 0) {
			//取消原本的默认地址
			this.memberAddressService.removeDefault(memberAddress.getMemberId());
		}
		return RFactory.generateR(this.memberAddressService.updateMemberAddress(memberAddress));
	}

}
