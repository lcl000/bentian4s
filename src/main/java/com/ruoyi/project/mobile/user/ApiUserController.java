package com.ruoyi.project.mobile.user;

import com.ruoyi.common.utils.*;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.project.system.banner.info.service.IBannerMemberInfoService;
import com.ruoyi.project.system.coinrecord.service.IHondaCoinRecordService;
import com.ruoyi.project.system.coupon.usercoupon.domain.UserCoupon;
import com.ruoyi.project.system.coupon.usercoupon.service.IUserCouponService;
import com.ruoyi.project.system.giftbag.giftbagtinfo.service.IGiftBagInfoService;
import com.ruoyi.project.system.goods.goods.service.IGoodsGoodsService;
import com.ruoyi.project.system.hondaconfig.service.IHondaConfigService;
import com.ruoyi.project.system.member.domain.Member;
import com.ruoyi.project.system.member.service.IMemberService;
import com.ruoyi.project.system.membercar.domain.MemberCar;
import com.ruoyi.project.system.membercar.dto.MemberCarDto;
import com.ruoyi.project.system.membercar.service.IMemberCarService;
import com.ruoyi.project.system.order.order.service.IOrderOrderService;
import com.ruoyi.project.system.proclamation.domain.Proclamation;
import com.ruoyi.project.system.proclamation.service.IProclamationService;
import com.ruoyi.project.system.userSupplyCard.dto.UserSupplyCardDto;
import com.ruoyi.project.system.userSupplyCard.service.IUserSupplyCardService;
import com.ruoyi.project.system.userSupplyLog.domain.UserSupplySendLog;
import com.ruoyi.project.system.userSupplyLog.service.IUserSupplySendLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/24 16:25
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
@RestController
@RequestMapping("/api/user")
public class ApiUserController extends BaseController {

	@Autowired
	private IMemberService memberService;

	@Autowired
	private IOrderOrderService orderOrderService;

	@Autowired
	private IGoodsGoodsService goodsGoodsService;

	@Autowired
	private IHondaConfigService hondaConfigService;

	@Autowired
	private IUserSupplyCardService userSupplyCardService;

	@Autowired
	private IUserSupplySendLogService userSupplySendLogService;

	@Autowired
	private IProclamationService proclamationService;

	@Autowired
	private IMemberCarService memberCarService;

	@Autowired
	private IBannerMemberInfoService bannerMemberInfoService;

	@Autowired
	private IUserCouponService userCouponService;

	@Autowired
	private IGiftBagInfoService giftBagInfoService;

	@Autowired
	private IHondaCoinRecordService hondaCoinRecordService;

	/**
	 * 点击会员中心 查询用户信息
	 * @param uid
	 * @return
	 */
	@GetMapping
	public R findUser(@RequestParam String uid){
		HashMap<String, Object> map = new HashMap<>();

		Member oldMember = this.memberService.selectMemberById(uid);


		if(oldMember==null){
			return R.ok();
		}


		map.put("user",oldMember);


		UserCoupon userCoupon = new UserCoupon();
		userCoupon.setType(0);
		userCoupon.setMemberId(uid);
		userCoupon.setStatus(0);
		userCoupon.setOverdue(0);
		map.put("couponNum",this.userCouponService.selectUserCouponList(userCoupon).size());
		userCoupon.setType(1);
		map.put("voucher",this.userCouponService.selectUserCouponList(userCoupon).size());
		return RFactory.generateR(map);
	}

	/**
	 * 查询车主信息
	 * @param uid
	 * @return
	 */
	@GetMapping("/car")
	public R userCar(@RequestParam String uid){
		MemberCarDto memberCar = this.memberCarService.findByUid(uid);
		return RFactory.generateR(memberCar);
	}

	/**
	 * 设置车主信息
	 * @param memberCar
	 * @return
	 */
	@PostMapping("/car")
	@Transactional
	public R savaUserCar(@RequestBody MemberCar memberCar){
		//判断用户ID是否为空
		if(StringUtils.isBlank(memberCar.getMemberId())){
			return R.error("用户不存在,请重新登录");
		}
		//查询车主是否存在审核通过的车辆
		MemberCarDto byUid = this.memberCarService.findByUid(memberCar.getMemberId());
		if(byUid!=null){
			if(byUid.getStatus()==2){
				//删除原本存在的车辆信息
				this.memberCarService.deleteByUid(memberCar.getMemberId());
				memberCar.setStatus(0);
				memberCar.setCreated(DateUtils.getNowDate());
				//添加车辆信息
				this.memberCarService.insertMemberCar(memberCar);
			} else if (byUid.getStatus()==0) {
                //删除原本存在的车辆信息
                this.memberCarService.deleteByUid(memberCar.getMemberId());
            } else if(byUid.getStatus()==1){
				//更新数据
				byUid.setName(memberCar.getName());
				byUid.setMobile(memberCar.getMobile());
				byUid.setPayTime(memberCar.getPayTime());
				byUid.setCarType(memberCar.getCarType());
				byUid.setDealerId(memberCar.getDealerId());
				byUid.setFrameNo(memberCar.getFrameNo());
				byUid.setLicensePlate(memberCar.getLicensePlate());
				this.memberCarService.updateMemberCar(byUid);
			}else{
				return R.error("状态异常,不允许修改");
			}
		}else{
            //删除原本存在的车辆信息
            this.memberCarService.deleteByUid(memberCar.getMemberId());
			memberCar.setStatus(0);
			memberCar.setCreated(DateUtils.getNowDate());
			//添加车辆信息
			this.memberCarService.insertMemberCar(memberCar);
		}
		return R.ok();
	}

	/**
	 * 解绑车辆
	 * @param member
	 * @return
	 */
	@PostMapping("/removecar")
	@Transactional
	public R removeCar(@RequestBody Member member){
		//清除该用户下的车辆信息
		this.memberCarService.deleteByUid(member.getId());
		//修改用户的绑定车辆状态
//		member.setHasCar(0);
//		this.memberService.updateMember(member);
		return R.ok();
	}

	/**
	 * 查询用户详细信息
	 * @param uid
	 * @return
	 */
	@GetMapping("/info")
	public R userInfo(@RequestParam String uid){
		return RFactory.generateR(this.memberService.selectMemberById(uid));
	}

	/**
	 * 查询用户中心轮播图
	 * @param uid
	 * @return
	 */
	@GetMapping("/banners")
	public R userBanners(@RequestParam String uid){
		return RFactory.generateR(this.bannerMemberInfoService.findBannerList());
	}


	/**
	 * 修改用户信息
	 * @param map
	 * @return
	 */
	@PostMapping("/info")
	public R updateUserInfo(@RequestBody Map map){
		String uid = (String) map.get("uid");

		String mobile = (String) map.get("mobile");
		Integer sex = (Integer) map.get("sex");
		String headpic = (String) map.get("headpic");
		String city = (String) map.get("city");
		String username = (String) map.get("username");
		String birthday = (String) map.get("birthday");
		String memeberNo = (String) map.get("memeberNo");
		Member member = new Member();
		member.setId(uid);
		member.setCity(city);
		member.setSex(sex);
		member.setHeadpic(headpic);
		member.setUsername(username);
		member.setMobile(mobile);
		member.setBirthday(DateUtils.parseDate(birthday));
		member.setMemeberNo(memeberNo);
		this.memberService.updateMember(member);
		return R.ok();
	}


	/**
	 * 查询推荐人列表
	 * @param uid
	 * @return
	 */
	@GetMapping("/recommend")
	public R userRecommend(@RequestParam String uid){
		startPage();
		Member member = new Member();
		member.setIntroUser(uid);
		List<Member> members = this.memberService.selectMemberList(member);
		return RFactory.generateR(getDataTable(members));
	}


	/**
	 * 查询优惠券列表
	 * @param uid
	 * @param type
	 * @param status
	 * @param overdue
	 * @return
	 */
	@GetMapping("/coupons")
	public R userCouponsList(@RequestParam String uid,@RequestParam Integer type,@RequestParam Integer status,
							 @RequestParam(required = false) Integer overdue){
		startPage();
		UserCoupon userCoupon = new UserCoupon();
		userCoupon.setMemberId(uid);
		userCoupon.setType(type);
		userCoupon.setStatus(status);
		userCoupon.setOverdue(overdue);
		return RFactory.generateR(getDataTable(this.userCouponService.selectUserCouponList(userCoupon)));
	}

	/**
	 * 查询优惠券详情
	 * @param id
	 * @return
	 */
	@GetMapping("/coupons/info")
	public R couponsInfo(@RequestParam String id){
		UserCoupon userCoupon = this.userCouponService.selectUserCouponById(id);
		if(userCoupon==null){
			return R.error("优惠券不存在");
		}
		return RFactory.generateR(userCoupon);
	}

	/**
	 * 查询顾问信息
	 * @param uid
	 * @return
	 */
	@GetMapping("/introuser")
	public R findIntroUser(@RequestParam String uid){
		Member member = this.memberService.selectMemberById(uid);
		if(member==null){
			return R.ok();
		}
		if(member.getIntroUser()==null){
			return R.ok();
		}
		return RFactory.generateR(this.memberService.selectMemberById(member.getIntroUser()));
	}

	/**
	 * 查询客服
	 * @param uid
	 * @return
	 */
	@GetMapping("/cservice")
	public R findCustomerService(@RequestParam String uid){
		return RFactory.generateR(this.hondaConfigService.findAll().get(0));
	}

	/**
	 * 查询设置
	 * @param uid
	 * @return
	 */
	@GetMapping("/config")
	public R findHondaConfig(@RequestParam String uid){
		return RFactory.generateR(this.hondaConfigService.findAll().get(0));
	}


	/**
	 * 查询用户供应卡信息
	 * @param uid
	 * @return
	 */
	@GetMapping("/supply")
	public R findUserSupply(@RequestParam String uid){
		startPage();
		List<UserSupplyCardDto> userSupplyCardDtos = this.userSupplyCardService.findListByUid(uid);
		return RFactory.generateR(getDataTable(userSupplyCardDtos));
	}

	/**
	 * 查询供应卡配送记录
	 * @param uid
	 * @param id
	 * @return
	 */
	@GetMapping("/supply/log")
	public R findSupplyLog(@RequestParam String uid,@RequestParam String id){
		UserSupplySendLog userSupplySendLog = new UserSupplySendLog();
		userSupplySendLog.setUserSupplyId(id);
		List<UserSupplySendLog> userSupplySendLogs = this.userSupplySendLogService.selectUserSupplySendLogList(userSupplySendLog);
		return RFactory.generateR(userSupplySendLogs);
	}

	/**
	 * 查询公告列表
	 * @param uid
	 * @return
	 */
	@GetMapping("/proclamations")
	public R findProclamationList(@RequestParam String uid){
		startPage();
		Proclamation proclamation = new Proclamation();
		proclamation.setStatus(0);
		List<Proclamation> proclamations = this.proclamationService.selectProclamationList(proclamation);
		return RFactory.generateR(getDataTable(proclamations));
	}




	@GetMapping("/share")
	public R share(@RequestParam String uid){
		HashMap<String, Object> map = new HashMap<>();
		Member member = this.memberService.selectMemberById(uid);
		if(StringUtils.isBlank(member.getQrCode())){
			String qrCode = WeixinUtils.getQrCodeImg(member);
			member.setQrCode(qrCode);
			Member update = new Member();
			update.setQrCode(qrCode);
			update.setId(uid);
			this.memberService.updateMember(update);
		}
		map.put("avator",member.getHeadpic());
		map.put("name",member.getUsername());
		map.put("imgurl",java.util.Arrays.asList(this.hondaConfigService.findAll().get(0).getShareImg()));
		map.put("qrCode",member.getQrCode());
		return RFactory.generateR(map);
	}


}
