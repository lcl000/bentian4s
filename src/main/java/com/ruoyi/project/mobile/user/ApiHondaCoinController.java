package com.ruoyi.project.mobile.user;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.R;
import com.ruoyi.common.utils.RFactory;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.project.system.coinrecord.domain.HondaCoinRecord;
import com.ruoyi.project.system.coinrecord.dto.HondaCoinRecordDto;
import com.ruoyi.project.system.coinrecord.service.IHondaCoinRecordService;
import com.ruoyi.project.system.member.domain.Member;
import com.ruoyi.project.system.member.service.IMemberService;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/29 21:15
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
@RestController
@RequestMapping("/api/coin")
public class ApiHondaCoinController extends BaseController {

	@Autowired
	private IHondaCoinRecordService hondaCoinRecordService;

	@Autowired
	private IMemberService memberService;

	/**
	 * 查询收益数量
	 * @param uid
	 * @return
	 */
	@GetMapping
	public R hondaCoinNum(@RequestParam String uid){
		Member member = this.memberService.selectMemberById(uid);
		if(member==null){
			return R.error("用户不存在");
		}
		HashMap<String, Object> map = new HashMap<>();
		map.put("coin",member.getCoin());
		map.put("sumCoin",member.getSumCoin());
		return RFactory.generateR(map);
	}

	/**
	 * 查询收益历史列表
	 * @param uid
	 * @return
	 */
	@GetMapping("/history")
	public R hondaCoinHistory(@RequestParam String uid){
		startPage();
		Member member = this.memberService.selectMemberById(uid);
		if(member==null){
			return R.error("用户不存在");
		}
		HondaCoinRecord haoqiCoinRecord = new HondaCoinRecord();
		haoqiCoinRecord.setMemberId(uid);
		List<HondaCoinRecordDto> haoqiCoinRecords = this.hondaCoinRecordService.selectHondaCoinRecordList(haoqiCoinRecord);
		return RFactory.generateR(getDataTable(haoqiCoinRecords));
	}

	/**
	 * 查询支出列表
	 * @param uid
	 * @return
	 */
	@GetMapping("/expenditure")
	public R hondaCoinExpenditure(@RequestParam String uid){
		startPage();
		Member member = this.memberService.selectMemberById(uid);
		if(member==null){
			return R.error("用户不存在");
		}
		HondaCoinRecord haoqiCoinRecord = new HondaCoinRecord();
		haoqiCoinRecord.setType(1);
		haoqiCoinRecord.setMemberId(uid);
		List<HondaCoinRecordDto> haoqiCoinRecords = this.hondaCoinRecordService.selectHondaCoinRecordList(haoqiCoinRecord);
		return RFactory.generateR(getDataTable(haoqiCoinRecords));
	}

	/**
	 * 查询收入列表
	 * @param uid
	 * @return
	 */
	@GetMapping("/income")
	public R hondaCoinIncome(@RequestParam String uid){
		startPage();
		Member member = this.memberService.selectMemberById(uid);
		if(member==null){
			return R.error("用户不存在");
		}
		HondaCoinRecord haoqiCoinRecord = new HondaCoinRecord();
		haoqiCoinRecord.setType(0);
		haoqiCoinRecord.setMemberId(uid);
		List<HondaCoinRecordDto> haoqiCoinRecords = this.hondaCoinRecordService.selectHondaCoinRecordList(haoqiCoinRecord);
		return RFactory.generateR(getDataTable(haoqiCoinRecords));
	}


	/**
	 * 提现
	 * @param hondaCoinRecord
	 * @return
	 */
	@PostMapping("/withdrawal")
	@Transactional
	public R hondaCoinWithdrawal(@RequestBody HondaCoinRecord hondaCoinRecord){
		Member member = this.memberService.selectMemberById(hondaCoinRecord.getMemberId());
		if(member.getCoin().compareTo(hondaCoinRecord.getNum())==-1){
			return R.error("余额不足");
		}

		hondaCoinRecord.setType(1);
		hondaCoinRecord.setStatus(0);
		hondaCoinRecord.setCreated(DateUtils.getNowDate());
		this.hondaCoinRecordService.insertHondaCoinRecord(hondaCoinRecord);
		//修改个人余额
		Member update = new Member();
		update.setId(member.getId());
		update.setCoin(member.getCoin().subtract(hondaCoinRecord.getNum()));
		this.memberService.updateMember(update);
		return R.ok();
	}

}
