package com.ruoyi.project.mobile.platform;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.ruoyi.common.utils.R;
import com.ruoyi.common.utils.RFactory;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.project.system.platform.PlatformApply.domain.PlatformApply;
import com.ruoyi.project.system.platform.PlatformApply.service.IPlatformApplyService;
import com.ruoyi.project.system.platform.platform.domain.Platform;
import com.ruoyi.project.system.platform.platform.service.IPlatformService;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/7/31 14:17
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
@RestController
@RequestMapping("/api/platform")
public class ApiPlatformController extends BaseController {

	@Autowired
	private IPlatformApplyService platformApplyService;

	@Autowired
	private IPlatformService platformService;


	/**
	 * 查询平台列表
	 * @param uid
	 * @return
	 */
	@GetMapping
	private R platformList(@RequestParam String uid){
		List<Platform> platforms = this.platformService.findAll();
		return RFactory.generateR(platforms);
	}

	/**
	 * 查询详情
	 * @param uid
	 * @return
	 */
	@GetMapping("/info")
	private R platformInfo(@RequestParam String uid,@RequestParam String id){
		Platform platform = this.platformService.selectPlatformById(id);
		return RFactory.generateR(platform);
	}

	/**
	 * 提交平台申请
	 * @param platformApply
	 * @return
	 */
	@PostMapping("/apply")
	private R addPlatformApply(@RequestBody PlatformApply platformApply){
		platformApply.setStatus(0);
		this.platformApplyService.insertPlatformApply(platformApply);
		return R.ok();
	}

}
