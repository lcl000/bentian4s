package com.ruoyi.project.mobile.prize;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.system.coinrecord.domain.HondaCoinRecord;
import com.ruoyi.project.system.coinrecord.enums.CoinExpenditure;
import com.ruoyi.project.system.coinrecord.enums.CoinIncome;
import com.ruoyi.project.system.coinrecord.service.IHondaCoinRecordService;
import com.ruoyi.project.system.coupon.coupon.domain.Coupon;
import com.ruoyi.project.system.coupon.coupon.service.ICouponService;
import com.ruoyi.project.system.coupon.usercoupon.domain.UserCoupon;
import com.ruoyi.project.system.coupon.usercoupon.service.IUserCouponService;
import com.ruoyi.project.system.member.domain.Member;
import com.ruoyi.project.system.member.service.IMemberService;
import com.ruoyi.project.system.prize.domain.Prize;
import com.ruoyi.project.system.prize.service.IPrizeService;
import com.ruoyi.project.system.prizelog.domain.PrizeLog;
import com.ruoyi.project.system.prizelog.service.IPrizeLogService;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/11/10 1:16
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
@RestController
@RequestMapping("/api/prize")
public class ApiPrizeController extends BaseController {

	@Autowired
	private IPrizeService prizeService;

	@Autowired
	private IPrizeLogService prizeLogService;


	@Autowired
	private IHondaCoinRecordService hondaCoinRecordService;

	@Autowired
	private IMemberService memberService;

	@Autowired
	private ICouponService couponService;

	@Autowired
	private IUserCouponService userCouponService;

	/**
	 * 查询奖品列表
	 * @return
	 */
	@GetMapping
	public AjaxResult prizeList(){
		Prize prize = new Prize();
		prize.setStatus(0);
		List<Prize> prizes = this.prizeService.selectPrizeList(prize);
		return AjaxResult.success(prizes);
	}

	/**
	 * 抽奖
	 * @param uid
	 * @return
	 */
	@PostMapping("/run")
	@Transactional
	public AjaxResult prizeRun(@RequestParam String uid){
		Member mem = this.memberService.selectMemberById(uid);
		//判断余额是否充足
		if(mem.getCoin().compareTo(new BigDecimal(100))<0){
			return AjaxResult.error("智通币不足");
		}
		// 智通币扣款记录
		HondaCoinRecord deduction = new HondaCoinRecord();
		deduction.setType(1);
		deduction.setGetType(CoinExpenditure.LUCK_DRAW.getValue());
		deduction.setStatus(1);
		deduction.setMemberId(uid);
		deduction.setNum(new BigDecimal(100));
		deduction.setCreated(DateUtils.getNowDate());
		this.hondaCoinRecordService.insertHondaCoinRecord(deduction);
		//更新用户智通币余额
		Member upd = new Member();
		upd.setId(uid);
		upd.setCoin(mem.getCoin().subtract(new BigDecimal(100)));
		this.memberService.updateCoinMember(upd,mem);
		//查询奖品列表
		Prize select = new Prize();
		select.setStatus(0);
		List<Prize> prizes = this.prizeService.selectPrizeList(select);
		//计算中奖概率
		BigDecimal sumProbability = new BigDecimal(0);
		for(Prize prize:prizes){
			sumProbability=sumProbability.add(prize.getProbability());
		}
		//获取随机数
		Random random = new Random();
		int i = random.nextInt(100);
		//判断是否中奖
		if(sumProbability.compareTo(new BigDecimal(i))>0){
			//判断中的是哪个奖
			for(Prize prize:prizes){
				//挨个减去中奖几率  当中奖几率小于随机数时  代表找到奖品了
				sumProbability = sumProbability.subtract(prize.getProbability());
				if(sumProbability.compareTo(new BigDecimal(i))<0){
					// 判断奖品是否还有
					if(prize.getNum()>0){
						//生成中奖纪录
						PrizeLog prizeLog = new PrizeLog();
						prizeLog.setCreated(DateUtils.getNowDate());
						prizeLog.setuId(uid);
						prizeLog.setpId(prize.getId());
						this.prizeLogService.insertPrizeLog(prizeLog);
						//奖品生成
						if(prize.getType()==2){
							// 智通币增加记录
							HondaCoinRecord hondaCoinRecord = new HondaCoinRecord();
							hondaCoinRecord.setType(0);
							hondaCoinRecord.setGetType(CoinIncome.PRIZE.getValue());
							hondaCoinRecord.setStatus(1);
							hondaCoinRecord.setMemberId(uid);
							hondaCoinRecord.setNum(prize.getCoinNum());
							hondaCoinRecord.setResId(prizeLog.getId());
							hondaCoinRecord.setCreated(DateUtils.getNowDate());
							this.hondaCoinRecordService.insertHondaCoinRecord(hondaCoinRecord);
							//更新用户智通币余额
							Member member = this.memberService.selectMemberById(uid);
							Member update = new Member();
							update.setId(uid);
							update.setSumCoin(member.getSumCoin().add(prize.getCoinNum()));
							update.setCoin(member.getCoin().add(prize.getCoinNum()));
							this.memberService.updateCoinMember(update,member);
						}else{
							//查询优惠券 折扣券
							Coupon coupon = this.couponService.selectCouponById(prize.getResId());
							//赠送给用户
							UserCoupon userCoupon = new UserCoupon();
							userCoupon.setCouponId(coupon.getId());
							userCoupon.setEndTime(coupon.getEndTime());
							userCoupon.setMemberId(uid);
							userCoupon.setNotes(coupon.getNotes());
							userCoupon.setPrice(coupon.getPrice());
							userCoupon.setStartTime(coupon.getStartTime());
							userCoupon.setStatus(0);
							userCoupon.setType(coupon.getType());
							userCoupon.setUseCondition(coupon.getUseCondition());
							userCoupon.setName(coupon.getName());
							userCoupon.setSendPost("抽奖部门");
							userCoupon.setCreated(DateUtils.getNowDate());
							this.userCouponService.insertUserCoupon(userCoupon);
						}
						//更新奖品剩余数量
						Prize updatePrize = new Prize();
						updatePrize.setId(prize.getId());
						updatePrize.setNum(prize.getNum()-1);
						this.prizeService.updatePrize(updatePrize);
						return AjaxResult.success(prize);
					}
				}
			}
		}
		return AjaxResult.success();
	}

	/**
	 * 查询用户中奖纪录
	 * @param uid
	 * @return
	 */
	@GetMapping("/log")
	public AjaxResult findPrizeLog(@RequestParam String uid){
		startPage();
		PrizeLog prizeLog = new PrizeLog();
		prizeLog.setuId(uid);
		List<PrizeLog> prizeLogs = this.prizeLogService.selectPrizeLogList(prizeLog);
		return AjaxResult.success(getDataTable(prizeLogs));
	}

}
