package com.ruoyi.project;

import me.chanjar.weixin.mp.api.WxMpService;

import com.ruoyi.common.utils.SpringContextUtil;
import com.ruoyi.common.utils.express.kd100.Kd100Util;
import com.ruoyi.common.utils.express.kdniao.ExpressUtil;
import com.ruoyi.common.utils.mobile.weixin.Receive;
import com.ruoyi.common.utils.mobile.weixin.WeixinConfig;
import com.ruoyi.common.utils.mobile.weixin.service.IMsgService;
import com.ruoyi.common.utils.qiniu.QiNiuUploadUtil;
import com.ruoyi.framework.config.ServerConfig;

/**
 * 功能：
 *
 * @author：Administrator
 * @create：2019/6/13/013 16:43
 * @version：2019 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public abstract class AppBeanInjector {

	public static final WeixinConfig weixinConfig;
	public static final Receive receive;
	public static final WxMpService wxMpService;
	public static final IMsgService weixinTemplateMsgService;
	public static final QiNiuUploadUtil qiniuUploadUtil;
	public static final ExpressUtil expressUtil;
	public static final Kd100Util kd100Util;
	public static final ServerConfig serverConfig;
	static {

		weixinConfig = (WeixinConfig) SpringContextUtil.getBean("weixinCfg");
		receive = (Receive) SpringContextUtil.getBean("receive");
		wxMpService = (WxMpService) SpringContextUtil.getBean("wxMpService");
		weixinTemplateMsgService = (IMsgService) SpringContextUtil.getBean("weixinTemplateMsgService");
		qiniuUploadUtil = (QiNiuUploadUtil) SpringContextUtil.getBean("qiniuUploadUtil");
		expressUtil = (ExpressUtil) SpringContextUtil.getBean("expressUtil");
		kd100Util = (Kd100Util) SpringContextUtil.getBean("kd100Util");
		serverConfig = (ServerConfig) SpringContextUtil.getBean("serverConfig");
	}
}
