package com.ruoyi.common.utils;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/19 10:31
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public class RFactory {

	private static final String KEY_DATA = "data";

	public static R generateR(Object result){
		return RFactory.newInstance(result);
	}

	private static R newInstance(Object result) {
		R r = new R();
		r.put(KEY_DATA,result);
		return r;
	}

}
