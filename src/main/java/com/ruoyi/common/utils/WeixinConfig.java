package com.ruoyi.common.utils;

import me.chanjar.weixin.mp.api.WxMpConfigStorage;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 功能：
 *
 * @author：Administrator
 * @create：2019/6/27/027 15:54
 * @version：2019 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
@Configuration
public class WeixinConfig {

	@Bean
	public WxMpService wxMpService(WxMpConfigStorage wxMpConfigStorage){
		WxMpServiceImpl wxMpService = new WxMpServiceImpl();
		wxMpService.setWxMpConfigStorage(wxMpConfigStorage);
		return wxMpService;
	}

	@Bean
	public WxMpConfigStorage wxMpConfigStorage(com.ruoyi.common.utils.mobile.weixin.WeixinConfig weixinConfig){
		WxMpInMemoryConfigStorage wxMpInMemoryConfigStorage = new WxMpInMemoryConfigStorage();
		wxMpInMemoryConfigStorage.setAppId(weixinConfig.getGzhAppId());
		wxMpInMemoryConfigStorage.setSecret(weixinConfig.getGzhAppsecret());
		wxMpInMemoryConfigStorage.setAccessToken(weixinConfig.getToken());
		return wxMpInMemoryConfigStorage;
	}
}
