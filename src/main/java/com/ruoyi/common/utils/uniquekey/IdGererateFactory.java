package com.ruoyi.common.utils.uniquekey;

import org.springframework.stereotype.Component;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/5 17:08
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public final class IdGererateFactory implements IIdGenerator {
	private IIdGenerator idGenerator;

	private IdGererateFactory() {
	}

	public IdGererateFactory(Long workerId) {
		this.idGenerator = new WorkerIdGenerator(workerId);
	}

	public IdGererateFactory(Long workerId, Long dataCenterId) {
		if (dataCenterId == null) {
			this.idGenerator = new WorkerIdGenerator(workerId);
		} else {
			// 待完善
//			this.idGenerator = new SnowflakeIdGenerator(workerId, dataCenterId);

		}

	}

	public Long nextId() {
		return this.idGenerator.nextId();
	}
}
