package com.ruoyi.common.utils.uniquekey;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/5 17:10
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public class WorkerIdGenerator implements IIdGenerator {
	private IdWorker idWorker;

	public WorkerIdGenerator() {
		this.idWorker = IdWorker.getFlowIdWorkerInstance();
	}

	public WorkerIdGenerator(long workerId) {
		this.idWorker = IdWorker.getFlowIdWorkerInstance(workerId);
	}

	public synchronized Long nextId() {
		return this.idWorker.nextId();
	}
}