package com.ruoyi.common.utils.uniquekey;

import java.nio.ByteBuffer;

import com.ruoyi.common.utils.StringUtils;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/5 17:09
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public interface IIdGenerator {
	Long nextId();

	default String nextStringId() {
		return StringUtils.highFill0(Long.toString(this.nextId(), 36), 12);
	}

	/** @deprecated */
	@Deprecated
	default byte[] longToBytes(Long l) {
		ByteBuffer buffer = ByteBuffer.allocate(8);
		buffer.putLong(l);
		return buffer.array();
	}
}
