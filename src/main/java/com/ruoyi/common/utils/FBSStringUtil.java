package com.ruoyi.common.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 * 功能：
 *
 * @author：Administrator
 * @create：2019/6/27/027 9:42
 * @version：2019 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public class FBSStringUtil extends StringUtils {

	//正则表达式 匹配 {N}  N:数字
	private static final Pattern pattern = Pattern.compile("\\{(\\d+)\\}");

	public static String format(String string,Object... args){
		if(string==null){
			return null;
		}
		Matcher matcher = pattern.matcher(string);
		//判断是否找到需要替换的
		if(!matcher.find()){
			return string;
		}
		StringBuffer stringBuffer = new StringBuffer();
		//由于已经调用了.find()方法 下标已经挪动 因此 需要先执行 再判断 使用 do while
		do{
			int i = Integer.parseInt(matcher.group(1));
			//判断占位符是否大于0  并且 是否在 替换的数组中有值
			if(i>=0&&i<args.length){
				matcher.appendReplacement(stringBuffer,Matcher.quoteReplacement(args[i].toString()));
			}
		}while (matcher.find());
		matcher.appendTail(stringBuffer);
		return stringBuffer.toString();
	}
}
