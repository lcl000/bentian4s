package com.ruoyi.common.utils.mobile.weixin.template;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.mobile.weixin.WeixinBaseTemplateMsg;

/**
 * 功能：
 *
 * @author：Administrator
 * @create：2019/7/4/004 15:12
 * @version：2019 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public class SendSucMsg extends WeixinBaseTemplateMsg {
	private static String MSG_FIRST = "en ! 消息发送成功了";

	private static String MSG_REMARK = "刺不刺激 开不开心";

	public SendSucMsg(){
//		this.setTemplate_id(weixinConfig.getSendSucTemplate());
		this.setFirst(MSG_FIRST);
		this.setRemark(MSG_REMARK);
		this.PutInfoToData(MSG_KEY_KEYWORD1,DateUtils.dateTimeNow());
	}



}
