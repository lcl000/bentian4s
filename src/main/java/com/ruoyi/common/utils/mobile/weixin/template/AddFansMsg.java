package com.ruoyi.common.utils.mobile.weixin.template;

import java.util.Map;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.mobile.weixin.WeixinBaseTemplateMsg;

/**
 * 功能：
 *
 * @author：Administrator
 * @create：2019/6/26/026 16:07
 * @version：2019 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public class AddFansMsg extends WeixinBaseTemplateMsg {
	private static String MSG_FIRST="不可思议!竟然有人关注你";
	private static String MSG_REMARK="去打个招呼?算了吧,谁理你呀";

	public AddFansMsg() {
//		this.setTemplate_id(weixinConfig.getAddFansTemplate());
		this.setFirst(MSG_FIRST);
		this.setRemark(MSG_REMARK);
	}

	public void setContentMsg(Map<String,String> map){
		this.PutInfoToData(MSG_KEY_KEYWORD1,map.get("name"));
		this.PutInfoToData(MSG_KEY_KEYWORD2,DateUtils.dateTimeNow());
		this.PutInfoToData(MSG_KEY_KEYWORD3,map.get("openId"));
//		String str = "GBK格式";
//		try {
//			System.out.println(new String(str.getBytes("GBK"),"UTF-8"));
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//		}
	}
}
