package com.ruoyi.common.utils.mobile.weixin;

import com.ruoyi.common.utils.FBSStringUtil;

/**
 * 功能：
 *
 * @author：Administrator
 * @create：2019/6/26/026 17:03
 * @version：2019 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public abstract class WeixinBaseTemplateMsg extends BaseTemplateMsg{
	public void setRemark(String remark){
		if(FBSStringUtil.isBlank(remark)){
			return;
		}
		this.PutInfoToData(MSG_KEY_REMARK,remark);
	}
	public void setFirst(String first){
		if(FBSStringUtil.isBlank(first)){
			return;
		}
		this.PutInfoToData(MSG_KEY_FIRST,first);
	}
}
