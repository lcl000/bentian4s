package com.ruoyi.common.utils.mobile.weixin.service;

/**
 * WeixinOrder
 * @author
 * @version
 * @since 2017年02月28日
 **/
public class WeixinOrder {

    public WeixinOrder(){

    }

    public WeixinOrder(String appId, String partnerId, String prepayId, String timeStamp) {
        this.appId = appId;
        this.partnerId = partnerId;
        this.prepayId = prepayId;
        this.timeStamp = timeStamp;
    }
    private String appId;

    // NOTE: 支付类型
    private String partnerId;

    // NOTE: * (必填项)微信支付分配的商户号
    private String prepayId ;

    // NOTE: (非必填项)HTTP/HTTPS开头字符串
    private String packages = "Sign=WXPay";

    // NOTE: 订单生成时间，格式为yyyyMMddHHmmss，如2009年12月25日9点10分10秒表示为20091225091010。其他详见时间规则
    private String timeStamp;

    private String nonceStr;

    // NOTE: 订单失效时间，格式为yyyyMMddHHmmss，如2009年12月27日9点10分10秒表示为20091227091010。其他详见时间规则
    private String sign;


    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getPrepayId() {
        return prepayId;
    }

    public void setPrepayId(String prepayId) {
        this.prepayId = prepayId;
    }

    public String getPackages() {
        return packages;
    }

    public void setPackages(String packages) {
        this.packages = packages;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getNonceStr() {
        return nonceStr;
    }

    public void setNonceStr(String nonceStr) {
        this.nonceStr = nonceStr;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
