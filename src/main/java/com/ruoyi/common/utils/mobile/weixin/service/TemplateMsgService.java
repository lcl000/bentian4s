package com.ruoyi.common.utils.mobile.weixin.service;

import org.springframework.stereotype.Component;

/**
 * 功能：
 *
 * @author：Administrator
 * @create：2019/6/26/026 15:59
 * @version：2019 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
@Component("weixinTemplateMsgService")
public class TemplateMsgService extends BaseMsgService {
	@Override
	protected String getUrl() {
		return "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={0}";
	}
}
