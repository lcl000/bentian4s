package com.ruoyi.common.utils.mobile.weixin.service;

import com.ruoyi.common.utils.mobile.IMobileMsg;

/**
 * 功能：
 *
 * @author：Administrator
 * @create：2019/6/26/026 15:15
 * @version：2019 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public interface IMsgService {
	void pushMsg(final IMobileMsg msgTemplate);
}
