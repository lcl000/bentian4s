package com.ruoyi.common.utils.mobile.weixin.template;

import java.util.Map;

import com.ruoyi.common.utils.mobile.weixin.WeixinBaseTemplateMsg;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/9/29 17:36
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
public class AddCarServerMsg extends WeixinBaseTemplateMsg {
	private static String MSG_FIRST = "有新的客户预约";
	private static String MSG_REMARK = "请尽快处理";

	public AddCarServerMsg() {
		this.setTemplate_id(weixinConfig.getCarServiceTemplate());
		this.setFirst(MSG_FIRST);
		this.setRemark(MSG_REMARK);
	}

	public void setContentMsg(Map<String,String> map){
		this.PutInfoToData(MSG_KEY_KEYWORD1,map.get("name"));
		this.PutInfoToData(MSG_KEY_KEYWORD2,map.get("mobile"));
		this.PutInfoToData(MSG_KEY_KEYWORD3,map.get("created"));
		this.PutInfoToData(MSG_KEY_KEYWORD4,map.get("typeName"));
	}
}
