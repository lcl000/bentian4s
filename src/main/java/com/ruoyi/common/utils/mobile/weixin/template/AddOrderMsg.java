package com.ruoyi.common.utils.mobile.weixin.template;


import com.ruoyi.common.utils.mobile.weixin.WeixinBaseTemplateMsg;

import java.util.Map;

/**
 * 订单微信推送消息
 *
 * @autor LCL
 * @date - 2020/12/1
 */
public class AddOrderMsg extends WeixinBaseTemplateMsg {

    private static String MSG_FIRST = "有新的订单";
    private static String MSG_REMARK = "请尽快处理";

    public AddOrderMsg() {
        this.setTemplate_id(weixinConfig.getOrderServiceTemplate());
        this.setFirst(MSG_FIRST);
        this.setRemark(MSG_REMARK);
    }

    public void setContentMsg(Map<String,String> map){
        this.PutInfoToData(MSG_KEY_KEYWORD1,map.get("created"));//
        this.PutInfoToData(MSG_KEY_KEYWORD2,map.get("goodsName"));//
        this.PutInfoToData(MSG_KEY_KEYWORD3,map.get("orderNo"));//
    }

}
