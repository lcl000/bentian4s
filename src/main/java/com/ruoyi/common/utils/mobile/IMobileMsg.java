package com.ruoyi.common.utils.mobile;

/**
 * 功能：
 *
 * @author：Administrator
 * @create：2019/6/26/026 14:59
 * @version：2019 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public interface IMobileMsg {
	//设置发送给谁
	void setToOpenId(String openId);
	//序列化 (转换成json字符串)
	String serialized();


}
