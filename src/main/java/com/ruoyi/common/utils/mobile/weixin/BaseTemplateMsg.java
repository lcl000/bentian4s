package com.ruoyi.common.utils.mobile.weixin;

import net.sf.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import org.springframework.util.Assert;

import com.ruoyi.common.utils.SpringContextUtil;
import com.ruoyi.common.utils.mobile.IMobileMsg;

/**
 * 功能：
 *
 * @author：Administrator
 * @create：2019/6/26/026 16:08
 * @version：2019 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public abstract class BaseTemplateMsg implements IMobileMsg {
	protected static String MSG_KEY_FIRST="first";
	protected static String MSG_KEY_KEYWORD1="keyword1";
	protected static String MSG_KEY_KEYWORD2="keyword2";
	protected static String MSG_KEY_KEYWORD3="keyword3";
	protected static String MSG_KEY_KEYWORD4="keyword4";
	protected static String MSG_KEY_KEYWORD5="keyword5";
	protected static String MSG_KEY_REMARK="remark";
	protected static WeixinConfig weixinConfig;
	static {
		weixinConfig = (WeixinConfig) SpringContextUtil.getBean("weixinCfg");
	}
	//接收人openId
	private String touser;
	//消息模板ID
	private String template_id;
	//数据
	private Map<String,Object> data = new HashMap<String, Object>();
	//点击模板消息后跳转的页面 空的话 ios为 空白页 android为 无可点击
	private String url;

	public String getTouser() {
		return touser;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public void setToOpenId(String openId) {
		this.touser=openId;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	public String getTemplate_id() {
		return template_id;
	}

	public void setTemplate_id(String template_id) {
		this.template_id = template_id;
	}

	protected void PutInfoToData(String key, String info){
		Assert.notNull(this.data,"消息内容不能为空");
		if(info==null||info.equals("")){
			this.data.put(key,null);
			return;
		}
		Map<String,Object> infoMap = new HashMap<>();
		infoMap.put("value",info);
		infoMap.put("color","#173177");
		this.data.put(key,infoMap);
	}

	@Override
	public String serialized() {
		return JSONObject.fromObject(this).toString();
	}
}
