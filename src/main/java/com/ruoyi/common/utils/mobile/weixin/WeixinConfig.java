package com.ruoyi.common.utils.mobile.weixin;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 功能：
 *
 * @author：Administrator
 * @create：2019/6/12/012 15:27
 * @version：2019 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
@Component("weixinCfg")
public class WeixinConfig {

	// 第三方用户唯一凭证 小程序的 appId
	private String appId = "wxabdef7f6a2296e29";
	// 第三方用户唯一凭证密钥
	private String appsecret = "3db35dbb90c414a573d9d262a011fd5d";
	// 公众号的 appId
	private String gzhAppId = "wxcf7e4ec95a1179f1";
	// 公众号的秘钥
	private String gzhAppsecret = "a42a5364b22252b4af8fd67c6acc2660";
	// 商户 Id
	private String mch_id = "1600098554";
	// Token
	private String token = "Token";
	@Value("${haoqi.wx.notifyurl}")
	private String notify_url = "http://honda.ngrok2.xiaomiqiu.cn/honda/api/order/result";
	@Value("${haoqi.wx.refuseurl}")
	private String refuse_notify_url = "http://honda.ngrok2.xiaomiqiu.cn/honda/refuse/result";
	@Value("${haoqi.wx.supplyurl}")
	private String supply_notify_url = "http://honda.ngrok2.xiaomiqiu.cn/honda/api/supply/result";
//	@Value("${baisi.wx.msg.template.addFuns}")
//	private String addFansTemplate;
//	@Value("${baisi.wx.msg.template.sendSuc}")
//	private String sendSucTemplate;
	@Value("${bentian.wx.msg.template.memberHelp}")
	private String memberHelpTemplate;
	@Value("${bentian.wx.msg.template.carAccident}")
	private String carAccidentTemplate;
	@Value("${bentian.wx.msg.template.carService}")
	private String carServiceTemplate;
    @Value("${bentian.wx.msg.template.orderService}")
    private String orderServiceTemplate;//
	@Value("${bentian.msggroup.accident}")
	private List<String> accident;
	@Value("${bentian.msggroup.help}")
	private List<String> help;
	@Value("${bentian.msggroup.order}")
    private List<String> orderMemberList;//


	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppsecret() {
		return appsecret;
	}

	public void setAppsecret(String appsecret) {
		this.appsecret = appsecret;
	}

	public String getMch_id() {
		return mch_id;
	}

	public void setMch_id(String mch_id) {
		this.mch_id = mch_id;
	}

//	public String getAddFansTemplate() {
//		return addFansTemplate;
//	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

//	public String getSendSucTemplate() {
//		return sendSucTemplate;
//	}


	public String getNotify_url() {
		return notify_url;
	}

	public String getRefuse_notify_url() {
		return refuse_notify_url;
	}

	public String getSupply_notify_url() {
		return supply_notify_url;
	}

	public String getGzhAppId() {
		return gzhAppId;
	}

	public void setGzhAppId(String gzhAppId) {
		this.gzhAppId = gzhAppId;
	}

	public String getGzhAppsecret() {
		return gzhAppsecret;
	}

	public void setGzhAppsecret(String gzhAppsecret) {
		this.gzhAppsecret = gzhAppsecret;
	}

	public String getMemberHelpTemplate() {
		return memberHelpTemplate;
	}

	public void setMemberHelpTemplate(String memberHelpTemplate) {
		this.memberHelpTemplate = memberHelpTemplate;
	}

	public List<String> getAccident() {
		return accident;
	}

	public void setAccident(List<String> accident) {
		this.accident = accident;
	}

	public List<String> getHelp() {
		return help;
	}

	public void setHelp(List<String> help) {
		this.help = help;
	}

	public String getCarAccidentTemplate() {
		return carAccidentTemplate;
	}

	public void setCarAccidentTemplate(String carAccidentTemplate) {
		this.carAccidentTemplate = carAccidentTemplate;
	}


	public String getCarServiceTemplate() {
		return carServiceTemplate;
	}

	public void setCarServiceTemplate(String carServiceTemplate) {
		this.carServiceTemplate = carServiceTemplate;
	}

    public List<String> getOrderMemberList() {
        return orderMemberList;
    }

    public String getOrderServiceTemplate() {
        return orderServiceTemplate;
    }
}
