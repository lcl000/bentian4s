package com.ruoyi.common.utils.mobile.weixin.template;

import java.util.Map;

import com.ruoyi.common.utils.mobile.weixin.WeixinBaseTemplateMsg;
import com.ruoyi.common.utils.mobile.weixin.WeixinConfig;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/9/25 16:15
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
public class MemberHelpMsg extends WeixinBaseTemplateMsg {

	private static String MSG_FIRST = "车辆请求救援";
	private static String MSG_REMARK = "请尽快处理";

	public MemberHelpMsg() {
		this.setTemplate_id(weixinConfig.getMemberHelpTemplate());
		this.setFirst(MSG_FIRST);
		this.setRemark(MSG_REMARK);
	}

	public void setContentMsg(Map<String,String> map){
		this.PutInfoToData(MSG_KEY_KEYWORD1,map.get("address"));
		this.PutInfoToData(MSG_KEY_KEYWORD2,map.get("created"));
		this.PutInfoToData(MSG_KEY_KEYWORD3,map.get("mobile"));
		this.PutInfoToData(MSG_KEY_KEYWORD4,"联系客户确认情况");
	}
}
