package com.ruoyi.common.utils.mobile.weixin;



import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/9/29 18:57
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
@Configuration
@ConfigurationProperties(prefix = "bentian.msggroup")
@Component("receive")
public class Receive {

	private Map<String,Map<String,List<String>>> dealer;

	public Map<String, Map<String, List<String>>> getDealer() {
		return dealer;
	}

	public void setDealer(Map<String, Map<String, List<String>>> dealer) {
		this.dealer = dealer;
	}
}
