package com.ruoyi.common.utils.mobile.weixin.template;

import java.util.Map;

import com.ruoyi.common.utils.mobile.weixin.WeixinBaseTemplateMsg;

/**
 * 功能：
 *
 * @author：LCL
 * @create：2020/9/26 14:10
 * @version：2020 Version：1.0
 * @company：小牛科技 Created with IntelliJ IDEA
 */
public class CarAccidentMsg extends WeixinBaseTemplateMsg {
	private static String MSG_FIRST = "事故车推荐";
	private static String MSG_REMARK = "请尽快处理";

	public CarAccidentMsg() {
		this.setTemplate_id(weixinConfig.getCarAccidentTemplate());
		this.setFirst(MSG_FIRST);
		this.setRemark(MSG_REMARK);
	}

	public void setContentMsg(Map<String,String> map){
		this.PutInfoToData(MSG_KEY_KEYWORD1,map.get("address"));
		this.PutInfoToData(MSG_KEY_KEYWORD2,map.get("created"));
		this.PutInfoToData(MSG_KEY_KEYWORD3,map.get("mobile"));
		this.PutInfoToData(MSG_KEY_KEYWORD4,"联系客户确认情况");
	}
}
