package com.ruoyi.common.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 功能：
 *
 * @author：Administrator
 * @create：2019/6/27/027 14:06
 * @version：2019 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
@Configuration
public class CommonBeanConfig {

	@Bean
	public SpringContextUtil springContextUtil(){
		return new SpringContextUtil();
	}
}
