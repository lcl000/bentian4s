package com.ruoyi.framework.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ruoyi.common.utils.uniquekey.IdGererateFactory;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/6 10:40
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
@Configuration
public class UniqueKeyConfig {
//	@Value("${lwxf.uniquekey.workerId}")
//	private Long workId;
//	@Value("${lwxf.uniquekey.datacenterId}")
//	private Long datacenterId;

	@Bean
	public IdGererateFactory idGererateFactory() {
//		return new IdGererateFactory(this.workId);
		return new IdGererateFactory(1L);
	}
}