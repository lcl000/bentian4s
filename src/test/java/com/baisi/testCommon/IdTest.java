package com.baisi.testCommon;




import java.math.BigDecimal;
import java.util.*;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SpringContextUtil;
import com.ruoyi.common.utils.uniquekey.IIdGenerator;
import com.ruoyi.common.utils.uniquekey.WorkerIdGenerator;
import com.ruoyi.project.AppBeanInjector;
import com.ruoyi.project.system.address.domain.MemberAddress;
import com.ruoyi.project.system.goods.img.service.IGoodsImgService;
import com.ruoyi.project.system.member.domain.Member;
import com.ruoyi.project.system.order.comment.domain.OrderComment;
import com.ruoyi.project.system.order.order.domain.OrderOrder;

/**
 * 功能：
 *
 * @author：dell
 * @create：2020/6/5 17:23
 * @version：2020 Version：1.0
 * @company：老屋新房 Created with IntelliJ IDEA
 */
public class IdTest {


	@org.junit.Test
	public void testGeneralEntityId(){
		IIdGenerator idGenerator = new WorkerIdGenerator(1);
		for (int i = 0; i < 1; i++) {
			System.out.println(idGenerator.nextId());
			System.out.println(idGenerator.nextStringId());
		}
	}

	@org.junit.Test
	public void testBoolean(){
		System.out.println(new WorkerIdGenerator().nextId().toString());
	}

	@org.junit.Test
	public void testDate(){
		System.out.println(DateUtils.getNowDate());
		System.out.println(DateUtils.getNowDate().getTime());
		System.out.println(new BigDecimal(100).multiply(new BigDecimal(0.01)).setScale(2,BigDecimal.ROUND_HALF_DOWN));;
	}


	@org.junit.Test
	public void testJson(){
		System.out.println(new OrderComment().toString());
	}



	@org.junit.Test
	public void testService(){
//		IGoodsImgService iGoodsImgService = (IGoodsImgService) SpringContextUtil.getBean("GoodsImgServiceImpl");
//		System.out.println(iGoodsImgService.findListByGoodsIdAndType("60qvu62rdxxc",0));

		Double a = 2.22;
		Double b = 3.33;
		System.out.println(a+b);

		BigDecimal bigDecimal = new BigDecimal(2.22);
		BigDecimal bigDecimal1 = new BigDecimal(3.33);
		System.out.println(bigDecimal.add(bigDecimal1).setScale(2,BigDecimal.ROUND_HALF_UP));

		String aaa = "xlalala";
		String ccc = "la";
		System.out.println(aaa.indexOf(ccc));


	}


	@org.junit.Test
	public void testBigDecimal(){
		System.out.println(new BigDecimal(0).add(new BigDecimal(100).multiply(new BigDecimal(3))));
	}


	@org.junit.Test
	public void testList(){
//		List<String[]> strings = Arrays.asList(new String[]{"张三", "李四"}, null, new String[]{"张三", "李四"}, null, new String[]{"张三", "李四"});
//		for(String[] aa:strings){
//			System.out.println(aa.length);
//		}
		//测试排序
//		Integer[] integers = {10, 30, 50, 45, 90, 1, 5};
//		Arrays.sort(integers);
//		for(Integer i:integers){
//			System.out.println(i);
//		}
		String a="xxx";
	}

	@org.junit.Test
	public void testString(){
//		String aa = "http:4040/xxxx/http:/555";
//		String a= aa.replaceFirst("http","https");
//		System.out.println(aa);
//		System.out.println(a);
//		String ccc= aa.substring(0,5);
//		System.out.println(ccc);
//		if(ccc.equals("https")){
//			System.out.println("ok");
//		}else{
//
//			System.out.println();
//		}

//		if(!(false&&true)){
//			System.out.println("xxx");
//		}

		String aa = null;
		System.out.println(aa==null?"xxx":"aa");

	}

	@org.junit.Test
	public void testMonthsBetween(){
		Date nowDate = DateUtils.getNowDate();
		Date endDate = DateUtils.parseDate("2020-12-01");
		Calendar c1 = Calendar.getInstance();
		c1.setTime(nowDate);
		Calendar c2 = Calendar.getInstance();
		c2.setTime(endDate);
//		int months = Months.monthsBetween(new DateTime().minus(endDate.getTime()), new DateTime().minus(nowDate.getTime())).getMonths();
		Integer result = (c2.get(Calendar.YEAR)-c1.get(Calendar.YEAR))*12+c2.get(Calendar.MONTH) - c1.get(Calendar.MONTH);
		System.out.println(result);
	}


	@org.junit.Test
	public void testListSort(){

		SysUser sysUser = new SysUser();
		sysUser.setUserId(111L);
		SysUser sysUser1 = new SysUser();
		sysUser1.setUserId(222L);
		SysUser sysUser2 = new SysUser();
		sysUser2.setUserId(333L);
		ArrayList<SysUser> sysUsers = new ArrayList<>();
		sysUsers.add(sysUser);
		sysUsers.add(sysUser1);
		sysUsers.add(sysUser2);
		for(SysUser sysUser3:sysUsers){
			System.out.println(sysUser3.getUserId());
		}

		Collections.sort(sysUsers, new Comparator<SysUser>() {
			@Override
			public int compare(SysUser o1, SysUser o2) {
				//排序属性
				if(o1.getUserId()<o2.getUserId()){
					return 1;
				}
				return -1;
			}
		});
		for(SysUser sysUser3:sysUsers){
			System.out.println(sysUser3.getUserId());
		}
	}

	class SysUser{
		Long userId;

		public Long getUserId() {
			return userId;
		}

		public void setUserId(Long userId) {
			this.userId = userId;
		}
	}
}
